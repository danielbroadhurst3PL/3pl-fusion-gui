<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="../cropped-favicon-1-32x32.png" type="x-icon">
    <link rel="stylesheet" href="css/styles.css">
    <title>Welspun Embroidery Shipping</title>
</head>
<body>

    <div class="container">
        <div class="card">
            <img src="../images/customers/3PL/3pl-logo.svg" alt="3PL Logo" class="logo3PL">
            <h1>Welspun Embroidery Shipping</h1>

            <form method="post" id="welspunEmbroid">
                <label for="orderNumber">Enter Order Number</label>
                <input type="text" name="orderNumber" id="orderNumber" autofocus required>
                <button type="submit">Mark Order as Shipped</button>
            </form>

            <div id="responses"></div>
        </div>
    </div>


    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css" rel="stylesheet" />
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="js/scripts.js"></script>

</body>
</html>