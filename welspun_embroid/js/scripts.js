// Stores any search query terms.
let searchQuery = getUrlVars();

// Sets Order Number input to be the Search Query.
if (searchQuery != null) {
    $('#orderNumber').val(`${searchQuery.orderNumber}`);
    $('#orderNumber').focus();
}

// Prevents Form being sent and calls for Confirm Dialog box.
$('#welspunEmbroid').submit(function (e) {
    e.preventDefault();
    let orderNumber = $('#orderNumber').val();
    ConfirmDialog(`${orderNumber}`, orderNumber);
})

/**
 * Creates Form Data and sends a PUT call to api endpoint.
 * 'https://api.3p-logistics.co.uk/orders/process_order'
 * 
 * @param {String} orderNumber - Order number which will be marked as shipped.
 */
function confirmOrderShipped(orderNumber) {
    var settings = {
        "url": "https://api.3p-logistics.co.uk/orders/process_order",
        "method": "PUT",
        "timeout": 0,
        "headers": {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        "data": {
          "API-KEY": "7467a781-abf0-4c01-8d7e-682dc1bb776e",
          "orderNumber": orderNumber
        },
      };
    $.ajax(settings).done(function (response) {
        $('form#welspunEmbroid').trigger("reset");
        $('#orderNumber').focus();
        $('#responses').html("");
        $('#responses').append(`<p>Order Number: <strong>${orderNumber}</strong> marked as shipped.</p>`); 
    }).fail( function (response) {
        $('#responses').append(`<p>${response.responseJSON.errorMessage[0]}.</p>`); 

    }); 
}

// Returns any search queries as an Array.
function getUrlVars() {
    var vars = [],
        hash;
    if (window.location.href.includes('?')) {
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }
}

/**
 * Displays a dialog to the user.
 * Yes =  Order Number marked as shipped
 * Close & No = Form is reset.
 * 
 * @param {String} message - Message which is displayed to end user.
 * @param {String} orderNumber - Order Number which will be marked as shipped.
 */
function ConfirmDialog(message, orderNumber) {
    $('<div></div>').appendTo('body')
        .html('<div><p>Mark order <strong>' + message + '</strong> as shipped?</p></div>')
        .dialog({
            modal: true,
            title: 'Are you sure?',
            zIndex: 10000,
            autoOpen: true,
            width: '350px',
            resizable: false,
            buttons: {
                Yes: function () {
                    confirmOrderShipped(orderNumber);
                    $(this).dialog("close");
                },
                No: function () {
                    $('form#welspunEmbroid').trigger("reset");
                    $('#orderNumber').focus();

                    $('#responses').html("");
                    $(this).dialog("close");
                }
            },
            close: function (event, ui) {
                $(this).remove();
            }
        });
};