<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FUSION_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->library(array("ion_auth", "form_validation"));
        $this->load->helper(array("url", "language", "form", "date"));

        $data = array();


        $this->data[LOGGED_USER] = "";
        $this->data[LOGGED_USER_SESSION] = "";
        
        $this->data[ERRORS] = array();

        switch (SYSTEM_TYPE) {
            case LIVE:
                $this->data[URL_3PL_API] = URL_3PL_API_LIVE;
                $this->data[URL_3PL_DESIGNER] = URL_3PL_DESIGNER_LIVE;
            break;
            default:
                $this->data[URL_3PL_API] = URL_3PL_API_TEST;
                $this->data[URL_3PL_DESIGNER] = URL_3PL_DESIGNER_TEST;
            break;
        }


        $this->customerContactFields = array(
            array(ID => FIRST_NAME, TITLE => TITLE_FIRST_NAME, TYPE => TEXT, REQUIRED => true, TABINDEX => 2, DIVSIZE => 6),
            array(ID => LAST_NAME, TITLE => TITLE_LAST_NAME, TYPE => TEXT, REQUIRED => true, TABINDEX => 3, DIVSIZE => 6),
            array(ID => PHONE_NUMBER, TITLE => TITLE_PHONE_NUMBER, TYPE => PHONE_NUMBER, REQUIRED => true, TABINDEX => 4, DIVSIZE => 12),
            array(ID => EMAIL, TITLE => TITLE_EMAIL, TYPE => EMAIL, REQUIRED => true, TABINDEX => 5, DIVSIZE => 12)
        );

        $this->addressFieldsOrder = array(
            array(ID => ADDRESS_LINE_1, TITLE => TITLE_ADDRESS_LINE_1, TYPE => TEXT, REQUIRED => true, TABINDEX => 6, DIVSIZE => 12),
            array(ID => ADDRESS_LINE_2, TITLE => TITLE_ADDRESS_LINE_2, TYPE => TEXT, REQUIRED => true, TABINDEX => 7, DIVSIZE => 6),
            array(ID => TOWN, TITLE => TITLE_TOWN, TYPE => TEXT, REQUIRED => true, TABINDEX => 8, DIVSIZE => 6),
            array(ID => REGION, TITLE => TITLE_REGION, TYPE => TEXT, REQUIRED => true, TABINDEX => 9, DIVSIZE => 6),
            array(ID => POSTCODE, TITLE => TITLE_POSTCODE, TYPE => TEXT, REQUIRED => true, TABINDEX => 10, DIVSIZE => 6),
            array(ID => COUNTRY, TITLE => TITLE_COUNTRY, TYPE => SELECT_, REQUIRED => true, TABINDEX => 11, DIVSIZE => 12)
        );

        $this->referenceFields = array(
            array(ID => REFERENCE_CUSTOMER, TITLE => TITLE_REFERENCE, TYPE => TEXT, REQUIRED => false, TABINDEX => 15, DIVSIZE => 6),
            array(ID => REFERENCE_PURCHASE_ORDER, TITLE => TITLE_PURCHASE_ORDER, TYPE => TEXT, REQUIRED => false, TABINDEX => 16, DIVSIZE => 6),
        );

        $this->referenceFieldsEdit = array(
            array(ID => ORDER_NUMBER, TITLE => TITLE_ORDER_NUMBER, TYPE => TEXT, REQUIRED => true, TABINDEX => 21, DIVSIZE => 6),
            array(ID => REFERENCE_CUSTOMER, TITLE => TITLE_REFERENCE, TYPE => TEXT, REQUIRED => false, TABINDEX => 22, DIVSIZE => 6),
            array(ID => REFERENCE_PURCHASE_ORDER, TITLE => TITLE_PURCHASE_ORDER, TYPE => TEXT, REQUIRED => false, TABINDEX => 23, DIVSIZE => 6),
        );

        $this->deliveryFields = array(
            array(ID => DISPATCH_DATE, TITLE => TITLE_DISPATCH_DATE, TYPE => DATE, REQUIRED => true, TABINDEX => 12, DIVSIZE => 12),
            array(ID => SERVICE, TITLE => TITLE_SERVICE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 13, DIVSIZE => 12),
            array(ID => DELIVERY_NOTE, TITLE => TITLE_DELIVERY_NOTE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 14, DIVSIZE => 12),
        );

        $this->instructionsFields = array(
            array(ID => INSTRUCTIONS_PACKING, TITLE => TITLE_INSTRUCTIONS_PACKING, TYPE => TEXTAREA, REQUIRED => false, TABINDEX => 17, DIVSIZE => 12),
        );

        $this->currencyFields = array(
            array(ID => CURRENCY, TITLE => TITLE_CURRENCY, TYPE => SELECT_, REQUIRED => true, TABINDEX => 18, DIVSIZE => 12),
        );

        $this->orderForm = array(
            TITLE_CUSTOMER_CONTACT => $this->customerContactFields,
            TITLE_CUSTOMER_ADDRESS => $this->addressFieldsOrder,
            TITLE_DELIVERY => $this->deliveryFields,
            TITLE_REFERENCES => $this->referenceFields,
            TITLE_INSTRUCTIONS => $this->instructionsFields,
            TITLE_CURRENCY => $this->currencyFields,
            TITLE_ITEMS => array(VALUE => true)
        );

        $this->orderFormEdit = array(
            TITLE_CUSTOMER_CONTACT => $this->customerContactFields,
            TITLE_CUSTOMER_ADDRESS => $this->addressFieldsOrder,
            TITLE_REFERENCES => $this->referenceFieldsEdit,
            TITLE_DELIVERY => $this->deliveryFields,
            TITLE_INSTRUCTIONS => $this->instructionsFields,
            TITLE_CURRENCY => $this->currencyFields,
            TITLE_ITEMS => array(VALUE => true)
        );

        $this->orderFormAddress = array(
            TITLE_CUSTOMER_CONTACT => $this->customerContactFields,
            TITLE_CUSTOMER_ADDRESS => $this->addressFieldsOrder,
            TITLE_DELIVERY => $this->deliveryFields,
        );

        $this->productFields = array(
            array(ID => TITLE, TITLE => TITLE_TITLE, TYPE => TEXT, REQUIRED => true, TABINDEX => 2, DIVSIZE => 6),
            array(ID => SKU, TITLE => TITLE_SKU, TYPE => TEXT, REQUIRED => true, TABINDEX => 3, DIVSIZE => 6),
            array(ID => DESCRIPTION, TITLE => TITLE_DESCRIPTION, TYPE => TEXTAREA, REQUIRED => false, TABINDEX => 4, DIVSIZE => 12),
            array(ID => BARCODE, TITLE => TITLE_BARCODE, TYPE => TEXT, REQUIRED => true, TABINDEX => 5, DIVSIZE => 6),
            array(ID => ASIN, TITLE => TITLE_ASIN, TYPE => TEXT, REQUIRED => false, TABINDEX => 6, DIVSIZE => 6),
            array(ID => COMMODITY_CODE, TITLE => TITLE_COMMODITY_CODE, TYPE => TEXT, REQUIRED => true, TABINDEX => 7, DIVSIZE => 12),
        );

        $this->weightsDimsFields = array(
            array(ID => WEIGHT, TITLE => TITLE_WEIGHT, TYPE => NUMBER, REQUIRED => true, TABINDEX => 12, DIVSIZE => 12),
            array(ID => HEIGHT, TITLE => TITLE_HEIGHT, TYPE => NUMBER, REQUIRED => true, TABINDEX => 13, DIVSIZE => 12),
            array(ID => WIDTH, TITLE => TITLE_WIDTH, TYPE => NUMBER, REQUIRED => true, TABINDEX => 14, DIVSIZE => 12),
            array(ID => DEPTH, TITLE => TITLE_DEPTH, TYPE => NUMBER, REQUIRED => true, TABINDEX => 15, DIVSIZE => 12)
        );

        $this->productDetailsFields = array(
            array(ID => DGN_TYPE, TITLE => TITLE_DGN_TYPE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 8, DIVSIZE => 12),
            array(ID => DGN_DETAILS, TITLE => TITLE_DGN_DETAILS, TYPE => SELECT_, REQUIRED => true, TABINDEX => 9, DIVSIZE => 12),
            array(ID => COUNTRY, TITLE => TITLE_COUNTRY_OF_ORIGIN, TYPE => SELECT_, REQUIRED => true, TABINDEX => 10, DIVSIZE => 12),
            array(ID => AVERAGE_MONTHLY_UNIT_SALES, TITLE => TITLE_AVERAGE_MONTHLY_UNIT_SALES, TYPE => NUMBER, REQUIRED => true, TABINDEX => 11, DIVSIZE => 12),
        );

        $this->productDetailsFieldsC = array(
            array(ID => DGN_TYPE, TITLE => TITLE_DGN_TYPE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 8, DIVSIZE => 12),
            array(ID => DGN_DETAILS, TITLE => TITLE_DGN_DETAILS, TYPE => SELECT_, REQUIRED => true, TABINDEX => 9, DIVSIZE => 12),
            array(ID => COUNTRY, TITLE => TITLE_COUNTRY_OF_ORIGIN, TYPE => SELECT_, REQUIRED => true, TABINDEX => 10, DIVSIZE => 12),
            array(ID => AVERAGE_MONTHLY_UNIT_SALES, TITLE => TITLE_AVERAGE_MONTHLY_UNIT_SALES, TYPE => NUMBER, REQUIRED => true, TABINDEX => 11, DIVSIZE => 12),
        );

        $this->productCurrencyFields = array(
            array(ID => CURRENCY, TITLE => TITLE_CURRENCY, TYPE => SELECT_, REQUIRED => true, TABINDEX => 16, DIVSIZE => 12),
            array(ID => VALUE, TITLE => TITLE_VALUE, TYPE => NUMBER, REQUIRED => true, TABINDEX => 17, DIVSIZE => 12),
        );

        $this->quantityFields = array(
            array(ID => QUANTITY_INNER, TITLE => TITLE_INNER_QUANTITY, TYPE => NUMBER, REQUIRED => false, TABINDEX => 18, DIVSIZE => 12),
            array(ID => QUANTITY_MASTER_CARTON, TITLE => TITLE_MASTER_CARTON_QUANTITY, TYPE => NUMBER, REQUIRED => false, TABINDEX => 19, DIVSIZE => 12),
            array(ID => QUANTITY_PALLET, TITLE => TITLE_PALLET_QUANTITY, TYPE => NUMBER, REQUIRED => false, TABINDEX => 20, DIVSIZE => 12)
        );

        $this->productExtras = array(
            array(ID => DATE_EXPIRY, TITLE => TITLE_DATE_EXPIRY, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 21, DIVSIZE => 4),
            array(ID => SERIAL, TITLE => TITLE_SERIAL, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 22, DIVSIZE => 4),
            array(ID => BATCH, TITLE => TITLE_BATCH, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 23, DIVSIZE => 4)
        );

        $this->productForm = array(
            TITLE_PRODUCT_INFORMATION => $this->productFields,
            TITLE_PRODUCT_DETAILS => $this->productDetailsFieldsC,
            TITLE_WEIGHT_DIMS => $this->weightsDimsFields,
            TITLE_CURRENCY => $this->productCurrencyFields,
            TITLE_QUANTITIES => $this->quantityFields,
            TITLE_EXTRAS => $this->productExtras
        );

        $this->productFormU = array(
            TITLE_PRODUCT_INFORMATION => $this->productFields,
            TITLE_PRODUCT_DETAILS => $this->productDetailsFields,
            TITLE_WEIGHT_DIMS => $this->weightsDimsFields,
            TITLE_CURRENCY => $this->productCurrencyFields,
            TITLE_QUANTITIES => $this->quantityFields,
            TITLE_EXTRAS => $this->productExtras
        );

        $this->addressFieldsCompany = array(
            array(ID => COMPANY_NAME, TITLE => TITLE_COMPANY, TYPE => TEXT, REQUIRED => true, TABINDEX => 2, DIVSIZE => 8),
            array(ID => COMPANY_CODE, TITLE => TITLE_COMPANY_CODE, TYPE => TEXT, REQUIRED => true, TABINDEX => 3, DIVSIZE => 4),
            array(ID => ADDRESS_LINE_1, TITLE => TITLE_ADDRESS_LINE_1, TYPE => TEXT, REQUIRED => true, TABINDEX => 4, DIVSIZE => 12),
            array(ID => ADDRESS_LINE_2, TITLE => TITLE_ADDRESS_LINE_2, TYPE => TEXT, REQUIRED => true, TABINDEX => 5, DIVSIZE => 6),
            array(ID => TOWN, TITLE => TITLE_TOWN, TYPE => TEXT, REQUIRED => true, TABINDEX => 6, DIVSIZE => 6),
            array(ID => REGION, TITLE => TITLE_REGION, TYPE => TEXT, REQUIRED => true, TABINDEX => 7, DIVSIZE => 6),
            array(ID => POSTCODE, TITLE => TITLE_POSTCODE, TYPE => TEXT, REQUIRED => true, TABINDEX => 8, DIVSIZE => 6),
            array(ID => COUNTRY, TITLE => TITLE_COUNTRY, TYPE => SELECT_, REQUIRED => true, TABINDEX => 9, DIVSIZE => 12)
        );

        $this->companyFields = array(
            array(ID => PRIMARY_EMAIL, TITLE => TITLE_PRIMARY_EMAIL, TYPE => EMAIL, REQUIRED => true, TABINDEX => 10, DIVSIZE => 12),
            array(ID => SECONDARY_EMAIL, TITLE => TITLE_SECONDARY_EMAIL, TYPE => EMAIL, REQUIRED => false, TABINDEX => 11, DIVSIZE => 12),
        );

        $this->companyExtras = array(
            array(ID => LICENCES, TITLE => TITLE_LICENCES, TYPE => NUMBER,  REQUIRED => true, TABINDEX => 23, DIVSIZE => 12),
            array(ID => ACTIVE, TITLE => TITLE_ACTIVE, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 3),
            array(ID => BACKORDERS, TITLE => TITLE_BACK_ORDER, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 25, DIVSIZE => 3),
            array(ID => EMAIL_TRACKING, TITLE => TITLE_EMAIL_TRACKING, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 26, DIVSIZE => 3),
            array(ID => LINNWORKS, TITLE => TITLE_LINNWORKS, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 27, DIVSIZE => 3),
            array(ID => 'linnworksToken', TITLE => 'Linnworks Token', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 6),
            array(ID => 'vatNumber', TITLE => 'Vat Number', TYPE => TEXT,  REQUIRED => true, TABINDEX => 29, DIVSIZE => 6),
            array(ID => "warehouse", TITLE => "Warehouse", TYPE => RADIO, REQUIRED => true, TABINDEX => 31, DIVSIZE => 12, ITEMS => array('The Fulfilment Hub', 'Retail Distribution Centre')),
        );

        $this->companyStockArea = array(
            array(ID => PICKING_AISLE, TITLE => TITLE_PICKING_AISLE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 12, DIVSIZE => 12),
            array(ID => BULK_STORAGE, TITLE => TITLE_BULK_STORAGE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 13, DIVSIZE => 12)
        );

        $this->companyForm = array(
            TITLE_COMPANY_ADDRESS => $this->addressFieldsCompany,
            TITLE_COMPANY_EMAIL => $this->companyFields,
            TITLE_STOCK_AREA => $this->companyStockArea,
            TITLE_EXTRAS => $this->companyExtras,
        );

        $this->companyDropdown = array(
            array(ID => 'companySettingsDropdown', TITLE => TITLE_COMPANY, TYPE => SELECT_, REQUIRED => true, TABINDEX => 9, DIVSIZE => 6),
            array(ID => 'companyType', TITLE => 'Company Type', TYPE => SELECT_, REQUIRED => true, TABINDEX => 9, DIVSIZE => 6)
        );

        $this->companySettings = array(
            array(ID => ACTIVE, TITLE => TITLE_ACTIVE, TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 3),
            array(ID => 'onStop', TITLE => 'On Stop', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 3),
            array(ID => BACKORDERS, TITLE => 'Send Back Order Email', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 25, DIVSIZE => 3),
            array(ID => 'dailySummary', TITLE => 'Send Daily Summary Email', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 25, DIVSIZE => 3),
            array(ID => LICENCES, TITLE => TITLE_LICENCES, TYPE => NUMBER,  REQUIRED => true, TABINDEX => 23, DIVSIZE => 6),
            array(ID => 'tradeTriggerQty', TITLE => 'Trade Trigger Qty', TYPE => NUMBER,  REQUIRED => true, TABINDEX => 23, DIVSIZE => 6),
            array(ID => "warehouse", TITLE => "Warehouse", TYPE => RADIO, REQUIRED => true, TABINDEX => 31, DIVSIZE => 6, ITEMS => array('The Fulfilment Hub', 'Retail Distribution Centre')),
            array(ID => 'rulesEngine', TITLE => 'Rules Engine', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 3),
            array(ID => 'printDeliveryNote', TITLE => 'Print Delivery Note', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 3),
        );

        $this->companySnapSettings = array(
            array(ID => 'snapCustomer', TITLE => 'Snap Customer', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 4),
            array(ID => 'snapUsername', TITLE => 'Snap Username', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 4),
            array(ID => 'snapPassword', TITLE => 'Snap Password', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 4),
        );

        $this->companyLinnworksSettings = array(
            array(ID => 'linnworksToken', TITLE => 'Linnworks Token', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 6),
            array(ID => 'linnworksFC', TITLE => 'Linnworks FC', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 6),
            array(ID => 'linnworksOrder', TITLE => 'Linnworks Order Sync', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 4),
            array(ID => 'fullStockCSV', TITLE => 'Export Stock & Processed File', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 24, DIVSIZE => 4),
        );

        $this->companyFTPSettings = array(
            array(ID => 'ftpUrl', TITLE => 'FTP URL', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 4),
            array(ID => 'ftpUsername', TITLE => 'FTP Username', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 4),
            array(ID => 'ftpPassword', TITLE => 'FTP Password', TYPE => TEXT, REQUIRED => false, TABINDEX => 28, DIVSIZE => 4),
        );

        $this->companyFields = array(
            array(ID => 'internalEmail', TITLE => '3PL Email Address', TYPE => EMAIL, REQUIRED => true, TABINDEX => 10, DIVSIZE => 12),
            array(ID => PRIMARY_EMAIL, TITLE => TITLE_PRIMARY_EMAIL, TYPE => EMAIL, REQUIRED => true, TABINDEX => 10, DIVSIZE => 12),
            array(ID => SECONDARY_EMAIL, TITLE => TITLE_SECONDARY_EMAIL, TYPE => EMAIL, REQUIRED => false, TABINDEX => 11, DIVSIZE => 12),
        );

        $this->companyDeliveryNotes = array(
            array(ID => 'deliveryNote', TITLE => 'Delivery Notes', TYPE => SELECT_, REQUIRED => true, TABINDEX => 25, DIVSIZE => 12),
            array(ID => "deliveryNoteType", TITLE => "Delivery Note Type", TYPE => RADIO, REQUIRED => true, TABINDEX => 26, DIVSIZE => 6, ITEMS => array('Default', 'Custom')),
            array(ID => "deliveryNoteClass", TITLE => "Delivery Note Class", TYPE => RADIO, REQUIRED => true, TABINDEX => 27, DIVSIZE => 6, ITEMS => array('Vendor', 'Trade', 'B2C')),
            array(ID => 'print', TITLE => 'Print', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 28, DIVSIZE => 3),
            array(ID => 'file', TITLE => 'File', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 29, DIVSIZE => 3),
            array(ID => 'embroid', TITLE => 'Embroid', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 30, DIVSIZE => 3),
            array(ID => 'dropship', TITLE => 'Dropship', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 31, DIVSIZE => 3),
            array(ID => 'emailCompany', TITLE => 'Email Company', TYPE => TEXT, REQUIRED => false, TABINDEX => 32, DIVSIZE => 6),
            array(ID => 'special', TITLE => 'Special Note', TYPE => TEXT, REQUIRED => false, TABINDEX => 33, DIVSIZE => 6),
        );

        $this->companySettingsForm = array(
            TITLE_COMPANY => $this->companyDropdown,
            TITLE_COMPANY_SETTINGS => $this->companySettings,
            TITLE_COMPANY_EMAIL => $this->companyFields,
            'Snap Settings' => $this->companySnapSettings,
            'Linnworks Settings' => $this->companyLinnworksSettings,
            'FTP Settings' => $this->companyFTPSettings,
            'Delivery Note Settings' => $this->companyDeliveryNotes
        );

        $this->grnFields = array(
            array(ID => REFERENCE_CUSTOMER, TITLE => TITLE_CUSTOMER_REFERENCE, TYPE => TEXT, REQUIRED => true, TABINDEX => 1, DIVSIZE => 12),
        );

        $this->grnForm = array(
            TITLE_REFERENCES => $this->grnFields,
            TITLE_ITEMS => array(VALUE => false)
        );

        $this->inboundFields = array(
            array(ID => "poNumber", TITLE => "PO Number", TYPE => SELECT_,  REQUIRED => true, TABINDEX => 1, DIVSIZE => 12),
            array(ID => "manualPoNumber", TITLE => "Manually Enter PO Number", TYPE => TEXT, REQUIRED => false, TABINDEX => 2, DIVSIZE => 12),
            array(ID => "carrierName", TITLE => "Carrier Name", TYPE => TEXT, REQUIRED => true, TABINDEX => 2, DIVSIZE => 12),
            array(ID => "transitType", TITLE => "Transit Type", TYPE => RADIO, REQUIRED => true, TABINDEX => 3, DIVSIZE => 12, ITEMS => array('Cartons', 'Pallets', 'Container Loose', 'Container Pallets')),
            array(ID => "containerTypePallets", TITLE => "Container Type Pallets", TYPE => RADIO, REQUIRED => true, TABINDEX => 4, DIVSIZE => 12, ITEMS => array('20 Foot Palletised', '40 Foot Palletised', '20 Foot High Cube Palletised', '40 Foot High Cube Palletised')),
            array(ID => "containerTypeLoose", TITLE => "Container Type Loose", TYPE => RADIO, REQUIRED => true, TABINDEX => 5, DIVSIZE => 12, ITEMS => array('20 Foot Loose Cartons', '40 Foot Loose Cartons', '20 Foot High Cube Loose Cartons', '40 Foot High Cube Loose Cartons')),
            array(ID => "numberCartons", TITLE => "Number of Cartons", TYPE => NUMBER,  REQUIRED => true, TABINDEX => 6, DIVSIZE => 12),
            array(ID => "numberPallets", TITLE => "Number of Pallets", TYPE => NUMBER,  REQUIRED => true, TABINDEX => 7, DIVSIZE => 12),
            array(ID => "requestedDate", TITLE => "Requested Date", TYPE => DATE, REQUIRED => true, TABINDEX => 9, DIVSIZE => 6),
            array(ID => "estimatedTime", TITLE => "Estimated Time", TYPE => 'time', REQUIRED => true, TABINDEX => 9, DIVSIZE => 6),
            array(ID => "timeSlot", TITLE => "Time Slots Available", TYPE => RADIO, REQUIRED => true, TABINDEX => 3, DIVSIZE => 12, ITEMS => array('7am', '11am')),
            array(ID => "inboundService", TITLE => "Inbound Service", TYPE => RADIO, REQUIRED => true, TABINDEX => 10, DIVSIZE => 12, ITEMS => array('Standard', 'Express')),
        );

        $this->inboundForm = array(
            "Create Inbound Booking" => $this->inboundFields
        );

        $this->orderTypes = array(
            array(VALUE => STATUS, TITLE => TITLE_STATUS),
            array(VALUE => ORDER_NUMBER, TITLE => TITLE_ORDER_NUMBER),
            array(VALUE => OPEN_ORDERS, TITLE => TITLE_OPEN_ORDERS)
        );

        $this->orderStatuses = array(
            array(VALUE => 'dataReceived', TITLE => 'Data Received'),
            array(VALUE => 'awaitingPick', TITLE => 'Awaiting Pick'),
            array(VALUE => 'beingPicked', TITLE => 'Being Picked'),
            array(VALUE => 'awaitingPack', TITLE => 'Awaiting Pack'),
            array(VALUE => 'awaitingBespokeProcess', TITLE => 'Bespoke Process'),
            array(VALUE => 'awaitingShipping', TITLE => 'Awaiting Shipping'),
            array(VALUE => 'collectionArea', TITLE => 'Collection Area'),
            array(VALUE => 'shipped', TITLE => 'Shipped'),
            array(VALUE => 'exceptions', TITLE => 'Exceptions'),
            array(VALUE => 'courierExceptions', TITLE => 'Courier Exceptions'),
        );

        $this->orderFilters = array(
            TYPES => $this->orderTypes,
            STATUSES => $this->orderStatuses
        );

        $this->grnTypes = array(
            array(VALUE => OPEN, TITLE => TITLE_OPEN),
            array(VALUE => CLOSED, TITLE => TITLE_CLOSED),
            array(VALUE => GRN_REFERENCE, TITLE => TITLE_GRN_REFERENCE)
        );

        $this->grnFilters = array(
            TYPES => $this->grnTypes,
        );

        $this->userDetails = array(
            array(ID => FIRST_NAME, TITLE => TITLE_FIRST_NAME, TYPE => TEXT, REQUIRED => true, TABINDEX => 3, DIVSIZE => 6),
            array(ID => LAST_NAME, TITLE => TITLE_LAST_NAME, TYPE => TEXT, REQUIRED => true, TABINDEX => 4, DIVSIZE => 6),
            array(ID => EMAIL, TITLE => TITLE_EMAIL, TYPE => TEXT, REQUIRED => true, TABINDEX => 5, DIVSIZE => 12),
            array(ID => PASSWORD, TITLE => TITLE_PASSWORD, TYPE => PASSWORD, REQUIRED => true, TABINDEX => 6, DIVSIZE => 12),
            array(ID => COMPANY, TITLE => TITLE_COMPANY, TYPE => SELECT_, REQUIRED => true, TABINDEX => 7, DIVSIZE => 12),
            array(ID => ACCOUNT_TYPE, TITLE => TITLE_ACCOUNT_TYPE, TYPE => RADIO, REQUIRED => true, TABINDEX => 8, DIVSIZE => 12, ITEMS => array('Customer', 'Admin'))
        );

        $this->userForm = array(
            TITLE_USER_DETAILS => $this->userDetails,
        );

        $this->ticketCreateFields = array(
            array(ID => PRIORITY, TITLE => TITLE_PRIORITY, TYPE => RADIO, REQUIRED => true, TABINDEX => 3, DIVSIZE => 12, ITEMS => array('Low', 'Normal', 'High')),
            array(ID => AREA, TITLE => TITLE_AREA, TYPE => RADIO, REQUIRED => true, TABINDEX => 4, DIVSIZE => 12, ITEMS => array('Amendment', 'Complaint', 'Finance', 'Inventory', 'Post Despatch', 'Pre Despatch', 'Systems')),
            array(ID => ORDER_NUMBER, TITLE => TITLE_ORDER_NUMBER, TYPE => TEXT, REQUIRED => true, TABINDEX => 6, DIVSIZE => 12),
            array(ID => SUBJECT, TITLE => TITLE_SUBJECT, TYPE => TEXT, REQUIRED => true, TABINDEX => 7, DIVSIZE => 12),
            array(ID => DESCRIPTION, TITLE => TITLE_DESCRIPTION, TYPE => TEXTAREA, REQUIRED => true, TABINDEX => 8, DIVSIZE => 12),
            array(ID => FILE, TITLE => TITLE_FILE, TYPE => FILE, REQUIRED => false, TABINDEX => 9, DIVSIZE => 12)
        );

        $this->ticketCreateForm = array(
            TITLE_TICKET_CREATE => $this->ticketCreateFields,
        );

        $this->reworkingBookingFields = array(
            array(ID => 'customerRef', TITLE => 'Your Reference', TYPE => TEXT, REQUIRED => true, TABINDEX => 8, DIVSIZE => 4),
            array(ID => ORDER_NUMBER, TITLE => 'Order Number to Pick', TYPE => TEXT, REQUIRED => true, TABINDEX => 8, DIVSIZE => 4),
            array(ID => GRN_REFERENCE, TITLE => 'GRN Reference', TYPE => TEXT, REQUIRED => true, TABINDEX => 8, DIVSIZE => 4),
            array(ID => "reworkType", TITLE => "Rework Type", TYPE => RADIO, REQUIRED => true, TABINDEX => 3, DIVSIZE => 12, ITEMS => array('Kitting', 'De-Kitting', 'Labelling', 'Bag and Tag', 'Other')),
            array(ID => DESCRIPTION, TITLE => "Detailed Description of Work to Undertake", TYPE => TEXTAREA, REQUIRED => true, TABINDEX => 8, DIVSIZE => 12),
            array(ID => "requiredByDate", TITLE => "Requested Completion By Date", TYPE => DATE, REQUIRED => true, TABINDEX => 9, DIVSIZE => 12),
            array(ID => FILE, TITLE => TITLE_FILE, TYPE => FILE, MULTIPLE => true, REQUIRED => false, TABINDEX => 9, DIVSIZE => 12),
        );

        $this->reworkForm = array(
            TITLE_BOOK_REWORK => $this->reworkingBookingFields,
        );

        $this->viewTicketsTypes = array(
            array(VALUE => OPEN, TITLE => TITLE_OPEN),
            array(VALUE => CLOSED, TITLE => TITLE_CLOSED),
        );

        $this->viewTicketsFilters = array(
            TYPES => $this->viewTicketsTypes,
        );

        $this->ftpFolderTypes = array(
            array(VALUE => IMPORT, TITLE => TITLE_IMPORT),
            array(VALUE => EXPORT, TITLE => TITLE_EXPORT),
        );

        $this->ftpFolderilters = array(
            TYPES => $this->ftpFolderTypes,
        );

        $this->serviceMappingFields = array(
            array(ID => "clientService", TITLE => "Client Service", TYPE => TEXT, REQUIRED => true, TABINDEX => 2, DIVSIZE => 6),
            array(ID => 'edit', TITLE => 'Edit', TYPE => CHECKBOX, REQUIRED => false, TABINDEX => 31, DIVSIZE => 3),
        );

        $this->serviceMappingCriteriaFields = array(
            array(ID => "region", TITLE => "Region", TYPE => RADIO, REQUIRED => true, TABINDEX => 3, DIVSIZE => 4, ITEMS => array('Any', 'Non-UK', 'UK')),
            array(ID => "appliesTo", TITLE => "Service Applies To", TYPE => RADIO, REQUIRED => true, TABINDEX => 4, DIVSIZE => 4, ITEMS => array('Delivery Note', 'Whole Account')),
            array(ID => DELIVERY_NOTE, TITLE => TITLE_DELIVERY_NOTE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 5, DIVSIZE => 4),
        );

        $this->serviceMappingMapsFields = array(
            array(ID => "serviceType", TITLE => "Service Type", TYPE => RADIO, REQUIRED => true, TABINDEX => 6, DIVSIZE => 6, ITEMS => array('Service Group', 'General Service')),
            array(ID => SERVICE, TITLE => TITLE_SERVICE, TYPE => SELECT_, REQUIRED => true, TABINDEX => 7, DIVSIZE => 6),
            array(ID => "generalService", TITLE => "General Service", TYPE => RADIO, REQUIRED => true, TABINDEX => 8, DIVSIZE => 6, ITEMS => array('Economy', 'Standard', 'Express')),
        );

        $this->serviceMappingTool = array(
            'Service Mapping Tool' => $this->serviceMappingFields,
            'Criteria' => $this->serviceMappingCriteriaFields,
            'Maps To' => $this->serviceMappingMapsFields,
        );

        $this->newsArticleFields = array(
            array(ID => "subject", TITLE => "Title (Email Subject)", TYPE => TEXT, REQUIRED => true, TABINDEX => 1, DIVSIZE => 6),
            array(ID => "newsArticle", TITLE => 'News Article', TYPE => TEXTAREA, REQUIRED => false, TABINDEX => 2, DIVSIZE => 12),
            array(ID => "notify", TITLE => "Publish News Article To", TYPE => RADIO, REQUIRED => true, TABINDEX => 3, DIVSIZE => 6, ITEMS => array('Fusion', 'Email', 'Both')),
            array(ID => "sendToContacts", TITLE => "Send News Article To", TYPE => RADIO, REQUIRED => true, TABINDEX => 3, DIVSIZE => 6, ITEMS => array('Primary Contacts', 'Primary and Secondary Contacts', 'All Active Users')),
        );

        $this->newsArticle = array(
            'Create News Article' => $this->newsArticleFields,
        );

       // MODELS
        $this->load->model("login_model");
        $this->load->model("user_model");
        $this->load->vars($this->data);
    }


    // NEW SHIT HERE

    
    
    public function getPageFile($page)
    {
        return strtolower($page->router->fetch_class()) . "/" . strtolower($page->router->fetch_method());
    }

    public function getModal($page)
    {
        return strtolower(MODALS) ."/" . strtolower($page->router->fetch_class());
    }

    public function is_post()
    {
        return $_SERVER[REQUEST_METHOD] == POST_METHOD ? TRUE : FALSE;
    }

    public function setPageDetails($page)
    {
        $this->data[TITLE] = $this->getPageTitle($page);
        $this->data[CURRENT_PAGE] = $page->router->fetch_class() . "/" . $page->router->fetch_method();

        $this->data["fPageIndex"] = "";
        $this->data["fControllerLogin"] = "";
        $this->data["fPage"] = "";
        $this->data["fPageIndex"] = "";
        $this->data["fControllerError"] = "";
    }

    public function getPageTitle($page)
    {
        $unformattedTitle = strtolower($page->router->fetch_method());
        $unformattedTitle = str_replace("_", " ", $unformattedTitle);
        return ucwords($unformattedTitle);
    }

    public function logged_in()
    {
        $data = $this->data;

        if ($this->ion_auth->logged_in()):
            redirect(PAGE_MAIN, "refresh");
        endif;
    }

    public function login()
    {
        $data = $this->data;
        if (!$this->ion_auth->logged_in()):
            redirect(PAGE_LOGIN, "refresh");
        endif;
    }

    public function logout()
    {
        $this->ion_auth->logout();
    }

    public function title($fTitle)
    {
        return strtoupper(str_replace("_", " ", $fTitle));
    }
}

class MY_Secure_Controller extends FUSION_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->login();

        $this->data[API_KEY] = $this->api_key();
        $this->data['apiKeys'] = $this->api_keys();


            if ($this->data[API_KEY] === false):
                // FIX THIS ERROR!!!!!!
                redirect(PAGE_ERROR_APIKEY, "refresh");
            else:
                $this->data[API_KEY] = $this->data[API_KEY]->apiKey;
                $this->data[LOGGED_USER] = $this->logged_user();

                if ($this->data[LOGGED_USER] === false):
                    // FIX THIS ERROR!!!!!!
                    redirect(PAGE_ERROR_USER, "refresh");
                else:   
                    $this->data[SESSION] = $_COOKIE['ci_session'];

                    $this->data[USER_ID] = $this->data[LOGGED_USER]->id;
                    $this->data[USER_COMPANY] = $this->data[LOGGED_USER]->company->id;

                    $this->data[PROFILEPICTURE] = base_url();
                    /*
                    if ($this->data[self::KEY_LOGGEDUSER]->picture === "1"):
                        $this->data[self::PROFILEPICTURE] = self::URL_3PLUSERPICTURES . $this->data[self::KEY_LOGGEDUSER]->id . "/user.png";
                    else:
                        $this->data[self::PROFILEPICTURE] = base_url(self::DIRECTORY_THEME) . "images/user.png";
                    endif;
                    */
                endif;
            endif;

    }

    public function api_key()
    {
        $apiKey = $this->user_model->api_key($this->ion_auth->user()->row()->id);
        
        if($apiKey !== false)
        {
             switch (SYSTEM_TYPE) {
                case LIVE:
                    $apiKeyValue = $apiKey->{KEY};
                    break;
                default:
                    $apiKeyValue = $apiKey->{KEY_DEV};
                    break;
            }

            $apiKey = array(API_KEY => $apiKeyValue);
            $apiKey = (object) $apiKey;
        } 

        return $apiKey;
    }

    public function api_keys()
    {
        $apiKeys = $this->user_model->api_key($this->ion_auth->user()->row()->id);
        
        if($apiKeys !== false)
        {
            $apiKeys = array(
                LIVE => $apiKeys->{KEY},
                TEST => $apiKeys->{KEY_DEV}
            );
            $apiKeys = (object) $apiKeys;
        } 

        return $apiKeys;
    }

    public function logged_user()
    {
        $data = $this->data;

        $fUserId = $this->ion_auth->user()->row()->id;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->data[URL_3PL_API] . USERS . "/?" . API_KEY_2 . "=" . $this->data[API_KEY],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => URL_3PL_API_MAXREDIRS,
            CURLOPT_TIMEOUT => URL_3PL_API_TIMEOUT,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => GET_METHOD,
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: " . URL_3PL_API_CACHECONTROL,
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        } else {
            $response = json_decode($response);
            if ($response->status == 202) {
                $error = $response->errorMessage[0];
                $this->logout();
                redirect(PAGE_LOGIN_INDEX. "/?error=" .$error, "refresh");
            }
            return $response->data["0"];
        }
    }
}