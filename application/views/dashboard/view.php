<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="info-box bg-blue hover-expand-effect">
            <div class="icon">
                <i class="material-icons"></i>
            </div>
            <div class="content">
                <div class="text"></div>
                <div class="number demo-preloader" style="margin-top: 1px;"
                     id="loader">
                    <div class="preloader pl-size-sm">
                        <div class="spinner-layer pl-white">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" data-from="0"
                     data-to="0" data-speed="1000" data-fresh-interval="20">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="info-box bg-blue hover-expand-effect">
            <div class="icon">
                <i class="material-icons"></i>
            </div>
            <div class="content">
                <div class="text"></div>
                <div class="number demo-preloader" style="margin-top: 1px;"
                     id="loader">
                    <div class="preloader pl-size-sm">
                        <div class="spinner-layer pl-white">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" data-from="0"
                     data-to="0" data-speed="1000" data-fresh-interval="20">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="info-box bg-blue hover-expand-effect">
            <div class="icon">
                <i class="material-icons"></i>
            </div>
            <div class="content">
                <div class="text"></div>
                <div class="number demo-preloader" style="margin-top: 1px;"
                     id="loader">
                    <div class="preloader pl-size-sm">
                        <div class="spinner-layer pl-white">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" data-from="0"
                     data-to="0" data-speed="1000" data-fresh-interval="20">
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
        <div class="info-box bg-blue hover-expand-effect">
            <div class="icon">
                <i class="material-icons"></i>
            </div>
            <div class="content">
                <div class="text"></div>
                <div class="number demo-preloader" style="margin-top: 1px;"
                     id="loader">
                    <div class="preloader pl-size-sm">
                        <div class="spinner-layer pl-white">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="" data-from="0"
                     data-to="0" data-speed="1000" data-fresh-interval="20">
                </div>
            </div>
        </div>
    </div>
</div>