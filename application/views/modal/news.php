<div class="modal fade" id="modalNews" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="newsSubject"></h4>
        <p class="tab-content" id="createdAt"></p>
      </div>
      <div class="modal-body">
        <div class="tab-content" id="newsBody">
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="<?php echo BUTTON_LINK; ?>" data-dismiss="modal"><?php echo TITLE_CLOSE; ?></button>
      </div>
    </div>
  </div>
</div>