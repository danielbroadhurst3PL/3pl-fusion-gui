<div class="modal fade" id="modalApiKey" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="modalApiKeyTitle">API Keys
        </h4>
      </div>
      <div class="modal-body">
        <div class="tab-content">
          <h3>Live API Key</h3>
          <a href="https://dashboard.3p-logistics.co.uk/" target="_blank">Fusion Live System</a> 
          <p id="usersApiKeyLive"></p>
          <h3>Test API Key</h3>
          <a href="https://dashboard-test.3p-logistics.co.uk/" target="_blank">Fusion Test System</a> 
          <p id="usersApiKeyTest"></p>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="<?php echo BUTTON_LINK; ?>" data-dismiss="modal"><?php echo TITLE_CLOSE; ?></button>
      </div>
    </div>
  </div>
</div>