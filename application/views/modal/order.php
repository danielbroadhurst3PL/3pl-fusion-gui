<div class="modal fade" id="<?php echo MODAL_ORDER; ?>" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="<?php echo MODAL_ORDER_LABEL; ?>"></h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active" id="modelView"><a href="#<?php echo MODAL_ORDER_VIEW; ?>" data-toggle="tab" aria-expanded="false"><?php echo TITLE_VIEW; ?></a></li>
                    <li role="presentation" class=""><a href="#<?php echo MODAL_ORDER_ITEM; ?>" data-toggle="tab" aria-expanded="false"><?php echo TITLE_ITEM; ?></a></li>
                    <li role="presentation" class="" id="modalPacking"><a href="#<?php echo MODAL_ORDER_PACKING; ?>" data-toggle="tab" aria-expanded="false"><?php echo TITLE_PACKING_LIST; ?></a></li>
                    <li role="presentation" class="" id="modalShipping"><a href="#shippingLabel" data-toggle="tab" aria-expanded="false">Shipping Label</a></li>
                    <!-- <li role="presentation" class="" id="modalBilling"><a href="#billingInformation" data-toggle="tab" aria-expanded="false">Billing Information</a></li> -->
                    <li role="presentation" class=""><a href="#modalOrderNotes" data-toggle="tab" aria-expanded="false">Notes</a></li>

                </ul>   

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="<?php echo MODAL_ORDER_VIEW; ?>">
                        <div class="modal-name">
                            <div class="modal-name-group">
                                <div>
                                    <b><?php echo TITLE_FIRST_NAME; ?></b>
                                    <p id="<?php echo MODAL_ORDER_FIRST_NAME; ?>"></p>
                                </div>
                                <div>
                                    <b><?php echo TITLE_LAST_NAME; ?></b>
                                    <p id="<?php echo MODAL_ORDER_LAST_NAME; ?>"></p>
                                </div>
                            </div>
                            <div class="modal-contact">
                                <b><?php echo TITLE_PHONE_NUMBER; ?></b>
                                <p id="<?php echo MODAL_ORDER_PHONE_NUMBER; ?>"></p>
                                <b><?php echo TITLE_EMAIL; ?></b>
                                <p id="<?php echo MODAL_ORDER_EMAIL; ?>"></p>
                            </div>
                            <div class="modal-address">
                                <b><?php echo TITLE_COMPANY; ?></b>
                                <p id="<?php echo MODAL_ORDER_COMPANY; ?>"></p>
                                <b><?php echo TITLE_ADDRESS_LINE_1; ?></b>
                                <p id="<?php echo MODAL_ORDER_ADDRESS_LINE_1; ?>"></p>
                                <b><?php echo TITLE_ADDRESS_LINE_2; ?></b>
                                <p id="<?php echo MODAL_ORDER_ADDRESS_LINE_2; ?>"></p>
                                <div class="modal-town-region">
                                    <div>
                                        <b><?php echo TITLE_TOWN; ?></b>
                                        <p id="<?php echo MODAL_ORDER_TOWN; ?>"></p>
                                    </div>
                                    <div>
                                        <b><?php echo TITLE_REGION; ?></b>
                                        <p id="<?php echo MODAL_ORDER_REGION; ?>"></p>
                                    </div>
                                </div>
                                <div class="modal-post-country">                             
                                    <div>
                                        <b><?php echo TITLE_POSTCODE; ?></b>
                                        <p id="<?php echo MODAL_ORDER_POSTCODE; ?>"></p>
                                    </div>
                                </div>
                                <b><?php echo TITLE_COUNTRY; ?></b>
                                <p id="<?php echo MODAL_ORDER_COUNTRY; ?>"></p>
                            </div>
                        </div>
                        <div class="modal-order">
                            <b><?php echo TITLE_SERVICE; ?></b>
                            <p id="<?php echo MODAL_ORDER_SERVICE; ?>"></p>
                            <b><?php echo TITLE_CARRIER; ?></b>
                            <p id="<?php echo MODAL_ORDER_CARRIER; ?>"></p>
                            <b><?php echo TITLE_TRACKING; ?></b>
                            <p id="<?php echo MODAL_ORDER_TRACKING; ?>"></p>
                            <b><?php echo TITLE_REFERENCE; ?></b>
                            <p id="<?php echo MODAL_ORDER_REFERENCE; ?>"></p>
                            <b><?php echo TITLE_PURCHASE_ORDER; ?></b>
                            <p id="<?php echo MODAL_ORDER_PURCHASE_ORDER; ?>"></p>
                            <b><?php echo TITLE_CURRENCY; ?></b>
                            <p id="<?php echo MODAL_ORDER_CURRENCY; ?>"></p>
                            <b><?php echo TITLE_DATE_CREATED; ?></b>
                            <p id="<?php echo MODAL_ORDER_DATE_CREATED; ?>"></p>
                            <b><?php echo TITLE_DISPATCH_DATE; ?></b>
                            <p id="<?php echo MODAL_ORDER_DISPATCH_DATE; ?>"></p>
                            <b><?php echo TITLE_DELIVERY_NOTE; ?></b>
                            <p id="<?php echo MODAL_ORDER_DELIVERY_NOTE; ?>"></p>
                            <b><?php echo TITLE_INSTRUCTIONS_PACKING; ?></b>
                            <p id="<?php echo MODAL_ORDER_INSTRUCTIONS_PACKING; ?>"></p>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="<?php echo MODAL_ORDER_ITEM; ?>">
                    <table id="modalItemTable" class="table table-bordered table-striped table-hover dataTable no-footer">
                        <thead>
                            <th>Shipment ID</th>
                            <th>SO Line ID</th>
                            <th>SKU</th>
                            <th>Product Name</th>
                            <th>Quantity Required</th>
                            <th>Quantity Shipped</th>
                            <th>Stock Status</th>
                        </thead>
                    </table>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="<?php echo MODAL_ORDER_PACKING; ?>">
                        <div class="table-responsive" id="packingListTableDiv">
                            <table id="boxInfoTable" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>Box ID</th>
                                        <th>Box Weight</th>
                                        <th>Box Dims</th>
                                        <th>Pieces</th>
                                    </tr>
                                </thead>
                            </table>
                            <table id="packingListTable" class="table table-bordered table-striped table-hover dataTable">
                                <thead>
                                    <tr>
                                        <th>Consignment ID</th>
                                        <th>Box Reference</th>
                                        <th>Product</th>
                                        <th>Batch ID</th>
                                        <th>Product Title</th>
                                        <th>Box Quantity</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="shippingLabel">
                        <p>

                        </p>
                    </div>
                    <!-- <div role="tabpanel" class="tab-pane fade" id="billingInformation">
                        <table id="modalBillingTable" class="table table-bordered table-striped table-hover dataTable">
                            <thead>
                                <tr>
                                    <th>Shipment ID</th>
                                    <th>Billing Info</th>
                                    <th>Pieces</th>
                                    <th>Billing Type</th>
                                    <th>Amount</th>
                                    <th>Charge Code</th>
                                </tr>
                            </thead>
                        </table>
                    </div> -->
                    <div role="tabpanel" class="tab-pane fade" id="modalOrderNotes">
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="<?php echo BUTTON_LINK; ?>" value="" id="<?php echo MODAL_ORDER_EDIT; ?>"><?php echo TITLE_EDIT; ?></button>
                <button type="button" class="<?php echo BUTTON_LINK; ?>" data-dismiss="modal"><?php echo TITLE_CLOSE; ?></button>
            </div>
        </div>
    </div>
</div>