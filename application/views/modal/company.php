<div class="modal fade" id="<?php echo MODAL_COMPANY; ?>" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="<?php echo MODAL_COMPANY_LABEL; ?>"></h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#<?php echo MODAL_COMPANY_VIEW; ?>" data-toggle="tab" aria-expanded="true"><?php echo TITLE_VIEW; ?></a></li>
                    <li role="presentation"><a href="#<?php echo MODAL_COMPANY_LOGS; ?>" data-toggle="tab" aria-expanded="false"><?php echo TITLE_LOGS; ?></a></li>
                </ul>   

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="<?php echo MODAL_COMPANY_VIEW; ?>">
                        <b><?php echo TITLE_COMPANY_CODE; ?></b>
                        <p id="<?php echo MODAL_COMPANY_CODE; ?>"></p>
                        <b><?php echo TITLE_3PL_EMAIL; ?></b>
                        <p id="<?php echo MODAL_COMPANY_EMAIL; ?>"></p>
                        <b><?php echo TITLE_PRIMARY_EMAIL; ?></b>
                        <p id="<?php echo MODAL_COMPANY_PRIMARY_EMAIL; ?>"></p>
                        <b><?php echo TITLE_SECONDARY_EMAIL; ?></b>
                        <p id="<?php echo MODAL_COMPANY_SECONDARY_EMAIL; ?>"></p>
                        <b><?php echo TITLE_ADDRESS_LINE_1; ?></b>
                        <p id="<?php echo MODAL_COMPANY_ADDRESS_LINE_1; ?>"></p>
                        <b><?php echo TITLE_ADDRESS_LINE_2; ?></b>
                        <p id="<?php echo MODAL_COMPANY_ADDRESS_LINE_2; ?>"></p>
                        <b><?php echo TITLE_TOWN; ?></b>
                        <p id="<?php echo MODAL_COMPANY_TOWN; ?>"></p>
                        <b><?php echo TITLE_REGION; ?></b>
                        <p id="<?php echo MODAL_COMPANY_REGION; ?>"></p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="<?php echo MODAL_COMPANY_LOGS; ?>">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="<?php echo BUTTON_LINK; ?>" value="" id="<?php echo MODAL_COMPANY_EDIT; ?>"><?php echo TITLE_EDIT; ?></button>
                <button type="button" class="<?php echo BUTTON_LINK; ?>" data-dismiss="modal"><?php echo TITLE_CLOSE; ?></button>
            </div>
        </div>
    </div>
</div>