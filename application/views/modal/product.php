<div class="modal fade" id="<?php echo MODAL_PRODUCT; ?>" tabindex="-1" role="dialog" style="display: none;">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="<?php echo MODAL_PRODUCT_LABEL; ?>"></h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs tab-nav-right" role="tablist">
                    <li role="presentation" class="active"><a href="#<?php echo MODAL_PRODUCT_STOCK; ?>" data-toggle="tab" aria-expanded="true"><?php echo TITLE_STOCK; ?></a></li>
                    <li role="presentation"><a href="#<?php echo MODAL_PRODUCT_VIEW; ?>" data-toggle="tab" aria-expanded="false"><?php echo TITLE_VIEW; ?></a></li>
                    <li role="presentation"><a href="#<?php echo MODAL_PRODUCT_LOGS; ?>" data-toggle="tab" aria-expanded="false"><?php echo TITLE_LOGS; ?></a></li>
                </ul>   

                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="<?php echo MODAL_PRODUCT_STOCK; ?>">
                        <b><?php echo TITLE_QUANTITY; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_QUANTITY; ?>"></p>
                        <b><?php echo TITLE_ALLOCATED; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_ALLOCATED; ?>"></p>
                        <b><?php echo TITLE_ASSIGNED; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_ASSIGNED; ?>"></p>
                        <b><?php echo TITLE_ORDER_IN; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_ORDER_IN; ?>"></p>
                        <b><?php echo TITLE_DUE_IN; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_DUE_IN; ?>"></p>
                        <b><?php echo TITLE_ORDER_OUT; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_ORDER_OUT; ?>"></p>
                        <b><?php echo TITLE_DUE_OUT; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_DUE_OUT; ?>"></p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="<?php echo MODAL_PRODUCT_VIEW; ?>">
                        <b><?php echo TITLE_DESCRIPTION; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_DESCRIPTION; ?>"></p>
                        <b><?php echo TITLE_BARCODE; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_BARCODE; ?>"></p>
                        <b><?php echo TITLE_ASIN; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_ASIN; ?>"></p>
                        <b><?php echo TITLE_DGN_TYPE; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_DGN_TYPE; ?>"></p>
                        <b><?php echo TITLE_DGN_DETAILS; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_DGN_DETAILS; ?>"></p>
                        <b><?php echo TITLE_COUNTRY; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_COUNTRY; ?>"></p>
                        <b><?php echo TITLE_COMMODITY_CODE; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_COMMODITY_CODE; ?>"></p>
                        <b><?php echo TITLE_HEIGHT; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_HEIGHT; ?>"></p>
                        <b><?php echo TITLE_WIDTH; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_WIDTH; ?>"></p>
                        <b><?php echo TITLE_DEPTH; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_DEPTH; ?>"></p>
                        <b><?php echo TITLE_WEIGHT; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_WEIGHT; ?>"></p>
                        <b><?php echo TITLE_CURRENCY; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_CURRENCY; ?>"></p>
                        <b><?php echo TITLE_VALUE; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_VALUE; ?>"></p>
                        <b><?php echo TITLE_INNER_QUANTITY; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_INNER_QUANTITY; ?>"></p>
                        <b><?php echo TITLE_MASTER_CARTON_QUANTITY; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_MASTER_CARTON_QUANTITY; ?>"></p>
                        <b><?php echo TITLE_PALLET_QUANTITY; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_PALLET_QUANTITY; ?>"></p>
                        <b><?php echo TITLE_DATE_EXPIRY; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_DATE_EXPIRY; ?>"></p>
                        <b><?php echo TITLE_SERIAL; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_SERIAL; ?>"></p>
                        <b><?php echo TITLE_BATCH; ?></b>
                        <p id="<?php echo MODAL_PRODUCT_BATCH; ?>"></p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="<?php echo MODAL_PRODUCT_LOGS; ?>">
                    </div>
                </div>


            
            </div>
            <div class="modal-footer">
                <button type="button" class="<?php echo BUTTON_LINK; ?>" value="" id="<?php echo MODAL_PRODUCT_EDIT; ?>"><?php echo TITLE_EDIT; ?></button>
                <button type="button" class="<?php echo BUTTON_LINK; ?>" data-dismiss="modal"><?php echo TITLE_CLOSE; ?></button>
            </div>
        </div>
    </div>
</div>