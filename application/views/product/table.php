<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_PRODUCTS; ?></h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_PRODUCTS_VIEW; ?>">
                    <thead>
                    <tr>
                        <th><?php echo TITLE_SKU; ?></th>
                        <th><?php echo TITLE_DESCRIPTION; ?></th>
                        <th><?php echo TITLE_TITLE; ?></th>
                        <th><?php echo TITLE_BARCODE; ?></th>
                        <th><?php echo TITLE_QUANTITY; ?></th>
                        <th><?php echo TITLE_ALLOCATED; ?></th>
                        <th><?php echo TITLE_ASSIGNED; ?></th>
                        <th><?php echo TITLE_DUE_IN; ?></th>
                        <th><?php echo TITLE_DUE_OUT; ?></th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>