<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    Edit Bulk Products
                </h2>
            </div>
            <div class="body">
                <center>
                    <div class="fallback">
                        <input name="file" required="required" type="file" id="editBulkFiles" />
                    </div>
                </center>
                <br>
                <div id="fileValidate"></div>
                <br>
                <center><button id="editBulkProductUpload" type="button" class="<?php echo BUTTON_BLUE; ?>">Upload Edited Products File</button></center>
                <br>
                <center><button id="templateDownload" type="button" class="btn btn-success waves-effect">Download Products File</button></center>
                <br>
                <p>To edit products you must include the correct SKU for the products which you wish to update.</p>
                <p>Only data which is being updated needs to be entered alongside the SKU.</p>
        </div>
    </div>
</div