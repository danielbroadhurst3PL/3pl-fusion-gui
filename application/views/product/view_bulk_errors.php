<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="card">
            <div class="header">
                <h2 id="titleProducts">
                    Bulk Product Error View
                </h2>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_PRODUCTS_BULK_ERROR_VIEW; ?>">
                        <thead>
                        <tr>
                            <th><?php echo TITLE_EDIT; ?></th>
                            <th><?php echo TITLE_SKU; ?></th>
                            <th><?php echo TITLE_DESCRIPTION; ?></th>
                            <th><?php echo TITLE_BARCODE; ?></th>
                            <th><?php echo TITLE_ASIN; ?></th>
                            <th><?php echo TITLE_DGN_TYPE; ?></th>
                            <th><?php echo TITLE_DGN_DETAILS; ?></th>
                            <th><?php echo TITLE_COUNTRY_OF_ORIGIN; ?></th>
                            <th><?php echo TITLE_COMMODITY_CODE; ?></th>
                            <th><?php echo TITLE_HEIGHT; ?></th>
                            <th><?php echo TITLE_WIDTH; ?></th>
                            <th><?php echo TITLE_DEPTH; ?></th>
                            <th><?php echo TITLE_WEIGHT; ?></th>
                            <th><?php echo TITLE_CURRENCY; ?></th>
                            <th><?php echo TITLE_VALUE; ?></th>
                            <th><?php echo TITLE_INNER_QUANTITY; ?></th>
                            <th><?php echo TITLE_MASTER_CARTON_QUANTITY; ?><th>
                            <th><?php echo TITLE_PALLET_QUANTITY; ?></th>
                            <th><?php echo TITLE_DATE_EXPIRY; ?></th>
                            <th><?php echo TITLE_SERIAL; ?></th>
                            <th><?php echo TITLE_BATCH; ?></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>