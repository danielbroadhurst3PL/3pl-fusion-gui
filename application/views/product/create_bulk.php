<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Create Bulk Products</h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <center>
                    <div class="fallback">
                        <input name="file" required="required" type="file" id="bulkFiles" />
                    </div>
                </center>
                <br>
                <div id="fileValidate"></div>
                <br>
                <center><button id="bulkProductUpload" type="button" class="<?php echo BUTTON_BLUE; ?>">Upload Bulk Products File</button></center>
                <br>
                <center><button id="templateDownload" type="button" class="btn btn-warning waves-effect">Download Bulk Products Template</button></center>
                <br>
                <center><button id="issueProductsDownload" type="button" class="btn btn-danger waves-effect">Download Outstanding Issue Products</button></center>            </div>
        </div>
    </div>
</div