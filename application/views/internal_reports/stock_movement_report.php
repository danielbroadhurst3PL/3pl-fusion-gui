<form id="stockMovementForm">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Date <?php echo TITLE_FILTERS; ?></h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix">
                <div class="col-md-3" id="skuInput">
                        <label for="filterStatus">SKU</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="3" type="text" id="sku" class="form-control">
                                <div id="divFilterShippedFromValidate"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" id="MoveTypeBox">
                        <label for="filterStatus">Move Type</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="4" type="text" id="MoveType" class="form-control">
                                <div id="divFilterShippedToValidate"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" id="<?php echo DIV_FILTER_DATE_SHIPPED_FROM; ?>">
                        <label for="filterStatus">Date & Time Created From</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="3" type="text" id="<?php echo DATE_SHIPPED_FROM; ?>Time" class="datetimepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedFromValidate"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3" id="<?php echo DIV_FILTER_DATE_SHIPPED_TO; ?>">
                        <label for="filterStatus">Date & Time Created To</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="4" type="text" id="<?php echo DATE_SHIPPED_TO; ?>Time" class="datetimepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedToValidate"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="stockMovementButton">
                        <button id="buttonStockMovement" class="btn btn-primary waves-effect">SUBMIT</button>
                    </div>
                    <div class="col-lg-12" id="divFilterShippedToValidateFuture"></div>
                </div>
            </div>
        </div>
    </div>
</form>


<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="stockMovementReport">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_STOCK_MOVEMENT_REPORT; ?></h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table class="table table-bordered table-striped table-hover dt-responsive dataTable" id="<?php echo TABLE_STOCK_MOVEMENT_REPORT; ?>">
                <thead>
                    <tr>
                        <th>Move Type</th>
                        <th>Move ID</th>
                        <th>Move Line ID</th>
                        <th>Status</th>
                        <th>Class</th>
                        <th>SKU ID</th>
                        <th>Qty Tasked</th>
                        <th>Qty Actioned</th>
                        <th>Unit of Measure</th>
                        <th>Box Qty</th>
                        <th>From Facility</th>
                        <th>From Zone</th>
                        <th>From Bay</th>
                        <th>From Section</th>
                        <th>From Slot</th>
                        <th>From StU</th>
                        <th>From StT</th>
                        <th>From Owner ID</th>
                        <th>From Status</th>
                        <th>From Assign Type</th>
                        <th>From Assign Ref</th>
                        <th>From Load</th>
                        <th>To Facility</th>
                        <th>To Zone</th>
                        <th>To Bay</th>
                        <th>To Section</th>
                        <th>To Slot</th>
                        <th>To StU</th>
                        <th>To StT</th>
                        <th>To Owner ID</th>
                        <th>To Status</th>
                        <th>To Assign Type</th>
                        <th>To Assign Ref</th>
                        <th>To Load</th>
                        <th>Sequence</th>
                        <th>Job ID</th>
                        <th>Job Sequence</th>
                        <th>Operator</th>
                        <th>Supervisor</th>
                        <th>Reason ID</th>
                        <th>Date Created</th>
                        <th>Date Suspended</th>
                        <th>Date Picked</th>
                        <th>Date Closed</th>
                        <th>Stage</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
