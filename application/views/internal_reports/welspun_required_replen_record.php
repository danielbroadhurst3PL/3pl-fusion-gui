<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="stockMovementReport">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_WELSPUN_REQUIRED_REPLEN; ?></h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">   
                <table id="requiredReplenTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
                    <thead>
                        <tr>
                            <th>SKU</th>
                            <th>Box Qty</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Area</th>
                            <th>Status</th>
                            <th>Min Quantity</th>
                            <th>Max Quantity</th>
                            <th>Replen Action</th>
                            <th>Qty</th>
                            <th>Qty Due In</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>