<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Sorted File Upload</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <form id="sortedBillingUpload">
                      <label>Upload Sorted Billing File: <input type="file" class="form-control" name="sortedFileUpload" id="sortedFileUpload"></label>
                      <button type="submit" id="formSubmit" class="btn btn-primary waves-effect">Upload File</button>
                  </form>
                  <div id="fileValidate"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Sorted Billing View</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <form id="sortedBillingGetData">
<!--                     <div class="col-md-4" id="divCompanyList">
                        <label for="companyList">Choose a Company:</label>
                        <span class="required-label">*</span>
                            <div class="form-group">
                                <div class="form-line">
                                <input list="companies3PL" id="companyList" class="form-control" name="companyList" autofocus required />
                                <datalist id="companies3PL"></datalist>
                            </div>
                        </div>
                    </div> -->
                    <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_FROM; ?>">
                        <label for="filterStatus"><?php echo TITLE_DATE_SHIPPED_FROM; ?></label>
                        <span class="required-label">*</span>
                            <div class="form-group">
                                <div class="form-line">
                                <input tabindex="3" type="text" id="<?php echo DATE_SHIPPED_FROM; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedFromValidate"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_TO; ?>">
                        <label for="filterStatus"><?php echo TITLE_DATE_SHIPPED_TO; ?></label>
                        <span class="required-label">*</span>
                            <div class="form-group">
                                <div class="form-line">
                                <input tabindex="4" type="text" id="<?php echo DATE_SHIPPED_TO; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedToValidate"></div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary waves-effect" id="companyPickerFormSubmit">Select Date Range</button>
                </form>
                <div id="fileValidate"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="stockMovementReport">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_ACOUNT_MANAGER_REPORT; ?></h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
              <table id="sortedUploadTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
                  <thead>
                      <tr>
                        <th>Company ID</th>
                        <th>Electio Reference</th>
                        <th>Order Reference</th>
                        <th>State</th>
                        <th>Carrier Name</th>
                        <th>Carrier Service</th>
                        <th>Date Shipped</th>
                        <th>Total Cost</th>
                        <th>Number of Packages</th>
                        <th>Customer Name</th>
                        <th>Address</th>
                        <th>Postcode</th>
                        <th>Country</th>
                        <th>Tracking</th>
                      </tr>
                  </thead>
              </table>
            </div>
        </div>
    </div>
</div>
