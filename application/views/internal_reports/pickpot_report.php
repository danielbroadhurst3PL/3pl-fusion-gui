<form id="pickpotReportWarehouseForm">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="header">
        <h2>Choose Warehouse</h2>
        <ul class="header-dropdown m-r--5">
          <li>
            <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')" id="<?php echo str_replace(' ', '', $pageTitle); ?>">
              <i class="material-icons info">info</i>
            </a>
          </li>
        </ul>
      </div>
      <div class="body">
        <div class="row clearfix">

          <div class="col-md-4" id="divCompanyList">
            <div class="form-group">
              <label for="warehouse">Warehouse</label>
              <div class="form-line">
                <select id="warehouse" class="form-control show-tick">
                  <option value="choose">Please Choose Warehouse</option>
                  <option value="WAREHOUSE1">The Fulfilment Hub</option>
                  <option value="WAREHOUSE2">Retail Distribution Centre</option>
                </select>
              </div>
            </div>
          </div>

          <div class="col-md-4" id="accountManagerButton">
            <button id="buttonAccountManager" class="btn btn-primary waves-effect">SUBMIT</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="pickpotReport">
  <div class="card">
    <div class="header">
      <h2>Pickpot Report</h2>
      <ul class="header-dropdown m-r--5">
        <li>
          <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')" id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
            <i class="material-icons info">info</i>
          </a>
        </li>
      </ul>
    </div>
    <div class="body">
      <div class="table-responsive">
        <table id="pickpotReportTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
          <thead>
            <tr>
              <th>ReleasedDate</th>
              <th>DateCreated</th>
              <th>OrderNo</th>
              <th>OrderStage</th>
              <th>OrderStatus</th>
              <th>RecipientName</th>
              <th>Client</th>
              <th>ShippingMethod</th>
              <th>OrderClass</th>
              <th>DelNote</th>
              <th>WFNotes</th>
              <th>Lines</th>
              <th>PCS</th>
              <th>CusRef</th>
              <th>CusRef2</th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>