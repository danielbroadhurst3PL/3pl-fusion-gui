<form id="movementBySkuForm">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Date <?php echo TITLE_FILTERS; ?></h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix">

                <div class="col-md-4" id="divCompanyList">
                        <label for="companyList">Choose a Company:</label>
                        <span class="required-label">*</span>
                            <div class="form-group">
                                <div class="form-line">
                                <input list="companies3PL" id="companyList" class="form-control" name="companyList" autofocus required />
                                <datalist id="companies3PL"></datalist>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_FROM; ?>">
                        <label for="filterStatus">Date Created From</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="3" type="text" id="<?php echo DATE_SHIPPED_FROM; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedFromValidate"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_TO; ?>">
                        <label for="filterStatus">Date Created To</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="4" type="text" id="<?php echo DATE_SHIPPED_TO; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedToValidate"></div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="accountManagerButton">
                        <button id="buttonAccountManager" class="btn btn-primary waves-effect">SUBMIT</button>
                    </div>
                    <div class="col-lg-12" id="divFilterShippedToValidateFuture"></div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="movementBySku">
    <div class="card">
        <div class="header">
            <h2>Movement by SKU</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table id="movementBySkuTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
              <thead>
                  <tr>
                    <th>SKU</th>
                    <th>Product Title</th>
                    <th>Times Ordered</th>
                    <th>Average Shipped</th>
                    <th>Total Shipped</th>
                    <th>Box Quantity</th>
                    <th>Replen Quantity</th>
                    <th>Replen Area</th>
                  </tr>
              </thead>
            </table>
            </div>
        </div>
    </div>
</div>
