<form id="consolidationReportForm">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Filter by Company</h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix">

                <div class="col-md-4" id="divCompanyList">
                        <label for="companyList">Choose a Company:</label>
                        <span class="required-label">*</span>
                            <div class="form-group">
                                <div class="form-line">
                                <input list="companies3PL" id="companyList" class="form-control" name="companyList" autofocus required />
                                <datalist id="companies3PL"></datalist>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="accountManagerButton">
                        <button id="buttonAccountManager" class="btn btn-primary waves-effect">SUBMIT</button>
                    </div>
                    <div class="col-lg-12" id="divFilterShippedToValidateFuture"></div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="consolidationReport">
    <div class="card">
        <div class="header">
            <h2>Consolidation Report</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table id="consolidationReportTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
              <thead>
                  <tr>
                    <th>No. of Locations</th>
                    <th>SKU</th>
                    <th>Product</th>
                    <th>Total Pieces</th>
                    <th>Slot</th>
                    <th>Location of Pieces</th>
                  </tr>
              </thead>
            </table>
            </div>
        </div>
    </div>
</div>
