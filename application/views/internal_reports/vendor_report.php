<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="vendorReport">
    <div class="card">
        <div class="header">
            <h2>Vendor Report</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table class="table table-bordered table-striped table-hover dt-responsive dataTable" id="vendorReportTable">
                <thead>
                    <tr>
                      <th>ShipmentId</th>
                      <th>Stage</th>
                      <th>CustomerId</th>
                      <th>DelNote</th>
                      <th>CustomerRef</th>
                      <th>ASN</th>
                      <th>Service</th>
                      <th>Carrier</th>
                      <th>Tracking</th>
                      <th>LNS</th>
                      <th>PCS</th>
                      <th>TransitItems</th>
                      <th>DateCreated</th>
                      <th>DueBy</th>
                      <th>PriorityAllocation</th>
                      <th>PriorityDespatch</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="vendorPackingList">
    <div class="card">
        <div class="header">
            <h2>Packing List</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table class="table table-bordered table-striped table-hover dt-responsive dataTable" id="vendorPackingListTable">
                <thead>
                    <tr>
                      <th>AssignRef</th>
                      <th>BoxNo</th>
                      <th>SKUId</th>
                      <th>StU</th>
                      <th>PCS</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
