<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>GRN Search</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="grnExpectedReceived">
                        <label>Search GRN Ref: <input type="text" name="grnExpectedReceivedRef" id="grnExpectedReceivedRef"></label>
                        <button type="submit" id="formSubmit">Find GRN Ref</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="grnExpectedReceivedCard">
    <div class="card">
        <div class="header">
            <h2 id="expectedReceivedTitle"></h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
                <table id="grnExpectedReceivedTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
                    <thead>
                        <tr>
                            <th>Product Code</th>
                            <th>Title</th>
                            <th>Description</th>
                            <th>Expected Quantity</th>
                            <th>Received Quantity</th>
                            <th>Variance</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="grnItemStuCheckCard">
    <div class="card">
        <div class="header">
            <h2 id="itemStuCheckTitle"></h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">   
                <table id="grnItemStuCheckTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
                    <thead>
                        <tr>
                            <th>Consignment ID</th>
                            <th>SKU ID</th>
                            <th>Description</th>
                            <th>Title</th>
                            <th>STU</th>
                            <th>Quantity</th>
                            <th>Status</th>
                            <th>Stage</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

