<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="card">
    <div class="header">
      <h2>Tickets with 3PL Ownership</h2>
    </div>
    <div class="body">
      <div id="ticketCount3PL"></div>
    </div>
  </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="card">
    <div class="header">
      <h2>Tickets with Courier Ownership</h2>
    </div>
    <div class="body">
      <div id="ticketCountCourier"></div>
    </div>
  </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="card">
    <div class="header">
      <h2>Oldest Open Ticket</h2>
    </div>
    <div class="body">
      <div id="oldestTicket"></div>
    </div>
  </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="card">
    <div class="header">
      <h2>Average First Response Time</h2>
    </div>
    <div class="body">
      <div id="averageResponseTime"></div>
    </div>
  </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="card">
    <div class="header">
      <h2>Average Full Resolution Time</h2>
    </div>
    <div class="body">
      <div id="averageFullResolutionTime"></div>
    </div>
  </div>
</div>
<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
  <div class="card">
    <div class="header">
      <h2>Ticket Categories</h2>
    </div>
    <div class="body" style="margin-top: 20px;">
      <canvas id="myChart" width="400" height="400"></canvas>
    </div>
  </div>
</div>



<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="shortShipped">
    <div class="card">
        <div class="header">
            <h2>Short Shipped Report</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table id="shortShippedTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
              <thead>
                  <tr>
                    <th>Company</th>
                    <th>Warehouse</th>
                    <th>Orders</th>
                    <th>Lines Ordered</th>
                    <th>Pieces Ordered</th>
                    <th>Pieces Shipped</th>
                    <th>Orders Shipped Short</th>
                    <th>Lines Shipped Short</th>
                    <th>Pieces Shipped Short</th>
                    <th>Order Success</th>
                    <th>Lines Success</th>
                    <th>Pieces Success</th>
                  </tr>
              </thead>
            </table>
            </div>
        </div>
    </div>
</div>