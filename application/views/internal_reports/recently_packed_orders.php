<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="stockMovementReport">
    <div class="card">
        <div class="header">
            <h2>Recently Packed Orders</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table id="recentlyPackedTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
              <thead>
                  <tr>
                    <th>Date Closed</th>
                    <th>Operator</th>
                    <th>From Slot</th>
                    <th>From Assignment Reference</th>
                    <th>Lines</th>
                    <th>Pieces</th>
                    <th>ASN</th>
                    <th>Customer Reference</th>
                    <th>Carrier ID</th>
                  </tr>
              </thead>
            </table>
            </div>
        </div>
    </div>
</div>
