<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Search Cubiscan Record: </h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="cubiscanFinder">
                        <input type="text" name="searchQuery" id="searchQuery" required autofocus>
                        <button type="submit" class="<?php echo BUTTON_BLUE; ?>" id="formSubmit">Find Cubiscan Record</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Cubiscan Details</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="row clearfix">
                <form id="cubiscanFinderEdit">
                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 dataInput">
                        <h2>Barcode</h2>
                        <input type="text" id="SerialBarcode" value="" required></input>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 dataInput">
                        <h2>Length</h2>
                        <input type="number" id="SerialLength" step="0.1" value="0"></input>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 dataInput">
                        <h2>Height</h2>
                        <input type="number" id="SerialHeight" step="0.1" value="0"></input>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 dataInput">
                        <h2>Width</h2>
                        <input type="number" id="SerialWidth" step="0.1" value="0"></input>
                    </div>
                    <div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 dataInput">
                        <h2>Weight</h2>
                        <input type="number" id="SerialWeight" step="0.01" value="0"></input>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <button type="submit" class="<?php echo BUTTON_BLUE; ?>" id="createCubiscanRecord" value="create" onclick="createRecord()">Create</button>
                        <button type="submit" class="<?php echo BUTTON_BLUE; ?>" id="editCubiscanRecord" value="Update" onclick="updateRecord()">Update</button>
                        <button type="reset" class="<?php echo BUTTON_LINK; ?>"  id="resetForm" value="Reset" onclick="resetBothForms()">Reset</button>
                        <button type="submit" class="<?php echo BUTTON_RED; ?>" id="deleteCubiscanRecord" value="Delete" onclick="deleteRecord()">Delete</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="apiResponseBlock">
    <div class="card">
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div id="apiResponse"></div>
                </div>
            <div>
        </div>
    </div>
</div>