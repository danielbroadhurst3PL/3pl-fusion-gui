<form id="returnsForm">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><?php echo TITLE_FILTERS; ?></h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix">
                    <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_FROM; ?>">
                        <label for="filterStatus">Date Returned From</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="3" type="text" id="<?php echo DATE_SHIPPED_FROM; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedFromValidate"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_TO; ?>">
                        <label for="filterStatus">Date Returned To</label>
                        <span class="required-label">*</span>
                        <div class="form-group">
                            <div class="form-line">
                                <input tabindex="4" type="text" id="<?php echo DATE_SHIPPED_TO; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                <div id="divFilterShippedToValidate"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" id="button-div-returns">
                        <button form="returnsForm" id="buttonReturnsForm" type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                    </div>
                </div>
                <div class="col-lg-12" id="divFilterShippedToValidateFuture"></div>
                <div class="col-lg-12" id="divFilterOrderNumberNotFound"></div>
                </div>
            </div>
        </div>
    </div>
</form>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2 id="returnsTitle"><?php echo TITLE_RETURNS; ?> From Current Month</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_RETURNS_VIEW; ?>">
                    <thead>
                        <tr>
                            <th><?php echo TITLE_ORDER_NUMBER; ?></th>
                            <th><?php echo TITLE_SUPPLIER_REF; ?></th>
                            <th><?php echo TITLE_SKU; ?></th>
                            <th><?php echo TITLE_RECEIVED; ?></th>
                            <th><?php echo TITLE_STATUS; ?></th>
                            <th><?php echo TITLE_RETURN_REASON; ?></th>
                            <th><?php echo TITLE_DATE_CLOSED; ?></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>