<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2><?php echo TITLE_CUSTOM_REPORTS; ?></h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">    
                <table class="table table-bordered table-striped table-hover dt-responsive dataTable" id="<?php echo TABLE_FILES_VIEW_DOWNLOAD; ?>">
                    <thead>
                    <tr>
                        <th><?php echo TITLE_INVOICES; ?></th>
                        <th><?php echo TITLE_DATE; ?></th>
                        <th class="fileLinks"><?php echo TITLE_VIEW; ?></th>
                        <th class="fileLinks"><?php echo TITLE_DOWNLOAD; ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
</div>