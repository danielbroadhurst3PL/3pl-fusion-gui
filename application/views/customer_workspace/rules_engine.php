<div class="col-sm-12 col-md-12 col-sm-12 col-xs-12 rules-padding">
  <div class="card">
    <div class="header">
      <h2>
        Rules Engine
        <small>Please ensure that the correct mappings have been created in the <a href="<?php echo base_url(); ?>customer_workspace/service_mapping_tool" target="_blank">Service Mapping Tool.</a></small>
      </h2>
    </div>
    <div class="body">
      <h2 class="card-inside-title">Switch Rules Engines</h2>
      <div class="demo-switch">
        <div class="switch">
          <label>INTERNATIONAL<input type="checkbox" id="destinationSwtich" name="destinationSwtich"><span class="lever"></span>UK</label>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- International -->
<div id="international">
  <!-- Economy -->
  <div class="rulesEngineContainer">
    <div class="col-sm-12">
      <div class="col-sm-2">
        <button class="btn btn-sm waves-effect bg-red btn-block ruleButton" type="button" id="oversize-int">Oversize</button>
      </div>
      <div class="col-sm-10 removePadding">
        <div class="col-sm-3 column-box">
          <div class="col-sm-10 col-sm-offset-2">
          </div>
        </div>
        <div class="col-sm-9 removePadding">
          <div class="col-sm-12 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-4">
              </div>
              <div class="column-box col-sm-8 tablet">
                <div class="col-sm-12 button-group">
                  <button class="btn btn-sm bg-black ruleButton small" id="oversize-s-int">===</button>
                  <button class="btn btn-sm waves-effect bg-blue btn-block" onclick="setService(this.id)" type="button" id="oversize-service-int">Oversize Service</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Economy -->
    <div class="rulesEngineContainer">
      <div class="col-sm-12 rulesCard">
        <div class="col-sm-2">
          <button class="btn btn-sm waves-effect bg-grey btn-block ruleButton" type="button" id="economy-int">Economy</button>
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton tablet" type="button" id="economy-dangerous-int">DNG Goods</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-dangerous-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-dng-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" onclick="setService(this.id)" type="button" id="economy-dangerous-serviceAbove-int">Economy Dangerous Goods Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-dng-sB-int"><</button>
                    <button class="btn btn-sm waves-effect bg-blue btn-block" onclick="setService(this.id)" type="button" id="economy-dangerous-serviceBelow-int">Economy Dangerous Goods Below</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="economy-packet-int">Packet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-packet-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pack-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="economy-packet-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pack-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="economy-packet-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="economy-parcel-int">Parcel</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-parcel-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-parc-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="economy-parcel-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-parc-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="economy-parcel-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="economy-pallet-int">Pallet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-pallet-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pal-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="economy-pallet-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pal-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="economy-pallet-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Standard -->
      <div class="col-sm-12 rulesEngine rulesCard">
        <div class="col-sm-2">
          <button class="btn btn-sm waves-effect bg-green btn-block ruleButton" type="button" id="standard-int">Standard</button>
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton tablet" type="button" id="standard-dangerous-int">DNG Goods</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-dangerous-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-dng-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-dangerous-serviceAbove-int">Standard Dangerous Goods Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-dng-sB-int"><</button>
                    <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-dangerous-serviceBelow-int">Standard Dangerous Goods Below</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="standard-packet-int">Packet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-packet-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pack-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-packet-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pack-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-packet-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="standard-parcel-int">Parcel</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-parcel-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-parc-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-parcel-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-parc-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-parcel-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="standard-pallet-int">Pallet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-pallet-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pal-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-pallet-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pal-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-pallet-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Express -->
      <div class="col-sm-12 rulesEngine rulesCard">
        <div class="col-sm-2">
          <button class="btn btn-sm waves-effect bg-deep-purple btn-block ruleButton" type="button" id="express-int">Express</button>
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton tablet" type="button" id="express-dangerous-int">DNG Goods</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-dangerous-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-dng-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-dangerous-serviceAbove-int">Express Dangerous Goods Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-dng-sB-int"><</button>
                    <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-dangerous-serviceBelow-int">Express Dangerous Goods Below</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="express-packet-int">Packet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-packet-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pack-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-packet-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pack-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-packet-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="express-parcel-int">Parcel</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-parcel-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-parc-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-parcel-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-parc-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-parcel-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="express-pallet-int">Pallet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-pallet-order-value-int">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pal-sA-int">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-pallet-serviceAbove-int">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pal-sB-int">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-pallet-serviceBelow-int">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- UK -->
<div id="uk">
  <!-- Economy -->
  <div class="rulesEngineContainer">
    <div class="col-sm-12">
      <div class="col-sm-2">
        <button class="btn btn-sm waves-effect bg-red btn-block ruleButton" type="button" id="oversize-uk">Oversize</button>
      </div>
      <div class="col-sm-10 removePadding">
        <div class="col-sm-3 column-box">
          <div class="col-sm-10 col-sm-offset-2">
          </div>
        </div>
        <div class="col-sm-9 removePadding">
          <div class="col-sm-12 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-4">
              </div>
              <div class="column-box col-sm-8 tablet">
                <div class="col-sm-12 button-group">
                  <button class="btn btn-sm bg-black ruleButton small" id="oversize-s-uk">===</button>
                  <button class="btn btn-sm waves-effect bg-blue btn-block" onclick="setService(this.id)" type="button" id="oversize-service-uk">Oversize Service</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Economy -->
    <div class="rulesEngineContainer">
      <div class="col-sm-12 rulesCard">
        <div class="col-sm-2">
          <button class="btn btn-sm waves-effect bg-grey btn-block ruleButton" type="button" id="economy-uk">Economy</button>
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton tablet" type="button" id="economy-sm-letter-uk">Large Letter</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-letter-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-let-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" onclick="setService(this.id)" type="button" id="economy-letter-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-let-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" onclick="setService(this.id)" type="button" id="economy-letter-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="economy-packet-uk">Packet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-packet-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pack-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="economy-packet-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pack-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="economy-packet-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="economy-parcel-uk">Parcel</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-parcel-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-parc-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="economy-parcel-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-parc-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="economy-parcel-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="economy-pallet-uk">Pallet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="economy-pallet-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pal-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="economy-pallet-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="economy-pal-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="economy-pallet-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Standard -->
      <div class="col-sm-12 rulesEngine rulesCard">
        <div class="col-sm-2">
          <button class="btn btn-sm waves-effect bg-green btn-block ruleButton" type="button" id="standard-uk">Standard</button>
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton tablet" type="button" id="standard-sm-letter-uk">Large Letter</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-letter-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-let-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-letter-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-let-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-letter-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="standard-packet-uk">Packet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-packet-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pack-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-packet-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pack-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-packet-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="standard-parcel-uk">Parcel</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-parcel-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-parc-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-parcel-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-parc-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-parcel-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="standard-pallet-uk">Pallet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="standard-pallet-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pal-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="standard-pallet-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="std-pal-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="standard-pallet-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Express -->
      <div class="col-sm-12 rulesEngine rulesCard">
        <div class="col-sm-2">
          <button class="btn btn-sm waves-effect bg-deep-purple btn-block ruleButton" type="button" id="express-uk">Express</button>
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton tablet" type="button" id="express-sm-letter-uk">Large Letter</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-letter-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-let-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-letter-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-let-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-letter-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="express-packet-uk">Packet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-packet-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pack-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-packet-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pack-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-packet-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="express-parcel-uk">Parcel</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-parcel-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-parc-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-parcel-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-parc-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-parcel-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-10 removePadding">
          <div class="col-sm-3 column-box">
            <div class="col-sm-10 col-sm-offset-2">
              <button class="btn btn-sm btn-block waves-effect bg-blue-grey ruleButton" type="button" id="express-pallet-uk">Pallet</button>
            </div>
          </div>
          <div class="col-sm-9 removePadding">
            <div class="col-sm-12 removePadding">
              <div class="col-sm-12 removePadding">
                <div class="col-sm-4">
                  <button class="btn btn-sm waves-effect bg-amber" type="button" onclick="setOrderValue(this.id)" id="express-pallet-order-value-uk">Order Value</button>
                </div>
                <div class="column-box col-sm-8 tablet">
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pal-sA-uk">>=</button>
                    <button class="btn btn-sm waves-effect bg-teal btn-block" type="button" onclick="setService(this.id)" id="express-pallet-serviceAbove-uk">Service Equal or Above</button>
                  </div>
                  <div class="col-sm-12 button-group">
                    <button class="btn btn-sm bg-black ruleButton small" id="exp-pal-sB-uk">
                      <</button> <button class="btn btn-sm waves-effect bg-blue btn-block" type="button" onclick="setService(this.id)" id="express-pallet-serviceBelow-uk">Service Below
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Order Value Modal -->
<div class="modal fade in" id="orderValueModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel">Enter Order Value</h4>
      </div>
      <div class="modal-body" id="modalMessages">
        <div class="col-sm-12" style="display: none;">
          <div class="form-group form-float">
            <div class="form-line">
              <input type="text" class="form-control" id="editID" hidden>
            </div>
          </div>
        </div>
        <div class="input-group">
          <span class="input-group-addon">
            £
          </span>
          <div class="form-line">
            <input type="number" class="form-control money-euro" id="orderValue" placeholder="Ex: £34.99">
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="storeOrderValue()" class="btn btn-link waves-effect">SAVE CHANGES</button>
        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>

<!-- Service Selection Modal -->

<div class="modal fade in" id="selectServiceModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel">Choose Service</h4>
      </div>
      <div class="modal-body" id="modalMessages">
        <div class="col-sm-12" style="display: none;">
          <div class="form-group form-float">
            <div class="form-line">
              <input type="text" class="form-control" id="editServiceID" hidden>
            </div>
          </div>
        </div>
        <div class="form-group">
          <label for="service">Choose Service</label>
          <div class="form-line">
            <select tabindex="2" id="service" class="form-control show-tick"></select>
          </div>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" onclick="storeService()" class="btn btn-link waves-effect">SAVE CHANGES</button>
        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>