<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Create New Delivery Note</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="deliveryNoteCreate">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <div class="form-line">
                            <label for="name">New Delivery Note Name:</label>
                              <input type="text" name="name" id="name" class="form-control" required></input>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="form-group">
                          <label for="id">Choose Template Delivery Note:</label>
                          <select id="id" name="id" class="form-control" required></select>
                        </div>
                      </div>
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <button type="submit" class="btn btn-primary waves-effect" id="deliveryNoteForm">Create Delivery Note</button>
                      </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_DELIVERY_NOTE; ?>s</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="deliveryNoteTable">
                    <thead>
                    <tr>
                        <th>Filename</th>
                        <th>Type</th>
                        <th>Class</th>
                        <th>Edit</th>                    
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>