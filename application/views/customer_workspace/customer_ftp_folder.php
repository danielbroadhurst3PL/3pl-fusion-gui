<div class="row clearfix">
    <form id="<?php echo $formId; ?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo TITLE_FTP_FOLDERS; ?></h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                                <i class="material-icons info">info</i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                        <div class="row clearfix">

                            <div class="col-md-4" id="<?php echo DIV_FILTER_TYPE; ?>">
                                <label for="filterType">Folders</label>
                                <span class="required-label">*</span>
                                    <div class="form-line">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons" id="type">
                                        <!-- <select tabindex="1" id="<?php echo TYPE; ?>" class="form-control show-tick" required> -->
                                            <!-- <option value="" disabled selected>-- Please select --</option> -->
                                            <?php
                                                foreach ($orderFilters[TYPES] as $orderType)
                                                {
                                            ?>
                                              <label class="btn btn-secondary">
                                                <input type="radio" name="options" id="<?php echo $orderType[VALUE] ?>" autocomplete="off" class="switch-input"> <?php echo $orderType[TITLE] ?>
                                              </label>

                                                    <!-- <button value="<?php echo $orderType[VALUE] ?>"><?php echo $orderType[TITLE] ?></button> -->
                                            <?php
                                                }
                                            ?>
                                        <!-- </select> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">

                        <div class="col-md-4" id="button-div">
                            <button form="formOrderView" id="buttonOrderView" type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="viewGrn">
            <?php $this->load->view(PAGE_CUSTOMER_FTP_TABLE); ?>
        </div>
    </form>
</div>
<!-- #END# Vertical Layout -->