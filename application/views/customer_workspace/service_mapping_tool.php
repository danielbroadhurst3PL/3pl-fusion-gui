<div class="container-fluid">
  <div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header">
            <h2>Current Client Service Mappings</h2>
            <ul class="header-dropdown m-r--5">
              <li>
                <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')" id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                  <i class="material-icons info">info</i>
                </a>
              </li>
            </ul>
          </div>
          <div class="body">
            <div class="table-responsive">
              <table class="table table-bordered table-striped table-hover dt-responsive dataTable" id="tableClientServiceMappings">
                <thead>
                  <tr>
                    <th>Client Service</th>
                    <th>Priority</th>
                    <th>Region</th>
                    <th>Applies To</th>
                    <th>Delivery Note</th>
                    <th>Service Type</th>
                    <th>General Service</th>
                    <th>Service Name</th>
                    <th>Actions</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in" id="smallModalMessages" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel">Enter Priority for Mapping</h4>
      </div>
      <div class="modal-body" id="modalMessages">
        <div class="col-sm-12" style="display: none;">
          <div class="form-group form-float">
            <div class="form-line">
              <input type="number" min="1" class="form-control" id="editID" hidden>
            </div>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="form-group form-float">
            <div class="form-line">
              <input type="number" min="1" class="form-control" id="priority" value="1">
              <label class="form-label">Priority</label>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" onclick="savePriority()" class="btn btn-link waves-effect">SAVE CHANGES</button>
        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>