<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Serial Number Record</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="serialNumberQuery">
                        <label>Search by SKU or Order Number: <input type="text" name="serialNumberSearch" id="serialNumberSearch"></label>
                        <button type="submit" class="btn btn-primary waves-effect" id="formSubmit">Search for Serial Numbers</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="serialNumberRecord">
    <div class="card">
        <div class="header">
            <h2 id="serialNumberRecordTitle"></h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
                <table id="serialNumberRecordTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
                    <thead>
                        <tr>
                            <th>SKU</th>
                            <th>Serial Number</th>
                            <th>Shipment ID</th>
                            <th>Shipment StU</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>