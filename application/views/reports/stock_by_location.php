<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="stockByLocation">
    <div class="card">
        <div class="header">
            <h2>Stock by Location</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table class="table table-bordered table-striped table-hover dt-responsive dataTable" id="stockByLocationTable">
                <thead>
                    <tr>
                    <th>Stock Id</th>
                    <th>Biz Id</th>
                    <th>Owner Id</th>
                    <th>SKUId</th>
                    <th>State</th>
                    <th>Site</th>
                    <th>Facility</th>
                    <th>Zone</th>
                    <th>Bay</th>
                    <th>Section</th>
                    <th>Slot</th>
                    <th>StU</th>
                    <th>StT</th>
                    <th>Qty</th>
                    <th>Unit Of Measure</th>
                    <th>BoxQty</th>
                    <th>Status</th>
                    <th>Date Production</th>
                    <th>Date Receipt</th>
                    <th>Date Release</th>
                    <th>Date Expiry</th>
                    <th>Country</th>
                    <th>Batch Id</th>
                    <th>Assign Type</th>
                    <th>Assign Ref</th>
                    <th>Consignment</th>
                    <th>Load Id</th>
                    <th>Pair Ind</th>
                    <th>Pair StU</th>
                    <th>Origin Stock Id</th>
                    <th>Date Created</th>
                    <th>Date Held</th>
                    <th>Date Last Moved</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
