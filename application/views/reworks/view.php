<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

    <div class="card">
      <div class="header">
        <h2 id="titleProducts">
          Current Reworks
        </h2>
      </div>
      <div class="body">
        <div class="table-responsive">
          <table class="table table-bordered table-striped table-hover dataTable" id="currentReworks">
            <thead>
              <tr>
                <th>Your Reference:</th>
                <th>Rework ID</th>
                <th>Requested Completion By Date</th>
                <th>Proposed Completion Date</th>
                <th>Rework Type</th>
                <th>Extra Info</th>
                <th>Progress</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="largeModal" tabindex="-1" role="dialog" style="display: none;">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="largeModalLabel">Time Study:</h4>
      </div>
      <div class="modal-body" id="reworkModalBody">
        
      </div>
      <div class="modal-footer">
        <button type="button" id="reworkModalConfirmButton" class="btn btn-primary waves-effect">APPROVE TIME STUDY</button>
        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade in" id="largeModalMessages" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Messages</h4>
            </div>
            <div class="modal-body" id="modalMessages">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>