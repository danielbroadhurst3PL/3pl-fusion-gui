<!DOCTYPE html>
<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155109154-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-155109154-1');
    </script>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo SYSTEM_NAME; ?> - <?php echo $pageTitle; ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('cropped-favicon-1-32x32.png'); ?>" <?php echo TYPE_IMAGE_XICON ?> />

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAP_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(THEME_NODEWAVES_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Animation Css -->
    <link href="<?php echo base_url(THEME_ANIMATECSS_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Sweet Alert Css -->
    <link href="<?php echo base_url(THEME_SWEETALERT_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Spinner Css -->
    <link href="<?php echo base_url(THEME_JQUERYSPINNER_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAPMATERIALDATETIMEPICKER_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Wait Me Css -->
    <link href="<?php echo base_url(THEME_WAITME_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAPSELECT_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url(THEME_JQUERYDATATABLE_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Dropzone Css -->
    <link href="<?php echo base_url(THEME_DROPZONE_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Custom Css -->
    <link href="<?php echo base_url(THEME_CUSTOM_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Custom DB Css -->
    <link href="<?php echo base_url(THEME_DB_CUSTOM_CSS); ?>?<?php echo FUSION_VERSION; ?>" <?php echo REL_STYLESHEET ?> />
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">-->
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url(THEME_ALLTHEMES_CSS); ?>" <?php echo REL_STYLESHEET ?> />
</head>
<body class="login-page">
  <div class="login-box">
      <div class="logo">
          <img src="<?php echo base_url(IMAGE_LOGO); ?>">
      </div>
      <div class="card">
          <div class="body">
						<h1><?php echo lang('reset_password_heading');?></h1>

						<div id="infoMessage"><?php echo $message;?></div>

						<?php echo form_open('auth/reset_password/' . $code);?>

							<p>
								<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label> <br />
								<?php echo form_input($new_password);?>
							</p>

							<p>
								<?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?> <br />
								<?php echo form_input($new_password_confirm);?>
							</p>

							<?php echo form_input($user_id);?>
							<?php echo form_hidden($csrf); ?>

							<p><?php echo form_submit('submit', lang('reset_password_submit_btn'));?></p>

						<?php echo form_close();?>
					</div>
			</div>
	</div>
</body>
</html>