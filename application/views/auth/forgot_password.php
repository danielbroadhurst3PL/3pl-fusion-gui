<!DOCTYPE html>
<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155109154-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-155109154-1');
    </script>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo SYSTEM_NAME; ?> - <?php echo $pageTitle; ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('cropped-favicon-1-32x32.png'); ?>" <?php echo TYPE_IMAGE_XICON ?> />

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAP_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(THEME_NODEWAVES_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Animation Css -->
    <link href="<?php echo base_url(THEME_ANIMATECSS_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Sweet Alert Css -->
    <link href="<?php echo base_url(THEME_SWEETALERT_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Spinner Css -->
    <link href="<?php echo base_url(THEME_JQUERYSPINNER_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAPMATERIALDATETIMEPICKER_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Wait Me Css -->
    <link href="<?php echo base_url(THEME_WAITME_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAPSELECT_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url(THEME_JQUERYDATATABLE_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Dropzone Css -->
    <link href="<?php echo base_url(THEME_DROPZONE_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Custom Css -->
    <link href="<?php echo base_url(THEME_CUSTOM_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Custom DB Css -->
    <link href="<?php echo base_url(THEME_DB_CUSTOM_CSS); ?>?<?php echo FUSION_VERSION; ?>" <?php echo REL_STYLESHEET ?> />
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">-->
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url(THEME_ALLTHEMES_CSS); ?>" <?php echo REL_STYLESHEET ?> />
</head>

<body class="login-page">
  <div class="login-box">
      <div class="logo">
          <img src="<?php echo base_url(IMAGE_LOGO); ?>">
      </div>
      <div class="card">
          <div class="body">
          <?php echo form_open("auth/forgot_password");?>
              <br>
              <div>
                <h4>Enter Email to Reset Password</h4>
                <?php echo form_input($email);?>
              </div>
              <div>
                <?php echo form_submit('submit', lang('forgot_password_submit_btn'), "class='btn btn-default submit'");?>
                <a class="reset_pass" href="../login">Login</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />


              </div>
            <?php echo form_close();?>
          </div>
      </div>
  </div>
</body>