    <div class="five-zero-zero-container">
        <div class="error-code"><?php echo $fErrorCode; ?></div>
        <div class="error-message"><?php echo $fErrorMessage; ?></div>
        <div class="button-place">
            <a href="<?php echo base_url();?>" class="btn btn-default btn-lg waves-effect">LOGIN</a>
        </div>
    </div>