<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="grnProducts">
    <div class="card">
        <div class="header">
            <h2 id="grnItemsTitle">
                <?php echo TITLE_GRN_ITEMS; ?>
            </h2>
            <?php if ($currentPage === PAGE_MAIN_INDEX): ?>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="closeTable('orderTableHome')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            <?php endif; ?>
        </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_GRNS_VIEW_ITEMS; ?>">
                    <thead>
                    <tr>
                        <th><?php echo TITLE_LINE_ID; ?></th>
                        <th><?php echo TITLE_SKU; ?></th>
                        <th><?php echo TITLE_DUE_IN; ?></th>
                        <th><?php echo TITLE_RECEIVED; ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>