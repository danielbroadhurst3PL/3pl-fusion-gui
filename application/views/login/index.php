<div class="login-box">
    <div class="logo">
        <img src="<?php echo base_url(IMAGE_LOGO); ?>">
    </div>
    <div class="card">
        <div class="body">
            <form id="formLogin" method="POST">
            <div class="msg"></div>
            <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                <div class="form-line">
                    <input type="text" class="form-control" name="user"
                           id="user" placeholder="Email" required
                           autofocus  autocomplete="username">
                </div>
            </div>
            <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                <div class="form-line">
                    <input type="password" class="form-control" name="password"
                           id="password" placeholder="Password" required autocomplete="current-password">
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <center>
                    <div class="demo-preloader <?php echo LOADER_CLASS; ?>" style="display: none"
                         id="loaderLogin">
                        <div class="preloader">
                            <div class="spinner-layer pl-red">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                        <button class="<?php echo BUTTON_PINK . ' ' . BUTTON_TOGGLEABLE; ?>"
                                id="buttonLogin" type="submit">SIGN IN
                        </button>
                        <div><a href="/auth/forgot_password" class="<?php echo BUTTON_BLUE . ' ' . BUTTON_TOGGLEABLE; ?>"
                                id="buttonForgotten" type="submit">Forgotten Password
                            </a></div>
                    </center>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<?php if($message): ?>
<div class="alert-success"><p><?php echo $message ?></p></div>
<?php endif; ?>