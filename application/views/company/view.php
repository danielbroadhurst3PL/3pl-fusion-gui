<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        <div class="card">
            <div class="header">
                <h2><?php echo TITLE_COMPANIES; ?></h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_COMPANY_VIEW; ?>">
                        <thead>
                        <tr>
                            <th><?php echo TITLE_COMPANY; ?></th>
                            <th><?php echo TITLE_COMPANY_CODE; ?></th>
                            <th><?php echo TITLE_EMAIL; ?></th>
                            <th><?php echo TITLE_ADDRESS_LINE_1; ?></th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
