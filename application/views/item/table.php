    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    <?php echo TITLE_ITEMS; ?>
                </h2>
            </div>
                <div class="body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_ITEMS_VIEW; ?>">
                            <thead>
                            <tr>
                                <th><?php echo TITLE_SKU; ?></th>
                                <th><?php echo TITLE_QUANTITY; ?></th>
                                <?php if($value === true){ ?>
                                <th><?php echo TITLE_VALUE; ?></th>
                                <?php } ?>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>