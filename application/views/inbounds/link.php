<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="viewInboundBookings">
    <div class="card">
        <div class="header">
            <h2>Unknown Inbound Bookings</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table id="unknownInboundTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
              <thead>
                  <tr>
                    <th>PO Number</th>
                    <th>Carrier Name</th>
                    <th>Number of Cartons</th>
                    <th>Number of Pallets</th>
                    <th>Inbound Service</th>
                    <th>Container Type</th>
                    <th>Current Status</th>
                    <th>Actions</th>
                  </tr>
              </thead>
            </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="largeLinkGRN" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel">Open GRNs</h4>
            </div>
            <div class="modal-body" id="modalMessages">
                <select id='openGRNS' name='openGRNS' class='form-control'></select>
                <div id="messageBody"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
                <button type="button" id="linkGrn" disabled class="btn btn-primary waves-effect">LINK GRN to INBOUND</button>
            </div>
        </div>
    </div>
</div>