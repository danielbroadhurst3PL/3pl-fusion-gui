<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="viewInboundBookings">
    <div class="card">
        <div class="header">
            <h2>Inbound Bookings</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle . 'Table'); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive">    
            <table id="inboundBookingsTable" class="table table-bordered table-striped table-hover dt-responsive dataTable">
              <thead>
                  <tr>
                    <th>PO Number</th>
                    <th>Carrier Name</th>
                    <th>Number of Cartons</th>
                    <th>Number of Pallets</th>
                    <th>Inbound Service</th>
                    <th>Container Type</th>
                    <th>Requested Date</th>
                    <th>Time Slot</th>
                    <th>Additional Costs</th>
                    <th>Current Status</th>
                    <th>Actions</th>
                  </tr>
              </thead>
            </table>
            </div>
        </div>
    </div>
</div>
