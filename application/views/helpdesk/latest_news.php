<div class="block-header padding-news">
  <h2>Latest 3PL News</h2>
</div>
<div id="newsDiv"></div>
<nav>
  <ul class="pager">
    <li id="prev" onclick="loadPrev()"><a href="javascript:void(0);" class="waves-effect">Previous</a></li>
    <li id="next" onclick="loadNext()"><a href="javascript:void(0);" class="waves-effect">Next</a></li>
  </ul>
</nav>