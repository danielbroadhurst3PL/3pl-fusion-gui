<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="openTicketsDiv">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_TICKETS_VIEW; ?> - Open</h2>
        </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_VIEW_TICKETS; ?>Open">
                        <thead>
                            <tr>
                                <th><?php echo TITLE_TICKET_NUMBER; ?></th>
                                <th><?php echo TITLE_STATUS; ?></th>
                                <th><?php echo TITLE_PRIORITY; ?></th>
                                <th><?php echo TITLE_SUBJECT; ?></th>
                                <th><?php echo TITLE_DATE_CREATED; ?></th>
                                <th><?php echo TITLE_LAST_UPDATED; ?></th>
                            </tr>
                        </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="closedTicketsDiv">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_TICKETS_VIEW; ?> - Closed</h2>
        </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_VIEW_TICKETS; ?>Closed">
                        <thead>
                            <tr>
                                <th><?php echo TITLE_TICKET_NUMBER; ?></th>
                                <th><?php echo TITLE_STATUS; ?></th>
                                <th><?php echo TITLE_PRIORITY; ?></th>
                                <th><?php echo TITLE_SUBJECT; ?></th>
                                <th><?php echo TITLE_DATE_CREATED; ?></th>
                                <th><?php echo TITLE_LAST_UPDATED; ?></th>
                            </tr>
                        </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>