<div class="row clearfix">
    <form id="<?php echo $formId; ?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo TITLE_FILTERS; ?></h2>
                </div>
                <div class="body">
                        <div class="row clearfix">

                            <div class="col-md-4" id="<?php echo DIV_FILTER_TYPE; ?>">
                                <label for="filterType"><?php echo TITLE_TYPE; ?></label>
                                <span class="required-label">*</span>
                                    <div class="form-line">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons" id="type">
                                            <?php
                                                foreach ($orderFilters[TYPES] as $orderType)
                                                {
                                            ?>
                                              <label class="btn btn-secondary">
                                                <input type="radio" name="options" id="<?php echo $orderType[VALUE] ?>" autocomplete="off" class="switch-input"> <?php echo $orderType[TITLE] ?>
                                              </label>

                                            <?php
                                                }
                                            ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row clearfix">
                        <div class="col-md-4" id="button-div">
                            <button form="formOrderView" id="buttonOrderView" type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="viewTickets">
            <?php $this->load->view(PAGE_VIEW_TICKETS_TABLE); ?>
        </div>
    </form>
</div>

<div class="modal fade in" id="largeModalMessages" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="largeModalLabel"></h4>
            </div>
            <div class="modal-body" id="modalMessages">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
            </div>
        </div>
    </div>
</div>
<!-- #END# Vertical Layout -->