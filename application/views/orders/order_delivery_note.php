<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Order Delivery Note</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="deliveryNoteOrderNumber">
                        <label for="searchQuery">Enter Order Number:</label>
                        <input type="text" name="searchQuery" id="searchQuery" required autofocus>
                        <button type="submit" class="<?php echo BUTTON_BLUE; ?>" id="formSubmit">Search Order Number</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="deliveryNoteDiv">
    <div class="card">
        <div class="header">
            <h2 id="packingListTitle">Delivery Note</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive" id="orderDeliveryNote">
                <table id="orderDeliveryNoteTable" class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>Shipment ID</th>
                            <th>Customer Reference</th>
                            <th>ASN</th>
                            <th>Order Class</th>
                            <th>Delivery Note</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>