<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>
                Shipped <?php echo TITLE_ORDERS; ?>
            </h2>
            <?php if ($currentPage === PAGE_MAIN_INDEX): ?>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="closeTable('shippedOrderHome')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                </ul>
            <?php endif; ?>
        </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_ORDERS_SHIPPED_VIEW; ?>">
                    <thead>
                    <tr>
                        <th><?php echo TITLE_ORDER_NUMBER; ?></th>
                        <th><?php echo TITLE_NAME; ?></th>
                        <th><?php echo TITLE_ADDRESS; ?></th>
                        <th><?php echo TITLE_REFERENCE; ?></th>
                        <th>ASN</th>
                        <th><?php echo TITLE_CARRIER; ?></th>
                        <th><?php echo TITLE_STATUS; ?></th>
                        <th>Dispatch By</th>
                        <th><?php echo TITLE_DATE_SHIPPED; ?></th>
                        <th><?php echo TITLE_TRACKING; ?></th>
                        <th>Delivery Note</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>