<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Linnworks Import Errors</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive" id="linnworksImportError">
                <table id="linnworksImportErrorTable" class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th><?php echo TITLE_REFERENCE; ?></th>
                            <th><?php echo TITLE_ERROR; ?></th>
                            <th><?php echo TITLE_DATE; ?></th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
