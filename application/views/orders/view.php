<div class="row clearfix">
    <form id="<?php echo $formId; ?>">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2><?php echo TITLE_FILTERS; ?></h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                                <i class="material-icons info">info</i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                        <div class="row clearfix">

                            <div class="col-md-3" id="<?php echo DIV_FILTER_TYPE; ?>">
                                <label for="filterType"><?php echo TITLE_TYPE; ?></label>
                                <span class="required-label">*</span>
                                    <div class="form-line">
                                        <div class="btn-group btn-group-toggle" data-toggle="buttons" id="type">
                                        <!-- <select tabindex="1" id="<?php echo TYPE; ?>" class="form-control show-tick" required> -->
                                            <!-- <option value="" disabled selected>-- Please select --</option> -->
                                            <?php
                                                foreach ($orderFilters[TYPES] as $orderType)
                                                {
                                            ?>
                                              <label class="btn btn-secondary">
                                                <input type="radio" name="options" id="<?php echo $orderType[VALUE] ?>" autocomplete="off" class="switch-input"> <?php echo $orderType[TITLE] ?>
                                              </label>

                                                    <!-- <button value="<?php echo $orderType[VALUE] ?>"><?php echo $orderType[TITLE] ?></button> -->
                                            <?php
                                                }
                                            ?>
                                        <!-- </select> -->
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-9" id="<?php echo DIV_FILTER_STATUS; ?>">
                                <label for="filterStatus"><?php echo TITLE_STATUS; ?></label>
                                <span class="required-label">*</span>
                                <div class="form-group">
                                    <!-- <div class="form-line"> -->
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons" id="statusType">

                                        <!-- <select tabindex="2" id="<?php echo STATUS; ?>" class="form-control show-tick"> -->
                                            <!-- <option value="" disabled selected>-- Please select --</option> -->
                                            <?php
                                                foreach ($orderFilters[STATUSES] as $orderStatus)
                                                {
                                            ?>
                                            <label class="btn btn-secondary">
                                              <input type="radio" name="statusOptions" id="<?php echo $orderStatus[VALUE] ?>" autocomplete="off" > <?php echo $orderStatus[TITLE] ?>
                                              </label>

                                                    <!-- <option value="<?php echo $orderStatus[VALUE] ?>"><?php echo $orderStatus[TITLE] ?></option> -->
                                            <?php
                                                }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_FROM; ?>">
                                <label for="filterStatus"><?php echo TITLE_DATE_SHIPPED_FROM; ?></label>
                                <span class="required-label">*</span>
                                 <div class="form-group">
                                        <div class="form-line">
                                        <input tabindex="3" name="dateShippedFrom" type="text" id="<?php echo DATE_SHIPPED_FROM; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                        <div id="divFilterShippedFromValidate"></div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4" id="<?php echo DIV_FILTER_DATE_SHIPPED_TO; ?>">
                                <label for="filterStatus"><?php echo TITLE_DATE_SHIPPED_TO; ?></label>
                                <span class="required-label">*</span>
                                 <div class="form-group">
                                        <div class="form-line">
                                        <input tabindex="4" name="dateShippedTo" type="text" id="<?php echo DATE_SHIPPED_TO; ?>" class="datepicker form-control" placeholder="Please choose a date...">
                                        <div id="divFilterShippedToValidate"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="<?php echo DIV_FILTER_ORDER_NUMBER; ?>">
                                <label for="filterStatus"><?php echo TITLE_ORDER_NUMBER; ?></label>
                                <span class="required-label">*</span>
                                <div class="form-group">
                                    <div class="form-line">
                                        <input tabindex="5" name="orderNumber" type="text" id="<?php echo ORDER_NUMBER; ?>" class="form-control" novalidate>
                                    </div>
                                </div>
                            </div>

                        <div class="col-md-4" id="button-div">
                            <button form="formOrderView" id="buttonOrderView" type="submit" class="btn btn-primary waves-effect">SUBMIT</button>
                        </div>
                        <div class="col-lg-12" id="divFilterShippedToValidateFuture"></div>
                        <div class="col-lg-12" id="divFilterOrderNumberNotFound"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="viewOrder">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2><?php echo TITLE_ORDERS; ?></h2>
                        <ul class="header-dropdown m-r--5">
                            <?php if ($currentPage === PAGE_MAIN_INDEX): ?>
                            <li>
                                <a href="javascript:void(0);" onclick="closeTable('orderTableHome')" data-toggle="cardloading"
                                    data-loading-effect="ios">
                                    <i class="material-icons">close</i>
                                </a>
                            </li>
                            <?php endif; ?>
                            <li>
                                <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle.'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle.'Table'); ?>">
                                    <i class="material-icons info">info</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable" id="tableOrdersView">
                                <thead>
                                    <tr>
                                        <th><?php echo TITLE_ORDER_NUMBER; ?></th>
                                        <th>Stage</th>
                                        <th>Sub <?php echo TITLE_STATUS; ?></th>
                                        <th><?php echo TITLE_NAME; ?></th>
                                        <th><?php echo TITLE_ADDRESS; ?></th>
                                        <th><?php echo TITLE_REFERENCE; ?></th>
                                        <th>ASN</th>
                                        <th id="trackingShipped"><?php echo TITLE_SERVICE; ?></th>
                                        <th id="tracking">Dispatch By</th>
                                        <th>Delivery Note</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- #END# Vertical Layout -->