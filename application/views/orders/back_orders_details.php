<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Back Order Details</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
            <div class="table-responsive" id="linnworksImportError">
                <table id="backOrderDetails" class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>Shipment ID</th>
                            <th>SKU ID</th>
                            <th>Product Title</th>
                            <th>Quantity Ordered</th>
                            <th>Quantity Allocated</th>
                            <th>Quantity Tasked</th>
                            <th>Quantity Short</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
