<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>Create Bulk Orders</h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body">
                <div class="row clearfix bulk-button-group">
                    <div class="col-md-4">
                        <div class="fallback">
                            <input name="file" type="file" id="bulkFiles" />
                        </div>
                        <br>
                        <div id="fileValidate"></div>
                        <br>
                        <button id="bulkOrderUpload" type="button" class="<?php echo BUTTON_BLUE; ?>"><i class="material-icons">cloud_upload</i> Upload Bulk Order File</button>
                        <br>
                    </div>
                    <div class="col-md-4 bulk-button-flex">
                        <div>
                            <button id="templateDownload" type="button" class="btn btn-warning waves-effect"><i class="material-icons">cloud_download</i> Download Bulk Orders Template</button>
                        </div>
                        <div>
                            <button id="issueOrdersDownload" type="button" class="btn btn-danger waves-effect"><i class="material-icons">cloud_download</i> Download Outstanding Issue Orders</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div