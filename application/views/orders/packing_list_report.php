<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Packing List Report</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="packingListOrderNumber">
                        <label for="searchQuery">Enter Order Number:</label>
                        <input type="text" name="searchQuery" id="searchQuery" required autofocus>
                        <button type="submit" class="<?php echo BUTTON_BLUE; ?>" id="formSubmit">Search Order Number</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="packingListDiv">
    <div class="card">
        <div class="header">
            <h2 id="packingListTitle">Packing List</h2>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle); ?>">
                        <i class="material-icons info">info</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body">
        <button type="submit" class="<?php echo BUTTON_BLUE; ?>" id="printPackingList" onclick="create_packing_list_report()">Print Packing List</button>
          <div id="header_packing">
            <div id="orderDetails">
              <div id="shipmentID"></div>
              <div id="customerRef"></div>
              <div id="ASN"></div>
            </div>
            <div id="addressDetails">
              <div id="addressText"></div>
            </div>
          </div>
            <div class="table-responsive" id="packingListTableDiv">
                <table id="boxInfoTable" class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>Box ID</th>
                            <th>Box Weight</th>
                            <th>Box Dims</th>
                            <th>Pieces</th>
                        </tr>
                    </thead>
                </table>
                <table id="packingListTable" class="table table-bordered table-striped table-hover dataTable">
                    <thead>
                        <tr>
                            <th>Box Reference</th>
                            <th>Product</th>
                            <th>Product Title</th>
                            <th>Box Quantity</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>