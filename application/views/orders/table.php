<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2><?php echo TITLE_ORDERS; ?></h2>
                <ul class="header-dropdown m-r--5">
                    <?php if ($currentPage === PAGE_MAIN_INDEX): ?>
                    <li>
                        <a href="javascript:void(0);" onclick="closeTable('orderTableHome')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">close</i>
                        </a>
                    </li>
                    <?php endif; ?>
                    <li>
                        <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle.'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle.'Table'); ?>">
                            <i class="material-icons info">info</i>
                        </a>
                    </li>
                </ul>
        </div>
            <div class="body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover dataTable" id="<?php echo TABLE_ORDERS_VIEW; ?>">
                    <thead>
                        <tr>
                            <th>Stage</th>
                            <th><?php echo TITLE_ORDER_NUMBER; ?></th>
                            <th><?php echo TITLE_NAME; ?></th>
                            <th><?php echo TITLE_ADDRESS; ?></th>
                            <th><?php echo TITLE_REFERENCE; ?></th>
                            <th>ASN</th>
                            <th><?php echo TITLE_SERVICE; ?></th>
                            <th>Dispatch By</th>
                            <th id="status"><?php echo TITLE_STATUS; ?></th>
                            <th>Delivery Note</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>