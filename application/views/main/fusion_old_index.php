<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="graphSelect">
    <div class="btn-group btn-group-toggle" data-toggle="buttons" id="selectpicker">
        <label class="btn btn-secondary active">
            <input type="radio" name="options" id="currentOrders" autocomplete="off" class="switch-input">Current Orders
        </label>
        <label class="btn btn-secondary">
            <input type="radio" name="options" id="ordersShipped" autocomplete="off" class="switch-input">Orders Shipped
        </label>
        <label class="btn btn-secondary">
            <input type="radio" name="options" id="currentStock" autocomplete="off" class="switch-input">Current Stock
        </label>
    </div>
</div>

<div id="orderTableHome">
    <?php $this->load->view(PAGE_ORDER_TABLE); ?>
</div>
<div id="shippedOrderHome">
    <?php $this->load->view(PAGE_ORDER_TABLE_SHIPPED); ?>
</div>

<div id="currentStockLevelsTable">
    <?php $this->load->view(PAGE_STOCK_TABLE); ?>
</div>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="shippingBreakdown">
    <div id="shippingSection">
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Back Orders</p>
                <h3 id="backOrders"></h3>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="ordersClicked('backOrders')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Next Flush</p>
                <h3 id="nextFlush"></h3>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="ordersClicked('nextFlush')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Awaiting Pick</p>
                <h3 id="awaitingPick"></h3>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="ordersClicked('awaitingPick')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Awaiting Pack</p>
                <h3 id="awaitingPack"></h3>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="ordersClicked('awaitingPack')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Awaiting Shipping</p>
                <h3 id="awaitingShip"></h3>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="ordersClicked('awaitingShip')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Shipped</p>
                <h3 id="shipped"></h3>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="ordersClicked('shipped')" data-toggle="cardloading"
                            data-loading-effect="ios">
                            <i class="material-icons">search</i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>

        <!-- Graph Cards -->
        <div class="card" id="currentOrders">
            <div class="header">
                <h2>Today's Current Orders</h2>
                <ul class="header-dropdown m-r--5">
                    <li>
                        <a href="javascript:void(0);" onclick="backOrder()" data-toggle="cardloading"
                            data-loading-effect="timer">
                            <i class="material-icons">loop</i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="body chart-box" id="shipping-data">
                <div class="demo-preloader">
                    <div class="preloader pl-size-xl">
                        <div class="spinner-layer">
                            <div class="circle-clipper left">
                                <div class="circle"></div>
                            </div>
                            <div class="circle-clipper right">
                                <div class="circle"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <canvas id="myChart" width="300" height="175"></canvas>
        </div>

        <div class="card" id="shippingMethods">
            <div class="header">
                <h2>Shipping Methods</h2>
            </div>
            <div class="body chart-box" id="shipping-data">
            </div>
            <canvas id="chart-area" width="300" height="175"></canvas>
        </div>



        <!-- End of Section -->
    </div>
</div>



<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="shippedLastWeek">
    <div id="shippingSection">
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Shipped this Month</p>
                <h3 id="currentMonth"></h3>
            </div>
        </div>
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Shipped last Month</p>
                <h3 id="lastMonth"></h3>
            </div>
        </div>
        <div class="card shippingBreakdown">
            <div class="header">
                <p>Shipped Month before Last</p>
                <h3 id="monthBeforeLast"></h3>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="header">
            <h2>Orders Shipped in Past 7 Days</h2>
            <ul class="header-dropdown m-r--5">

            </ul>
            <ul class="header-dropdown m-r--5">
                <li>
                    <a href="javascript:void(0);" onclick="shippedLastWeek()" data-toggle="cardloading"
                        data-loading-effect="timer">
                        <i class="material-icons">loop</i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="body chart-box" id="shipped-data">
            <div class="demo-preloader">
                <div class="preloader pl-size-xl">
                    <div class="spinner-layer">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <canvas id="myChartShipped" width="300" height="130"></canvas>
    </div>
</div>
<!-- 
<div id="currentStockLevelsTable">
    <?php $this->load->view(PAGE_STOCK_TABLE); ?>
</div> -->