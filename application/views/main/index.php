<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12" id="fusionMainContent">
	<div class="row clearfix">
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\004-application.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Data Received</div>
					<div class="number" id="dataReceived">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(dataReceived)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\022-container.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Awaiting Pick</div>
					<div class="number" id="awaitingPick">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(awaitingPick)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\120-trolley.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Being Picked</div>
					<div class="number" id="beingPicked">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(beingPicked)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\094-box-13.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Awaiting Pack</div>
					<div class="number" id="awaitingPack">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(awaitingPack)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\116-support.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Awaiting Bespoke Process</div>
					<div class="number" id="awaitingBespokeProcess">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(awaitingBespokeProcess)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\122-box-15.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Awaiting Shipping</div>
					<div class="number" id="awaitingShipping">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(awaitingShipping)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\064-logistic-6.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Collection Area</div>
					<div class="number" id="collectionsArea">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(collectionArea)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="info-box">
				<div class="icon">
					<img src="../files\fusion\assets\icons\123-box-16.svg" alt="">
				</div>
				<div class="content">
					<div class="text">Shipped</div>
					<div class="number" id="shippedOrders">0</div>
				</div>
				<div class="moreInfo">
					<ul class="header-dropdown m-r--5">
						<li>
							<a href="javascript:void(0);" onclick="ordersClicked(shipped)" data-toggle="cardloading"
									data-loading-effect="ios">
									<i class="material-icons">search</i>
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="orderTableHome">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    		<div class="card">
        	<div class="header">
          	<h2><?php echo TITLE_ORDERS; ?></h2>
            <ul class="header-dropdown m-r--5">
							<?php if ($currentPage === PAGE_MAIN_INDEX): ?>
								<li>
									<a href="javascript:void(0);" onclick="closeTable('orderTableHome')" data-toggle="cardloading"
											data-loading-effect="ios">
										<i class="material-icons">close</i>
									</a>
								</li>
							<?php endif; ?>
							<li>
								<a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $pageTitle.'Table'); ?>')"  id="<?php echo str_replace(' ', '', $pageTitle.'Table'); ?>">
										<i class="material-icons info">info</i>
								</a>
							</li>
						</ul>
        	</div>
					<div class="body">
						<div class="table-responsive">
							<table class="table table-bordered table-striped table-hover dataTable" id="fusionHomePageTable">
								<thead>
									<tr>
									<tr>
										<th><?php echo TITLE_ORDER_NUMBER; ?></th>
										<th>Stage</th>
										<th>Sub <?php echo TITLE_STATUS; ?></th>
										<th><?php echo TITLE_NAME; ?></th>
										<th><?php echo TITLE_ADDRESS; ?></th>
										<th><?php echo TITLE_REFERENCE; ?></th>
										<th>ASN</th>
										<th id="trackingShipped"><?php echo TITLE_SERVICE; ?></th>
										<th id="tracking">Dispatch By</th>
										<th>Delivery Note</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	

<div class="row clearfix">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="shippedLastWeek">
		<div class="row clearfix">
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box">
					<div class="icon">
						<img src="../files\fusion\assets\icons\007-box-1.svg" alt="">
					</div>
					<div class="content">
						<div class="text">Shipped this Month</div>
						<div class="number" id="currentMonth">0</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box">
					<div class="icon">
						<img src="../files\fusion\assets\icons\020-box-4.svg" alt="">
					</div>
					<div class="content">
						<div class="text">Shipped last Month</div>
						<div class="number" id="lastMonth">0</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
				<div class="info-box">
					<div class="icon">
						<img src="../files\fusion\assets\icons\030-delivery-3.svg" alt="">
					</div>
					<div class="content">
						<div class="text">Shipped Month before Last</div>
						<div class="number" id="monthBeforeLast">0</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
		<div class="card" id="ordersShipped">
				<div class="header">
						<h2>Orders Shipped in Past 7 Days</h2>
						<ul class="header-dropdown m-r--5">

						</ul>
						<ul class="header-dropdown m-r--5">
								<li>
										<a href="javascript:void(0);" onclick="shippedLastWeek()" data-toggle="cardloading"
												data-loading-effect="timer">
												<i class="material-icons">loop</i>
										</a>
								</li>
						</ul>
				</div>
				<div class="body chart-box" id="shipped-data">
						<div class="demo-preloader">
						<div class="preloader pl-size-xl">
						<div class="spinner-layer">
							<div class="circle-clipper left">
								<div class="circle"></div>
							</div>
							<div class="circle-clipper right">
								<div class="circle"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
				<canvas id="myChartShipped" width="300" height="130"></canvas>
		</div>
	</div>
	<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
		<div class="card" id="shippingMethods">
			<div class="header">
					<h2>Shipping Methods</h2>
			</div>
			<div class="body chart-box" id="shipping-data">
			</div>
			<canvas id="chart-area" width="300" height="175"></canvas>
		</div>
	</div>
</div>
</div>
<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
	<div class="row clearfix">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="exceptionsBox">
			<div class="info-box">
				<div class="icon bg-red">
						<i class="material-icons">report_problem</i>
				</div>
				<div class="content">
					<div class="text">Exceptions</div>
					<div class="number" id="exceptions">0</div>
				</div>
			</div>
			<div class="card" id="exceptionsBlank">
				<table class="table table-bordered table-striped table-hover homepageTable" id="exceptionsTable">
					<thead>
						<tr>
							<th>Order</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="exceptionListBody">
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="courierExceptionBox">
			<div class="info-box">
				<div class="icon bg-red">
						<i class="material-icons">error</i>
				</div>
				<div class="content">
					<div class="text">Courier Exception</div>
					<div class="number" id="courierException">0</div>
				</div>
			</div>
			<div class="card">
				<table class="table table-bordered table-striped table-hover homepageTable" id="courierExceptionTable">
					<thead>
						<tr>
							<th>Order</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody id="courierExceptionList">
					</tbody>
				</table>
			</div>
		</div>
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="linnworksExceptionBox">
			<div class="info-box">
				<div class="icon bg-red">
						<img src="<?php echo base_url(); ?>files/fusion/assets/img/linnworks.png" >
				</div>
				<div class="content">
					<div class="text">Linnworks Exception</div>
					<div class="number" id="linnworksException">0</div>
				</div>
			</div>
			<div class="card">
				<table class="table table-bordered table-striped table-hover homepageTable" id="linnworksExceptionTable">
					<thead>
						<tr>
							<th>Order Number</th>
						</tr>
					</thead>
					<tbody id="linnworksExceptionList">
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>