<div class="column">
    <div class="column">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2 id="titlenews">
                        3PL News
                        <small><a href="<?php echo base_url('news/view'); ?>">View More News</a></small>
                    </h2>
                </div>
                <div class="body">
                    <?php foreach ($get_news as $news):
                        $news_content = $news->news_content;
                        if (strlen($news_content) > 150):
                            $news_content = substr($news_content, 0, 150);
                        endif;
                        ?>
                        <h4><?php echo $news->news_title; ?>
                            <small> <?php echo $news->first_name . ' ' . $news->last_name . ' | ' . $news->news_created_date_time; ?></small>
                        </h4>
                        <form name="comms_form" class="form-horizontal form-label-left" method="post"
                              action="<?php echo base_url('main/view_news'); ?>" novalidate>
                            <input type="hidden" name="news_id" class="form-control col-md-7 col-xs-12"
                                   value="<?php echo $news->news_id; ?>">

                            <a data-toggle="modal" href="#modalViewNews"  class="open-viewNewsModel btn btn-primary" id="news-<?php echo $news->news_id; ?>">
                                Click to View
                            </a>

                        </form>
                        <div class="ln_solid"></div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="card">
            <div class="header">
                <h2 id="titlewidgets">
                    3PL Widgets
                </h2>
            </div>
            <div class="body">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons <?php echo $fWidget1ClassIcon; ?>"></i>
                            </div>
                            <div class="content">
                                <div class="text <?php echo $fWidget1ClassName; ?>"></div>

                                <div class="number demo-preloader <?php echo $fLoaderClass; ?>" style="margin-top: 1px;"
                                     id="<?php echo $fLoaderIdWidget1; ?>">
                                    <div class="preloader pl-size-sm">
                                        <div class="spinner-layer pl-white">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="number count-to <?php echo $fWidget1ClassValue; ?>" data-from="0"
                                     data-to="0" data-speed="1000" data-fresh-interval="20">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons <?php echo $fWidget2ClassIcon; ?>"></i>
                            </div>
                            <div class="content">
                                <div class="text <?php echo $fWidget2ClassName; ?>"></div>

                                <div class="number demo-preloader <?php echo $fLoaderClass; ?>" style="margin-top: 1px;"
                                     id="<?php echo $fLoaderIdWidget2; ?>">
                                    <div class="preloader pl-size-sm">
                                        <div class="spinner-layer pl-white">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="number count-to <?php echo $fWidget2ClassValue; ?>" data-from="0"
                                     data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons <?php echo $fWidget3ClassIcon; ?>"></i>
                            </div>
                            <div class="content">
                                <div class="text <?php echo $fWidget3ClassName; ?>"></div>

                                <div class="number demo-preloader <?php echo $fLoaderClass; ?>" style="margin-top: 1px;"
                                     id="<?php echo $fLoaderIdWidget3; ?>">
                                    <div class="preloader pl-size-sm">
                                        <div class="spinner-layer pl-white">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="number count-to <?php echo $fWidget3ClassValue; ?>" data-from="0"
                                     data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                        <div class="info-box bg-blue hover-expand-effect">
                            <div class="icon">
                                <i class="material-icons <?php echo $fWidget4ClassIcon; ?>"></i>
                            </div>
                            <div class="content">
                                <div class="text <?php echo $fWidget4ClassName; ?>"></div>

                                <div class="number demo-preloader <?php echo $fLoaderClass; ?>" style="margin-top: 1px;"
                                     id="<?php echo $fLoaderIdWidget4; ?>">
                                    <div class="preloader pl-size-sm">
                                        <div class="spinner-layer pl-white">
                                            <div class="circle-clipper left">
                                                <div class="circle"></div>
                                            </div>
                                            <div class="circle-clipper right">
                                                <div class="circle"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="number count-to <?php echo $fWidget4ClassValue; ?>" data-from="0"
                                     data-to="0" data-speed="1000" data-fresh-interval="20"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="column">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="card">
                <div class="body">
                    <a class="twitter-timeline" data-height="550"
                       href="https://twitter.com/3P_Logistics?ref_src=twsrc%5Etfw">Tweets by
                        3P_Logistics</a>
                    <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
            </div>
        </div>
    </div>
</div>