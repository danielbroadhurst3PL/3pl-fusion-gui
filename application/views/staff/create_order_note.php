<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
        <div class="header">
            <h2>Add note to Order</h2>
        </div>
        <div class="body">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <form id="createOrderNote">
                        <div class="form-group">
                            <div class="form-line">
                                <label for="orderNumber">Enter Order Number:</label>
                                <input type="text" name="orderNumber" id="orderNumber" class="form-control" required autofocus>
                            </div>                         
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label for="note_body">Enter Note Description:</label>
                                <textarea name="note_body" id="note_body" class="form-control" required rows="5"></textarea>
                            </div>                           
                        </div>
                        <div class="form-group">
                            <div class="form-line">
                                <label for="note_image">Upload Image:</label>
                                <input name="file" type="file" id="note_image" class="form-control" />
                            </div>
                        </div>
                        <button type="submit" class="<?php echo BUTTON_BLUE; ?>" id="formSubmit">Add Note to Order</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>