<div class="modal fade in" id="newsModal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="smallModalLabel">News Article Published</h4>
      </div>
      <div class="modal-header" id="newsInfo">
        
      </div>
      <div class="modal-body" id="newsBody"></div>
      <div class="modal-footer">
        <button type="button" class="btn btn-link waves-effect" data-dismiss="modal">CLOSE</button>
      </div>
    </div>
  </div>
</div>