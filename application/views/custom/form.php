<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <form id="<?php echo $formId; ?>" autocomplete="off">
    <?php if ($loggedUser->group->id == 1 or $loggedUser->group->id == 4): ?>
        <input id="userStaff" name="prodId" type="hidden" value="<?php echo $loggedUser->id; ?>">
    <?php endif; ?>
    <?php foreach ($formData as $cardTitle => $cardFields) { if($cardTitle !== TITLE_ITEMS) { ?>
        <?php if ($cardFields[0]["id"] === "referenceCustomer" and $formId === 'formGrnCreate'){ ?>
            <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12" id="<?php echo str_replace(' ', '', $cardTitle); ?>">
        <?php } else { ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="<?php echo str_replace(' ', '', $cardTitle); ?>">
        <?php }; ?>
            <div class="card">
                <div class="header">
                    <h2><?php echo $cardTitle; ?></h2>
                    <ul class="header-dropdown m-r--5">
                        <li>
                            <a href="javascript:void(0);" onclick="toggleInfoBar('<?php echo str_replace(' ', '', $cardTitle); ?>')"  id="<?php echo str_replace(' ', '', $cardTitle); ?>">
                                <i class="material-icons info">info</i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="body">
                    <div class="row clearfix">
                        <?php
                            foreach ($cardFields as $formField)
                            {
                            $formFieldId = $formField[ID];
                            $formFieldId = str_replace(' ', '', $formFieldId); // Removes WhiteSpace from ID
                            $formFieldTitle = $formField[TITLE];
                            $formFieldType = $formField[TYPE];
                            $formFieldRequired = $formField[REQUIRED];
                            $formFieldTabIndex = $formField[TABINDEX];
                            $formFieldDivSize = $formField[DIVSIZE];
                        ?>                                       
                            <div class="col-md-<?php echo $formFieldDivSize; ?>" id="<?php echo $formFieldId ?>_formDiv">                               
                                <?php   if($formFieldType !== ITEMS)
                                        {
                                ?>
                                                        
                                <?php
                                        }
                                        switch ($formFieldType)
                                        {
                                        case TEXTAREA:
                                ?>                        
                                        <div class="form-group">
                                            <div class="form-line">
                                                <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                                <textarea tabindex="<?php echo $formFieldTabIndex; ?>" placeholder=" " id="<?php echo $formFieldId; ?>" rows="5" class="form-control" <?php if($formFieldRequired === true):?>required<?php endif;?>></textarea>
                                            </div>
                                        </div>

                                    <?php
                                        break;
                                        case SELECT_:
                                    ?>                      
                                        <div class="form-group">
                                            <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                            <div class="form-line">
                                                <select tabindex="<?php echo $formFieldTabIndex; ?>" id="<?php echo $formFieldId; ?>" class="form-control show-tick" <?php if($formFieldRequired === true): ?>required<?php endif; ?>></select>
                                            </div>
                                        </div>

                                    <?php
                                        break;
                                        case DATE:
                                    ?>                  
                                        <div class="form-group">
                                            <div class="form-line" id="bs_datepicker_container">
                                                <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                                <input tabindex="<?php echo $formFieldTabIndex; ?>" type="text" id="<?php echo $formFieldId; ?>" class="datepickercurrent form-control" placeholder="Please choose a date..." <?php if($formFieldRequired === true): ?>required<?php endif; ?>>
                                            </div>
                                        </div>
                                    <?php
                                        break;
                                        case PASSWORD:
                                    ?>                  
                                        <div class="form-group">
                                            <div class="form-line" id="password">
                                            <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                                <input tabindex="<?php echo $formFieldTabIndex; ?>" name="password" autocomplete="new-password" type="password" id="<?php echo $formFieldId; ?>" class="form-control" placeholder="Please choose a password..." <?php if($formFieldRequired === true): ?>required<?php endif; ?>>
                                            </div>
                                        </div>
                                    <?php
                                        break;
                                        case NUMBER:
                                            $type = "text";
                                            if (strpos($formFieldId, QUANTITY) !== false or strpos($formFieldId, "numberCartons") !== false or strpos($formFieldId, "numberPallets") !== false)
                                            {
                                                $formFieldRule = "fusionNumber";
                                            }
                                            else if (strpos($formFieldId, AVERAGE_MONTHLY_UNIT_SALES) !== false or strpos($formFieldId, LICENCES) !== false)
                                            {
                                                $formFieldRule = "fusionNumber";
                                            }
                                            else if (strpos($formFieldId, HEIGHT) !== false or strpos($formFieldId, WIDTH) !== false or strpos($formFieldId, DEPTH) !== false)
                                            {
                                                $formFieldRule = "fusionNumber";
                                            }
                                            else
                                            {
                                                $formFieldRule = "fusionDecimal";
                                            }
                                    ?>
                                            <div class="input-group spinner" data-trigger="spinner">
                                                <div class="form-line">
                                                    <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                                    <input tabindex="<?php echo $formFieldTabIndex; ?>" id="<?php echo $formFieldId; ?>" type="<?php echo $type; ?>" class="form-control text-center" value="0" data-rule="<?php echo $formFieldRule; ?>" <?php if($formFieldRequired === true): ?>required<?php endif; ?>>
                                                </div>
                                                <span class="input-group-addon">
                                                    <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                                    <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                                </span>
                                            </div>

                                    <?php
                                        break;
                                        case CHECKBOX:
                                    ?>
                                            <div class="form-group">
                                                <div class="demo-checkbox">
                                                    <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                                    <input tabindex="<?php echo $formFieldTabIndex; ?>" type="checkbox" id="<?php echo $formFieldId; ?>" />
                                                    <label for="<?php echo $formFieldId; ?>">
                                                </div>
                                            </div>

                                    <?php
                                        break;
                                        case PHONE_NUMBER:
                                            ?>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input tabindex="<?php echo $formFieldTabIndex; ?>" type="tel" id="<?php echo $formFieldId; ?>" class="form-control" placeholder=" " />
                                                            <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                                        </div>
                                                    </div>
        
                                            <?php
                                                break;
                                        case RADIO:
                                    ?> 
                                            <div class="form-group">
                                                <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span><?php endif ?></label>
                                                <div class="btn-group btn-group-toggle" data-toggle="buttons" id="<?php echo $formFieldId ?>">
                                                    <?php $formFieldRadioValue = $formField[ITEMS]; ?>
                                                    <?php foreach ($formFieldRadioValue as $key => $value) { 
                                                    ?>
                                                    <label class="btn btn-secondary" for="<?php echo $formFieldId . "_" . str_replace(' ', '', $value); ?>"><?php echo $value; ?>
                                                    <input name="<?php echo $formFieldId ?>" tabindex="<?php echo $formFieldTabIndex; ?>" type="radio" id="<?php echo $formFieldId . "_" . str_replace(' ', '', $value); ?>" />
                                                    </label>
                                                    <?php }; ?> 
                                                </div>
                                            </div>                    
                                    <?php
                                        break;
                                        case ITEMS:

                                        break;
                                        default:
                                    ?>
                                            <div class="form-group">
                                                <div class="form-line">
                                                <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?><?php if($formFieldRequired === false):?> <span>(optional)</span></span><?php endif ?></label>
                                                    <input tabindex="<?php echo $formFieldTabIndex; ?>" placeholder=" " type="<?php echo $formFieldType; ?>" <?php if(array_key_exists('multiple', $formField) && $formField['multiple']  === true):?> multiple <?php endif ?> id="<?php echo $formFieldId; ?>" class="form-control"
                                                    <?php if($formFieldRequired === true):?>required<?php endif; ?>>
                                                    
                                                </div>
                                            </div>
                                    <?php
                                        break;
                                    }
                                    ?>
                                </div>        
                        <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    <?php
            }
            else
            {
               $this->load->view(PAGE_ITEM_TABLE, $cardFields);
               $this->load->view(PAGE_PRODUCT_TABLE);
            }
        }
    ?>
    </form>
        <!-- Vertical Layout -->
        <div class="row clearfix stickyFormButtonDiv">
            <div class="col-md-12 stickyFormButton">
                <button form="<?php echo $formId; ?>" id="<?php echo $formButton; ?>" type="submit" class="<?php echo BUTTON_BLUE; ?>"><?php echo $pageTitle; ?></button>
            </div>
        </div>
    </div>
</div>
<!-- #END# Vertical Layout -->