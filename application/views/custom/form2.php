<!-- Vertical Layout -->
<div class="row clearfix">
    <div class="col-md-12">
        <button form="<?php echo $formId; ?>" id="<?php echo $formButton; ?>" type="submit" class="<?php echo BUTTON_BLUE; ?>">SUBMIT</button>
    </div>
    <div class="col-md-12">
        <br>
    </div>
</div>
<div class="row clearfix">
    <form id="<?php echo $formId; ?>">
    <?php
        foreach ($formData as $cardTitle => $cardFields)
        {
            if($cardTitle !== TITLE_ITEMS)
            {
    ?>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
                <div class="header">
                    <h2>
                        <?php echo $cardTitle; ?>
                    </h2>
                </div>
                <div class="body">
                        <div class="row clearfix">
                        <?php
                            foreach ($cardFields as $formField)
                            {

                                $formFieldId = $formField[ID];
                                $formFieldTitle = $formField[TITLE];
                                $formFieldType = $formField[TYPE];
                                $formFieldRequired = $formField[REQUIRED];
                                $formFieldTabIndex = $formField[TABINDEX];
                                $formFieldDivSize = $formField[DIVSIZE];


                        ?>
                                        
                                <div class="col-md-<?php echo $formFieldDivSize; ?>">
                                
                                    <?php   if($formFieldType !== ITEMS)
                                            {
                                    ?>
                                                <label for="<?php echo $formFieldId; ?>"><?php echo $formFieldTitle; ?></label>

                                                <?php   if($formFieldRequired === true)
                                                        {
                                                ?>
                                                            <span class="label label-danger">Required</span>
                                                <?php
                                                        }
                                                ?>
                                                            
                                    <?php
                                            }

                                            switch ($formFieldType)
                                            {
                                                case TEXTAREA:
                                    ?>                        
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <textarea tabindex="<?php echo $formFieldTabIndex; ?>" id="<?php echo $formFieldId; ?>" rows="4" class="form-control no-resize"
                                                                <?php
                                                                    if($formFieldRequired === true)
                                                                    {
                                                                ?>
                                                                        required
                                                                <?php
                                                                    }
                                                                ?>
                                                            ></textarea>
                                                        </div>
                                                    </div>

                                    <?php
                                                break;
                                                case SELECT_:
                                    ?>                      
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <select tabindex="<?php echo $formFieldTabIndex; ?>" id="<?php echo $formFieldId; ?>" class="form-control show-tick"
                                                                <?php
                                                                    if($formFieldRequired === true)
                                                                    {
                                                                ?>
                                                                        required
                                                                <?php
                                                                    }
                                                                ?>
                                                            ></select>
                                                        </div>
                                                    </div>

                                    <?php
                                                break;
                                                case DATE:
                                    ?>                  
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input tabindex="<?php echo $formFieldTabIndex; ?>" type="text" id="<?php echo $formFieldId; ?>" class="datepickercurrent form-control" placeholder="Please choose a date..."
                                                            <?php
                                                                if($formFieldRequired === true)
                                                                {
                                                            ?>
                                                                    required
                                                            <?php
                                                                }
                                                            ?>
                                                            >
                                                        </div>
                                                    </div>
                                    <?php
                                                break;
                                                case NUMBER:
                                                    if (strpos($formFieldId, QUANTITY) !== false)
                                                    {
                                                        $formFieldRule = "fusionNumber";
                                                    }
                                                    else if (strpos($formFieldId, PHONE_NUMBER) !== false)
                                                    {
                                                        $formFieldRule = "fusionNumber";
                                                    }
                                                    else
                                                    {
                                                        $formFieldRule = "fusionDecimal";
                                                    }

                                    ?>
                                                    <div class="input-group spinner" data-trigger="spinner">
                                                        <div class="form-line">
                                                            <input tabindex="<?php echo $formFieldTabIndex; ?>" id="<?php echo $formFieldId; ?>" type="text" class="form-control text-center" value="0" data-rule="<?php echo $formFieldRule; ?>"
                                                                <?php
                                                                    if($formFieldRequired === true)
                                                                    {
                                                                ?>
                                                                        required
                                                                <?php
                                                                    }
                                                                ?>
                                                            >
                                                        </div>
                                                        <span class="input-group-addon">
                                                            <a href="javascript:;" class="spin-up" data-spin="up"><i class="glyphicon glyphicon-chevron-up"></i></a>
                                                            <a href="javascript:;" class="spin-down" data-spin="down"><i class="glyphicon glyphicon-chevron-down"></i></a>
                                                        </span>
                                                    </div>

                                    <?php
                                                break;
                                                case CHECKBOX:
                                    ?>
                                                    <div class="form-group">
                                                        <div class="demo-checkbox">
                                                            <input tabindex="<?php echo $formFieldTabIndex; ?>" type="checkbox" id="<?php echo $formFieldId; ?>" />
                                                            <label for="<?php echo $formFieldId; ?>"></label>
                                                        </div>
                                                    </div>
                                                                        
                                    <?php
                                                break;
                                                case ITEMS:

                                                break;
                                                default:
                                    ?>
                                                    <div class="form-group">
                                                        <div class="form-line">
                                                            <input tabindex="<?php echo $formFieldTabIndex; ?>" type="<?php echo $formFieldType; ?>" id="<?php echo $formFieldId; ?>" class="form-control"
                                                                <?php
                                                                    if($formFieldRequired === true)
                                                                    {
                                                                ?>
                                                                        required
                                                                <?php
                                                                    }
                                                                ?>
                                                            >
                                                        </div>
                                                    </div>
                                    <?php
                                                break;
                                            }
                                    ?>
                                
                                </div>
                                        
                        <?php
                            }

                        ?>

                        </div>
                </div>
            </div>
        </div>
    <?php
            }
            else
            {
               $this->load->view(PAGE_ITEM_TABLE, $cardFields);
               $this->load->view(PAGE_PRODUCT_TABLE);
            }
        }
    ?>
    </form>
</div>
<!-- #END# Vertical Layout -->