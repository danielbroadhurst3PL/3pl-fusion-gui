<!-- LOAD VIEW MODALS FOR PAGES -->
<?php $this->load->view(PAGE_MODAL_API_KEY); ?>
<?php $this->load->view(PAGE_MODAL_NEWS); ?>
<?php
switch ($currentPage) {
	case PAGE_PRODUCT_VIEW:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;

	case PAGE_PRODUCT_CURRENT_STOCK:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;

	case PAGE_ORDERS_VIEW:
		$this->load->view(PAGE_MODAL_ORDER);
		break;
	case PAGE_HISTORIC_ORDERS:
		$this->load->view(PAGE_MODAL_ORDER);
		break;

	case PAGE_COMPANY_VIEW:
		$this->load->view(PAGE_MODAL_COMPANY);
		break;

	case PAGE_ORDERS_CREATE:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
	case PAGE_STOCK_ADJUSTMENT:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
	case PAGE_STOCK_BY_LOCATION:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
	case PAGE_SERIAL_NUMBER:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
	case PAGE_GRN_CREATE:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
	case PAGE_MAIN_INDEX:
		$this->load->view(PAGE_MODAL_ORDER);
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
	case PAGE_RETURNS:
		$this->load->view(PAGE_MODAL_PRODUCT);
		$this->load->view(PAGE_MODAL_ORDER);
		break;
	case PAGE_INTERNAL_SHORT_SHIPPED_REPORT:
		$this->load->view(PAGE_MODAL_PRODUCT);
		$this->load->view(PAGE_MODAL_ORDER);
		break;
	case PAGE_INTERNAL_MOVEMENT_BY_SKU:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
	case PAGE_INTERNAL_CONSOLIDATION_REPORT:
		$this->load->view(PAGE_MODAL_PRODUCT);
		break;
}
?>
</div>
</section>

<div class="sideInformationBar" id="sideInformationBarID">
	<h2 id="infoHeader"></h2>
	<div class="menu">
		<div id="infoTextDiv">
			<p id="infoText"></p>
		</div>
	</div>
</div>
<!-- JAVASCRIPT VARS -->

<script>
	// GENERAL
	var baseUrl = "<?php echo base_url(); ?>";
	var currentPage = "<?php echo $currentPage; ?>";
	var system = "<?php echo SYSTEM_NAME; ?>";
	var user = "<?php echo (!isset($userId) ? "" : $userId); ?>";
	var userCompany = "<?php echo (!isset($userCompany) ? "" : $userCompany); ?>";
	var key = "<?php echo (!isset($apiKey) ? "" : $apiKey); ?>";
	<?php if ($currentPage != 'login/index') : ?>
		var testKey = "<?php echo isset($this->data['apiKeys']) ? $this->data['apiKeys']->test : ""; ?>";
		var liveKey = "<?php echo isset($this->data['apiKeys']) ? $this->data['apiKeys']->live : ""; ?>";
	<?php endif; ?>
	var errorMessage = "<?php echo (!isset($this->data['errorAPI']) ? "" : $this->data['errorAPI']); ?>";
	var companyCode = "<?php echo (!isset($loggedUser->company->code) ? "" : $loggedUser->company->code); ?>";
	// FIELDS
	var apiKey = "<?php echo API_KEY_2; ?>";
	var page = "<?php echo PAGE; ?>";
	var stock = "<?php echo STOCK; ?>";
	var sku = "<?php echo SKU; ?>";
	var description = "<?php echo DESCRIPTION; ?>";
	var customerCode = "<?php echo CUSTOMER_CODE; ?>";
	var barcode = "<?php echo BARCODE; ?>";
	var value = "<?php echo VALUE; ?>";
	var dateExpiry = "<?php echo DATE_EXPIRY; ?>";
	var height = "<?php echo HEIGHT;; ?>";
	var width = "<?php echo WIDTH; ?>";
	var depth = "<?php echo DEPTH; ?>";
	var weight = "<?php echo WEIGHT; ?>";
	var quantityInner = "<?php echo QUANTITY_INNER; ?>";
	var quantityMasterCarton = "<?php echo QUANTITY_MASTER_CARTON; ?>";
	var quantityPallet = "<?php echo QUANTITY_PALLET; ?>";
	var commodityCode = "<?php echo COMMODITY_CODE; ?>";
	var serial = "<?php echo SERIAL; ?>";
	var batch = "<?php echo BATCH; ?>";
	var asin = "<?php echo ASIN; ?>";
	var dgnType = "<?php echo DGN_TYPE; ?>";
	var dgnDetails = "<?php echo DGN_DETAILS; ?>";
	var currency = "<?php echo CURRENCY; ?>";
	var countries = "<?php echo COUNTRIES ?>";
	var country = "<?php echo COUNTRY ?>";
	var companyList = "<?php echo COMPANY_SELECT ?>";
	var instructionsPicking = "<?php echo INSTRUCTIONS_PICKING; ?>";
	var services = "<?php echo SERVICES; ?>";
	var service = "<?php echo SERVICE; ?>";
	var nameFirst = "<?php echo FIRST_NAME; ?>";
	var nameLast = "<?php echo LAST_NAME; ?>";
	var name = "<?php echo NAME; ?>";
	var orderNumber = "<?php echo ORDER_NUMBER; ?>";
	var grnReference = "<?php echo GRN_REFERENCE; ?>";
	var postCode = "<?php echo POSTCODE; ?>";
	var company = "<?php echo COMPANY; ?>";
	var addressLine1 = "<?php echo ADDRESS_LINE_1; ?>";
	var addressLine2 = "<?php echo ADDRESS_LINE_2; ?>";
	var town = "<?php echo TOWN; ?>";
	var region = "<?php echo REGION; ?>";
	var telephone = "<?php echo PHONE_NUMBER; ?>";
	var email = "<?php echo EMAIL; ?>";
	var service = "<?php echo SERVICE; ?>";
	var reference = "<?php echo REFERENCE_CUSTOMER; ?>";
	var purchaseOrder = "<?php echo REFERENCE_PURCHASE_ORDER; ?>";
	var dispatchDate = "<?php echo DISPATCH_DATE; ?>";
	var packingInstructions = "<?php echo INSTRUCTIONS_PACKING; ?>";
	var companyCodeHeaderSelect = "<?php echo COMPANYCODEHEADERSELECT; ?>";
	var title = "<?php echo TITLE; ?>";
	var deliveryNote = "<?php echo DELIVERY_NOTE; ?>";
	var logisticsEmail = "<?php echo THREE_PL_EMAIL; ?>";
	var primaryEmail = "<?php echo PRIMARY_EMAIL; ?>";
	var secondaryEmail = "<?php echo SECONDARY_EMAIL; ?>";
	var linnworks = "<?php echo LINNWORKS; ?>";
	var backOrders = "<?php echo BACKORDERS; ?>";
	var emailTracking = "<?php echo EMAIL_TRACKING; ?>";
	var exportStockLevel = "<?php echo EXPORT_STOCK_LEVEL; ?>";
	var liteAccount = "<?php echo LITE_ACCOUNT; ?>";
	var createMacs = "<?php echo CREATE_MACS; ?>";
	var lifetimeCode = "<?php echo LIFETIME_CODE; ?>";
	var notes = "<?php echo NOTES; ?>";
	var type = "<?php echo TYPE; ?>";
	var status = "<?php echo STATUS; ?>";
	var dateShippedFrom = "<?php echo DATE_SHIPPED_FROM; ?>";
	var dateShippedTo = "<?php echo DATE_SHIPPED_TO; ?>";
	var orderNumber = "<?php echo ORDER_NUMBER; ?>";
	var pickingAisle = "<?php echo PICKING_AISLE; ?>";
	var bulkStorage = "<?php echo BULK_STORAGE; ?>";

	var statusBack = "<?php echo STATUS_BACK; ?>";
	var statusNextFlush = "<?php echo STATUS_NEXT_FLUSH; ?>";
	var statusAwaitingPick = "<?php echo STATUS_AWAITING_PICK; ?>";
	var statusAwaitingPack = "<?php echo STATUS_AWAITING_PACK; ?>";
	var statusAwaitingShip = "<?php echo STATUS_AWAITING_SHIP; ?>";
	var statusShipped = "<?php echo STATUS_SHIPPED; ?>";

	var apiCompletedRequests = 0;

	// 3PL API - URL
	var urlApi = "<?php echo $url3plApi; ?>";
	var urlApiProducts = "<?php echo PRODUCTS . "/"; ?>";
	var urlApiOrders = "<?php echo ORDERS . "/"; ?>";
	var urlApiCountries = "<?php echo COUNTRIES . "/"; ?>";
	var urlApiCompanies = "<?php echo COMPANIES . "/"; ?>";
	var urlApiUsers = "<?php echo USERS . "/"; ?>";
	var urlApiDgnDetails = "<?php echo DGN . "/" . DETAILS . "/"; ?>";
	var urlApiDgnTypes = "<?php echo DGN . "/" . TYPES . "/"; ?>";
	var urlApiCurrency = "<?php echo CURRENCY . "/"; ?>";
	var urlApiService = "<?php echo SERVICES . "/"; ?>";
	var urlApiDeliveryNotes = "<?php echo DELIVERY_NOTES . "/" . CHECK; ?>";
	var urlApiGrns = "<?php echo GRNS; ?>";

	var url3PLDesigner = "<?php echo $url3PLDesigner; ?>";

	// 3PL FUSION - URLS
	var urlLogin = "<?php $urlLogin; ?>";
	var urlSuffixMain = "<?php echo PAGE_MAIN_INDEX; ?>";
	var urlSuffixError = "<?php echo PAGE_ERROR_INDEX; ?>";
	var urlSuffixLogin = "<?php echo PAGE_LOGIN_INDEX; ?>";
	var urlSuffixLogin_Login = "<?php echo PAGE_LOGIN_LOGIN; ?>";
	var urlPageProductEdit = "<?php echo PAGE_PRODUCT_EDIT; ?>";
	var urlPageProductEditBulk = "<?php echo PAGE_PRODUCT_EDIT_BULK; ?>";
	var urlPageOrderDelete = "<?php echo PAGE_ORDERS_DELETE; ?>";
	var urlPageOrderEdit = "<?php echo PAGE_ORDERS_EDIT; ?>";
	var urlPageCompanyEdit = "<?php echo PAGE_COMPANY_EDIT; ?>";
	var urlPageOrdersView = "<?php echo PAGE_ORDERS_VIEW; ?>";

	// API - METHODS
	var methodGet = "<?php echo GET_METHOD; ?>";
	var methodPost = "<?php echo POST_METHOD; ?>";
	var methodPut = "<?php echo PUT_METHOD; ?>";
	var methodDelete = "<?php echo DELETE_METHOD; ?>";

	// ALERTS   
	var alertTypeSuccess = "<?php echo SUCCESS; ?>";
	var alertTypeError = "<?php echo ERROR; ?>";

	// BUTTONS
	var buttonNext = "<?php echo BUTTON_NEXT; ?>";
	var buttonPrevious = "<?php echo BUTTON_PREVIOUS; ?>";
	var buttonPageMin = "<?php echo BUTTON_PAGE_MIN; ?>";
	var buttonPageMax = "<?php echo BUTTON_PAGE_MAX; ?>";
	var buttonPageCurrent = "<?php echo BUTTON_PAGE_CURRENT; ?>";
	var buttonPageNext = "<?php echo BUTTON_PAGE_NEXT; ?>";
	var buttonPagePrevious = "<?php echo BUTTON_PAGE_PREVIOUS; ?>";

	var buttonAdd = "<?php echo BUTTON_ADD; ?>";
	var buttonRemove = "<?php echo BUTTON_REMOVE; ?>";
	var buttonEdit = "<?php echo BUTTON_EDIT; ?>";
	var buttonProductsView = "<?php echo BUTTON_PRODUCTS_VIEW; ?>";
	var buttonToggleable = "<?php echo BUTTON_TOGGLEABLE; ?>";
	var buttonLoader = "<?php echo BUTTON_LOADER; ?>";

	var buttonDefault = "<?php echo BUTTON_DEFAULT; ?>";
	var buttonBlue = "<?php echo BUTTON_BLUE; ?>";
	var buttonRed = "<?php echo BUTTON_RED; ?>";
	var buttonPink = "<?php echo BUTTON_PINK; ?>";
	var buttonLink = "<?php echo BUTTON_LINK; ?>";

	var buttonItemAdd = "<?php echo BUTTON_ITEM_ADD; ?>";

	// FORMS
	var formProductCreate = "<?php echo FORM_PRODUCT_CREATE; ?>";
	var formProductEdit = "<?php echo FORM_PRODUCT_EDIT; ?>";
	var formOrderCreate = "<?php echo FORM_ORDER_CREATE; ?>";
	var formOrderEdit = "<?php echo FORM_ORDER_EDIT; ?>";
	var formCompanyCreate = "<?php echo FORM_COMPANY_CREATE; ?>";
	var formCompanyEdit = "<?php echo FORM_COMPANY_EDIT; ?>";
	var formGrnCreate = "<?php echo FORM_GRN_CREATE; ?>";
	var formOrderView = "<?php echo FORM_ORDER_VIEW; ?>";
	var formUserCreate = "<?php echo FORM_USER_CREATE; ?>";
	var formTicketCreate = "<?php echo FORM_TICKET_CREATE; ?>";

	// MODALS
	var modalProductLogs = "<?php echo MODAL_PRODUCT_LOGS; ?>";
	var modalProductTitle = "<?php echo MODAL_PRODUCT_TITLE; ?>";
	var modalProduct = "<?php echo MODAL_PRODUCT; ?>";
	var modalProductLabel = "<?php echo MODAL_PRODUCT_LABEL; ?>";
	var modalProductEdit = "<?php echo MODAL_PRODUCT_EDIT; ?>";
	var modalProductTotal = "<?php echo MODAL_PRODUCT_TOTAL; ?>";
	var modalProductAllocated = "<?php echo MODAL_PRODUCT_ALLOCATED; ?>";
	var modalProductFree = "<?php echo MODAL_PRODUCT_FREE; ?>";
	var modalProductOnPick = "<?php echo MODAL_PRODUCT_ON_PICK; ?>";
	var modalProductShort = "<?php echo MODAL_PRODUCT_SHORT; ?>";
	var modalProductHold = "<?php echo MODAL_PRODUCT_HOLD; ?>";
	var modalProductQuarantine = "<?php echo MODAL_PRODUCT_QUARANTINE; ?>";
	var modalProductDescription = "<?php echo MODAL_PRODUCT_DESCRIPTION; ?>";
	var modalProductBarcode = "<?php echo MODAL_PRODUCT_BARCODE; ?>";
	var modalProductASIN = "<?php echo MODAL_PRODUCT_ASIN; ?>";
	var modalProductDGNType = "<?php echo MODAL_PRODUCT_DGN_TYPE; ?>";
	var modalProductDGNDetails = "<?php echo MODAL_PRODUCT_DGN_DETAILS; ?>";
	var modalProductCountry = "<?php echo MODAL_PRODUCT_COUNTRY; ?>";
	var modalProductCommodityCode = "<?php echo MODAL_PRODUCT_COMMODITY_CODE; ?>";
	var modalProductHeight = "<?php echo MODAL_PRODUCT_HEIGHT; ?>";
	var modalProductWidth = "<?php echo MODAL_PRODUCT_WIDTH; ?>";
	var modalProductDepth = "<?php echo MODAL_PRODUCT_DEPTH; ?>";
	var modalProductWeight = "<?php echo MODAL_PRODUCT_WEIGHT; ?>";
	var modalProductCurrency = "<?php echo MODAL_PRODUCT_CURRENCY; ?>";
	var modalProductValue = "<?php echo MODAL_PRODUCT_VALUE; ?>";
	var modalProductInnerQuantity = "<?php echo MODAL_PRODUCT_INNER_QUANTITY; ?>";
	var modalProductMasterCartonQuantity = "<?php echo MODAL_PRODUCT_MASTER_CARTON_QUANTITY; ?>";
	var modalProductPalletQuantity = "<?php echo MODAL_PRODUCT_PALLET_QUANTITY; ?>";
	var modalProductDateExpiry = "<?php echo MODAL_PRODUCT_DATE_EXPIRY; ?>";
	var modalProductSerial = "<?php echo MODAL_PRODUCT_SERIAL; ?>";
	var modalProductBatch = "<?php echo MODAL_PRODUCT_BATCH; ?>";
	var modalProductQuantity = "<?php echo MODAL_PRODUCT_QUANTITY; ?>";
	var modalProductAssigned = "<?php echo MODAL_PRODUCT_ASSIGNED; ?>";
	var modalProductOrderIn = "<?php echo MODAL_PRODUCT_ORDER_IN; ?>";
	var modalProductOrderOut = "<?php echo MODAL_PRODUCT_ORDER_OUT; ?>";
	var modalProductDueIn = "<?php echo MODAL_PRODUCT_DUE_IN; ?>";
	var modalProductDueOut = "<?php echo MODAL_PRODUCT_DUE_OUT; ?>";

	var modalOrder = "<?php echo MODAL_ORDER; ?>";
	var modalOrderItem = "<?php echo MODAL_ORDER_ITEM; ?>";
	var modalOrderLabel = "<?php echo MODAL_ORDER_LABEL; ?>";
	var modalOrderEdit = "<?php echo MODAL_ORDER_EDIT; ?>";
	var modalOrderFirstName = "<?php echo MODAL_ORDER_FIRST_NAME; ?>";
	var modalOrderLastName = "<?php echo MODAL_ORDER_LAST_NAME; ?>";
	var modalOrderCompany = "<?php echo MODAL_ORDER_COMPANY; ?>";
	var modalOrderAddressLine1 = "<?php echo MODAL_ORDER_ADDRESS_LINE_1; ?>";
	var modalOrderAddressLine2 = "<?php echo MODAL_ORDER_ADDRESS_LINE_2; ?>";
	var modalOrderTown = "<?php echo MODAL_ORDER_TOWN; ?>";
	var modalOrderRegion = "<?php echo MODAL_ORDER_REGION; ?>";
	var modalOrderPostcode = "<?php echo MODAL_ORDER_POSTCODE; ?>";
	var modalOrderCountry = "<?php echo MODAL_ORDER_COUNTRY; ?>";
	var modalOrderPhoneNumber = "<?php echo MODAL_ORDER_PHONE_NUMBER; ?>";
	var modalOrderEmail = "<?php echo MODAL_ORDER_EMAIL; ?>";
	var modalOrderService = "<?php echo MODAL_ORDER_SERVICE; ?>";
	var modalOrderReference = "<?php echo MODAL_ORDER_REFERENCE; ?>";
	var modalOrderPurchaseOrder = "<?php echo MODAL_ORDER_PURCHASE_ORDER; ?>";
	var modalOrderCurrency = "<?php echo MODAL_ORDER_CURRENCY; ?>";
	var modalOrderDispatchDate = "<?php echo MODAL_ORDER_DISPATCH_DATE; ?>";
	var modalOrderDateCreated = "<?php echo MODAL_ORDER_DATE_CREATED; ?>";
	var modalOrderDeliveryNote = "<?php echo MODAL_ORDER_DELIVERY_NOTE; ?>";
	var modalOrderInstructionsPacking = "<?php echo MODAL_ORDER_INSTRUCTIONS_PACKING; ?>";
	var modalOrderTrackingNumber = "<?php echo MODAL_ORDER_TRACKING; ?>";
	var modalOrderCarrier = "<?php echo MODAL_ORDER_CARRIER; ?>";

	var modalCompanyLogs = "<?php echo MODAL_COMPANY_LOGS; ?>";
	var modalCompany = "<?php echo MODAL_COMPANY; ?>";
	var modalCompanyID = "<?php echo MODAL_COMPANY_ID; ?>";
	var modalCompanyLabel = "<?php echo MODAL_COMPANY_LABEL; ?>";
	var modalCompanyEdit = "<?php echo MODAL_COMPANY_EDIT; ?>";
	var modalCompanyTitle = "<?php echo MODAL_COMPANY_TITLE; ?>";
	var modalCompanyCode = "<?php echo MODAL_COMPANY_CODE; ?>";
	var modalCompanyEmail = "<?php echo MODAL_COMPANY_EMAIL ?>";
	var modalCompanyprimaryEmail = "<?php echo MODAL_COMPANY_PRIMARY_EMAIL ?>";
	var modalCompanysecondaryEmail = "<?php echo MODAL_COMPANY_SECONDARY_EMAIL ?>";
	var modalCompanyAddressLine1 = "<?php echo MODAL_COMPANY_ADDRESS_LINE_1 ?>";
	var modalCompanyAddressLine2 = "<?php echo MODAL_COMPANY_ADDRESS_LINE_2 ?>";
	var modalCompanyTown = "<?php echo MODAL_COMPANY_TOWN ?>";
	var modalCompanyRegion = "<?php echo MODAL_COMPANY_REGION ?>";

	// NOTIFICATIONS
	var notificationSuccess = "<?php echo ALERT_SUCCESS; ?>";
	var notificationSuccessTimer = "<?php echo ALERT_SUCCESS_TIMER; ?>";
	var notificationDanger = "<?php echo ALERT_DANGER; ?>";
	var notificationDangerTimer = "<?php echo ALERT_DANGER_TIMER; ?>";

	// TABLES
	var tableProductsView = "<?php echo TABLE_PRODUCTS_VIEW; ?>";
	var tableOrdersView = "<?php echo TABLE_ORDERS_VIEW; ?>";
	var tableCompanyView = "<?php echo TABLE_COMPANY_VIEW; ?>";
	var tableItemsView = "<?php echo TABLE_ITEMS_VIEW; ?>";
	var tableCustomActions = "<?php echo TABLE_CUSTOM_ACTIONS; ?>";
	var tableProductsViewBulkErrors = "<?php echo TABLE_PRODUCTS_BULK_ERROR_VIEW; ?>";
	var tableStockView = "<?php echo TABLE_STOCK_VIEW; ?>";
	var tableStockBreakdownView = "<?php echo TABLE_STOCK_BREAKDOWN_VIEW; ?>";
	var tableAccountManagerReport = "<?php echo TABLE_ACCOUNT_MANAGER_REPORT; ?>";
	var tableStockMovementReport = "<?php echo TABLE_STOCK_MOVEMENT_REPORT; ?>";
	var tableReturnsView = "<?php echo TABLE_RETURNS_VIEW; ?>";
	var tableOrdersShippedView = "<?php echo TABLE_ORDERS_SHIPPED_VIEW; ?>";
	var tableCustomActionsButtonPrevious = "<?php echo TABLE_CUSTOM_ACTIONS_BUTTON_PREVIOUS; ?>";
	var tableCustomActionsButtonPageNumbers = "<?php echo TABLE_CUSTOM_ACTIONS_BUTTON_PAGE_NUMBERS; ?>";
	var tableCustomActionsButtonNext = "<?php echo TABLE_CUSTOM_ACTIONS_BUTTON_NEXT; ?>";
	var tableItemQuantityTextbox = "<?php echo TABLE_ITEM_QUANTITY_TEXTBOX; ?>";
	var tableItemValueTextbox = "<?php echo TABLE_ITEM_VALUE_TEXTBOX; ?>";
	var tableGrnsViewItems = "<?php echo TABLE_GRNS_VIEW_ITEMS; ?>";
	var tableCustomerFTP = "<?php echo TABLE_CUSTOMER_FTP ?>";
	var tableFilesViewDownload = "<?php echo TABLE_FILES_VIEW_DOWNLOAD; ?>";
	var tableGrnView = "<?php echo TABLE_GRNS_VIEW; ?>"

	// TITLES
	var titleSuccess = "<?php echo ucfirst(SUCCESS); ?>";
	var titleError = "<?php echo ucfirst(ERROR); ?>";
	var titleNotification = "<?php echo ucfirst(NOTIFICATION); ?>";
	var titleBarcode = "<?php echo TITLE_BARCODE; ?>";
	var titleCommodityCode = "<?php echo TITLE_COMMODITY_CODE; ?>";
	var titleDescription = "<?php echo TITLE_DESCRIPTION; ?>";
	var titleSku = "<?php echo TITLE_SKU; ?>";
	var titleTitle = "<?php echo TITLE_TITLE; ?>";
	var titleAsin = "<?php echo TITLE_ASIN; ?>";
	var titleDgnDetails = "<?php echo TITLE_DGN_DETAILS; ?>";
	var titleDgnType = "<?php echo TITLE_DGN_TYPE; ?>";
	var titleHeight = "<?php echo TITLE_HEIGHT; ?>";
	var titleWidth = "<?php echo TITLE_WIDTH; ?>";
	var titleDepth = "<?php echo TITLE_DEPTH; ?>";
	var titleWeight = "<?php echo TITLE_WEIGHT; ?>";
	var titleCountry = "<?php echo TITLE_COUNTRY; ?>";
	var titleCurrency = "<?php echo TITLE_CURRENCY; ?>";
	var titleValue = "<?php echo TITLE_VALUE; ?>";
	var titlePickingInstructions = "<?php echo TITLE_INSTRUCTIONS_PICKING; ?>";
	var titleInnerQuantity = "<?php echo TITLE_INNER_QUANTITY; ?>";
	var titleMasterCartonQuantity = "<?php echo TITLE_MASTER_CARTON_QUANTITY; ?>";
	var titlePalletQuantity = "<?php echo TITLE_PALLET_QUANTITY; ?>";
	var titleExpiryDate = "<?php echo TITLE_DATE_EXPIRY; ?>";
	var titleBatch = "<?php echo TITLE_BATCH; ?>";
	var titleSerial = "<?php echo TITLE_SERIAL; ?>";
	var titlePostcode = "<?php echo TITLE_POSTCODE; ?>";

	var titleShipped = "<?php echo TITLE_SHIPPED; ?>";
	var titleNextFlush = "<?php echo TITLE_NEXT_FLUSH; ?>";
	var titleBackOrders = "<?php echo TITLE_BACK_ORDERS; ?>";
	var titleAwaitingPick = "<?php echo TITLE_AWAITING_PICK; ?>";
	var titleAwaitingPack = "<?php echo TITLE_AWAITING_PACK; ?>";
	var titleAwaitingShip = "<?php echo TITLE_AWAITING_SHIP; ?>";

	var titleDateShippedTo = "<?php echo TITLE_DATE_SHIPPED_TO; ?>";
	var titleDateShippedFrom = "<?php echo TITLE_DATE_SHIPPED_FROM; ?>";

	var divFilterOrderNumber = "<?php echo DIV_FILTER_ORDER_NUMBER; ?>";
	var divFilterDateShippedTo = "<?php echo DIV_FILTER_DATE_SHIPPED_TO; ?>";
	var divFilterDateShippedFrom = "<?php echo DIV_FILTER_DATE_SHIPPED_FROM; ?>";
	var divFilterStatus = "<?php echo DIV_FILTER_STATUS; ?>";
	var divCompanyList = "<?php echo DIV_COMPANY_LIST; ?>";

	var divFilterCustomerRef = "<?php echo DIV_FILTER_CUSTOMER_REFERENCE; ?>";
</script>

<!-- Jquery Core Js -->
<script src="<?php echo base_url(THEME_JQUERYMIN_JS); ?>"></script>
<!-- <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script> -->

<!-- Bootstrap Core Js -->
<script src="<?php echo base_url(THEME_BOOTSTRAP_JS); ?>"></script>

<!-- Select Plugin Js -->
<script src="<?php echo base_url(THEME_BOOSTRAPSELECT_JS); ?>"></script>

<?php if ($currentPage !== PAGE_LOGIN_INDEX && $currentPage !== PAGE_ERROR_INDEX && $currentPage !== PAGE_REGISTER_INDEX) { ?>
	<!-- Slimscroll Plugin Js -->
	<script src="<?php echo base_url(THEME_JQUERYSLIMSCROLL_JS); ?>"></script>
<?php } ?>

<!-- Bootstrap Notify Plugin Js -->
<script src="<?php echo base_url(THEME_BOOTSTRAPNOTIY_JS); ?>"></script>

<!-- Dropzone Plugin Js -->
<script src="<?php echo base_url(THEME_DROPZONE_JS); ?>"></script>

<!-- Jquery Validation Plugin Css -->
<script src="<?php echo base_url(THEME_JQUERYVALIDATION_JS); ?>"></script>

<!-- JQuery Steps Plugin Js -->
<script src="<?php echo base_url(THEME_JQUERYSTEPS_JS); ?>"></script>

<!-- Sweet Alert Plugin Js -->
<script src="<?php echo base_url(THEME_SWEETALERT_JS); ?>"></script>

<!-- Jquery Spinner Plugin Js -->
<script src="<?php echo base_url(THEME_JQUERYSPINNER_JS); ?>"></script>

<!-- Waves Effect Plugin Js -->
<script src="<?php echo base_url(THEME_NODEWAVES_JS); ?>"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/jquery.dataTables.js') ?>"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') ?>"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') ?>"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/extensions/export/buttons.flash.min.j') ?>s"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/extensions/export/jszip.min.js') ?>"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/extensions/export/pdfmake.min.js') ?>"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/extensions/export/vfs_fonts.js') ?>"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/extensions/export/buttons.html5.min.js') ?>"></script>
<script src="<?php echo base_url(DIR_FILES_THEME . 'plugins/jquery-datatable/extensions/export/buttons.print.min.js') ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tagsinput/0.8.0/bootstrap-tagsinput.min.js.map"></script>

<!-- Autosize Plugin Js -->
<script src="<?php echo base_url(DIR_THEME_AUTOSIZE_JS); ?>"></script>

<!-- Text Area Autosize Plugin Js -->
<script src="<?php echo base_url(DIR_THEME_AUTOSIZE_TEXTAREA_JS); ?>"></script>

<!-- Moment Plugin Js -->
<script src="<?php echo base_url(DIR_THEME_MOMENT_JS); ?>"></script>

<!-- Bootstrap Material Datetime Picker Plugin Js -->
<!-- Commented out one is from theme, is outdated, disabledays does not work, below this is the updated version -->
<!-- <script src="<?php // echo base_url($fAdminbsbMaterialDesign);
									?>plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> -->
<script src="<?php echo base_url(DIR_THEME_MATERIALDATETIMEPICKER_JS); ?>"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="<?php echo base_url(DIR_THEME_JS_COUNT_TO); ?>"></script>
<script src="https://nightly.datatables.net/buttons/js/buttons.html5.min.js"></script>

<!-- Custom Js -->
<script src="<?php echo base_url(THEME_ADMIN_JS); ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_FORM); ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_DATETIMEPICKER); ?>"></script>

<?php if ($currentPage !== PAGE_LOGIN_INDEX && $currentPage !== PAGE_ERROR_INDEX && $currentPage !== PAGE_REGISTER_INDEX) { ?>
	<!-- Demo Js -->
	<script src="<?php echo base_url(THEME_DEMO_JS); ?>"></script>
<?php } ?>

<!-- Scripts General -->
<script src="<?php echo base_url(SCRIPT_GENERAL_ALERTUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_BUTTONUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_COMMON); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_ELEMENTUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_VALIDATION); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_SELECT); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_TABLEUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_COUNTRYUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_CURRENCYUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_COMPANYUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_DGNUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_SERVICEUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_DELIVERYNOTEUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_WAREHOUSENOTEUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>

<!-- Standard Scripts -->
<script src="<?php echo base_url(SCRIPT_GENERAL_NOTIFICATIONS); ?>"></script>
<script src="<?php echo base_url(SCRIPT_3PLAPI_API); ?>?<?php echo FUSION_VERSION; ?>"></script>

<?php if ($currentPage !== PAGE_LOGIN_INDEX && $currentPage !== PAGE_ERROR_INDEX && $currentPage !== PAGE_REGISTER_INDEX) { ?>
	<script src="<?php echo base_url(SCRIPT_GENERAL_DISABLEKEYS); ?>"></script>
	<script src="<?php echo base_url(SCRIPT_ALL); ?>?<?php echo FUSION_VERSION; ?>"></script>
<?php } ?>

<?php if (!empty($loggedUser) && $loggedUser->group->id == 4) { ?>
	<script src="<?php echo base_url(SCRIPT_COMPANY_LIST); ?>?<?php echo FUSION_VERSION; ?>"></script>
<?php } ?>


<!-- TABLES SETTINGS -->

<script>
	var tableCompanyViewSettings = $("#" + tableCompanyView).DataTable({
		dom: 'Bfrtip',
		responsive: true,
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	});

	var tableItemsViewSettings = $("#" + tableItemsView).DataTable({
		dom: 'Bfrtip',
		responsive: true,
		buttons: [],
		paging: false
	});

	var tableAccountManagerSettings = $("#" + tableAccountManagerReport).DataTable({
		dom: 'Bfrtip',
		responsive: false,
		autoWidth: false,
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	});

	var tableStockMovementReportSettings = $("#" + tableStockMovementReport).DataTable({
		dom: 'Bfrtip',
		responsive: false,
		autoWidth: false,
		buttons: [
			'copy', 'csv', 'excel', 'pdf', 'print'
		]
	});
</script>
<!-- JAVASCRIPT PAGES -->

<?php
switch ($currentPage) {
	case PAGE_LOGIN_INDEX:
?>
		<script src="<?php echo base_url(SCRIPT_LOGIN_INDEX); ?>"></script>
	<?php
		break;
	case PAGE_MAIN_INDEX:
	?>
		<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
		<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
		<script src="<?php echo base_url(SCRIPT_HOME); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_ORDER); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_VIEW:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_CURRENT_STOCK:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_CURRENT_STOCK); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_STOCK_BREAKDOWN:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_STOCK_BREAKDOWN); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_VIEW_BULK_ERROR:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_VIEW_BULK_ERROR); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_CREATE:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_CREATE); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_CREATE_BULK:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_CREATE_BULK); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_BULK_EDIT:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_BULK_EDIT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_EDIT_BULK:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_EDIT_BULK); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PRODUCT_EDIT:
	?>
		<script src="<?php echo base_url(SCRIPT_PRODUCT_EDIT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_ORDERS_CREATE:
	?>
		<script src="<?php echo base_url(SCRIPT_GENERAL_ITEMSUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_ORDERS_CREATE); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>

	<?php
		break;
	case PAGE_ORDERS_CREATE_BULK:
	?>
		<script src="<?php echo base_url(SCRIPT_ORDERS_CREATE_BULK); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_ORDERS_VIEW:
	?>
		<script src="<?php echo base_url(SCRIPT_ORDERS_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_ORDER); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_ORDER_DELIVERY_NOTE:
	?>
		<script src="<?php echo base_url(SCRIPT_ORDERS_VIEW_DELIVERY_NOTE); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_LINNWORKS_IMPORT_ERRORS:
	?>
		<script src="<?php echo base_url(SCRIPT_ORDERS_LINNWORKS_IMPORT_ERRORS); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_ORDER_BACK_ORDERS_DETAILS:
	?>
		<script src="<?php echo base_url(SCRIPT_ORDERS_BACK_ORDER_DETAILS); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_ORDERS_EDIT:
	?>
		<script src="<?php echo base_url(SCRIPT_GENERAL_ITEMSUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_ORDERS_EDIT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_ORDER_ADDRESS_UPDATE:
	?>
		<script src="<?php echo base_url(SCRIPT_ORDERS_ADDRESS_UPDATE); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_GRN_CREATE:
	?>
		<script src="<?php echo base_url(SCRIPT_GENERAL_ITEMSUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_GRN_CREATE); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_GRN_CREATE_BULK); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_GRN_VIEW:
	?>
		<script src="<?php echo base_url(SCRIPT_GRN_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INVOICES:
	?>
		<script src="<?php echo base_url(SCRIPT_CUSTOMER_WORKSPACE_INVOICES); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_CUSTOM_REPORTS:
	?>
		<script src="<?php echo base_url(SCRIPT_CUSTOMER_WORKSPACE_CUSTOM_REPORTS); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_COMPANY_VIEW:
	?>
		<script src="<?php echo base_url(SCRIPT_COMPANY_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_COMPANY); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_COMPANY_CREATE:
	?>
		<script src="<?php echo base_url(SCRIPT_COMPANY_CREATE); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_COMPANY_EDIT:
	?>
		<script src="<?php echo base_url(SCRIPT_COMPANY_EDIT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
		case PAGE_HISTORIC_ORDERS:
			?>
				<script src="<?php echo base_url(SCRIPT_HISTORIC_ORDERS); ?>?<?php echo FUSION_VERSION; ?>"></script>
				<script src="<?php echo base_url(SCRIPT_MODAL_ORDER); ?>?<?php echo FUSION_VERSION; ?>"></script>
			<?php
				break;
	case PAGE_CREATE_TICKET:
	?>
		<script src="<?php echo base_url(SCRIPT_CREATE_TICKET); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
		case PAGE_LATEST_NEWS:
			?>
				<script src="<?php echo base_url(SCRIPT_LATEST_NEWS); ?>?<?php echo FUSION_VERSION; ?>"></script>
			<?php
				break;
	case PAGE_VIEW_TICKETS:
	?>
		<script src="<?php echo base_url(SCRIPT_VIEW_TICKETS); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_PACKING_LIST_REPORT:
	?>
		<script src="<?php echo base_url(SCRIPT_ORDERS_PACKING_LIST_REPORT); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url('files\fusion\plugins\jspdf\jspdf.debug.js'); ?>"></script>
		<script src="<?php echo base_url('files\fusion\plugins\jspdf\jspdf.plugin.autotable.js'); ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_ACCOUNT_MANAGER:
	?>
		<script src="<?php echo base_url(SCRIPT_ACCOUNT_MANAGER_REPORT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_VENDOR:
	?>
		<script src="<?php echo base_url(SCRIPT_ACCOUNT_VENDOR); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_API_DOCUMENTATION:
	?>
		<script src="<?php echo base_url(SCRIPT_API_DOCUMENTATION); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_GRN_PRECHECK:
	?>
		<script src="<?php echo base_url(SCRIPT_GRN_PRECHECK); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_INBOUND_BOOKINGS:
	?>
		<script src="<?php echo base_url(SCRIPT_INTERNAL_INBOUND_BOOKINGS); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_CONSOLIDATION_REPORT:
	?>
		<script src="<?php echo base_url(SCRIPT_INTERNAL_CONSOLIDATION_REPORT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_DFR_REPORT:
		?>
			<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0"></script>
			<script src="<?php echo base_url(SCRIPT_INTERNAL_DFR_REPORT); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<?php
	break;
	case PAGE_INTERNAL_PICKPOT:
	?>
		<script src="<?php echo base_url(SCRIPT_INTERNAL_PICKPOT_REPORT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_CUBISCAN_EDITOR:
	?>
		<script src="<?php echo base_url(SCRIPT_CUBISCAN_SERIAL_EDITOR); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_STOCK_ADJUSTMENT:
	?>
		<script src="<?php echo base_url(SCRIPT_STOCK_ADJUSTMENT); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_STOCK_BY_LOCATION:
	?>
		<script src="<?php echo base_url(SCRIPT_STOCK_BY_LOCATION); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_SERIAL_NUMBER:
	?>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_SERIAL_RECORD); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_STOCK_MOVEMENT:
	?>
		<script src="<?php echo base_url(SCRIPT_STOCK_MANAGER_REPORT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_STOCK_MOVEMENT_HISTORIC:
	?>
		<script src="<?php echo base_url(SCRIPT_STOCK_MANAGER_REPORT_HISTORIC); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
	break;
	case PAGE_INTERNAL_WELSPUN_BILLING:
	?>
		<script src="<?php echo base_url(SCRIPT_WELSPUN_BILLING); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_SERVICE_MAPPING_TOOL:
	?>
		<script src="<?php echo base_url(SCRIPT_SERVICE_MAPPING_TOOL); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_RULES_ENGINE:
		?>
		<script src="<?php echo base_url()?>files/fusion/plugins/leaderline/leader-line.min.js"></script>
		<script src="<?php echo base_url(SCRIPT_RULES_ENGINE); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
		case PAGE_CREATE_NEWS_ARTICLE:
			?>
			<script src="https://cdn.tiny.cloud/1/fw7hnfl0ssi7fipu4ibx9kkubsexckc1sq92cssjqg2003as/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
			<script src="<?php echo base_url(SCRIPT_CREATE_NEWS_ARTICLE); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<?php
			break;
	case PAGE_INTERNAL_FAIRY_LOOT_BILLING:
	?>
		<script src="<?php echo base_url(SCRIPT_FAIRY_LOOT_BILLING); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_PRECIOUS_LITTLE_ONE_BILLING:
	?>
		<script src="<?php echo base_url(SCRIPT_PRECIOUS_LITTLE_ONE_BILLING); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_WELSPUN_DEBENHAMS:
	?>
		<script src="<?php echo base_url(SCRIPT_WELSPUN_DEBENHAMS_PRINT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_SHORT_SHIPPED_REPORT:
	?>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_ORDER); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_SHORT_SHIPPED_REPORT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_WELSPUN_REPLEN:
	?>
		<script src="<?php echo base_url(SCRIPT_WELSPUN_REPLEN_RECORD); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_MOVEMENT_BY_SKU:
	?>
		<script src="<?php echo base_url(SCRIPT_MOVEMENT_BY_SKU); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_SORTED_BILLING:
	?>
		<script src="<?php echo base_url(SCRIPT_SORTED_BILLING_UPLOAD); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INTERNAL_RECENTLY_PACKED:
	?>
		<script src="<?php echo base_url(SCRIPT_INTERNAL_RECENTLY_PACKED); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_CUSTOMER_FTP:
	?>
		<script src="<?php echo base_url(SCRIPT_CUSTOMER_FTP_FOLDER); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_REWORK_BOOK:
	?>
		<script src="<?php echo base_url(SCRIPT_REWORKS_BOOK); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_REWORK_VIEW:
	?>
		<script src="<?php echo base_url(SCRIPT_REWORKS_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_DELIVERY_NOTE_EDITOR:
	?>
		<script src="<?php echo base_url(SCRIPT_DELIVERY_NOTE_EDITOR); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_RETURNS:
	?>
		<script src="<?php echo base_url(SCRIPT_RETURNS_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_PRODUCT); ?>?<?php echo FUSION_VERSION; ?>"></script>
		<script src="<?php echo base_url(SCRIPT_MODAL_ORDER); ?>?<?php echo FUSION_VERSION; ?>"></script>

	<?php
		break;
	case PAGE_STAFF_NOTES:
	?>
		<script src="<?php echo base_url(SCRIPT_STAFF_NOTES); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_EXCEPTIONS:
	?>
		<script src="<?php echo base_url(SCRIPT_EXCEPTIONS_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_USER_CREATE:
	?>
		<script src="<?php echo base_url(SCRIPT_USERS_CREATE); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_USER_VIEW:
	?>
		<script src="<?php echo base_url(SCRIPT_USERS_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INBOUND_BOOKINGS:
	?>
		<script src="<?php echo base_url(SCRIPT_INBOUNDS_CREATE); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INBOUND_BOOKINGS_LINK:
	?>
		<script src="<?php echo base_url(SCRIPT_INBOUNDS_LINK); ?>?<?php echo FUSION_VERSION; ?>"></script>
	<?php
		break;
	case PAGE_INBOUND_BOOKINGS_VIEW:
	?>
		<script src="<?php echo base_url(SCRIPT_INBOUNDS_VIEW); ?>?<?php echo FUSION_VERSION; ?>"></script>
<?php
		break;
}
?>
<script src="<?php echo base_url(SCRIPT_LOGIN_INDEX); ?>?<?php echo FUSION_VERSION; ?>"></script>
<script src="<?php echo base_url(SCRIPT_GENERAL_BUTTONUTILITY); ?>?<?php echo FUSION_VERSION; ?>"></script>


</body>

</html>