<!DOCTYPE html>
<html>

<head>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-155109154-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-155109154-1');
    </script>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo SYSTEM_NAME; ?> - <?php echo $pageTitle; ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?php echo base_url('cropped-favicon-1-32x32.png'); ?>" <?php echo TYPE_IMAGE_XICON ?> />

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAP_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Waves Effect Css -->
    <link href="<?php echo base_url(THEME_NODEWAVES_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Animation Css -->
    <link href="<?php echo base_url(THEME_ANIMATECSS_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Sweet Alert Css -->
    <link href="<?php echo base_url(THEME_SWEETALERT_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Spinner Css -->
    <link href="<?php echo base_url(THEME_JQUERYSPINNER_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAPMATERIALDATETIMEPICKER_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Wait Me Css -->
    <link href="<?php echo base_url(THEME_WAITME_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Bootstrap Select Css -->
    <link href="<?php echo base_url(THEME_BOOTSTRAPSELECT_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- JQuery DataTable Css -->
    <link href="<?php echo base_url(THEME_JQUERYDATATABLE_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Dropzone Css -->
    <link href="<?php echo base_url(THEME_DROPZONE_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Custom Css -->
    <link href="<?php echo base_url(THEME_CUSTOM_CSS); ?>" <?php echo REL_STYLESHEET ?> />

    <!-- Custom DB Css -->
    <link href="<?php echo base_url(THEME_DB_CUSTOM_CSS); ?>?<?php echo FUSION_VERSION; ?>" <?php echo REL_STYLESHEET ?> />
    <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css">-->
    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="<?php echo base_url(THEME_ALLTHEMES_CSS); ?>" <?php echo REL_STYLESHEET ?> />
</head>

<?php
if ($currentPage !== PAGE_LOGIN_INDEX) {
    $companyCode = $loggedUser->company->code;
    switch ($companyCode) {
        
        case '1GA':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.jpg");
            break;
        case '2NA':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo2.jpg");
            break;
        case 'ACC':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.gif");
            break;
        case 'CBL':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.png");
            break;
        case 'FLL':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.png");
            break;
        case 'PLO':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.png");
            break;
        case 'PER':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo2.jpg");
            break;
        case 'WEL':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.png");
            break;
        case 'THE':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.jpg");
            break;
        case 'SIG':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.png");
            break;
        case 'SLM':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.png");
            break;
        case 'SOC':
            $profileImage = base_url("images/customers/$companyCode/" . $companyCode . "_logo.png");
            break;
        default:
            $profileImage = base_url("images/customers/3PL/3pl_logo.jpg");
            break;
    }
}

?>
<?php

if ($currentPage === PAGE_LOGIN_INDEX){ ?>

<body class="login-page" style="background-color: #E9E9E9;">
    <?php }
elseif ($currentPage === PAGE_REGISTER_INDEX){ ?>

    <body class="signup-page">
        <?php }
elseif ($currentPage === PAGE_ERROR_INDEX){ ?>

        <body class="five-zero-zero">
            <?php }else{ ?>

            <body class="theme-red">
                <!-- Page Loader -->
                <!--
    <div class="page-loader-wrapper" style="background-color: #EDEBED;">
        <div class="loader">
            <div class="col-lg-4 col-md-3 col-sm-2">
            </div>
            <div class="col-lg-4 col-md-6 col-sm-8 col-xs-12">
                <div style="margin-top: -30%;">
                    <img width="100%" height="100%" src="<?php echo base_url(DIR_THEME_LOADER); ?>">
                </div>
            </div>

        </div>
    </div>-->
                <!-- Page Loader -->
                <div class="page-loader-wrapper">
                    <div class="loader">
                        <div class="preloader">
                            <div class="spinner-layer pl-red">
                                <div class="circle-clipper left">
                                    <div class="circle"></div>
                                </div>
                                <div class="circle-clipper right">
                                    <div class="circle"></div>
                                </div>
                            </div>
                        </div>
                        <p>Please wait...</p>
                    </div>
                </div>
                <!-- #END# Page Loader -->
                <!-- Overlay For Sidebars -->
                <div class="overlay"></div>
                <!-- #END# Overlay For Sidebars -->
                <!-- Search Bar -->
                <div class="search-bar autocomplete">
                    <div class="search-icon">
                        <i class="material-icons">search</i>
                    </div>
                    <input type="text" name="fusionSearch" autocomplete="off" id="autocomplete" placeholder="Type Order Number...">
                    <div class="close-search">
                        <i class="material-icons">close</i>
                    </div>
                </div>
                <!-- #END# Search Bar -->
                <!-- Top Bar -->
                <nav class="navbar">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <a href="javascript:void(0);" class="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#navbar-collapse" aria-expanded="false"></a>
                            <a href="javascript:void(0);" class="bars"></a>
                            <a class="navbar-brand" href="<?php echo base_url(); ?>"><strong>3PL</strong> | FUSION</a>
                        </div>

                        <div class="collapse navbar-collapse" id="navbar-collapse">
                            <div class="nav navbar-nav navbar-left companyList" id="<?php echo DIV_COMPANY_LIST; ?>">
                                <?php if ($loggedUser != NULL and $loggedUser->group->id == 4): ?>
                                <form action='<?php echo base_url(); ?>main/staff_change_company'
                                    method='post'>
                                    <select id='userCompany' name='userCompany' class='form-control'></select>
                                    <input type='hidden' id='user' name='user' value=''>
                                    <button type='submit' class='btn btn-success'> <img
                                            src="<?php echo base_url(); ?>/files/AdminBSBMaterialDesign-master/images/outline_save_white_18dp.png"
                                            alt="load user"> </button>
                                </form>
                                <?php endif;?>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <!-- Call Search -->
                                <li><a href="javascript:void(0);" class="js-search" data-close="true"><i
                                            class="material-icons">search</i></a></li>
                                <li><a href="javascript:void(0);" id="showApi" class="js-api_key" data-close="true"><i
                                class="material-icons">vpn_key</i></a></li>
                                <!-- #END# Call Search -->
                                <!-- Notifications -->
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                        role="button">
                                        <i class="material-icons">notifications</i>
                                        <span class="label-count" id="notificationsLabel"></span>
                                    </a>
                                    
                                    <ul class="dropdown-menu">
                                        <li class="header">Exceptions</li>
                                        <li class="body">
                                            <ul class="menu" id="exceptionsList">
                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <a href="<?php echo base_url(); ?>exceptions/view">View All Notifications</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                        role="button">
                                        <i class="material-icons">mail_outline</i>
                                        <span class="label-count" id="newsLabel"></span>
                                    </a>
                                    
                                    <ul class="dropdown-menu">
                                        <li class="header">Latest News</li>
                                        <li class="body">
                                            <ul class="menu" id="newsList">
                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <a href="<?php echo base_url(); ?>helpdesk/latest_news">View All News Articles</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- #END# Notifications -->
                                <!-- Tasks -->
                                <li class="dropdown">
                                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
                                        role="button">
                                        <i class="material-icons">flag</i>
                                        <span class="label-count" id="reworkMessagesCount"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <li class="header">Rework Messages</li>
                                        <li class="body">
                                            <ul class="menu tasks" id="reworkMessagesList">
                                            </ul>
                                        </li>
                                        <li class="footer">
                                            <a href='<?php echo base_url(); ?>reworks/view/'>View All Messages</a>
                                        </li>
                                    </ul>
                                </li>
                                <!-- #END# Tasks -->
                                <li class="pull-right"><a href="javascript:void(0);" class="js-right-sidebar"
                                        data-close="true"><i class="material-icons">more_vert</i></a></li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- #Top Bar -->
                <section>

                    <!-- Left Sidebar -->
                    <aside id="leftsidebar" class="sidebar striped-bg">
                        <!-- User Info -->
                        <div class="user-info row-bg-overlay <?php if ($companyCode == '1GA') {echo 'ONEGA';} else echo $companyCode ?>" style="background-image: url(<?php echo $profileImage; ?>)">
<!--                             <div class="image">
                                <img src="" width="48" height="48" alt="User" />
                            </div> -->
                            <div class="info-container">
                                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="fullName">
                                    <?php echo $loggedUser->name->{"1"} . " " . $loggedUser->name->{"2"};?> </div>
                                <div class="email"><?php echo $loggedUser->email; ?></div>
                                <div class="btn-group user-helper-dropdown">
                                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                                    <ul class="dropdown-menu pull-right">
                                        <li><a href="<?php echo base_url(PAGE_LOGOUT_INDEX); ?>"><i class="material-icons">input</i>Sign Out</a></li>
                                    </ul>
                                    <div id="menu-close"><a href="javascript:void(0);" class="js-left-sidebar" data-close="true"><i class="material-icons">more_vert</i></a></div>

                                </div>
                            </div>
                        </div>
                        <!-- #User Info -->
                        <!-- Menu -->
                        <div class="menu">
                            <ul class="list">
                                <li class="header"><?php echo TITLE_MAIN_NAVIGATION; ?></li>
                                <li <?php if ($currentPage === PAGE_MAIN_INDEX): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo base_url(PAGE_MAIN_INDEX); ?>">
                                        <i class="material-icons"><?php echo ICON_HOME; ?></i>
                                        <span>Fusion <?php echo TITLE_HOME; ?></span>
                                    </a>
                                </li>

                                <!-- Product -->
                                <li <?php if ($currentPage === PAGE_PRODUCT_CREATE or $currentPage === PAGE_PRODUCT_CREATE_BULK or $currentPage === PAGE_PRODUCT_BULK_EDIT or $currentPage === PAGE_PRODUCT_VIEW or $currentPage === PAGE_PRODUCT_CURRENT_STOCK or $currentPage === PAGE_PRODUCT_STOCK_BREAKDOWN): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">gradient</i>
                                        <span>Products</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_PRODUCT_CREATE or $currentPage === PAGE_PRODUCT_CREATE_BULK or $currentPage === PAGE_PRODUCT_BULK_EDIT): ?>
                                            class="active" <?php endif; ?>>
                                            <a href="javascript:void(0)" class="menu-toggle">
                                                <i class="material-icons"><?php echo ICON_CREATE; ?></i>
                                                <span><?php echo TITLE_CREATE; ?> Product</span>
                                            </a>
                                            <ul class="ml-menu">
                                                <li <?php if ($currentPage === PAGE_PRODUCT_CREATE): ?> class="active" <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_PRODUCT_CREATE); ?>">Create Single Product</a>
                                                </li>
                                                
                                                <li <?php if ($currentPage === PAGE_PRODUCT_CREATE_BULK): ?> class="active" <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_PRODUCT_CREATE_BULK); ?>">Bulk Product Upload</a>
                                                </li>

<!--                                                 <li <?php if ($currentPage === PAGE_PRODUCT_BULK_EDIT): ?> class="active" <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_PRODUCT_BULK_EDIT); ?>">Edit Bulk Products</a>
                                                </li> -->
                                            </ul>
                                        </li>
                                    
                                        <li <?php if ($currentPage === PAGE_PRODUCT_VIEW or $currentPage === PAGE_PRODUCT_CURRENT_STOCK or $currentPage === PAGE_PRODUCT_STOCK_BREAKDOWN): ?>
                                            class="active" <?php endif; ?>>
                                            <a href="javascript:void(0)" class="menu-toggle">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span><?php echo TITLE_VIEW; ?> Products</span>
                                            </a>
                                            <ul class="ml-menu">
                                                <li <?php if ($currentPage === PAGE_PRODUCT_VIEW): ?> class="active"
                                                    <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_PRODUCT_VIEW); ?>">View All Products</a>
                                                </li>
                                                
                                                <li <?php if ($currentPage === PAGE_PRODUCT_CURRENT_STOCK): ?> class="active" <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_PRODUCT_CURRENT_STOCK); ?>">Stock Levels</a>
                                                </li>

                                                <li <?php if ($currentPage === PAGE_PRODUCT_STOCK_BREAKDOWN): ?> class="active" <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_PRODUCT_STOCK_BREAKDOWN); ?>">Stock Breakdown</a>
                                                </li>
                                
                                            </ul>
                                        </li>
                                    </ul>
                                </li>

                                <!-- Orders -->
                                <li <?php if ($currentPage === PAGE_ORDER_BACK_ORDERS_DETAILS or $currentPage === PAGE_ORDER_DELIVERY_NOTE or $currentPage === PAGE_PACKING_LIST_REPORT or $currentPage === PAGE_LINNWORKS_IMPORT_ERRORS or $currentPage === PAGE_ORDERS_CREATE or $currentPage === PAGE_ORDERS_VIEW or $currentPage === PAGE_ORDERS_CREATE_BULK): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">folder_open</i>
                                        <span>Orders</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_ORDERS_CREATE or $currentPage === PAGE_ORDERS_CREATE_BULK): ?> class="active" <?php endif; ?>>
                                            <a href="javascript:void(0)" class="menu-toggle">
                                                <i class="material-icons"><?php echo ICON_CREATE; ?></i>
                                                <span><?php echo TITLE_CREATE; ?> Orders</span>
                                            </a>
                                            <ul class="ml-menu">
                                                <li <?php if ($currentPage === PAGE_ORDERS_CREATE): ?> class="active"
                                                    <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_ORDERS_CREATE); ?>">Create Single Order</a>
                                                </li>
                                                <li <?php if ($currentPage === PAGE_ORDERS_CREATE_BULK): ?> class="active"
                                                    <?php endif; ?>>
                                                    <a href="<?php echo base_url(PAGE_ORDERS_CREATE_BULK); ?>">Bulk Orders</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_ORDERS_VIEW): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_ORDERS_VIEW); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span><?php echo TITLE_VIEW; ?> Orders</span>
                                            </a>
                                        </li>
<!--                                         <li <?php if ($currentPage === PAGE_ORDER_ADDRESS_UPDATE): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_ORDER_ADDRESS_UPDATE); ?>">
                                                <i class="material-icons">update</i>
                                                <span>Address & Service Update</span>
                                            </a>
                                        </li> -->
                                        <li <?php if ($currentPage === PAGE_PACKING_LIST_REPORT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_PACKING_LIST_REPORT); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span>Packing List Report</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_ORDER_DELIVERY_NOTE): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_ORDER_DELIVERY_NOTE); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span>Order Delivery Note</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_LINNWORKS_IMPORT_ERRORS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_LINNWORKS_IMPORT_ERRORS); ?>">
                                                <i class="material-icons">error_outline</i>
                                                <span>Linnworks Import Errors</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_ORDER_BACK_ORDERS_DETAILS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_ORDER_BACK_ORDERS_DETAILS); ?>">
                                                <i class="material-icons">error_outline</i>
                                                <span>Back Orders Details</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                
                                <!-- GRN -->
                                <li <?php if ($currentPage === PAGE_GRN_CREATE or $currentPage === PAGE_GRN_VIEW): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">low_priority</i>
                                        <span>Goods In Notifications</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_GRN_CREATE): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_GRN_CREATE); ?>">
                                                <i class="material-icons"><?php echo ICON_CREATE; ?></i>
                                                <span><?php echo TITLE_CREATE; ?> GRN</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_GRN_VIEW): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_GRN_VIEW); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span><?php echo TITLE_VIEW; ?> GRN</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <!-- INBOUND BOOKINGS -->
                                <li <?php if ($currentPage ===  PAGE_INBOUND_BOOKINGS_LINK or $currentPage === PAGE_INBOUND_BOOKINGS or $currentPage === PAGE_INBOUND_BOOKINGS_VIEW): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">send</i>
                                        <span>Inbound Bookings</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_INBOUND_BOOKINGS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INBOUND_BOOKINGS); ?>">
                                                <i class="material-icons"><?php echo ICON_CREATE; ?></i>
                                                <span><?php echo TITLE_CREATE; ?> Inbound Booking</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INBOUND_BOOKINGS_VIEW): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INBOUND_BOOKINGS_VIEW); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span><?php echo TITLE_VIEW; ?> Inbound Bookings</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INBOUND_BOOKINGS_LINK): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INBOUND_BOOKINGS_LINK); ?>">
                                                <i class="material-icons">link</i>
                                                <span>Link Unknown Inbound</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <!-- Returns -->
                                <li <?php if ($currentPage === PAGE_RETURNS): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo base_url(PAGE_RETURNS); ?>">
                                        <i class="material-icons">reply_all</i>
                                        <span>Returns</span>
                                    </a>
                                </li>

                                <!-- Exceptions -->
                                <li <?php if ($currentPage === PAGE_EXCEPTIONS): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo base_url(PAGE_EXCEPTIONS); ?>">
                                        <i class="material-icons">warning</i>
                                        <span>Exceptions</span>
                                    </a>
                                </li>

                                <!-- Reworks -->
                                <li <?php if ($currentPage === PAGE_REWORK_BOOK or $currentPage === PAGE_REWORK_VIEW): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">cached</i>
                                        <span>Reworks</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_REWORK_BOOK): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_REWORK_BOOK); ?>">
                                                <i class="material-icons">book</i>
                                                <span><?php echo TITLE_BOOK_REWORK; ?></span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_REWORK_VIEW): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_REWORK_VIEW); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span>View Reworks</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <!-- Customer Workspace -->
                                <li <?php if ($currentPage === PAGE_RULES_ENGINE or $currentPage === PAGE_SERVICE_MAPPING_TOOL or $currentPage ===  PAGE_ADDITIONAL_ACTIVITY or $currentPage === PAGE_API_DOCUMENTATION or $currentPage === PAGE_DELIVERY_NOTE_EDITOR or $currentPage === PAGE_INVOICES or $currentPage === PAGE_CUSTOM_REPORTS or $currentPage === PAGE_CUSTOMER_FTP): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">account_box</i>
                                        <span>My Workspace</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_API_DOCUMENTATION): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo PAGE_API_DOCUMENTATION; ?>" target="_blank">
                                                <i class="material-icons">vpn_key</i>
                                                <span>API Documentation</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_CUSTOM_REPORTS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_CUSTOM_REPORTS); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span>Custom Reports</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_DELIVERY_NOTE_EDITOR): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_DELIVERY_NOTE_EDITOR); ?>">
                                                <i class="material-icons">note</i>
                                                <span>Delivery Notes</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_CUSTOMER_FTP): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_CUSTOMER_FTP); ?>">
                                                <i class="material-icons">folder_shared</i>
                                                <span><?php echo TITLE_FTP_FOLDER; ?></span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INVOICES): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INVOICES); ?>">
                                                <i class="material-icons">payment</i>
                                                <span><?php echo TITLE_VIEW; ?> Invoices</span>
                                            </a>
                                        </li>
                                        <?php if ($loggedUser->company->rulesEngine): ?>
                                        <li <?php if ($currentPage === PAGE_RULES_ENGINE): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_RULES_ENGINE); ?>">
                                                <i class="material-icons">merge_type</i>
                                                <span>Rules Engine</span>
                                            </a>
                                        </li>
                                        <?php endif; ?>
                                        <li <?php if ($currentPage === PAGE_SERVICE_MAPPING_TOOL): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_SERVICE_MAPPING_TOOL); ?>">
                                                <i class="material-icons">compare_arrows</i>
                                                <span>Services Mapping Tool</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <!-- Reports -->
                                <li <?php if ($currentPage === PAGE_SERIAL_NUMBER or $currentPage === PAGE_STOCK_BY_LOCATION or $currentPage === PAGE_STOCK_ADJUSTMENT): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">assessment</i>
                                        <span>Reports</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_SERIAL_NUMBER): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_SERIAL_NUMBER); ?>">
                                                <i class="material-icons">format_list_numbered</i>
                                                <span>Serial Number Record</span>
                                            </a>
                                        </li>    
                                        <li <?php if ($currentPage === PAGE_STOCK_ADJUSTMENT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_STOCK_ADJUSTMENT); ?>">
                                                <i class="material-icons">assignment_turned_in</i>
                                                <span>Stock Adjustment Report</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_STOCK_BY_LOCATION): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_STOCK_BY_LOCATION); ?>">
                                                <i class="material-icons">format_list_bulleted</i>
                                                <span>Stock by Location</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <!-- Historic Reports -->
                                <li <?php if ($currentPage === PAGE_HISTORIC_ORDERS): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">history</i>
                                        <span>Historic Reports</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_HISTORIC_ORDERS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_HISTORIC_ORDERS); ?>">
                                                <i class="material-icons">format_list_numbered</i>
                                                <span>Orders</span>
                                            </a>
                                        </li>    
                                    </ul>
                                </li>

                                <!-- Customer HelpDesk -->
                                <li <?php if ($currentPage === PAGE_LATEST_NEWS or $currentPage === PAGE_VIEW_TICKETS or $currentPage === PAGE_CREATE_TICKET): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">record_voice_over</i>
                                        <span>Helpdesk</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_LATEST_NEWS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_LATEST_NEWS); ?>">
                                                <i class="material-icons">archive</i>
                                                <span>Latest 3PL News</span>
                                            </a>
                                        </li>    
                                        <li <?php if ($currentPage === PAGE_CREATE_TICKET): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_CREATE_TICKET); ?>">
                                                <i class="material-icons">hearing</i>
                                                <span><?php echo TITLE_CREATE; ?> Ticket</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_VIEW_TICKETS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_VIEW_TICKETS); ?>">
                                                <i class="material-icons">chat</i>
                                                <span><?php echo TITLE_VIEW; ?> Tickets</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>

                                <!-- Management Section - DB -->
                                <?php if ($loggedUser->group->id == 4): ?>
                                <li class="header"><?php echo TITLE_MANAGEMENT; ?></li>
                                <li <?php if ($currentPage === PAGE_COMPANY_CREATE or $currentPage === PAGE_COMPANY_VIEW or $currentPage === PAGE_COMPANY_EDIT): ?>class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">business</i>
                                        <span><?php echo TITLE_COMPANIES; ?></span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_COMPANY_CREATE): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_COMPANY_CREATE); ?>">
                                            <i class="material-icons"><?php echo ICON_CREATE; ?></i>
                                            <span><?php echo TITLE_CREATE; ?> Company</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_COMPANY_EDIT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_COMPANY_EDIT); ?>">
                                            <i class="material-icons">check_box</i>
                                            <span>Company Settings</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_COMPANY_VIEW): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_COMPANY_VIEW); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span><?php echo TITLE_VIEW . " All Companies"; ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li <?php if ($currentPage === PAGE_USER_CREATE or $currentPage === PAGE_USER_VIEW): ?>class="active" <?php endif; ?>>
                                <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">person_add</i>
                                        <span><?php echo TITLE_USERS; ?></span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_USER_CREATE): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_USER_CREATE); ?>">
                                            <i class="material-icons"><?php echo ICON_CREATE; ?></i>
                                            <span><?php echo TITLE_CREATE; ?> User</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_USER_VIEW): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_USER_VIEW); ?>">
                                                <i class="material-icons"><?php echo ICON_VIEW; ?></i>
                                                <span><?php echo TITLE_VIEW . " Users"; ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php endif;?>
                                <!-- DB-END -->

                                <!-- Staff Only -->
                                <?php if ($loggedUser->group->id == 1 or $loggedUser->group->id == 4): ?>
                                <li class="header"><?php echo TITLE_STAFF; ?></li>
                                <!-- Returns -->
                                <li <?php if ($currentPage === PAGE_STAFF_NOTES): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo base_url(PAGE_STAFF_NOTES); ?>">
                                        <i class="material-icons">reply_all</i>
                                        <span>Add Note to Order</span>
                                    </a>
                                </li>
                                <li <?php if ($currentPage === PAGE_CREATE_NEWS_ARTICLE): ?> class="active" <?php endif; ?>>
                                    <a href="<?php echo base_url(PAGE_CREATE_NEWS_ARTICLE); ?>">
                                        <i class="material-icons">reply_all</i>
                                        <span>Create 3PL News Article</span>
                                    </a>
                                </li>
                                <li <?php if ($currentPage === PAGE_INTERNAL_PRECIOUS_LITTLE_ONE_BILLING or $currentPage === PAGE_INTERNAL_WELSPUN_BILLING or $currentPage === PAGE_INTERNAL_FAIRY_LOOT_BILLING): ?> class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">web</i>
                                        <span>Company Billing Reports</span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_INTERNAL_FAIRY_LOOT_BILLING): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_FAIRY_LOOT_BILLING); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Fairy Loot Billing</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_PRECIOUS_LITTLE_ONE_BILLING): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_PRECIOUS_LITTLE_ONE_BILLING); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Precious Little One Billing</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_WELSPUN_BILLING): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_WELSPUN_BILLING); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Welspun Billing</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li <?php if ($currentPage === PAGE_INTERNAL_DFR_REPORT or $currentPage === PAGE_INTERNAL_STOCK_MOVEMENT_HISTORIC or $currentPage === PAGE_INTERNAL_PICKPOT or $currentPage === PAGE_INTERNAL_VENDOR or $currentPage === PAGE_INTERNAL_SHORT_SHIPPED_REPORT or $currentPage === PAGE_INTERNAL_CONSOLIDATION_REPORT or $currentPage === PAGE_INTERNAL_INBOUND_BOOKINGS or $currentPage === PAGE_INTERNAL_MOVEMENT_BY_SKU or $currentPage === PAGE_INTERNAL_RECENTLY_PACKED or $currentPage === PAGE_INTERNAL_SORTED_BILLING or $currentPage === PAGE_INTERNAL_WELSPUN_REPLEN or $currentPage === PAGE_INTERNAL_WELSPUN_DEBENHAMS or $currentPage === PAGE_INTERNAL_CUBISCAN_EDITOR or $currentPage === PAGE_INTERNAL_GRN_PRECHECK or $currentPage === PAGE_INTERNAL_ACCOUNT_MANAGER or $currentPage === PAGE_INTERNAL_STOCK_MOVEMENT): ?> class="active" <?php endif; ?>>
                                    <a href="javascript:void(0)" class="menu-toggle">
                                        <i class="material-icons">web</i>
                                        <span><?php echo TITLE_INTERNAL_REPORTS; ?></span>
                                    </a>
                                    <ul class="ml-menu">
                                        <li <?php if ($currentPage === PAGE_INTERNAL_ACCOUNT_MANAGER): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_ACCOUNT_MANAGER); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span><?php echo TITLE_ACOUNT_MANAGER_REPORT; ?></span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_DFR_REPORT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_DFR_REPORT); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>DFR Report</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_CONSOLIDATION_REPORT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_CONSOLIDATION_REPORT); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Consolidation Report</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_CUBISCAN_EDITOR): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_CUBISCAN_EDITOR); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span><?php echo TITLE_CUBISCAN_SERIAL_EDITOR; ?></span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_GRN_PRECHECK): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_GRN_PRECHECK); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span><?php echo TITLE_GRN_PRECHECK; ?></span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_INBOUND_BOOKINGS): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_INBOUND_BOOKINGS); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Inbound Bookings</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_MOVEMENT_BY_SKU): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_MOVEMENT_BY_SKU); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Movement by SKU</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_PICKPOT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_PICKPOT); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Pickpot Report</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_RECENTLY_PACKED): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_RECENTLY_PACKED); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Recently Packed Orders</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_SHORT_SHIPPED_REPORT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_SHORT_SHIPPED_REPORT); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Short Shipped Report</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_SORTED_BILLING): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_SORTED_BILLING); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span><?php echo TITLE_SORTED_BILLING_UPLOAD; ?></span>
                                            </a>
                                        </li>
                                        
                                        <li <?php if ($currentPage === PAGE_INTERNAL_STOCK_MOVEMENT): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_STOCK_MOVEMENT); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span><?php echo TITLE_STOCK_MOVEMENT_REPORT; ?></span>
                                            </a>
                                        </li>

                                        <li <?php if ($currentPage === PAGE_INTERNAL_STOCK_MOVEMENT_HISTORIC): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_STOCK_MOVEMENT_HISTORIC); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span><?php echo TITLE_STOCK_MOVEMENT_REPORT; ?> Historic</span>
                                            </a>
                                        </li>

                                        <li <?php if ($currentPage === PAGE_INTERNAL_VENDOR): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_VENDOR); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Vendor Report</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_WELSPUN_BILLING): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_WELSPUN_BILLING); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span>Welspun Billing</span>
                                            </a>
                                        </li>
                                        <li <?php if ($currentPage === PAGE_INTERNAL_WELSPUN_REPLEN): ?> class="active" <?php endif; ?>>
                                            <a href="<?php echo base_url(PAGE_INTERNAL_WELSPUN_REPLEN); ?>">
                                                <i class="material-icons">view_compact</i>
                                                <span><?php echo TITLE_WELSPUN_REQUIRED_REPLEN; ?></span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <?php endif; ?>

                            </ul>
                        </div>
                        <!-- #Menu -->
                        <!-- Footer -->
                        <div class="legal">
                            <div class="copyright">
                            <a href="javascript:void(0);">3PL Fusion </a> Version: <?php echo FUSION_VERSION; ?> &copy; 2020 <?php if (SYSTEM_TYPE == 'test' or SYSTEM_TYPE == 'TEST') {echo('TEST SYS');} ?>
                            </div>
                        </div>
                        <!-- #Footer -->
                    </aside>
                    <!-- #END# Left Sidebar -->
                    <!-- Right Sidebar -->
                    <aside id="rightsidebar" class="right-sidebar">
                        <ul class="nav nav-tabs tab-nav-right" role="tablist">
                            <li role="presentation" class="active"><a href="#skins" data-toggle="tab">SKINS</a></li>
                            <li role="presentation"><a href="#settings" data-toggle="tab">SETTINGS</a></li>
                        </ul>
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane fade in active in active" id="skins">
                                <ul class="demo-choose-skin">
                                    <li data-theme="red" class="active">
                                        <div class="red"></div>
                                        <span>Red</span>
                                    </li>
                                    <li data-theme="pink">
                                        <div class="pink"></div>
                                        <span>Pink</span>
                                    </li>
                                    <li data-theme="purple">
                                        <div class="purple"></div>
                                        <span>Purple</span>
                                    </li>
                                    <li data-theme="deep-purple">
                                        <div class="deep-purple"></div>
                                        <span>Deep Purple</span>
                                    </li>
                                    <li data-theme="indigo">
                                        <div class="indigo"></div>
                                        <span>Indigo</span>
                                    </li>
                                    <li data-theme="blue">
                                        <div class="blue"></div>
                                        <span>Blue</span>
                                    </li>
                                    <li data-theme="light-blue">
                                        <div class="light-blue"></div>
                                        <span>Light Blue</span>
                                    </li>
                                    <li data-theme="cyan">
                                        <div class="cyan"></div>
                                        <span>Cyan</span>
                                    </li>
                                    <li data-theme="teal">
                                        <div class="teal"></div>
                                        <span>Teal</span>
                                    </li>
                                    <li data-theme="green">
                                        <div class="green"></div>
                                        <span>Green</span>
                                    </li>
                                    <li data-theme="light-green">
                                        <div class="light-green"></div>
                                        <span>Light Green</span>
                                    </li>
                                    <li data-theme="lime">
                                        <div class="lime"></div>
                                        <span>Lime</span>
                                    </li>
                                    <li data-theme="yellow">
                                        <div class="yellow"></div>
                                        <span>Yellow</span>
                                    </li>
                                    <li data-theme="amber">
                                        <div class="amber"></div>
                                        <span>Amber</span>
                                    </li>
                                    <li data-theme="orange">
                                        <div class="orange"></div>
                                        <span>Orange</span>
                                    </li>
                                    <li data-theme="deep-orange">
                                        <div class="deep-orange"></div>
                                        <span>Deep Orange</span>
                                    </li>
                                    <li data-theme="brown">
                                        <div class="brown"></div>
                                        <span>Brown</span>
                                    </li>
                                    <li data-theme="grey">
                                        <div class="grey"></div>
                                        <span>Grey</span>
                                    </li>
                                    <li data-theme="blue-grey">
                                        <div class="blue-grey"></div>
                                        <span>Blue Grey</span>
                                    </li>
                                    <li data-theme="black">
                                        <div class="black"></div>
                                        <span>Black</span>
                                    </li>
                                </ul>
                            </div>
                            <div role="tabpanel" class="tab-pane fade" id="settings">
                                <div class="demo-settings">
                                    <p>GENERAL SETTINGS</p>
                                    <ul class="setting-list">
                                        <li>
                                            <span>Report Panel Usage</span>
                                            <div class="switch">
                                                <label><input type="checkbox" checked><span
                                                        class="lever"></span></label>
                                            </div>
                                        </li>
                                        <li>
                                            <span>Email Redirect</span>
                                            <div class="switch">
                                                <label><input type="checkbox"><span class="lever"></span></label>
                                            </div>
                                        </li>
                                        <li>
                                            <span>Dark Mode</span>
                                            <div class="switch">
                                                <label><input type="checkbox" id="darkMode"><span class="lever"></span></label>
                                            </div>
                                        </li>
                                    </ul>
                                    <p>SYSTEM SETTINGS</p>
                                    <ul class="setting-list">
                                        <li>
                                            <span>Notifications</span>
                                            <div class="switch">
                                                <label><input type="checkbox" checked><span
                                                        class="lever"></span></label>
                                            </div>
                                        </li>
                                        <li>
                                            <span>Auto Updates</span>
                                            <div class="switch">
                                                <label><input type="checkbox" checked><span
                                                        class="lever"></span></label>
                                            </div>
                                        </li>
                                    </ul>
                                    <p>ACCOUNT SETTINGS</p>
                                    <ul class="setting-list">
                                        <li>
                                            <span>Offline</span>
                                            <div class="switch">
                                                <label><input type="checkbox"><span class="lever"></span></label>
                                            </div>
                                        </li>
                                        <li>
                                            <span>Location Permission</span>
                                            <div class="switch">
                                                <label><input type="checkbox" checked><span
                                                        class="lever"></span></label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </aside>
                    <!-- #END# Right Sidebar -->
                </section>

                <section class="content">
                    <div class="container-fluid">
                        <?php } ?>