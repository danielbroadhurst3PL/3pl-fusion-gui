<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_user_model extends CI_Model
{
    public function __construct()
    {
         
    }

    public function put($databaseQuery)
    {
        $db = $this->load->database("default", TRUE);
        $db->query($databaseQuery);
        if ($db->affected_rows() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
        $db->close();
    }

}