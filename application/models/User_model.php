<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function __construct()
    {
         
    }

    public function api_key($userId)
    {
        $db = $this->load->database("default", true);
        $db->select("*");
        $db->from("api_keys");
        $db->where("user_id", $userId);
        $query = $db->get();
        if($query->num_rows() > 0):
            return $query->row();
        else:
            return false;
        endif;
        $db->close();
    }
}