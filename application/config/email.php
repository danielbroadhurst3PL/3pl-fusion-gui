<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Email
| -------------------------------------------------------------------------
| This file lets you define parameters for sending emails.
| Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/libraries/email.html
|	
*/

// Reminder that this is set up using smtp relay on the server to the mail server

/*
$config['protocol'] = 'smtp';
$config['smtp_host'] = 'localhost'; 
$config['smtp_port'] = '25';
$config['mailtype'] = 'html';
$config['charset'] = 'utf8';
$config['wordwrap'] = TRUE;
$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard
$config['crlf'] = "\r\n"; 
*/

$config['protocol'] = 'smtp';
$config['smtp_host'] = 'smtp.office365.com'; 
$config['smtp_user'] = 'No-Reply@3p-logistics.co.uk';
$config['smtp_pass'] = 'Password56$!';
$config['smtp_port'] = '587';
$config['mailtype'] = 'html';
$config['charset'] = 'utf8';
$config['wordwrap'] = TRUE;
$config['newline'] = "\r\n"; //use double quotes to comply with RFC 822 standard
$config['crlf'] = "\r\n"; 
$config['smtp_timeout'] = '120';
$config['smtp_crypto'] = 'tls';



/* End of file email.php */
/* Location: ./application/config/email.php */