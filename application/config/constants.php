<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/https://3plogistics.zendesk.com/agent/dashboard
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


/*
|--------------------------------------------------------------------------
| Dynamic Script loading
|--------------------------------------------------------------------------
|
|
*/

define('FUSION_VERSION', 'v1.35.15');

define('DIR_IMAGES', "images/");

define('IMAGE_LOGO', DIR_IMAGES . "logo.svg");

define('ICON_HOME', "home");
define('ICON_CREATE', "create");
define('ICON_VIEW', "view_list");

define('DIV_FILTER_TYPE', "divFilterType");
define('DIV_FILTER_ORDER_NUMBER', "divFilterOrderNumber");
define('DIV_FILTER_CUSTOMER_REFERENCE', "divFilterCustomerRef");
define('DIV_FILTER_DATE_SHIPPED_FROM', "divFilterShippedFrom");
define('DIV_FILTER_DATE_SHIPPED_TO', "divFilterShippedTo");
define('DIV_FILTER_STATUS', "divFilterStatus");
define('DIV_COMPANY_LIST', "divCompanyList");

// FIELDS
define('ACTIVE', "active");
define('AVERAGE_MONTHLY_UNIT_SALES', "averageMonthlyUnitSales");
define('THREE_PL_EMAIL', "logisticsEmail");
define('PRIMARY_EMAIL', "primaryEmail");
define('SECONDARY_EMAIL', "secondaryEmail");
define('ACCOUNT_TYPE', "accountType");
define('ADDRESS_LINE_1', "addressLine1");
define('ADDRESS_LINE_2', "addressLine2");
define('ALL', "all");
define('ALLOCATED', "allocated");
define('API_KEY', "apiKey");
define('API_KEY_2', "API-KEY");
define('API_DOCUMENTATION', 'api_documentation');
define('ASIN', "asin");
define('BACKORDERS', "backOrders");
define('BATCH', "batch");
define('BULK', "create_bulk");
define('BOOK', "book");
define('EDIT_BULK', "edit_bulk");
define('BARCODE', "barcode");
define('BULK_ERRORS', "bulk_errors");
define('BULK_STORAGE', "bulkStorage");
define('CREATE_MACS', "createMacs");
define('CHECKBOX', "checkbox");
define('CSS', "css");
define('CLOSE', "close");
define('CLOSED', "closed");
define('CURRENT_PAGE', "currentPage");
define('CUSTOM', "custom");
define('CUSTOMER_FTP_FOLDER','customer_ftp_folder');
define('CUSTOMER_FTP_TABLE','customer_ftp_table');
define('CURRENCY', "currency");
define('COUNTRY', "country");
define('COUNTRIES', "countries");
define('COUNTRIES_ID', "countriesId");
define('COMPANIES', "companies");
define('COMPANY', "company");
define('COMPANY_NAME', "companyName");
define('COMPANY_SELECT', COMPANY . "List");
define('COMPANY_CODE', "companyCode");
define('COMPLETED', "completed");
define('CUSTOMER_CODE', "customerCode");
define('COMPANYCODEHEADERSELECT', "companyCodeHeaderSelect");
define('COMPANY_ID', "companyId");
define('CREATE', "create");
define('CUSTOM_REPORTS', "custom_reports");
define('CUSTOMER_WORKSPACE', "customer_workspace");
define('COMMODITY_CODE', "commodityCode");
define('CURRENT_STOCK_LEVELS', "current_stock_levels");
define('STOCK_BREAKDOWN', "stock_breakdown");
define('DATE', "date");
define('DELETE_', "delete");
define('DATE_EXPIRY', "dateExpiry");
define('DEPTH', "depth");
define('DESCRIPTION', "description");
define('DETAILS', "details");
define('DELIVERY_NOTE', "deliveryNote");
define('DELIVERY_NOTE_EDITOR', "delivery_note_editor");
define('BOOK_ADDITIONAL_ACTIVITY', "book_additional_activity");
define('SERVICE_MAPPING_TOOL', "service_mapping_tool");
define('RULES_ENGINE', "rules_engine");
define('DELIVERY_NOTES', "deliverynotes");
define('DGN', "dgn");
define('DGN_TYPE', DGN . "Type");
define('DGN_TYPES', DGN_TYPE . "s");
define('DGN_DETAILS', DGN . "Details");
define('DISPATCH_DATE', "dateDespatch");
define('EDIT', "edit");
define('EXPORT_STOCK_LEVEL', "exportStockLevel");
define('EXTENSION_PHP', ".php");
define('EXCEPTIONS', "exceptions");
define('ERROR', "error");
define('ERRORS', ERROR . "s");
define('EMAIL', "email");
define('EMAIL_TRACKING', EMAIL . "Tracking");
define('EXPORT', 'export');
define('FREE', "free");
define('FOOTER', "footer");
define('FORM', "form");
define('FORM_ID', FORM . "Id");
define('FORM_BUTTON', FORM . "Button");
define('FORM_DATA', FORM . "Data");
define('FORM_FIELDS', "formFields");
define('GET', "get");
define('GET_METHOD', "GET");
define('GRN', "grn");
define('GRN_REFERENCE', GRN . "Reference");
define('GRNS', "grns");
define('HEIGHT', "height");
define('HEADER', "header");
define('ITEM', "item");
define('INVOICES', 'invoices');
define('INTERNAL_REPORTS', 'internal_reports');
define('INTERNAL_BILLING', 'internal_billing');
define('ITEMS', "items");
define('INBOUNDS', "inbounds");
define('ID', "id");
define('IMAGE', "image");
define('INDEX', "index");
define('IMPORT', 'import');
define('INSTRUCTIONS_PICKING', "instructionsPicking");
define('INSTRUCTIONS_PACKING', "instructionsPacking");
define('HOLD', "hold");
define('HISTORIC_REPORTS', "historic_reports");
define('KEY', "key");
define('KEY_DEV', "key_dev");
define('LICENCES', "licences");
define('LIFETIME_CODE', "lifetimeCode");
define('LITE_ACCOUNT', "liteAccount");
define('LINNWORKS', "linnworks");
define('LINNWORKS_IMPORT_ERRORS', LINNWORKS . '_import_errors');
define('PACKING_LIST_REPORT', 'packing_list_report');
define('LIVE', "live");
define('LOADER_CLASS', "loaderClass");
define('LOG', "log");
define('LOGS', LOG . "s");
define('LOGIN', "login");
define('LOGOUT', "logout");
define('LOADING', "loading");
define('LOGGED', "logged");
define('LOGGED_USER', LOGGED . "User");
define('LOGGED_USER_SESSION', LOGGED_USER . "Session");
define('NOTIFICATION', "notofication");
define('MAIN', "main");
define('MODAL', "modal");
define('MODALS', MODAL . "s");
define('NAME', "name");
define('NOTES', "notes");
define('NUMBER', "number");
define('FIRST_NAME', "nameFirst");
define('NEXT', "next");
define('TEST', "test");
define('TYPES', "types");
define('ON_PICK', "onPick");
define('ORDER_NUMBER', "orderNumber");
define('PASSWORD', "password");
define('ORDERS', "orders");
define('ORDER', "order");
define('PAGE', "page");
define('PHONE_NUMBER', "telephone");
define('PICKING_AISLE', "pickingAisle");
define('POST_METHOD', "POST");
define('PUT_METHOD', "PUT");
define('DELETE_METHOD', "DELETE");
define('PREVIOUS', "previous");
define('PROFILEPICTURE', "profilePicture");
define('PRODUCT', 'product');
define('QUANTITY', "quantity");
define('PRODUCTS', PRODUCT . 's');
define('OPEN', 'open');
define('ORDER_DELIVERY_NOTE', "order_delivery_note");
define('BACK_ORDERS_DETAILS', "back_orders_details");
define('ADDRESS_SERVICE_UPDATE', "address_service_update");
define('POSTCODE', "postCode");
define('QUARANTINE', "quarantine");
define('QUANTITY_INNER', "quantityInner");
define('QUANTITY_MASTER_CARTON', "quantityMasterCarton");
define('QUANTITY_PALLET', "quantityPallet");
define('LAST_NAME', "nameLast");
define('RADIO', "radio");
define('RADIO_VALUE', "Value");
define('REL', "rel");
define('REPORTS', "reports");
define('REWORK', "rework");
define('REWORKS', "reworks");
define('REGION', "region");
define('REGISTER', "register");
define('REMEMBER', "remember");
define('RETURNS', 'returns');
define('REQUIRED', "required");
define('REQUEST_METHOD', "REQUEST_METHOD");
define('REFERENCE_CUSTOMER', "referenceCustomer");
define('REFERENCE_PURCHASE_ORDER', "referencePurchaseOrder");
define('SELECT_', "select");
define('SERIAL', "serial");
define('SERVICE', "service");
define('SERVICES', "services");
define('SESSION', "session");
define('SHORT', "SHORT");
define('STYLESHEET', "stylesheet");
define('SUCCESS', "success");
define('SKU', "sku");
define('STOCK', "stock");
define('TABINDEX', "tabindex");
define('TABLE', "table");
define('TABLE_CUSTOM_ACTIONS', "tableCustomActions");
define('TEL', "tel");
define('TEXT', "text");
define('TEXTAREA', "textarea");
define('TITLE', "title");
define('TYPE', "type");
define('TOTAL', "total");
define('TOWN', "town");
define('URL_SUFFIX', "urlSuffix");
define('URL_SUFFIX_LOGIN', URL_SUFFIX . "Login");
define('UPDATING', "updating");
define('USER', "user");
define('USERS', USER . "s");
define('USER_ID', USER . "Id");
define('USER_COMPANY', USER . "Company");
define('VALUE', "value");
define('VIEW', "view");
define('VIEW_TICKETS', "view_tickets");
define('WEIGHT', "weight");
define('WIDTH', "width");
define('XICON', "x-icon");
define('CHECK', "check");
define('DIVSIZE', "divsize");
define('ORDER_STATUSES', "orderStatuses");
define('OPEN_ORDERS', 'openOrders');
define('STATUS', "status");
define('STATUSES', "statuses");
define('ORDER_FILTERS', "orderFilters");
define('DATE_SHIPPED_TO', "dateShippedTo");
define('DATE_SHIPPED_FROM', "dateShippedFrom");
define('PRIORITY', "priority");
define('AREA', 'area');
define('CATEGORY', 'category');
define('SUBJECT', 'subject');
define('FILE', 'file');
define('MULTIPLE', 'multiple');
define('HELPDESK', 'helpdesk');
define('CREATE_TICKET', 'create_ticket');
define('LATEST_NEWS', 'latest_news');
define('ORDER_FILTER', "orderFilter");
define('STATUS_FILTER', "statusFilter");
define('DATE_PACKED_TO', "datePackedTo");
define('DATE_PACKED_FROM', "datePackedFrom");



// API - 3PL
define('URL_3PL', '3p-logistics.co.uk/');
define('URL_3PL_API_LIVE', 'https://api.' . URL_3PL);
define('URL_3PL_API_TEST', 'https://api-staging.' . URL_3PL);
define('URL_3PL_DESIGNER_LIVE', 'https://designer.' . URL_3PL);
define('URL_3PL_DESIGNER_TEST', 'https://designer-staging.' . URL_3PL);
define('URL_3PL_API_MAXREDIRS', 10);
define('URL_3PL_API_TIMEOUT', 30);
define('URL_3PL_API_CACHECONTROL', "no-cache");
define('URL_3PL_API', 'url3plApi');
define('URL_3PL_DESIGNER', 'url3PLDesigner');

// ALTERTS
define('ALERT_SUCCESS', "alert-sucess");
define('ALERT_SUCCESS_TIMER', "1000");
define('ALERT_DANGER', "alert-danger");
define('ALERT_DANGER_TIMER', "6000");

// BUTTONS
define('BUTTON_CLASS', "btn");
define('BUTTON_WAVES', "waves-effect");
define('BUTTON_DEFAULT', BUTTON_CLASS . " btn-default " . BUTTON_WAVES);
define('BUTTON_BLUE', BUTTON_CLASS . " btn-primary " . BUTTON_WAVES);
define('BUTTON_RED', BUTTON_CLASS . " btn-danger " . BUTTON_WAVES);
define('BUTTON_PINK', BUTTON_CLASS . " bg-pink " . BUTTON_WAVES);
define('BUTTON_LINK', BUTTON_CLASS . " btn-link " . BUTTON_WAVES);
define('BUTTON_TOGGLEABLE', "buttonToggleable");
define('BUTTON_LOADER', "buttonLoader");
define('BUTTON_NEXT_PRODUCTS_VIEW', "buttonNextProdcutsView");
define('BUTTON_PREVIOUS_PRODUCTS_VIEW', "buttonPreviousProdcutsView");
define('BUTTON_PRODUCTS_VIEW', "buttonProductsView");
define('BUTTON_PAGE_MIN', "buttonPageMin");
define('BUTTON_PAGE_PREVIOUS', "buttonPagePrevious");
define('BUTTON_PAGE_CURRENT', "buttonPageCurrent");
define('BUTTON_PAGE_NEXT', "buttonPageNext");
define('BUTTON_PAGE_MAX', "buttonPageMax");
define('BUTTON_PREVIOUS', "buttonPrevious");
define('BUTTON_NEXT', "buttonNext");
define('BUTTON_PRODUCT_CREATE', "buttonProductCreate");
define('BUTTON_PRODUCT_EDIT', "buttonProductEdit");
define('BUTTON_ORDER_CREATE', "buttonOrderCreate");
define('BUTTON_SERVICE_MAPPING_CREATE', "buttonServiceMappingCreate");
define('BUTTON_RULES_ENGINE_CREATE', "buttonRulesEngineCreate");
define('BUTTON_ORDER_DELETE', "buttonOrderDelete");
define('BUTTON_ORDER_EDIT', "buttonOrderEdit");
define('BUTTON_ORDER_ADDRESS', "buttonOrderAddress");
define('BUTTON_ORDER_VIEW', "buttonOrderView");
define('BUTTON_COMPANY_CREATE', "buttonCompanyCreate");
define('BUTTON_COMPANY_EDIT', "buttonCompanyEdit");
define('BUTTON_USER_CREATE', "buttonUserCreate");
define('BUTTON_ITEM_ADD', "buttonItemAdd");
define('BUTTON_INBOUND_CREATE', "buttonInboundCreate");
define('BUTTON_EDIT', "buttonEdit");
define('BUTTON_ADD', "buttonAdd");
define('BUTTON_REMOVE', "buttonRemove");
define('BUTTON_GRN_CREATE', "buttonGRNCreate");
define('BUTTON_TICKET_CREATE', 'buttonTicketCreate');
define('BUTTON_REWORK_CREATE', "buttonReworkCreate");
define('BUTTON_CREATE_NEWS', "buttonCreateNews");

// GOOGLE
define('GOOGLE_FONTS_CSS', "https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext");
define('GOOGLE_FONTS_ICONS', "https://fonts.googleapis.com/icon?family=Material+Icons");

// DIRECTORIES
define('DIR_FILES', "files/");
define('DIR_PLUGINS', "plugins/");
define('DIR_BOOTSTRAP', "bootstrap/");
define('DIR_DROPZONE', "dropzone/");
define('DIR_CSS', "css/");
define('DIR_NODEWAVES', "node-waves/");
define('DIR_ANIMATECSS', "animate-css/");
define('DIR_SWEETALERT', "sweetalert/");
define('DIR_THEMES', "themes/");
define('DIR_JQUERY', "jquery/");
define('DIR_JS', "js/");
define('DIR_BOOTSTRAPSELECT', "bootstrap-select/");
define('DIR_JQUERYSLIMSCROLL', "jquery-slimscroll/");
define('DIR_JQUERY_SPINNER', "jquery-spinner/");
define('DIR_BOOTSTRAPMATERIALDATETIMEPICKER', "bootstrap-material-datetimepicker/");
define('DIR_WAITME', "waitme/");
define('DIR_JQUERYDATATABLE', "jquery-datatable/");
define('DIR_SKIN', "skin/");
define('DIR_BOOTSTRAPNOTIFY', "bootstrap-notify/");
define('DIR_JQUERYVALIDATION', "jquery-validation/");
define('DIR_JQUERYSTEPS', "jquery-steps/");


define('DIR_FUSION', DIR_FILES . "fusion/");
define('DIR_FUSION_PLUGINS', DIR_FUSION . DIR_PLUGINS);
define('DIR_FUSION_PLUGINS_JQUERYSPINNER', DIR_FUSION_PLUGINS . DIR_JQUERY_SPINNER);
define('DIR_FUSION_PLUGINS_JQUERYSPINNER_JS', DIR_FUSION_PLUGINS_JQUERYSPINNER . DIR_JS);

define('DIR_THEME', DIR_FILES . "AdminBSBMaterialDesign-master/");
define('DIR_THEME_JS', DIR_THEME . DIR_JS);
define('DIR_THEME_PLUGINS', DIR_THEME . DIR_PLUGINS);
define('DIR_THEME_PLUGINS_BOOTSTRAP', DIR_THEME_PLUGINS . DIR_BOOTSTRAP);
define('DIR_THEME_PLUGINS_BOOTSTRAP_CSS', DIR_THEME_PLUGINS_BOOTSTRAP . DIR_CSS);
define('DIR_THEME_PLUGINS_BOOTSTRAP_JS', DIR_THEME_PLUGINS_BOOTSTRAP . DIR_JS);
define('DIR_THEME_PLUGINS_NODEWAVES', DIR_THEME_PLUGINS . DIR_NODEWAVES);
define('DIR_THEME_PLUGINS_ANIMATECSS', DIR_THEME_PLUGINS . DIR_ANIMATECSS);
define('DIR_THEME_PLUGINS_SWEETALERT', DIR_THEME_PLUGINS . DIR_SWEETALERT);
define('DIR_THEME_PLUGINS_BOOTSTRAPSELECT', DIR_THEME_PLUGINS . DIR_BOOTSTRAPSELECT);
define('DIR_THEME_PLUGINS_BOOTSTRAPSELECT_JS', DIR_THEME_PLUGINS_BOOTSTRAPSELECT . DIR_JS);
define('DIR_THEME_PLUGINS_BOOTSTRAPSELECT_CSS', DIR_THEME_PLUGINS_BOOTSTRAPSELECT . DIR_CSS);
define('DIR_THEME_PLUGINS_JQUERYSLIMSCROLL', DIR_THEME_PLUGINS . DIR_JQUERYSLIMSCROLL);
define('DIR_THEME_PLUGINS_JQUERY_SPINNER', DIR_THEME_PLUGINS . DIR_JQUERY_SPINNER);
define('DIR_THEME_PLUGINS_JQUERY_SPINNER_CSS', DIR_THEME_PLUGINS_JQUERY_SPINNER . DIR_CSS);
define('DIR_THEME_PLUGINS_JQUERY_SPINNER_JS', DIR_THEME_PLUGINS_JQUERY_SPINNER . DIR_JS);
define('DIR_THEME_PLUGINS_BOOTSTRAPDATEPICKER', DIR_THEME_PLUGINS . "bootstrap-datepicker/"); 
define('DIR_THEME_PLUGINS_BOOTSTRAPDATEPICKER_CSS', DIR_THEME_PLUGINS_BOOTSTRAPDATEPICKER . DIR_CSS); 
define('DIR_THEME_PLUGINS_BOOTSTRAPMATERIALDATETIMEPICKER', DIR_THEME_PLUGINS . DIR_BOOTSTRAPMATERIALDATETIMEPICKER);
define('DIR_THEME_PLUGINS_BOOTSTRAPMATERIALDATETIMEPICKER_CSS', DIR_THEME_PLUGINS_BOOTSTRAPMATERIALDATETIMEPICKER . DIR_CSS);
define('DIR_THEME_PLUGINS_BOOTSTRAP_WAITME', DIR_THEME_PLUGINS . DIR_WAITME);
define('DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE', DIR_THEME_PLUGINS . DIR_JQUERYDATATABLE);
define('DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE_SKIN', DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE . DIR_SKIN);
define('DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE_SKIN_BOOTSTRAP', DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE_SKIN . DIR_BOOTSTRAP);
define('DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE_SKIN_BOOTSTRAP_CSS', DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE_SKIN_BOOTSTRAP . DIR_CSS);
define('DIR_THEME_PLUGINS_BOOTSTRAPNOTIFY', DIR_THEME_PLUGINS . DIR_BOOTSTRAPNOTIFY);
define('DIR_THEME_PLUGINS_JQUERYVALIDATION', DIR_THEME_PLUGINS . DIR_JQUERYVALIDATION);
define('DIR_THEME_PLUGINS_JQUERYSTEPS', DIR_THEME_PLUGINS . DIR_JQUERYSTEPS);
define('DIR_THEME_PLUGINS_DROPZONE', DIR_THEME_PLUGINS . DIR_DROPZONE);

define('DIR_THEME_CSS', DIR_THEME . DIR_CSS);
define('DIR_THEME_CSS_THEMES', DIR_THEME_CSS . DIR_THEMES);

define('DIR_THEME_PLUGINS_JQUERY', DIR_THEME_PLUGINS . DIR_JQUERY);

// DOWNLOAD LINKS
define('DOWN_PRODUCT_CREATE_BULK', "E:/Universal Files/Fusion v4 Download Location/Bulk Product Template/BulkProductsUpload.xlsx");
define('DOWN_ORDER_CREATE_BULK', "E:/Universal Files/Fusion v4 Download Location/Bulk Order Template/BulkOrdersUpload.xlsx");

// UPLOAD
define('UP_PRODUCT_CREATE_BULK', "E:/Universal Files/Bulk Product Upload Test/");
define('UP_ORDER_CREATE_BULK', "E:/Universal Files/Bulk Orders Upload Test/");

// THEME
define('THEME_ICON', DIR_THEME . "favicon.ico");
define('THEME_BOOTSTRAP_CSS', DIR_THEME_PLUGINS_BOOTSTRAP_CSS . "bootstrap.css");
define('THEME_BOOTSTRAP_JS', DIR_THEME_PLUGINS_BOOTSTRAP_JS . "bootstrap.js");
define('THEME_NODEWAVES_CSS', DIR_THEME_PLUGINS_NODEWAVES . "waves.css");
define('THEME_NODEWAVES_JS', DIR_THEME_PLUGINS_NODEWAVES . "waves.js");
define('THEME_ANIMATECSS_CSS', DIR_THEME_PLUGINS_ANIMATECSS . "animate.css");
define('THEME_SWEETALERT_CSS', DIR_THEME_PLUGINS_SWEETALERT . "sweetalert.css");
define('THEME_SWEETALERT_JS', DIR_THEME_PLUGINS_SWEETALERT . "sweetalert-dev.js");
define('THEME_CUSTOM_CSS', DIR_THEME_CSS . "style.css");
define('THEME_ALLTHEMES_CSS', DIR_THEME_CSS_THEMES . "all-themes.css");
define('THEME_JQUERYMIN_JS', DIR_THEME_PLUGINS_JQUERY . "jquery.min.js");
define('THEME_BOOSTRAPSELECT_JS', DIR_THEME_PLUGINS_BOOTSTRAPSELECT_JS . "bootstrap-select.js");
define('THEME_JQUERYSLIMSCROLL_JS', DIR_THEME_PLUGINS_JQUERYSLIMSCROLL . "jquery.slimscroll.js");
define('THEME_ADMIN_JS', DIR_THEME_JS . "admin.js");
define('THEME_DEMO_JS', DIR_THEME_JS . "demo.js");
define('THEME_JQUERYSPINNER_CSS', DIR_THEME_PLUGINS_JQUERY_SPINNER_CSS . "bootstrap-spinner.min.css");
define('THEME_BOOTSTRAPMATERIALDATETIMEPICKER_CSS', DIR_THEME_PLUGINS_BOOTSTRAPMATERIALDATETIMEPICKER_CSS . "bootstrap-material-datetimepicker.css");
define('THEME_BOOTSTRAPDATEPICKER_CSS', DIR_THEME_PLUGINS_BOOTSTRAPDATEPICKER_CSS . "bootstrap-datepicker.css");
define('THEME_WAITME_CSS', DIR_THEME_PLUGINS_BOOTSTRAP_WAITME . "waitMe.css");
define('THEME_BOOTSTRAPSELECT_CSS', DIR_THEME_PLUGINS_BOOTSTRAPSELECT_CSS . "bootstrap-select.css");
define('THEME_JQUERYDATATABLE_CSS', DIR_THEME_PLUGINS_BOOTSTRAP_JQUERYDATATABLE_SKIN_BOOTSTRAP_CSS . "dataTables.bootstrap.css");
define('THEME_BOOTSTRAPNOTIY_JS', DIR_THEME_PLUGINS_BOOTSTRAPNOTIFY . "bootstrap-notify.js");
define('THEME_JQUERYVALIDATION_JS', DIR_THEME_PLUGINS_JQUERYVALIDATION . "jquery.validate.js");
define('THEME_JQUERYSTEPS_JS', DIR_THEME_PLUGINS_JQUERYSTEPS . "jquery.steps.js");
define('THEME_JQUERYSPINNER_JS', DIR_FUSION_PLUGINS_JQUERYSPINNER_JS . "jquery.spinner.js");
define('THEME_DROPZONE_CSS', DIR_THEME_PLUGINS_DROPZONE . "dropzone.css");
define('THEME_DROPZONE_JS', DIR_THEME_PLUGINS_DROPZONE . "dropzone.js");
define('THEME_DB_CUSTOM_CSS', DIR_THEME_CSS . "db-styles.css");

// DIRECTORIES
define('DIR_FILES_THEME', DIR_FILES . "AdminBSBMaterialDesign-master/");
define('DIR_FILES_THEME_PLUGINS', DIR_FILES_THEME . "plugins/");
define('DIR_FILES_THEME_CSS', DIR_FILES_THEME . "css/");
define('DIR_FILES_THEME_IMAGES', DIR_FILES_THEME . "images/");
define('DIR_FILES_FUSION', DIR_FILES . "fusion/");
define('DIR_CUSTOM_SCRIPTS', DIR_FILES_FUSION . "js/");
define('DIR_CUSTOM_SCRIPTS_PAGES', DIR_FILES_FUSION . "pages/");
define('DIR_3PLAPI', DIR_CUSTOM_SCRIPTS_PAGES . "3plapi/");
define('DIR_CUSTOM_SCRIPTS_GENERAL', DIR_FILES_FUSION . "general/");
define('DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD', DIR_CUSTOM_SCRIPTS_GENERAL . "standard/");
define('DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES', DIR_CUSTOM_SCRIPTS_GENERAL . "utilities/");
define('DIR_CUSTOM_SCRIPTS_GENERAL_MODALS', DIR_CUSTOM_SCRIPTS_GENERAL . "modals/");

// DIRECTORIES - THEME
define('DIR_THEME_FAVICON', DIR_FILES_THEME . "favicon.ico");

// DIRECTORIES - THEME CSS
define('DIR_THEME_STYLE', DIR_FILES_THEME_CSS . "style.css");
define('DIR_THEME_ALL_THEMES', DIR_FILES_THEME_CSS . "themes/all-themes.css");

// DIRECTORIES - THEME IMAGES
define('DIR_THEME_PROFILE_PICUTRE', DIR_FILES_THEME_IMAGES . "user.png");

// DIRECTORIES - THEME PLUGINS
define('DIR_THEME_SCRIPTS_JQUERY', DIR_FILES_THEME_PLUGINS . "jquery/jquery.min.js");
define('DIR_THEME_SCRIPTS_JQUERYSPINNER', DIR_FILES_THEME_PLUGINS . "jquery-spinner-new/js/jquery.spinner.js");
define('DIR_THEME_CSS_JQUERYSPINNER', DIR_FILES_THEME_PLUGINS . "jquery-spinner-new/css/bootstrap-spinner.min.css");
define('DIR_THEME_BOOTSTRAP_CSS', DIR_FILES_THEME_PLUGINS . "bootstrap/css/bootstrap.css");
define('DIR_THEME_BOOTSTRAP_JS', DIR_FILES_THEME_PLUGINS . "bootstrap/js/bootstrap.js");
define('DIR_THEME_BOOTSTRAP_SELECT_JS', DIR_FILES_THEME_PLUGINS . "bootstrap-select/js/bootstrap-select.js");
define('DIR_THEME_WAVES', DIR_FILES_THEME_PLUGINS . "node-waves/waves.css");
define('DIR_THEME_ANIMATE', DIR_FILES_THEME_PLUGINS . "animate-css/animate.css");
define('DIR_THEME_SWEETALERT', DIR_FILES_THEME_PLUGINS . "sweetalert/sweetalert.css");
define('DIR_THEME_BOOTSTRAP_DATETIMEPICKER', DIR_FILES_THEME_PLUGINS . "bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css");
define('DIR_THEME_WAITME', DIR_FILES_THEME_PLUGINS . "waitme/waitMe.css");
define('DIR_THEME_BOOTSTRAP_SELECT', DIR_FILES_THEME_PLUGINS . "bootstrap-select/css/bootstrap-select.css");
define('DIR_THEME_JQUERY_DATATABLE', DIR_FILES_THEME_PLUGINS . "jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css");
define('DIR_THEME_SLIMSCROLL', DIR_FILES_THEME_PLUGINS . "jquery-slimscroll/jquery.slimscroll.js");
define('DIR_THEME_BOOTSTRAP_NOTIFY', DIR_FILES_THEME_PLUGINS . "bootstrap-notify/bootstrap-notify.js");
define('DIR_THEME_VALIDATION', DIR_FILES_THEME_PLUGINS . "jquery-validation/jquery.validate.js");
define('DIR_THEME_STEPS', DIR_FILES_THEME_PLUGINS . "jquery-steps/jquery.steps.js");
define('DIR_THEME_SWEETALERT_JS', DIR_FILES_THEME_PLUGINS . "sweetalert/sweetalert-dev.js");
define('DIR_THEME_WAVES_JS', DIR_FILES_THEME_PLUGINS . "node-waves/waves.js");
define('DIR_THEME_AUTOSIZE_JS', DIR_FILES_THEME_PLUGINS . "autosize/autosize.js");
define('DIR_THEME_AUTOSIZE_TEXTAREA_JS', DIR_FILES_THEME_PLUGINS . "textarea-autosize/textarea-autosize.js");
define('DIR_THEME_MOMENT_JS', DIR_FILES_THEME_PLUGINS . "momentjs/moment.js");
define('DIR_THEME_MATERIALDATETIMEPICKER_JS', DIR_FILES_THEME_PLUGINS . "bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js");
define('DIR_THEME_BOOTSTRAP_DATEPICKER', DIR_FILES_THEME_PLUGINS . "bootstrap-datepicker/js/bootstrap-datepicker.js");
define('DIR_THEME_JS_COUNT_TO', DIR_FILES_THEME_PLUGINS . "jquery-countto/jquery.countTo.js");
define('DIR_THEME_DEMO', DIR_FILES_THEME . "js/demo.js");

// DIRECTORIES - CUSTOM
define('DIR_THEME_LOADER', DIR_FILES_FUSION . "/loader/white-g.gif");

// DIRECTORIES - SCRIPTS GENERAL STANDARD
define('SCRIPT_GENERAL_BASIC', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "basic.js");
define('SCRIPT_GENERAL_DATETIMEPICKER', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "datetimepicker.js");
define('SCRIPT_GENERAL_DISABLEKEYS', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "disable_keys.js");
define('SCRIPT_GENERAL_FORM', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "form.js");
define('SCRIPT_GENERAL_INFOBOX', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "infobox.js");
define('SCRIPT_GENERAL_NOTIFICATIONS', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "notifications.js");
define('SCRIPT_GENERAL_TABLES', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "tables.js");
define('SCRIPT_GENERAL_THEME', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "theme.js");
define('SCRIPT_GENERAL_COMPANY', DIR_CUSTOM_SCRIPTS_GENERAL_STANDARD . "company.js");

// DIRECTORIES - SCRIPTS GENERAL UTILITIES
define('SCRIPT_GENERAL_COMMON', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "common.js");
define('SCRIPT_GENERAL_COUNTRIES', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "countries.js");
define('SCRIPT_GENERAL_CURRENCY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "currency.js");
define('SCRIPT_GENERAL_DELIVERYSERVICES', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "deliveryServices.js");
define('SCRIPT_GENERAL_DELIVERYNOTEUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "deliveryNoteUtility.js");
define('SCRIPT_GENERAL_WAREHOUSENOTEUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "warehouseUtility.js");
define('SCRIPT_GENERAL_ALERTUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "alertUtility.js");
define('SCRIPT_GENERAL_BUTTONUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "buttonUtility.js");
define('SCRIPT_GENERAL_ELEMENTUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "elementUtility.js");
define('SCRIPT_GENERAL_DATETIMEUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "dateTimeUtility.js");
define('SCRIPT_GENERAL_LOGSUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "logsUtility.js");
define('SCRIPT_GENERAL_COMPANYUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "companyUtility.js");
define('SCRIPT_GENERAL_VALIDATION', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "validation.js");
define('SCRIPT_GENERAL_ADDRESS', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "address.js");
define('SCRIPT_GENERAL_ORDER', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "order.js");
define('SCRIPT_GENERAL_RANDOM', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "random.js");
define('CSS_GENERAL_CUSTOM', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "custom.css");
define('SCRIPT_GENERAL_TABLEUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "tableUtility.js");
define('SCRIPT_GENERAL_COUNTRYUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "countriesUtility.js");
define('SCRIPT_GENERAL_CURRENCYUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "currencyUtility.js");
define('SCRIPT_GENERAL_COMPANYCODEHEADERUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "companyCode.js");
define('SCRIPT_GENERAL_DGNUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "dgnUtility.js");
define('SCRIPT_GENERAL_SERVICEUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "serviceUtility.js");
define('SCRIPT_GENERAL_ITEMSUTILITY', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "itemsUtility.js");
define('SCRIPT_GENERAL_SELECT', DIR_CUSTOM_SCRIPTS_GENERAL_UTILITIES . "select.js");
// DIRECTORIES - SCRIPTS GENERAL MODALS
define('SCRIPT_GENERAL_EDITADDRESSMODAL', DIR_CUSTOM_SCRIPTS_GENERAL_MODALS . "editAddressModal.js");
define('SCRIPT_GENERAL_VIEWNEWSMODAL', DIR_CUSTOM_SCRIPTS_GENERAL_MODALS . "view_news.js");
define('SCRIPT_GENERAL_VIEWCOMPANIESMODAL', DIR_CUSTOM_SCRIPTS_GENERAL_MODALS . "view_companies.js");
define('SCRIPT_GENERAL_SORTEDDETAILSMODAL', DIR_CUSTOM_SCRIPTS_GENERAL_MODALS . "sortedDetailsModal.js");
define('SCRIPT_GENERAL_CARRIERSERVICESMODAL', DIR_CUSTOM_SCRIPTS_GENERAL_MODALS . "carrierServicesModal.js");
define('SCRIPT_GENERAL_LOGSMODAL', DIR_CUSTOM_SCRIPTS_GENERAL_MODALS . "logsModal.js");

// DIRECTORIES - SCRIPTS PAGES
define('SCRIPT_LOGIN_INDEX', DIR_CUSTOM_SCRIPTS_PAGES . LOGIN . "/" . INDEX . ".js");
define('SCRIPT_PRODUCT_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . VIEW . ".js");
define('SCRIPT_PRODUCT_VIEW_BULK_ERROR', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . VIEW . "_" . BULK_ERRORS . ".js");
define('SCRIPT_MODAL_PRODUCT', DIR_CUSTOM_SCRIPTS_PAGES . MODAL . "/" . PRODUCT . ".js");
define('SCRIPT_PRODUCT_CREATE', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . CREATE . ".js");
define('SCRIPT_PRODUCT_CREATE_BULK', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . 'create_bulk' . ".js");
define('SCRIPT_PRODUCT_BULK_EDIT', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . 'edit_bulk' . ".js");
define('SCRIPT_PRODUCT_EDIT_BULK', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . EDIT . "_" . BULK_ERRORS . ".js");
define('SCRIPT_PRODUCT_EDIT', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . EDIT . ".js");
define('SCRIPT_PRODUCT_CURRENT_STOCK', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . CURRENT_STOCK_LEVELS . ".js");
define('SCRIPT_PRODUCT_STOCK_BREAKDOWN', DIR_CUSTOM_SCRIPTS_PAGES . PRODUCT . "/" . STOCK_BREAKDOWN . ".js");
define('SCRIPT_ORDERS_CREATE', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . CREATE . ".js");
define('SCRIPT_ORDERS_CREATE_BULK', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . 'create_bulk' . ".js");
define('SCRIPT_ORDERS_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . VIEW . ".js");
define('SCRIPT_ORDERS_VIEW_DELIVERY_NOTE', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . ORDER_DELIVERY_NOTE . ".js");
define('SCRIPT_ORDERS_LINNWORKS_IMPORT_ERRORS', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . LINNWORKS_IMPORT_ERRORS . ".js");
define('SCRIPT_ORDERS_BACK_ORDER_DETAILS', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . BACK_ORDERS_DETAILS . ".js");
define('SCRIPT_ORDERS_ADDRESS_UPDATE', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . ADDRESS_SERVICE_UPDATE . ".js");
define('SCRIPT_ORDERS_PACKING_LIST_REPORT', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . PACKING_LIST_REPORT . ".js");
define('SCRIPT_ORDERS_EDIT', DIR_CUSTOM_SCRIPTS_PAGES . ORDERS . "/" . EDIT . ".js");
define('SCRIPT_MODAL_ORDER', DIR_CUSTOM_SCRIPTS_PAGES . MODAL . "/" . ORDER . ".js");
define('SCRIPT_GRN_CREATE', DIR_CUSTOM_SCRIPTS_PAGES . GRN. "/" . CREATE . ".js");
define('SCRIPT_GRN_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . GRN. "/" . VIEW . ".js");
define('SCRIPT_GRN_CREATE_BULK', DIR_CUSTOM_SCRIPTS_PAGES . GRN. "/" . BULK . ".js");
define('SCRIPT_CUSTOMER_WORKSPACE_INVOICES', DIR_CUSTOM_SCRIPTS_PAGES . CUSTOMER_WORKSPACE. "/" . INVOICES . ".js");
define('SCRIPT_CUSTOMER_WORKSPACE_CUSTOM_REPORTS', DIR_CUSTOM_SCRIPTS_PAGES . CUSTOMER_WORKSPACE. "/" . CUSTOM_REPORTS . ".js");
define('SCRIPT_CUSTOMER_FTP_FOLDER', DIR_CUSTOM_SCRIPTS_PAGES . CUSTOMER_WORKSPACE. "/" . CUSTOMER_FTP_FOLDER . ".js");
define('SCRIPT_DELIVERY_NOTE_EDITOR', DIR_CUSTOM_SCRIPTS_PAGES . CUSTOMER_WORKSPACE. "/" . DELIVERY_NOTE_EDITOR . ".js");
define('SCRIPT_SERVICE_MAPPING_TOOL', DIR_CUSTOM_SCRIPTS_PAGES . CUSTOMER_WORKSPACE. "/" . SERVICE_MAPPING_TOOL . ".js");
define('SCRIPT_RULES_ENGINE', DIR_CUSTOM_SCRIPTS_PAGES . CUSTOMER_WORKSPACE. "/" . RULES_ENGINE . ".js");
define('SCRIPT_CREATE_NEWS_ARTICLE', DIR_CUSTOM_SCRIPTS_PAGES . "staff/create_news_article.js");
define('SCRIPT_API_DOCUMENTATION', DIR_CUSTOM_SCRIPTS_PAGES . CUSTOMER_WORKSPACE. "/" . API_DOCUMENTATION . ".js");
define('SCRIPT_ALL', DIR_CUSTOM_SCRIPTS_PAGES . ALL . ".js");
define('SCRIPT_HOME', DIR_CUSTOM_SCRIPTS_PAGES . ICON_HOME . ".js");
define('SCRIPT_COMPANY_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . COMPANY . "/" . VIEW . ".js");
define('SCRIPT_MODAL_COMPANY', DIR_CUSTOM_SCRIPTS_PAGES . MODAL . "/" . COMPANY . ".js");
define('SCRIPT_COMPANY_CREATE', DIR_CUSTOM_SCRIPTS_PAGES . COMPANY . "/" . CREATE . ".js");
define('SCRIPT_COMPANY_EDIT', DIR_CUSTOM_SCRIPTS_PAGES . COMPANY . "/" . EDIT . ".js");
define('SCRIPT_COMPANY_LIST', DIR_CUSTOM_SCRIPTS_PAGES . COMPANY . "/" . "list" . ".js");
define('SCRIPT_USERS_CREATE', DIR_CUSTOM_SCRIPTS_PAGES . USERS . "/" . CREATE . ".js");
define('SCRIPT_USERS_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . USERS . "/" . VIEW . ".js");
define('SCRIPT_CREATE_TICKET', DIR_CUSTOM_SCRIPTS_PAGES . HELPDESK . "/" . CREATE . "_ticket.js");
define('SCRIPT_LATEST_NEWS', DIR_CUSTOM_SCRIPTS_PAGES . HELPDESK . "/" . LATEST_NEWS . ".js");
define('SCRIPT_VIEW_TICKETS', DIR_CUSTOM_SCRIPTS_PAGES . HELPDESK . "/" . VIEW . "_tickets.js");
define('SCRIPT_ACCOUNT_MANAGER_REPORT', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "account_manager_report.js");
define('SCRIPT_ACCOUNT_VENDOR', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "vendor_report.js");
define('SCRIPT_STOCK_MANAGER_REPORT', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "stock_movement_report.js");
define('SCRIPT_STOCK_MANAGER_REPORT_HISTORIC', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "stock_movement_historic_report.js");
define('SCRIPT_GRN_PRECHECK', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "grn_precheck_report.js");
define('SCRIPT_INTERNAL_INBOUND_BOOKINGS', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "inbound_bookings.js");
define('SCRIPT_INTERNAL_CONSOLIDATION_REPORT', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "consolidation_report.js");
define('SCRIPT_INTERNAL_DFR_REPORT', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "dfr_report.js");
define('SCRIPT_INTERNAL_PICKPOT_REPORT', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "pickpot_report.js");
define('SCRIPT_CUBISCAN_SERIAL_EDITOR', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "cubiscan_serial_editor.js");
define('SCRIPT_STOCK_ADJUSTMENT', DIR_CUSTOM_SCRIPTS_PAGES . REPORTS . "/" . "stock_adjustment.js");
define('SCRIPT_STOCK_BY_LOCATION', DIR_CUSTOM_SCRIPTS_PAGES . REPORTS . "/" . "stock_by_location.js");
define('SCRIPT_SERIAL_RECORD', DIR_CUSTOM_SCRIPTS_PAGES . REPORTS . "/" . "serial_number_record.js");
define('SCRIPT_WELSPUN_DEBENHAMS_PRINT', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "welspun_print_debenhams_delivery.js");
define('SCRIPT_SHORT_SHIPPED_REPORT', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "short_shipped_report.js");
define('SCRIPT_WELSPUN_BILLING', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_BILLING . "/" . "welspun_billing.js");
define('SCRIPT_FAIRY_LOOT_BILLING', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_BILLING . "/" . "fairy_loot_billing.js");
define('SCRIPT_PRECIOUS_LITTLE_ONE_BILLING', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_BILLING . "/" . "precious_little_one_billing.js");
define('SCRIPT_WELSPUN_REPLEN_RECORD', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "welspun_required_replen_record.js");
define('SCRIPT_MOVEMENT_BY_SKU', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "movement_by_sku.js");
define('SCRIPT_SORTED_BILLING_UPLOAD', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "sorted_billing_upload.js");
define('SCRIPT_INTERNAL_RECENTLY_PACKED', DIR_CUSTOM_SCRIPTS_PAGES . INTERNAL_REPORTS . "/" . "recently_packed_orders.js");
define('SCRIPT_RETURNS_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . RETURNS . "/" . VIEW . ".js");
define('SCRIPT_EXCEPTIONS_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . EXCEPTIONS . "/" . VIEW . ".js");
define('SCRIPT_STAFF_NOTES', DIR_CUSTOM_SCRIPTS_PAGES . "staff/create_order_note.js");
define('SCRIPT_INBOUNDS_CREATE', DIR_CUSTOM_SCRIPTS_PAGES . INBOUNDS . "/" . CREATE . ".js");
define('SCRIPT_INBOUNDS_LINK', DIR_CUSTOM_SCRIPTS_PAGES . INBOUNDS . "/link.js");
define('SCRIPT_INBOUNDS_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . INBOUNDS . "/" . VIEW . ".js");
define('SCRIPT_REWORKS_BOOK', DIR_CUSTOM_SCRIPTS_PAGES . REWORKS. "/" . BOOK . ".js");
define('SCRIPT_REWORKS_VIEW', DIR_CUSTOM_SCRIPTS_PAGES . REWORKS. "/" . VIEW . ".js");
define('SCRIPT_HISTORIC_ORDERS', DIR_CUSTOM_SCRIPTS_PAGES . HISTORIC_REPORTS . "/" . ORDERS . ".js");

// DIRECTORIES - 3PL API SCRIPTS
define('SCRIPT_3PLAPI_API', DIR_3PLAPI . "api.js");

// MODALS
define('MODAL_PRODUCT', "modalProduct");
define('MODAL_PRODUCT_EDIT', "modalProductEdit");
define('MODAL_PRODUCT_TITLE', MODAL_PRODUCT . "Title");
define('MODAL_PRODUCT_LABEL', MODAL_PRODUCT . "Label");
define('MODAL_PRODUCT_STOCK', MODAL_PRODUCT . "Stock");
define('MODAL_PRODUCT_VIEW', MODAL_PRODUCT . "View");
define('MODAL_PRODUCT_LOGS', MODAL_PRODUCT . "Logs");
define('MODAL_PRODUCT_TOTAL', MODAL_PRODUCT . "Total");
define('MODAL_PRODUCT_QUANTITY', MODAL_PRODUCT . "Quantity");
define('MODAL_PRODUCT_ASSIGNED', MODAL_PRODUCT . "Assigned");
define('MODAL_PRODUCT_ALLOCATED', MODAL_PRODUCT . "Allocated");
define('MODAL_PRODUCT_FREE', MODAL_PRODUCT . "Free");
define('MODAL_PRODUCT_ON_PICK', MODAL_PRODUCT . "OnPick");
define('MODAL_PRODUCT_SHORT', MODAL_PRODUCT . "Short");
define('MODAL_PRODUCT_HOLD', MODAL_PRODUCT . "Hold");
define('MODAL_PRODUCT_QUARANTINE', MODAL_PRODUCT . "Quarantine");
define('MODAL_PRODUCT_DESCRIPTION', MODAL_PRODUCT . "Description");
define('MODAL_PRODUCT_BARCODE', MODAL_PRODUCT . "Barcode");
define('MODAL_PRODUCT_ASIN', MODAL_PRODUCT . "Asin");
define('MODAL_PRODUCT_DGN_TYPE', MODAL_PRODUCT . "dgnType");
define('MODAL_PRODUCT_DGN_DETAILS', MODAL_PRODUCT . "dgnDetails");
define('MODAL_PRODUCT_COUNTRY', MODAL_PRODUCT . "Country");
define('MODAL_PRODUCT_COMMODITY_CODE', MODAL_PRODUCT . "CommodityCode");
define('MODAL_PRODUCT_HEIGHT', MODAL_PRODUCT . "Height");
define('MODAL_PRODUCT_WIDTH', MODAL_PRODUCT . "Width");
define('MODAL_PRODUCT_DEPTH', MODAL_PRODUCT . "Depth");
define('MODAL_PRODUCT_WEIGHT', MODAL_PRODUCT . "Weight");
define('MODAL_PRODUCT_CURRENCY', MODAL_PRODUCT . "Currency");
define('MODAL_PRODUCT_VALUE', MODAL_PRODUCT . "Value");
define('MODAL_PRODUCT_INNER_QUANTITY', MODAL_PRODUCT . "InnerQuantity");
define('MODAL_PRODUCT_MASTER_CARTON_QUANTITY', MODAL_PRODUCT . "MasterCartonQuantity");
define('MODAL_PRODUCT_PALLET_QUANTITY', MODAL_PRODUCT . "PalletQuantity");
define('MODAL_PRODUCT_DATE_EXPIRY', MODAL_PRODUCT . "DateExpiry");
define('MODAL_PRODUCT_SERIAL', MODAL_PRODUCT . "Serial");
define('MODAL_PRODUCT_BATCH', MODAL_PRODUCT . "Batch");
define('MODAL_PRODUCT_DUE_IN', MODAL_PRODUCT . "DueIn");
define('MODAL_PRODUCT_ORDER_IN', MODAL_PRODUCT . "OrderIn");
define('MODAL_PRODUCT_DUE_OUT', MODAL_PRODUCT . "DueOut");
define('MODAL_PRODUCT_ORDER_OUT', MODAL_PRODUCT . "OrderOut");
define('MODAL_ORDER', "modalOrder");
define('MODAL_ORDER_NUMBER', MODAL_ORDER . "orderNumber");
define('MODAL_ORDER_EDIT', MODAL_ORDER . "Edit");
define('MODAL_ORDER_ITEM', MODAL_ORDER . "Item");
define('MODAL_ORDER_PACKING', MODAL_ORDER . "Packing");
define('MODAL_ORDER_LOGS', MODAL_ORDER . "Logs");
define('MODAL_ORDER_LABEL', MODAL_ORDER . "Label");
define('MODAL_ORDER_VIEW', MODAL_ORDER . "View");
define('MODAL_ORDER_FIRST_NAME', MODAL_ORDER . "firstName");
define('MODAL_ORDER_LAST_NAME', MODAL_ORDER . "lastName");
define('MODAL_ORDER_COMPANY', MODAL_ORDER . "company");
define('MODAL_ORDER_ADDRESS_LINE_1', MODAL_ORDER . "addressLine1");
define('MODAL_ORDER_ADDRESS_LINE_2', MODAL_ORDER . "addressLine2");
define('MODAL_ORDER_TOWN', MODAL_ORDER . "town");
define('MODAL_ORDER_REGION', MODAL_ORDER . "region");
define('MODAL_ORDER_POSTCODE', MODAL_ORDER . "postCode");
define('MODAL_ORDER_COUNTRY', MODAL_ORDER . "country");
define('MODAL_ORDER_PHONE_NUMBER', MODAL_ORDER . "phoneNumber");
define('MODAL_ORDER_EMAIL', MODAL_ORDER . "email");
define('MODAL_ORDER_SERVICE', MODAL_ORDER . "service");
define('MODAL_ORDER_REFERENCE', MODAL_ORDER . "reference");
define('MODAL_ORDER_PURCHASE_ORDER', MODAL_ORDER . "purchaseOrder");
define('MODAL_ORDER_CURRENCY', MODAL_ORDER . "currency");
define('MODAL_ORDER_DISPATCH_DATE', MODAL_ORDER . "dispatchDate");
define('MODAL_ORDER_DATE_CREATED', MODAL_ORDER . "dateCreated");
define('MODAL_ORDER_DELIVERY_NOTE', MODAL_ORDER . "deliveryNote");
define('MODAL_ORDER_INSTRUCTIONS_PACKING', MODAL_ORDER . "instructionsPacking");
define('MODAL_ORDER_TRACKING', MODAL_ORDER . "trackingNumber");
define('MODAL_ORDER_CARRIER', MODAL_ORDER . "carrier");

define('MODAL_COMPANY', "modalCompany");
define('MODAL_COMPANY_ID', MODAL_COMPANY . "ID");
define('MODAL_COMPANY_LABEL', MODAL_COMPANY . "Label");
define('MODAL_COMPANY_EDIT', MODAL_COMPANY . "Edit");
define('MODAL_COMPANY_TITLE', MODAL_COMPANY . "Title");
define('MODAL_COMPANY_VIEW', MODAL_COMPANY . "View");
define('MODAL_COMPANY_LOGS', MODAL_COMPANY . "Logs");
define('MODAL_COMPANY_CODE', MODAL_COMPANY . "Code");
define('MODAL_COMPANY_EMAIL', MODAL_COMPANY . THREE_PL_EMAIL);
define('MODAL_COMPANY_PRIMARY_EMAIL', MODAL_COMPANY . PRIMARY_EMAIL);
define('MODAL_COMPANY_SECONDARY_EMAIL', MODAL_COMPANY . SECONDARY_EMAIL);
define('MODAL_COMPANY_ADDRESS_LINE_1', MODAL_COMPANY . "AddressLine1");
define('MODAL_COMPANY_ADDRESS_LINE_2', MODAL_COMPANY . "AddressLine2");
define('MODAL_COMPANY_TOWN', MODAL_COMPANY . "Town");
define('MODAL_COMPANY_REGION', MODAL_COMPANY . "Region");

//PAGES
define('PAGE_FORM', CUSTOM . "/" . FORM . EXTENSION_PHP);
define('PAGE_HEADER', CUSTOM . "/" . HEADER . EXTENSION_PHP);
define('PAGE_FOOTER', CUSTOM . "/" . FOOTER . EXTENSION_PHP);
define('PAGE_LOGOUT', LOGOUT . "/");
define('PAGE_LOGOUT_INDEX', LOGOUT . "/" . INDEX);
define('PAGE_LOGIN', LOGIN . "/");
define('PAGE_LOGIN_INDEX', PAGE_LOGIN . INDEX);
define('PAGE_LOGIN_LOGIN', PAGE_LOGIN . LOGIN);
define('PAGE_ERROR_INDEX', ERROR . "/" . INDEX);
define('PAGE_MAIN', MAIN . "/");
define('PAGE_MAIN_INDEX', PAGE_MAIN . INDEX);
define('PAGE_REGISTER', REGISTER . "/");
define('PAGE_REGISTER_INDEX', PAGE_REGISTER . INDEX);
define('PAGE_PRODUCT_CREATE', PRODUCT . "/". CREATE);
define('PAGE_PRODUCT_CREATE_BULK', PRODUCT . "/". BULK);
define('PAGE_PRODUCT_BULK_EDIT', PRODUCT . "/". EDIT_BULK);
define('PAGE_PRODUCT_EDIT', PRODUCT . "/". EDIT);
define('PAGE_PRODUCT_VIEW', PRODUCT . "/". VIEW);
define('PAGE_PRODUCT_CURRENT_STOCK', PRODUCT . "/". CURRENT_STOCK_LEVELS);
define('PAGE_PRODUCT_STOCK_BREAKDOWN', PRODUCT . "/". STOCK_BREAKDOWN);
define('PAGE_PRODUCT_VIEW_BULK_ERROR', PRODUCT . "/" . VIEW . "_" . BULK_ERRORS);
define('PAGE_PRODUCT_EDIT_BULK', PRODUCT . "/" . EDIT . "_" . BULK_ERRORS);
define('PAGE_ERROR_APIKEY', ERROR . "/" . "101");
define('PAGE_ERROR_USER', ERROR . "/" . "102");
define('PAGE_MODAL_PRODUCT', MODAL . "/" . PRODUCT);
define('PAGE_MODAL_COMPANY', MODAL . "/" . COMPANY);
define('PAGE_MODAL_API_KEY', MODAL . "/" . "api_key");
define('PAGE_MODAL_NEWS', MODAL . "/" . "news");
define('PAGE_ORDERS_CREATE', ORDERS . "/" . CREATE);
define('PAGE_ORDERS_CREATE_BULK' , ORDERS . "/" . BULK);
define('PAGE_ORDERS_VIEW', ORDERS . "/" . VIEW);
define('PAGE_ORDERS_EDIT', ORDERS . "/" . EDIT);
define('PAGE_ORDERS_DELETE', ORDERS . "/" . DELETE_);
define('PAGE_LINNWORKS_IMPORT_ERRORS', ORDERS . "/" . LINNWORKS_IMPORT_ERRORS);
define('PAGE_PACKING_LIST_REPORT', ORDERS . "/" . PACKING_LIST_REPORT);
define('PAGE_ORDER_DELIVERY_NOTE', ORDERS . "/" . ORDER_DELIVERY_NOTE);
define('PAGE_ORDER_BACK_ORDERS_DETAILS', ORDERS . "/" . BACK_ORDERS_DETAILS);
define('PAGE_ORDER_ADDRESS_UPDATE', ORDERS . "/" . ADDRESS_SERVICE_UPDATE);
define('PAGE_TITLE', "pageTitle");
define('PAGE_MODAL_ORDER', MODAL . "/" . ORDER);
define('PAGE_PRODUCT_TABLE', PRODUCT . "/" . TABLE);
define('PAGE_PRODUCT_CURRENT_STOCK_TABLE', PRODUCT . "/" . CURRENT_STOCK_LEVELS);
define('PAGE_ORDER_TABLE', ORDERS . "/" . TABLE);
define('PAGE_ORDER_TABLE_SHIPPED', ORDERS . "/" . TABLE . "_shipped");
define('PAGE_COMPANY_TABLE', COMPANY . "/" . TABLE);
define('PAGE_COMPANY_SETTINGS', COMPANY . "/settings");
define('PAGE_ITEM_TABLE', ITEM . "/" . TABLE);
define('PAGE_GRN_CREATE', GRN . "/" . CREATE);
define('PAGE_GRN_VIEW', GRN . "/" . VIEW);
define('PAGE_GRN_VIEW_TABLE', GRN . "/" . TABLE);
define('PAGE_GRN_VIEW_ITEMS_TABLE', GRN . "/" . "product_" . TABLE);
define('PAGE_STOCK_TABLE', STOCK . "/" . TABLE);
define('PAGE_STOCK_BREAKDOWN', STOCK . "/" . STOCK . "_breakdown");
// Rework
define('PAGE_REWORK_BOOK', REWORKS . "/" . BOOK);
define('PAGE_REWORK_VIEW', REWORKS . "/" . VIEW);
// Company
define('PAGE_COMPANY_CREATE', COMPANY . "/". CREATE);
define('PAGE_COMPANY_CREATE_BULK', COMPANY . "/". BULK);
define('PAGE_COMPANY_EDIT', COMPANY . "/". EDIT);
define('PAGE_COMPANY_VIEW', COMPANY . "/". VIEW);
define('PAGE_COMPANY_VIEW_BULK_ERROR', COMPANY . "/" . VIEW . "_" . BULK_ERRORS);
define('PAGE_COMPANY_EDIT_BULK', COMPANY . "/" . EDIT . "_" . BULK_ERRORS);
// User
define('PAGE_USER_CREATE', USERS . "/". CREATE);
define('PAGE_USER_VIEW', USERS . "/". VIEW);
// Reports
define('PAGE_SERIAL_NUMBER', REPORTS . "/". 'serial_number_record');
define('PAGE_STOCK_ADJUSTMENT', REPORTS . "/". 'stock_adjustment');
define('PAGE_STOCK_BY_LOCATION', REPORTS . "/". 'stock_by_location');
// Historic Reports
define('PAGE_HISTORIC_ORDERS', HISTORIC_REPORTS . "/". 'orders');
// Helpdesk
define('PAGE_LATEST_NEWS', HELPDESK . "/" . LATEST_NEWS);
define('PAGE_CREATE_TICKET', HELPDESK . "/" . CREATE_TICKET);
define('PAGE_VIEW_TICKETS', HELPDESK . "/" . VIEW_TICKETS);
define('PAGE_VIEW_TICKETS_TABLE', HELPDESK . "/" . VIEW_TICKETS . "_" . TABLE);
// Customer Workspace
define('PAGE_INVOICES', CUSTOMER_WORKSPACE . "/" . INVOICES);
define('PAGE_CUSTOM_REPORTS', CUSTOMER_WORKSPACE . "/" . CUSTOM_REPORTS);
define('PAGE_CUSTOMER_FTP', CUSTOMER_WORKSPACE . "/" . CUSTOMER_FTP_FOLDER);
define('PAGE_CUSTOMER_FTP_TABLE', CUSTOMER_WORKSPACE . "/" . CUSTOMER_FTP_TABLE);
define('PAGE_DELIVERY_NOTE_EDITOR', CUSTOMER_WORKSPACE . "/" . DELIVERY_NOTE_EDITOR);
define('PAGE_ADDITIONAL_ACTIVITY', CUSTOMER_WORKSPACE . "/" . BOOK_ADDITIONAL_ACTIVITY);
define('PAGE_SERVICE_MAPPING_TOOL', CUSTOMER_WORKSPACE . "/" . SERVICE_MAPPING_TOOL);
define('PAGE_RULES_ENGINE', CUSTOMER_WORKSPACE . "/" . RULES_ENGINE);
define('PAGE_API_DOCUMENTATION', "https://documenter.getpostman.com/view/9150747/SzYexbwj?version=latest");
// Internal Reports
define('PAGE_INTERNAL_ACCOUNT_MANAGER', INTERNAL_REPORTS . "/" . "account_manager_report");
define('PAGE_INTERNAL_DFR_REPORT', INTERNAL_REPORTS . "/" . "dfr_report");
define('PAGE_INTERNAL_VENDOR', INTERNAL_REPORTS . "/" . "vendor_report");
define('PAGE_INTERNAL_PICKPOT', INTERNAL_REPORTS . "/" . "pickpot_report");
define('PAGE_INTERNAL_STOCK_MOVEMENT', INTERNAL_REPORTS . "/" . "stock_movement_report");
define('PAGE_INTERNAL_STOCK_MOVEMENT_HISTORIC', INTERNAL_REPORTS . "/" . "stock_movement_historic_report");
define('PAGE_INTERNAL_GRN_PRECHECK', INTERNAL_REPORTS . "/" . "grn_precheck_report");
define('PAGE_INTERNAL_CUBISCAN_EDITOR', INTERNAL_REPORTS . "/" . "cubiscan_serial_editor");
define('PAGE_INTERNAL_WELSPUN_DEBENHAMS', INTERNAL_REPORTS . "/" . "welspun_print_debenhams_delivery");
define('PAGE_INTERNAL_WELSPUN_REPLEN', INTERNAL_REPORTS . "/" . "welspun_required_replen_record");
define('PAGE_INTERNAL_SORTED_BILLING', INTERNAL_REPORTS . "/" . "sorted_billing_upload");
define('PAGE_INTERNAL_RECENTLY_PACKED', INTERNAL_REPORTS . "/" . "recently_packed_orders");
define('PAGE_INTERNAL_MOVEMENT_BY_SKU', INTERNAL_REPORTS . "/" . "movement_by_sku");
define('PAGE_INTERNAL_INBOUND_BOOKINGS', INTERNAL_REPORTS . "/" . "inbound_bookings");
define('PAGE_INTERNAL_CONSOLIDATION_REPORT', INTERNAL_REPORTS . "/" . "consolidation_report");
define('PAGE_INTERNAL_SHORT_SHIPPED_REPORT', INTERNAL_REPORTS . "/" . "short_shipped_report");
// Internal Billing
define('PAGE_INTERNAL_WELSPUN_BILLING', INTERNAL_BILLING . "/" . "welspun_billing");
define('PAGE_INTERNAL_FAIRY_LOOT_BILLING', INTERNAL_BILLING . "/" . "fairy_loot_billing");
define('PAGE_INTERNAL_PRECIOUS_LITTLE_ONE_BILLING', INTERNAL_BILLING . "/" . "precious_little_one_billing");
// Returns
define('PAGE_RETURNS', RETURNS . "/" . VIEW);
// Exceptions
define('PAGE_EXCEPTIONS', EXCEPTIONS . "/" . VIEW);
// Inbounds
define('PAGE_INBOUND_BOOKINGS', INBOUNDS . "/" . CREATE);
define('PAGE_INBOUND_BOOKINGS_VIEW', INBOUNDS . "/" . VIEW);
define('PAGE_INBOUND_BOOKINGS_LINK', INBOUNDS . "/link");
// STAFF
define('PAGE_STAFF_NOTES',  "staff/create_order_note");
define('PAGE_CREATE_NEWS_ARTICLE',  "staff/create_news_article");
// SYSTEM
define('SYSTEM_NAME', '3PL Fusion');
define('SYSTEM_VERSION', 'Version 3.0');
define('SYSTEM_TYPE', getenv('SYSTEM_TYPE'));

// FORMS
define('FORM_PRODUCT_CREATE', "formProductCreate");
define('FORM_PRODUCT_EDIT', "formProductEdit");
define('FORM_ORDER_CREATE', "formOrderCreate");
define('FORM_ORDER_EDIT', "formOrderEdit");
define('FORM_ORDER_ADDRESS', "formOrderAddress");
define('FORM_ORDER_VIEW', "formOrderView");
define('FORM_GRN_CREATE', "formGrnCreate");
define('FORM_COMPANY_CREATE', "formCompanyCreate");
define('FORM_COMPANY_EDIT', "formCompanyEdit");
define('FORM_USER_CREATE', "formUserCreate");
define('FORM_TICKET_CREATE', "formTicketCreate");
define('FORM_VIEW_TICKETS', "formTicketsView");
define('FORM_INBOUND_CREATE', "formInboundCreate");
define('FORM_REWORK_BOOK', "formReworkBook");
define('FORM_SERVICE_MAPPING', "formServiceMappingTool");
define('FORM_RULES_ENGINE', "formRulesEngine");
define('FORM_CREATE_NEWS', "formCreateNews");

// TABLES
define('TABLE_PRODUCTS_VIEW', "tableProductsView");
define('TABLE_STOCK_VIEW', "tableStockView");
define('TABLE_STOCK_BREAKDOWN_VIEW', "tableStockBreakdownView");
define('TABLE_ORDERS_VIEW', "tableOrdersView");
define('TABLE_ORDERS_SHIPPED_VIEW', "tableOrdersShippedView");
define('TABLE_COMPANY_VIEW', "tableCompanyView");
define('TABLE_ITEMS_VIEW', "tableItemsView");
define('TABLE_PRODUCTS_BULK_ERROR_VIEW', "tableProductsBulkErrorView");
define('TABLE_GRNS_VIEW', "tableGrnsView");
define('TABLE_GRNS_VIEW_ITEMS', "tableGrnsViewItems");
define('TABLE_FILES_VIEW_DOWNLOAD', "tableFilesViewDownload");
define('TABLE_ACCOUNT_MANAGER_REPORT', "tableAccountManagerReport");
define('TABLE_STOCK_MOVEMENT_REPORT', 'tableStockMovementReport');
define('TABLE_RETURNS_VIEW', 'tableReturnsView');
define('TABLE_CUSTOMER_FTP', "tableCustomerFTP");
define('TABLE_VIEW_TICKETS', "tableTicketsView");

define('TABLE_ITEM_QUANTITY_TEXTBOX', "tableItemQuantityTextbox");
define('TABLE_ITEM_VALUE_TEXTBOX', "tableItemValueTextbox");

define('TABLE_CUSTOM_ACTIONS_BUTTON_PREVIOUS', "tableCustomActionsButtonPrevious");
define('TABLE_CUSTOM_ACTIONS_BUTTON_PAGE_NUMBERS', "tableCustomActionsButtonPageNumbers");
define('TABLE_CUSTOM_ACTIONS_BUTTON_NEXT', "tableCustomActionsButtonNext");

// DROP DOWN
define('SELECT_COUNTRIES', "selectCountries");
define('SELECT_DGN_DETAILS', "selectDgnDetails");
define('SELECT_DGN_TYPES', "selectDgnTypes");
define('SELECT_CURRENCY', "selectCurrency");

//TAGS
define('REL_STYLESHEET', REL . "='" . STYLESHEET . "'");
define('TYPE_IMAGE_XICON', TYPE . "='" . IMAGE . '/' . XICON . "'");
define('TYPE_TEXT_CSS', TYPE . "='" . TEXT . '/' . CSS . "'");

// TITLES
define('TITLE_ACCOUNT_TYPE', "Account Type");
define('TITLE_ACTIVE', "Active Account");
define('TITLE_ADDRESS_LINE_1', "Address Line 1");
define('TITLE_AVERAGE_MONTHLY_UNIT_SALES', "Average Monthly Unit Sales");
define('TITLE_TITLE', "Title");
define('TITLE_TIME', "Time");
define('TITLE_CUSTOMER_CONTACT', "Customer Contact");
define('TITLE_ADDRESS', "Address");
define('TITLE_COMPANY_ADDRESS', "Company " . TITLE_ADDRESS);
define('TITLE_COMPANY_SETTINGS', "Company Settings");
define('TITLE_ADDRESS_LINE_2', "Address Line 2");
define('TITLE_CM', "(CM)");
define('TITLE_KG', "(KG)");
define('TITLE_SKU', "SKU");
define('TITLE_REFERENCES', "References");
define('TITLE_DESCRIPTION', "Description");
define('TITLE_BARCODE', "Barcode");
define('TITLE_BULK_STORAGE', "Bulk Storage");
define('TITLE_ASIN', "ASIN");
define('TITLE_ASSIGNED', "Assigned");
define('TITLE_ORDER_IN', "Order In");
define('TITLE_ORDER_OUT', "Order Out");
define('TITLE_OPEN', "Open");
define('TITLE_OPEN_ORDERS', "Open Orders");
define('TITLE_CLOSED', "Closed");
define('TITLE_GRN_REFERENCE', "Customer Reference");
define('TITLE_DUE_IN', "Due In");
define('TITLE_DUE_OUT', "Due Out");
define('TITLE_COMMODITY_CODE', "Commodity Code");
define('TITLE_INSTRUCTIONS_PICKING', "Picking Instructions");
define('TITLE_INVOICES', "Invoices");
define('TITLE_CUSTOM_REPORTS', "Custom Reports");
define('TITLE_CREATE', "Create");
define('TITLE_CARRIER', "Carrier");
define('TITLE_NAME', "Name");
define('TITLE_FIRST_NAME', "First Name");
DEFINE('TITLE_FTP_FOLDER', 'FTP Folder');
define('TITLE_VIEW', "View");
define('TITLE_DOWNLOAD', "Download");
define('TITLE_CURRENT_STOCK_LEVELS', "Current Stock Levels");
define('TITLE_HOME', "Home");
define('TITLE_PICKING_AISLE', "Picking Aisle");
define('TITLE_PRODUCT', "PRODUCT");
define('TITLE_PRODUCT_INFORMATION', "Product Information");
define('TITLE_LAST_NAME', "Last Name");
define('TITLE_MAIN_NAVIGATION', "MAIN NAVIGATION");
define('TITLE_TOTAL', "Total");
define('TITLE_ALLOCATED', "Allocated");
define('TITLE_FREE', "Free");
define('TITLE_ON_PICK', "On Pick");
define('TITLE_SHORT', "Short");
define('TITLE_HOLD', "Hold");
define('TITLE_QUARANTINE', "Quarantine");
define('TITLE_STOCK', "Stock");
define('TITLE_STOCK_AREA', TITLE_STOCK . " Area");
define('TITLE_STOCK_LEVELS', TITLE_STOCK . " Levels");
define('TITLE_STOCK_BREAKDOWN', TITLE_STOCK . " Breakdown");
define('TITLE_EDIT', "Edit");
define('TITLE_ERROR', 'Error');
define('TITLE_LOGS', "Logs");
define('TITLE_CURRENCY', "Currency");
define('TITLE_VALUE', "Value");
define('TITLE_DATE_EXPIRY', "Expiry Date");
define('TITLE_CLOSE', "Close");
define('TITLE_DGN_TYPE', "DGN Type");
define('TITLE_DGN_DETAILS', "DGN Details");
define('TITLE_HEIGHT', "Height " . TITLE_CM);
define('TITLE_HELPDESK', "HELPDESK");
define('TITLE_WEIGHT', "Weight " . TITLE_KG);
define('TITLE_WORKSPACE', "MY WORKSPACE");
define('TITLE_WIDTH', "Width " . TITLE_CM);
define('TITLE_COUNTRY', "Country");
define('TITLE_COUNTRY_OF_ORIGIN', "Country of Origin");
define('TITLE_DEPTH', "Depth " . TITLE_CM);
define('TITLE_SERIAL', "Serial");
define('TITLE_BATCH', "Batch");
define('TITLE_ORDER', "ORDER");
define('TITLE_ORDERS', "Orders");
define('TITLE_PALLET_QUANTITY', "Pallet Quantity");
define('TITLE_PASSWORD', "Password");
define('TITLE_MASTER_CARTON_QUANTITY', "Master Carton Quantity");
define('TITLE_INNER_QUANTITY', "Inner Quantity");
define('TITLE_TOWN', "Town");
define('TITLE_REGION', "Region");
define('TITLE_RECEIVED', "Received");
define('TITLE_PHONE_NUMBER', "Phone Number");
define('TITLE_EMAIL', "Email");
define('TITLE_DISPATCH_DATE', "Dispatch By");
define('TITLE_ITEM', "Item");
define('TITLE_PACKING_LIST', "Packing List");
define('TITLE_ITEM_NUMBER', TITLE_ITEM . " Number");
define('TITLE_ITEMS', "Items");
define('TITLE_SERVICE', "Service");
define('TITLE_REFERENCE', "Reference");
define('TITLE_PURCHASE_ORDER', "Purchase Order");
define('TITLE_COMPANY', "Company");
define('TITLE_COMPANY_EMAIL', "Company " . TITLE_EMAIL);
define('TITLE_3PL_EMAIL', "3PL Logistics " . TITLE_EMAIL);
define('TITLE_PRIMARY_EMAIL', "Primary " . TITLE_EMAIL);
define('TITLE_SECONDARY_EMAIL', "Secondary " . TITLE_EMAIL);
define('TITLE_DELIVERY_NOTE', "Delivery Note");
define('TITLE_DELIVERY_NOTE_VIEW', TITLE_DELIVERY_NOTE . " Viewer");
define('TITLE_ORDER_DELIVERY_NOTE', TITLE_ORDER . " " . TITLE_DELIVERY_NOTE);
define('TITLE_DELIVERY', "Delivery");
define('TITLE_INSTRUCTIONS_PACKING', "Packing Instructions");
define('TITLE_ORDER_NUMBER', "Order Number");
define('TITLE_CUSTOMER_CODE', "Customer Code");
define('TITLE_ADD', "Add");
define('TITLE_QUANTITY', "Quantity");
define('TITLE_POSTCODE', "Postcode");
define('TITLE_CUSTOMER_REFERENCE', "Customer Reference");
define('TITLE_GRN', "GOODS IN NOTIFICATION");
define('TITLE_GRN_LOWER', "Goods In Notification");
define('TITLE_GRN_ITEMS', "Items on Goods In");
define('TITLE_GRN_SHORT', "GRN");
define('TITLE_INSTRUCTIONS', "Instructions");
define('TITLE_PRODUCTS', "Products");
define('TITLE_COMPANIES', "Companies");
define('TITLE_COMPANY_CODE', "Company Code");
define('TITLE_COMPANY_MENU', "COMPANIES");
define('TITLE_COMPANY_TYPES', "Company Type");
define('TITLE_USER', "User");
define('TITLE_USER_DETAILS', TITLE_USER . " Details");
define('TITLE_USERS', "Users");
define('TITLE_LITE_ACCOUNT', "Lite Account");
define('TITLE_EXPORT_LEVEL', "Stock Level and Processed Orders File");
define('TITLE_CREATE_MACS', "Create in Macs");
define('TITLE_EMAIL_TRACKING', "Email Tracking");
define('TITLE_LINNWORKS', "Linnworks");
define('TITLE_LINE_ID', "Line ID");
define('TITLE_BACK_ORDER', "Back Order Email");
define('TITLE_LIFETIME_CODE', "Lifetime Code");
define('TITLE_CUSTOMER_ADDRESS', "Customer Address");
define('TITLE_WEIGHT_DIMS', "Weight And Dimensions");
define('TITLE_PRODUCT_DETAILS', "Product Details");
define('TITLE_QUANTITIES', "Quantities");
define('TITLE_EXTRAS', "Extras");
define('TITLE_NOTES', "Notes");
define('TITLE_DATE', "Date");
define('TITLE_DATE_SHIPPED', "Date Shipped");
define('TITLE_DATE_CREATED', "Date Created");
define('TITLE_DATE_CLOSED', "Date Closed");
define('TITLE_DATE_SHIPPED_FROM', "Date Shipped From");
define('TITLE_DATE_SHIPPED_TO', "Date Shipped To");
define('TITLE_LAST_UPDATED', "Last Updated");
define('TITLE_BACK_ORDERS', "Back Orders");
define('TITLE_NEXT_FLUSH', "Next Flush");
define('TITLE_AWAITING_PICK', "Awaiting Pick");
define('TITLE_AWAITING_PACK', "Awaiting Pack");
define('TITLE_AWAITING_SHIP', "Awaiting Ship");
define('TITLE_SHIPPED', "Shipped");
define('TITLE_MANAGEMENT', "MANAGEMENT");
define('TITLE_TRACKING', "Tracking Number");
define('TITLE_LICENCES', "Licences");
define('TITLE_TICKET_CREATE', "Create a Support Ticket");
define('TITLE_BOOK_REWORK', "Request a Rework");
define('TITLE_TICKETS_VIEW', "View Support Ticket");
define('TITLE_TICKET_NUMBER', "Ticket Number");
define('TITLE_PRIORITY', "Priority");
define('TITLE_AREA', 'Area');
define('TITLE_CATEGORY', 'Category');
define('TITLE_SUBJECT', 'Subject');
define('TITLE_FILE', 'File');
define('TITLE_DATE_PACKED_FROM', "Date Packed From");
define('TITLE_DATE_PACKED_TO', "Date Packed To");
define('TITLE_FILTERS', "Filters");
define('TITLE_FTP_FOLDERS', 'FTP Folders');
define('TITLE_STATUS', "Status");
define('TITLE_TYPE', "Type");
define('TITLE_STAFF', 'STAFF');
define('TITLE_INTERNAL_REPORTS', 'Internal Reports');
define('TITLE_ACOUNT_MANAGER_REPORT','Account Manager Report');
define('TITLE_SORTED_BILLING_UPLOAD','Sorted Billing Upload');
define('TITLE_STOCK_MOVEMENT_REPORT','Stock Movement Report');
define('TITLE_GRN_PRECHECK', 'GRN Expected vs Recevied');
define('TITLE_CUBISCAN_SERIAL_EDITOR', 'Weights & Dims Report');
define('TITLE_WELSPUN_DEBENHAMS_DELIVERY', 'Debenhams Delivery Note Printing');
define('TITLE_WELSPUN_REQUIRED_REPLEN', 'Required Replen Record');
define('TITLE_IMPORT', 'Import');
define('TITLE_EXPORT', 'Export');
define('TITLE_RETURNS', 'Returns');
define('TITLE_SUPPLIER_REF', 'Supplier Reference');
define('TITLE_RETURN_REASON', 'Return Reason');

define('STATUS_SHIPPED', "SHIPPED");
define('STATUS_AWAITING_SHIP', "AWAITINGSHIP");
define('STATUS_AWAITING_PACK', "AWAITINGPACK");
define('STATUS_AWAITING_PICK', "AWAITINGPICK");
define('STATUS_NEXT_FLUSH', "NEXTFLUSH");
define('STATUS_BACK', "BACK");