<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Orders extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function create() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_ORDER_CREATE;
        $this->data[FORM_DATA] = $this->orderForm;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_CREATE;
        $this->data[PAGE_TITLE] = "Create Order";

        $this->load->view(PAGE_HEADER, $this->data);

        $this->load->view(PAGE_FORM, $this->data);
        
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function create_bulk() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_ORDER_CREATE;
        $this->data[FORM_DATA] = $this->orderForm;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_CREATE;
        $this->data[PAGE_TITLE] = "Create Bulk Order";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view('orders/create_bulk', $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function edit() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_ORDER_EDIT;
        $this->data[FORM_DATA] = $this->orderFormEdit;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_EDIT;   
        $this->data[PAGE_TITLE] = "Edit Order";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function view() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_ORDER_VIEW;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_VIEW;
        $this->data[PAGE_TITLE] = "View Order";
        $this->data[ORDER_FILTERS] = $this->orderFilters;

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view($this->getPageFile($this), $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function linnworks_import_errors() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Linnworks Import Errors";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_LINNWORKS_IMPORT_ERRORS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function packing_list_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Packing List Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_PACKING_LIST_REPORT, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function order_delivery_note() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Order Delivery Note";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_ORDER_DELIVERY_NOTE, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function back_orders_details() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Back Order Details";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_ORDER_BACK_ORDERS_DETAILS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function address_service_update() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Update Address and Service";

        $this->data[FORM_ID] = FORM_ORDER_ADDRESS;
        $this->data[FORM_DATA] = $this->orderFormAddress;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_ADDRESS;   

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
    
}