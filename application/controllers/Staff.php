<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Staff extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    public function create_order_note() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "Create Note on Order";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_STAFF_NOTES, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function create_news_article() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "Create 3PL New Article";
        $this->data[FORM_ID] = FORM_CREATE_NEWS;
        $this->data[FORM_DATA] = $this->newsArticle;
        $this->data[FORM_BUTTON] = BUTTON_CREATE_NEWS;

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_CREATE_NEWS_ARTICLE, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}