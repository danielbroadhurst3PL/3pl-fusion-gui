<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Historic_reports extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function orders() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "Orders";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_HISTORIC_ORDERS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}