<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Internal_Reports extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function account_manager_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Account Manager Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_ACCOUNT_MANAGER, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function dfr_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "DFR Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_DFR_REPORT, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function vendor_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Vendor Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_VENDOR, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function pickpot_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Pickpot Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_PICKPOT, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function stock_movement_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Stock Movement Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_STOCK_MOVEMENT, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function stock_movement_historic_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Stock Movement Report Historic";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_STOCK_MOVEMENT_HISTORIC, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function grn_precheck_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "GRN Expected vs Recevied";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_GRN_PRECHECK, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function cubiscan_serial_editor() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Weights & Dims Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_CUBISCAN_EDITOR, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function welspun_print_debenhams_delivery() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Welspun Debenhams Delivery Note Printing";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_WELSPUN_DEBENHAMS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function welspun_required_replen_record() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Welspun Required Replen Record";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_WELSPUN_REPLEN, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function sorted_billing_upload() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Sorted Billing Upload";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_SORTED_BILLING, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function recently_packed_orders() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Recently Packed Orders";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_RECENTLY_PACKED, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function movement_by_sku() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Movement by SKU";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_MOVEMENT_BY_SKU, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function inbound_bookings() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Inbound Bookings";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_INBOUND_BOOKINGS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function consolidation_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Consolidation Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_CONSOLIDATION_REPORT, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function short_shipped_report() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Short Shipped Report";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_SHORT_SHIPPED_REPORT, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}