<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Customer_workspace extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function invoices() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "View Invoices";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INVOICES, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function custom_reports() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Custom Reports";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_CUSTOM_REPORTS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function customer_ftp_folder() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_ORDER_VIEW;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_VIEW;
        $this->data[ORDER_FILTERS] = $this->ftpFolderilters;
        $this->data[PAGE_TITLE] = "Customer FTP Folder";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_CUSTOMER_FTP, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function delivery_note_editor() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Delivery Note Editor";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_DELIVERY_NOTE_EDITOR, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function api_documentation() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "API Documentation";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_API_DOCUMENTATION, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function service_mapping_tool() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Service Mapping Tool";

        $this->data[FORM_ID] = FORM_SERVICE_MAPPING;
        $this->data[FORM_DATA] = $this->serviceMappingTool;
        $this->data[FORM_BUTTON] = BUTTON_SERVICE_MAPPING_CREATE;

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_SERVICE_MAPPING_TOOL, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function rules_engine() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Rules Engine";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_RULES_ENGINE, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}