<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends FUSION_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {   
        if (isset($_GET['error'])) {
            $error = htmlspecialchars($_GET['error']);
            $this->data['errorAPI'] = $error;
        }
        $this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

        $this->setPageDetails($this);
        $data = $this->data;
        $this->data[PAGE_TITLE] = "Login";
        $this->logged_in();
        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view($this->getPageFile($this), $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function login()
    {
        $data = $this->data;

        $user = $this->input->post(USER);
        $password = $this->input->post(PASSWORD);
        $rememberMe = (bool) $this->input->post(REMEMBER);

        if($this->ion_auth->login($user, $password, $rememberMe)):

            if($this->set_session() === true):
                echo json_encode(array(true));                   
            else:
                echo json_encode(array(false, "An Error occured trying to log you in. Please try again."));
            endif;
        else:
            echo json_encode(array(false, "Incorrect login."));
        endif;
    }

    public function set_session()
    {
        return $this->login_model->set_session($this->ion_auth->user()->row()->id,$_COOKIE["ci_session"]);
    }
}