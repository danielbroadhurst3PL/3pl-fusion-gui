<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Returns extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    public function view() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "View Returns";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_RETURNS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}