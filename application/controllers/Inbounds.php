<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Inbounds extends MY_Secure_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create() {
        $this->setPageDetails($this);
        $this->data[FORM_ID] = FORM_INBOUND_CREATE;
        $this->data[FORM_DATA] = $this->inboundForm;
        $this->data[FORM_BUTTON] = BUTTON_INBOUND_CREATE;
        $this->data[PAGE_TITLE] = "Create Inbound Booking";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function view() {
        $this->setPageDetails($this);
        $this->data[FORM_ID] = FORM_ORDER_VIEW;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_VIEW;
        $this->data[ORDER_FILTERS] = $this->grnFilters;
        $this->data[PAGE_TITLE] = "View Inbound Bookings";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INBOUND_BOOKINGS_VIEW, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function link() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Link Unknown Inbound";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INBOUND_BOOKINGS_LINK, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}