<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Error extends FUSION_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->logout();
        $this->login();
    }

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $data = $this->data;
        $data["fPage"] = $data["fControllerError"].$data["fPageIndex"];
        $data["fErrorCode"] = $this->uri->segment(3);
        switch ($data["fErrorCode"]):
            case $data["fErrorCodeApiKey"]:
                $data["fErrorMessage"] = $data["fErrorMessageApiKey"];
            break;
            case $data["fErrorCodeUser"]:
                $data["fErrorMessage"] = $data["fErrorMessageUser"];
            break;
        endswitch;
        $this->load->view($data["fCustomFolder"].$data["fPageHeader"], $data);
        $this->load->view($data["fPage"], $data);
        $this->load->view($data["fCustomFolder"].$data["fPageFooter"], $data);
    }
}
