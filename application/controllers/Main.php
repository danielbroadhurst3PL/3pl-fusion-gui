<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Main extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();

        $this->load->model('change_user_model');

    }

    public function index()
    {
        $this->setPageDetails($this);
        $this->data[PAGE_TITLE] = "Dashboard Home";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view($this->getPageFile($this), $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);

    }

    public function staff_change_company() {

        if ($this->is_post()):
            $userID = $_POST['user'];
            $userCompany = $_POST['userCompany'];

            $databaseQuery = "UPDATE user_settings SET company_id = '$userCompany' WHERE user_id = '$userID'";

            $go = $this->change_user_model->put($databaseQuery);

            if ($go === TRUE):
                redirect('main/', 'refresh');
            else:
                redirect('main/', 'refresh');
            endif;
        else:
            redirect('main/', 'refresh');
        endif;
    }
}
