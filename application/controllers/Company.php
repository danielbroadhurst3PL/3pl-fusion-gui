<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Company extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

   public function create() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_COMPANY_CREATE;
        $this->data[FORM_DATA] = $this->companyForm;
        $this->data[FORM_BUTTON] = BUTTON_COMPANY_CREATE;
        $this->data[PAGE_TITLE] = "Create Company";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function edit() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_COMPANY_EDIT;
        $this->data[FORM_DATA] = $this->companySettingsForm;
        $this->data[FORM_BUTTON] = BUTTON_COMPANY_EDIT;
        $this->data[PAGE_TITLE] = "Save Company Settings";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function view() {
        $this->setPageDetails($this);
        $this->data[PAGE_TITLE] = "View All Companies";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_COMPANY_VIEW, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}