<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

   public function create() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_USER_CREATE;
        $this->data[FORM_DATA] = $this->userForm;
        $this->data[FORM_BUTTON] = BUTTON_USER_CREATE;
        $this->data[PAGE_TITLE] = "Create User";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function edit() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_COMPANY_EDIT;
        $this->data[FORM_DATA] = $this->companyForm;
        $this->data[FORM_BUTTON] = BUTTON_COMPANY_EDIT;
        $this->data[PAGE_TITLE] = "Edit Company";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function view() {
        $this->setPageDetails($this);
        $this->data[PAGE_TITLE] = "View Users";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_USER_VIEW, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}