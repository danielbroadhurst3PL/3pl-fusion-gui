<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Grn extends MY_Secure_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function create() {
        $this->setPageDetails($this);
        $this->data[FORM_ID] = FORM_GRN_CREATE;
        $this->data[FORM_DATA] = $this->grnForm;
        $this->data[FORM_BUTTON] = BUTTON_GRN_CREATE;
        $this->data[PAGE_TITLE] = "Create Goods in Notification";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function view() {
        $this->setPageDetails($this);
        $this->data[FORM_ID] = FORM_ORDER_VIEW;
        $this->data[FORM_BUTTON] = BUTTON_ORDER_VIEW;
        $this->data[ORDER_FILTERS] = $this->grnFilters;
        $this->data[PAGE_TITLE] = "View Goods in Notification";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view('grn/view', $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function create_grn_pdf() {

        $postData = $_GET;

        if (!$postData) {
            echo 'empty';die();
        }

        $companyCode = $postData['companyCode'];
        $referenceCustomer = $postData['referenceCustomer'];

        $referenceCustomer = str_replace(' ', '', $referenceCustomer);

        $this->load->library("Pdf");
        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        // set document information
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('3PL');
        $pdf->SetTitle($companyCode . ' GRN Receipt');
        $pdf->SetSubject($companyCode . ' GRN Receipt');

        // set default header data
        $pdf->SetHeaderData(PDF_HEADER_LOGO, 20, PDF_HEADER_TITLE. $companyCode . ' Good In Notification Reciept');

        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
            require_once(dirname(__FILE__).'/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

        // set font
        $pdf->SetFont('dejavusans', '', 10);

        // add a page
        $pdf->AddPage();

        // writeHTML($html, $ln=true, $fill=false, $reseth=false, $cell=false, $align='')
        // writeHTMLCell($w, $h, $x, $y, $html='', $border=0, $ln=0, $fill=0, $reseth=true, $align='', $autopadding=true)

        // create some HTML content
        $html = '<table cellspacing="1" cellpadding="10" border="1">
                    <tr>
                        <td>SHIP TO</td>
                        <td>INTERNAL USE</td>
                    </tr>
                    <tr>
                        <td>3P LOGISTICS<br />LOCKETT ROAD<br />SOUTH LANCS INDUSTRIAL ESTATE<br />ASHTON IN MAKERFIELD<br />WIGAN<br />WN4 8DE</td>
                        <td rowspan="2">ASN REF: ' . utf8_decode($referenceCustomer) . '<br />CUSTOMER CODE: ' . $companyCode . '<br /></td>
                    </tr>
                </table>';
        
        // output the HTML content
        $pdf->writeHTML($html, true, false, true, false, '');
        
        $pdf->SetY(30);

        // -----------------------------------------------------------------------------

        $pdf->SetFont("helvetica", "", 10);

        // define barcode style
        $style = array(
            "position" => "M",
            "align" => "C",
            "stretch" => false,
            "fitwidth" => false,
            "cellfitalign" => "",
            "border" => true,
            "hpadding" => "30",
            "vpadding" => "20",
            "fgcolor" => array(0, 0, 0),
            "bgcolor" => false, //array(255,255,255),
            "text" => true,
            "font" => "helvetica",
            "fontsize" => 8,
            "stretchtext" => 6
        );


        // EAN 13
        $pdf->Cell(0, 50, "", 0, 1);
        $style["position"] = "L";

        $pdf->write1DBarcode($referenceCustomer, "C128", "", "", "", 40, 1.0, $style, "N");
        // ---------------------------------------------------------

        //Close and output PDF document
        
        $file = $companyCode . '_' . $referenceCustomer . '_GRN_receipt.pdf';
        $pdf->Output($file, 'D');
    }
}