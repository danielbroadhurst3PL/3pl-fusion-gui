<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reworks extends MY_Secure_Controller
{

  public function __construct()
  {
    parent::__construct();
  }

  public function book()
  {
    $this->setPageDetails($this);

    $this->data[FORM_ID] = FORM_REWORK_BOOK;
    $this->data[FORM_DATA] = $this->reworkForm;
    $this->data[FORM_BUTTON] = BUTTON_REWORK_CREATE;
    $this->data[PAGE_TITLE] = TITLE_BOOK_REWORK;

    $this->load->view(PAGE_HEADER, $this->data);
    $this->load->view(PAGE_FORM, $this->data);
    $this->load->view(PAGE_FOOTER, $this->data);
  }

  public function view() {
    $this->setPageDetails($this);

    $this->data[PAGE_TITLE] = "View Booked Reworks";

    $this->load->view(PAGE_HEADER, $this->data);
    $this->load->view(PAGE_REWORK_VIEW, $this->data);
    $this->load->view(PAGE_FOOTER, $this->data);
}
}
