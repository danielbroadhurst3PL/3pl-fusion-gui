<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Exceptions extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }
    public function view() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "View Exceptions";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_EXCEPTIONS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}