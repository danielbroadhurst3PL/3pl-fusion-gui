<?php
defined('BASEPATH') or exit('No direct script access allowed');

require 'vendor/autoload.php';
use Zendesk\API\HttpClient as ZendeskAPI;

class Helpdesk extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function latest_news() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "3PL Latest News";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_LATEST_NEWS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function create_ticket() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_TICKET_CREATE;
        $this->data[FORM_DATA] = $this->ticketCreateForm;
        $this->data[FORM_BUTTON] = BUTTON_TICKET_CREATE;
        $this->data[PAGE_TITLE] = "Create Ticket";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function view_tickets() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_TICKET_CREATE;
        $this->data[ORDER_FILTERS] = $this->viewTicketsFilters;
        $this->data[PAGE_TITLE] = "View Tickets";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_VIEW_TICKETS, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function getFusionEmailToZendeskID($email) {
        $email_r = str_replace('@', '%40', $email);

        $curl = curl_init();

        curl_setopt_array($curl, array(
            // REPLACE WITH THE LOGGED IN USERS EMAIL LATER ON - $get_user->email
          CURLOPT_URL => "https://3plogistics.zendesk.com/api/v2/users/search.json?query=" . $email_r,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA==",
            "cache-control: no-cache",
            "postman-token: c48e91f2-49a0-c4b2-4434-7ac733e2d320"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);

        $rd = json_decode($response, true);

        $zendesk_user = $rd['users'][0]; // SHOULD ALWAYS BE ONE USER IN HERE - Think of better way here?
        $zendesk_user_id = $zendesk_user['id'];

        return $zendesk_user_id;
    }

    public function create_zendesk_ticket()
    {
        $full_name = $_POST['fullName'];
        $subject = $_POST['subject'];
        $description = $_POST['description'];
        $priority = $_POST['priority'];
        $customerCode = $_POST['companyCode'];
        $area = $_POST['area'];
        $cat = $_POST['category'];
        if (isset($_POST['orderNumber'])) {
            $on = $_POST['orderNumber'];
        }

        $subdomain = "3plogistics";
        $username  = "ryanwalker@3p-logistics.co.uk"; 
        $token     = "mWmjEmFcAjd2s1HlWJEv4IxJ3R0bJsSYCIHM8O02"; 

        $client = new ZendeskAPI($subdomain);
        $client->setAuth('basic', ['username' => $username, 'token' => $token]);

        // Checking if the user has a Zendesk account

        $usr_email = $this->session->userdata('email');

        $curl = curl_init();

        curl_setopt_array($curl, array(
                CURLOPT_URL => "https://3plogistics.zendesk.com/api/v2/search.json?query=type%3Auser%20email%3A$usr_email",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA==",
                    "cache-control: no-cache",
                    "postman-token: 5048a571-2244-52dd-d55e-d33c4a5ee1b1"
                ),
            ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $usercheck_json = json_decode($response, true);
        $usercheck_count = $usercheck_json['count'];

        $zendesk_id = '0';

        if($usercheck_count === 0) {

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://3plogistics.zendesk.com/api/v2/users.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\"user\": {\"name\": \"$full_name\", \"email\": \"$usr_email\"}}",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA==",
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "postman-token: cbfedf26-6bc7-c58f-7599-28e69c108d39"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                // echo "cURL Error #:" . $err;
            } else {
                // echo $response;
            }
        }

        if (!empty($_FILES)) {
            $attachment = $client->attachments()->upload([
                'file' => $_FILES['file']['tmp_name'],
                'type' => $_FILES['file']['type'],
                'name' => $_FILES['file']['name'] // Optional parameter, will default to filename.ext
            ]);
        }
        
        $zendesk_id = $this->getFusionEmailToZendeskID($usr_email);

        try {
            if (isset($attachment)) {
                $ticket = $client->tickets()->create([
                    'subject' => ($on == "" ? "" : $on . " ") . $subject,
                    'comment' => [
                        'body' => $description,
                        'uploads' => [$attachment->upload->token],
                    ],
                    'priority' => $priority,
                    'author_id' => $zendesk_id,
                    'requester_id' => $zendesk_id,
                    'fields' => [
                        [
                            'id' => '22677203',
                            'value' => $customerCode
                        ],
                        [
                            'id' => '27117802',
                            'value' => $area,
                        ],
                        [
                            'id' => '22502176',
                            'value' => $cat,
                        ],
                        [
                            'id' => '22696478',
                            'value' => $on,
                        ],
                    ],
                ]);
            } else {
                $ticket = $client->tickets()->create([
                    'subject' => ($on == "" ? "" : $on . " ") . $subject,
                    'comment' => [
                        'body' => $description,
                    ],
                    'priority' => $priority,
                    'author_id' => $zendesk_id,
                    'requester_id' => $zendesk_id,
                    'fields' => [
                        [
                            'id' => '22677203',
                            'value' => $customerCode
                        ],
                        [
                            'id' => '27117802',
                            'value' => $area,
                        ],
                        [
                            'id' => '22502176',
                            'value' => $cat,
                        ],
                        [
                            'id' => '22696478',
                            'value' => $on,
                        ],
                    ],
                ]);
            }

            $response_to_user = "Ticket submitted with ID: " . $ticket->ticket->id . ". You should receive an email shortly.";
            
            echo $response_to_user;

        } catch (\Zendesk\API\Exceptions\ApiResponseException $e) {

        }
    }

    public function update_zendesk_ticket()
    {
        if (isset([$_POST][0]['ticketID'])) {
            $ticketID = [$_POST][0]['ticketID'];
        }

        $description = $_POST['description'];

        $subdomain = "3plogistics";
        $username  = "ryanwalker@3p-logistics.co.uk"; 
        $token     = "mWmjEmFcAjd2s1HlWJEv4IxJ3R0bJsSYCIHM8O02"; 

        $client = new ZendeskAPI($subdomain);
        $client->setAuth('basic', ['username' => $username, 'token' => $token]);

        // Checking if the user has a Zendesk account

        $usr_email = $this->session->userdata('email');

        $curl = curl_init();

        curl_setopt_array($curl, array(
                CURLOPT_URL => "https://3plogistics.zendesk.com/api/v2/search.json?query=type%3Auser%20email%3A$usr_email",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA==",
                    "cache-control: no-cache",
                    "postman-token: 5048a571-2244-52dd-d55e-d33c4a5ee1b1"
                ),
            ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $usercheck_json = json_decode($response, true);
        $usercheck_count = $usercheck_json['count'];

        $zendesk_id = '0';

        if($usercheck_count === 0) {

            $full_name = [$_POST][0]['ticket']['fullName'];
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://3plogistics.zendesk.com/api/v2/users.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\"user\": {\"name\": \"$full_name\", \"email\": \"$usr_email\"}}",
                CURLOPT_HTTPHEADER => array(
                    "authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA==",
                    "cache-control: no-cache",
                    "content-type: application/json",
                    "postman-token: cbfedf26-6bc7-c58f-7599-28e69c108d39"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                // echo "cURL Error #:" . $err;
            } else {
                // echo $response;
            }
        }

        if (!empty($_FILES)) {
            $attachment = $client->attachments()->upload([
                'file' => $_FILES['file']['tmp_name'],
                'type' => $_FILES['file']['type'],
                'name' => $_FILES['file']['name'] // Optional parameter, will default to filename.ext
            ]);
        }
        
        $zendesk_id = $this->getFusionEmailToZendeskID($usr_email);

        try {
            if (isset($attachment)) {
                $ticket = $client->tickets()->update($ticketID, [
                    'comment' => [
                        'body' => $description,
                        'uploads' => [$attachment->upload->token],
                        'author_id' => $zendesk_id,
                    ],
                ]);
            } else {
                $ticket = $client->tickets()->update($ticketID,[
                    'comment' => [
                        'body' => $description,
                        'author_id' => $zendesk_id,
                    ],
                ]);
            }

            $response_to_user = "Ticket ID: " . $ticket->ticket->id . " updated. The response will be attached to this ticket shortly.";
            
            echo $response_to_user;

        } catch (\Zendesk\API\Exceptions\ApiResponseException $e) {

        }
    }

    public function view_zendesk_tickets() {
        $usr_email = $this->session->userdata('email');
        $zendesk_id = $this->getFusionEmailToZendeskID($usr_email);

        $curl = curl_init();

        $url = "https://3plogistics.zendesk.com/api/v2/search.json?";

        $openQuery = "query=status<solved+requester:$zendesk_id+type:ticket";
        $closedQuery = "query=status>=solved+requester:$zendesk_id+type:ticket";

        $url = ($this->input->get('ticketType') == 'open') ? $url . $openQuery : $url . $closedQuery;

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA=="
        ),
        ));

        $response = curl_exec($curl);
        curl_close($curl);
        header('Content-Type: application/json');
        echo $response;
    }

    public function view_zendesk_ticket() {

        if (isset($_GET['ticketID'])) {
            $ticketID = $_GET['ticketID'];
        } else {
            return false;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://3plogistics.zendesk.com/api/v2/tickets/$ticketID/comments.json",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "Authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA=="
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        header('Content-Type: application/json');
        echo $response;


    }

    public function zendeskID_to_name() {

        if (isset($_GET['authorID'])) {
            $authorID = $_GET['authorID'];
        } else {
            return false;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://3plogistics.zendesk.com/api/v2/users/" . $authorID . ".json",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "authorization: Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA==",
            "cache-control: no-cache",
            "postman-token: d2779346-e476-4f5d-a38c-c30092a70a18"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        header('Content-Type: application/json');
        echo $response;
    }
}