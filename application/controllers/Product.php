<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Product extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function create() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_PRODUCT_CREATE;
        $this->data[FORM_DATA] = $this->productForm;
        $this->data[FORM_BUTTON] = BUTTON_PRODUCT_CREATE;
        $this->data[PAGE_TITLE] = "Create Product";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function create_bulk() {
        $this->setPageDetails($this);

        $this->data[FORM_BUTTON] = BUTTON_ORDER_CREATE;
        $this->data[PAGE_TITLE] = "Create Bulk Product";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view('product/create_bulk', $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function edit_bulk() {
        $this->setPageDetails($this);

        $this->data[FORM_BUTTON] = BUTTON_ORDER_CREATE;
        $this->data[PAGE_TITLE] = "Edit Bulk Products";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view('product/edit_bulk', $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function edit() {
        $this->setPageDetails($this);

        $this->data[FORM_ID] = FORM_PRODUCT_EDIT;
        $this->data[FORM_DATA] = $this->productFormU;
        $this->data[FORM_BUTTON] = BUTTON_PRODUCT_EDIT;
        $this->data[PAGE_TITLE] = "Edit Product";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_FORM, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function current_stock_levels() {
        $this->setPageDetails($this);
        $this->data[PAGE_TITLE] = "Current Stock Levels";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_STOCK_TABLE, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function stock_breakdown() {
        $this->setPageDetails($this);
        $this->data[PAGE_TITLE] = "Stock Breakdown";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_STOCK_BREAKDOWN, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function view() {
        $this->setPageDetails($this);
        $this->data[PAGE_TITLE] = "View All Products";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_PRODUCT_TABLE, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}