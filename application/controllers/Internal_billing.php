<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Internal_Billing extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function welspun_billing() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Welspun Billing";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_WELSPUN_BILLING, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function fairy_loot_billing() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Fairy Loot Billing";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_FAIRY_LOOT_BILLING, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function precious_little_one_billing() {
        $this->setPageDetails($this);

        $this->data[PAGE_TITLE] = "Precious Little One Billing";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_INTERNAL_PRECIOUS_LITTLE_ONE_BILLING, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}