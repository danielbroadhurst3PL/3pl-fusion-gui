<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends MY_Secure_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function serial_number_record() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "Serial Number Record";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_SERIAL_NUMBER, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function stock_adjustment() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "Stock Adjustment";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_STOCK_ADJUSTMENT, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }

    public function stock_by_location() {
        $this->setPageDetails($this);
        
        $this->data[PAGE_TITLE] = "Stock by Location";

        $this->load->view(PAGE_HEADER, $this->data);
        $this->load->view(PAGE_STOCK_BY_LOCATION, $this->data);
        $this->load->view(PAGE_FOOTER, $this->data);
    }
}