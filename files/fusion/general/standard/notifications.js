$(function () {
    $('.jsdemo-notification-button button').on('click', function () {
        var placementFrom = $(this).data('placement-from');
        var placementAlign = $(this).data('placement-align');
        var animateEnter = $(this).data('animate-enter');
        var animateExit = $(this).data('animate-exit');
        var colorName = $(this).data('color-name');

        showNotification(colorName, null, placementFrom, placementAlign, animateEnter, animateExit);
    });
});

function showNotificationError(text){
    showNotification(notificationDanger, text, notificationDangerTimer);
}

function showNotificationSuccess(text){
    showNotification(notificationSuccess, text, notificationSuccessTimer);
}

function showNotification(notificationColor, notificationText, notificationTimer) {
    var animateEnter = 'animated fadeInDown';
    var animateExit = 'animated fadeOutUp';
    var allowDismiss = true;
    var placementFrom = "top";
    var placementAlign = "center";

    $.notify({
            message: notificationText
        },
        {
            type: notificationColor,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: notificationTimer,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}