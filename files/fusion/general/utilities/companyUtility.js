function get_companies(apiShowAlert, valueButton, valueModal)
{
	var api3plCompanies = urlApi + urlApiCompanies + '?' + apiKey + '=' + key + '&active=1';
	var apiData = "";
	api(false, apiShowAlert, "", api3plCompanies, methodGet, apiData, function (getCompanies){
		$.each(getCompanies.data, function(key,val){

			address = val.address.line
			var output = '';
			for (var property in address) {
  				output += address[property] + ', ';
			}
			output += val.address.region

			switch(valueButton){
                case buttonAdd:
					var valueButtonCompanyTable = "<button type='button' class='"+ buttonBlue + " " + buttonAdd + "' value='" + val.id + "'>ADD</button>";                
				break;
				case buttonEdit:
                    var valueButtonCompanyTable = "<a href='" + baseUrl + urlPageCompanyEdit + "/" + val.id + "' class='" + buttonBlue + "'>EDIT</a>";
                break;
            }

			switch(valueModal){
                case true:
                    var valueCompanyTable = "<a id='" + val.id + "' class='"+modalCompany+"' data-toggle='modal' data-target='#"+modalCompany+"' style='cursor: pointer;''>" + val.name + "</a>";
                break;
                default:
                    var valueCompanyTable = val.name;
            }


			tableCompanyViewSettings.row.add([
				valueCompanyTable,
				val.code,
				val.email.primary,
				output,
				valueButtonCompanyTable
			])
		})
		tableCompanyViewSettings.draw();
	});
}

function sortByName(a, b) {
    if (a['name'] < b['name']) return -1;
    if (a['name'] > b['name']) return 1;
    return 0;
}

function get_companies_list(apiShowAlert)
{   
    var selectId = $("#userCompany");
	var api3plCompanies = urlApi + urlApiCompanies + '?' + apiKey + '=' + key + '&active=1';
    var apiData = "";
	api(true, apiShowAlert, "", api3plCompanies, methodGet, apiData, function (getCompanies){
        companyList = getCompanies.data;
        companyList = companyList.sort(sortByName);
        for (let i = 0; i < companyList.length; i++) {
            const element = companyList[i];
            if (userCompany == element.id) {
                $("#userCompany").append("<option selected value=" + element.id + ">" + element.name + " " + "(" + element.code + ") " + "</option>")
            } else {
                $("#userCompany").append("<option value=" + element.id + ">" + element.name + " " + "(" + element.code + ") " + "</option>")
            }
        }
        selectId.selectpicker("refresh");
        document.getElementById("user").setAttribute("value", user);
        showElement(divCompanyList);
    });
}

function get_companies_form(apiShowAlert) {
    var selectId = $("#company");
	var api3plCompanies = urlApi + urlApiCompanies + '?' + apiKey + '=' + key + '&active=1';
    var apiData = "";
	api(true, apiShowAlert, "", api3plCompanies, methodGet, apiData, function (getCompanies){
        companyList = getCompanies.data;
        companyList = companyList.sort(sortByName);
        for (let i = 0; i < companyList.length; i++) {
            const element = companyList[i];
            if (userCompany == element.id) {
                $("#company").append("<option selected value=" + element.id + ">" + element.name + " " + "(" + element.code + ") " + "</option>")
            } else {
                $("#company").append("<option value=" + element.id + ">" + element.name + " " + "(" + element.code + ") " + "</option>")
            }
        }
        selectId.selectpicker("refresh");
        document.getElementById("user").setAttribute("value", user);
        showElement(divCompanyList);
    });
}

function get_company(apiShowAlert, valueCompanyNumber, populateModal, populatePage)
{

    if(populateModal === true){ 
        clear_modal_company(modalCompanyLabel);
    }

    if(populatePage === true)
    {
        $('#' + formCompanyEdit).trigger("reset"); 
    }

    var api3plCompany = urlApi + urlApiCompanies + '?' + apiKey + '=' + key + '&' + "id" + '=' + valueCompanyNumber;
    var apiData = "";
    api(false, apiShowAlert, "", api3plCompany, methodGet, apiData, function (getCompany){
        var getCompany = getCompany.data[0];
        if(populateModal === true)
        { 
            modal_company_misc(getCompany);
            modal_company_view(getCompany);
            model_company_logs(getCompany.logs);
        }

        if(populatePage === true)
        {
            populate_edit_company_form(getCompany);  
        }
        if (populateModal === false && populateModal === false) {
            return getCompany;
        }
    });
}

function get_company_promise(apiShowAlert, valueCompanyNumber)
{
    return new Promise((resolve, reject) => {
        var api3plCompany = urlApi + urlApiCompanies + '?' + apiKey + '=' + key + '&' + "id" + '=' + valueCompanyNumber;
        var apiData = "";
        api(false, apiShowAlert, "", api3plCompany, methodGet, apiData, async function (getCompany){
            var getCompany = getCompany.data[0];
            resolve(getCompany);
        });
    })
}