function obKey(lineStarting) {
    return lineStarting + "key=" + key;
}

function encode_utf8(s) {
    return unescape(encodeURIComponent(s));
}

function decodeUTF8(s) {
    return decodeURIComponent(escape(s));
}

function sortSelect(select, attr, order) {
    if (attr === 'text') {
        if (order === 'asc') {
            $(select).html($(select).children('option').sort(function (x, y) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }
        if (order === 'desc') {
            $(select).html($(select).children('option').sort(function (y, x) {
                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
            }));
            $(select).get(0).selectedIndex = 0;
            e.preventDefault();
        }
    }
}

function isVarEmpty(variable) {
    return variable === "" || variable === undefined;
}