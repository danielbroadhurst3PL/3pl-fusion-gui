function table_buttons(dataPages) {
    var dataPageMin = dataPages.min;
    var dataPageMax = dataPages.max;
    var dataPageCurrent = dataPages.current;    
    var dataPageNext = dataPages.next;
    var dataPagePrevious = dataPages.previous;

    // show all buttons
    showButton(buttonPageMin);
    showButton(buttonPagePrevious);
    showButton(buttonPageMax);
    showButton(buttonPageNext);
    showButton(buttonPageCurrent);
    showButton(buttonNext);
    showButton(buttonPrevious);

    // enable all buttons
    enableButton(buttonPageMin);
    enableButton(buttonPagePrevious);
    enableButton(buttonPageMax);
    enableButton(buttonPageNext);
    enableButton(buttonPageCurrent);
    enableButton(buttonNext);
    enableButton(buttonPrevious);
    
    $("#" + buttonPageMin).val(dataPageMin);
    $("#" + buttonPageMin).html(dataPageMin);

    $("#" + buttonPageMax).val(dataPageMax);
    $("#" + buttonPageMax).html(dataPageMax);

    $("#" + buttonPageCurrent).val(dataPageCurrent);
    $("#" + buttonPageCurrent).html(dataPageCurrent);

    $("#" + buttonPageNext).val(dataPageNext);
    $("#" + buttonPageNext).html(dataPageNext);

    $("#" + buttonPagePrevious).val(dataPagePrevious);
    $("#" + buttonPagePrevious).html(dataPagePrevious);

    $("#" + buttonNext).val(dataPageNext);
    $("#" + buttonPrevious).val(dataPagePrevious);

    if (dataPageMin === dataPageCurrent) {
        hideButton(buttonPageMin);
        disableButton(buttonPrevious);
    }

    if (dataPagePrevious === dataPageCurrent) {
        hideButton(buttonPagePrevious);
    }

    if(dataPagePrevious === dataPageMin)
    {
      hideButton(buttonPagePrevious);
    }

    if (dataPageMax === dataPageCurrent) {
        hideButton(buttonPageMax);
        disableButton(buttonNext);
    }

    if (dataPageNext === dataPageCurrent) {
        hideButton(buttonPageNext);
    }

    if(dataPageNext === dataPageMax)
    {
      hideButton(buttonPageNext);
    }

    
    disableButton(buttonPageCurrent);
    
}

function datatable_clear(datatableSettings)
{
    datatableSettings.clear()
    datatableSettings.draw();
}