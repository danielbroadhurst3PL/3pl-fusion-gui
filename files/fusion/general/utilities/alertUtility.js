function getErrorCode(error) {
    return error.status;
}

function alertError(data) {
    var message = "";

    if (data.error_message === undefined || data.error_message === null) {
        for (var i = 0; i < data.responseJSON.error_message.length; i++) {
            message = message + data.responseJSON.error_message[i] + "\n";
        }
    } else {
        for (var i = 0; i < data.error_message.length; i++) {
            message = message + data.error_message[i] + "\n";
        }
    }
    alertCustomError(decodeUTF8(message));
}

function getErrorString(code, data) {
    var message = "";

    switch (code) {
        case 200:
            for (var i = 0; i < data.error_message.length; i++) {
                message = message + data.error_message[i] + "\n";
            }
            break;
        case 202:
            for (var i = 0; i < data.error_message.length; i++) {
                message = message + data.error_message[i] + "\n";
            }
            break;
        default:
            for (var i = 0; i < data.responseJSON.error_message.length; i++) {
                message = message + data.responseJSON.error_message[i] + "\n";
            }
            break;
    }
    return decodeUTF8(message);
}

function alertPopup(alertTitle, alertMessage, alertType, alertTimeOut){
    var alertSpecial = alertType;

    if(alertType === "redirect")
    {
        alertType = "error";
    }

    swal({
        title: alertTitle,
        text: alertMessage,
        type: alertType,
        animation: "slide-from-top",
        closeOnClickOutside: false,
        closeOnEsc: false
    });

    if(alertSpecial === "info" || alertSpecial === "redirect")
    {
        swal.disableButtons();  
    } 

    // Fixes the tab button not working - https://github.com/t4t5/sweetalert/issues/391
    window.onkeydown = null;
    window.onfocus = null   
}

function alertCustomSuccess(methodType, filter) {
    if(methodType === methodPost){
        var alertMessage = "Created " + filter; 
    }
    else
    {
        var alertMessage = "Edited " + filter;
    }

    alertPopup("Success", alertMessage, "success");
}

function alertCustomError(message) {
    alertPopup("Error", message, "error");
}

function alertCustomWarning(message) {
    alertPopup("Warning", message, "warning");
}

function alertCustomInformation(message) {
    alertPopup("Information", message, "info");
}

function alertCustomQuestion(message) {
    alertPopup("Question", message, "warning");
}

function alertOperationTimedOut() {
    alertPopup(ERROR_OPERATION_TIMED_OUT.title, ERROR_OPERATION_TIMED_OUT.body, "error");
}

function alertCustomLoading() {
    alertPopup("Loading", "Loading Data", "info");
}

function alertCustomCreating(message) {
    alertPopup("Creating", message, "info");
}

function alertCustomEditing(message) {
    alertPopup("Editing", message, "info");
}

function alertCustomRedirect(message) {
    alertPopup("Error", message, "redirect");
}

function alertCustomRedirectSuccess(message) {
    alertPopup("Success", message, "info");
}


function closeAllSwalWindows() {
    swal.close();
}
