function generate_select(selectIdName, getData)
{	

    var selectId = $("#" + selectIdName);

    var arrayTemp = [];

    $.each(getData.data, function (key, val) {
    		var selectSelected = "";
    		switch(selectIdName)
    		{
    			case currency:
    				if(val.id === 52)
    				{
    					selectSelected = "selected";
    				}
    			break;
    			case country:
    				if(val.id === 234)
    				{
    					selectSelected = "selected";
    				}
    			break;
    			default:

    				if(val.id === 1){
    					selectSelected = "selected";
    				}
    		}
			if (val.aisle != null) {
				arrayTemp.push(`<option value=${val.id}>${val.aisle}</option>`)
			} else if (val.area > 0) {
				arrayTemp.push(`<option value=${val.id}>${val.area}</option>`)
			} else {
				arrayTemp.push('<option value="' + val.id + '" ' + selectSelected + '>' + val.name + '</option>');
			}
    });
    selectId.html(arrayTemp);
    selectId.selectpicker("refresh");
}