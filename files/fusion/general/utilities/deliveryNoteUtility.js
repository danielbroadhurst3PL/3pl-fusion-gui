function get_delivery_notes_select(apiShowAlert, filterCompanyId)
{
	var api3plDeliveryNotes = urlApi + urlApiDeliveryNotes + '?' + apiKey + '=' + key;

	if(filterCompanyId !== null)
	{
		api3plDeliveryNotes = api3plDeliveryNotes + "&companyId=" + filterCompanyId;
	}

	var apiData = "";
	api(false, apiShowAlert, "", api3plDeliveryNotes, methodGet, apiData, function (getDeliveryNotes){
		generate_select('deliveryNote', getDeliveryNotes);
	});
}