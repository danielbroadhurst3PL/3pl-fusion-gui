let servicesArray = {};
function get_service(apiShowAlert, uk = null)
{
	var api3plService = urlApi + urlApiService + '?' + apiKey + '=' + key;
	if (uk !== null) {
		api3plService += `&uk=${uk}`
	}
	var apiData = "";
	api(false, apiShowAlert, "", api3plService, methodGet, apiData, function (getService){		
		let customService = {
			id: getService.data.length,
			name: "CUSTOM SERVICE",
			parcelFormat: {
				id: 5
			}
		}
		getService.data.push(customService);
		servicesArray.data = multiSort(getService.data, {name: 'asc'});
		generate_select(service, servicesArray);
	});
}

function get_service_promise(apiShowAlert, uk = null)
{
	return new Promise((resolve, reject) => {
		try {
			var api3plService = urlApi + urlApiService + '?' + apiKey + '=' + key;
			if (uk !== null) {
				api3plService += `&uk=${uk}`
			}
			var apiData = "";
			api(false, apiShowAlert, "", api3plService, methodGet, apiData, function (getService) {		
				servicesArray.data = multiSort(getService.data, {name: 'asc'});
				generate_select(service, servicesArray);
				resolve(true);
			});
		} catch (error) {
			reject(error)
		}
	})
}