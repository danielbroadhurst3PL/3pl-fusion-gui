// Add button pressed
$("#" + tableProductsView +  " tbody").on( "click", "button", function () {
    var tableItemsRowAdd = tableProductsViewSettings.row( $(this).parents('tr') ).data();    
    var tableItemsRowAddJson = btoa(JSON.stringify(tableItemsRowAdd));

    tableProductsViewSettings
        .row( $(this).parents('tr') )
        .remove()
        .draw();

    if(currentPage !== "grn/create"){
        tableItemsViewSettings.row.add([
                tableItemsRowAdd.SKUId,
                "<input type='number' id=" + tableItemQuantityTextbox + "_" + btoa(JSON.stringify(tableItemsRowAdd.SKUId)) + " class='form-control' step='1' min='1' value='1'>",
                "<input type='number' id=" + tableItemValueTextbox + "_" + btoa(JSON.stringify(tableItemsRowAdd.SKUId)) + " class='form-control' step='0.01' min='1' value='1'>",
                "<button type='button' class='"+ buttonRed + " " + buttonRemove + "' value='" + tableItemsRowAddJson + "'>REMOVE</button>"
                ]);
    }
    else
    {
        tableItemsViewSettings.row.add([
                tableItemsRowAdd.SKUId,
                "<input type='number' id=" + tableItemQuantityTextbox + "_" + btoa(JSON.stringify(tableItemsRowAdd.SKUId)) + " class='form-control' step='1' min='1' value='1'>",
                "<button type='button' class='"+ buttonRed + " " + buttonRemove + "' value='" + tableItemsRowAddJson + "'>REMOVE</button>"
                ]);
    }

    
    tableItemsViewSettings.draw();
} );

// Remove button pressed
$(document).on("click", "." + buttonRemove, function () {    
    tableItemsViewSettings.row( $(this).parents('tr') ).remove().draw();
    var decodedItem = JSON.parse(atob($(this).val()));
    tableProductsViewSettings.row.add({
        "SKUId":        decodedItem.SKUId,
        "Description":  decodedItem.Description,
        "Text":         decodedItem.Text,
        "Barcode":      decodedItem.Barcode,
        "Qty":          decodedItem.Qty,
        "QtyAllocated": decodedItem.QtyAllocated,
        "QtyAssigned":  decodedItem.QtyAssigned,
        "QtyDueIn":     decodedItem.QtyDueIn,
        "QtyDueOut":    decodedItem.QtyDueOut,
        "Status":       decodedItem.Status
    }).draw();
});