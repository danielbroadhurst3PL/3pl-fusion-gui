function get_dgn_details(apiShowAlert)
{
	var api3plDgnDetails = urlApi + urlApiDgnDetails + '?' + apiKey + '=' + key;
	var apiData = "";
	api(false, apiShowAlert, "", api3plDgnDetails, methodGet, apiData, function (getDgnDetails){
		generate_select(dgnDetails, getDgnDetails);
	});
}

function get_dgn_types(apiShowAlert)
{
	var api3plDgnTypes = urlApi + urlApiDgnTypes + '?' + apiKey + '=' + key;
	var apiData = "";
	api(false, apiShowAlert, "", api3plDgnTypes, methodGet, apiData, function (getDgnTypes){
		generate_select(dgnType, getDgnTypes);
	});
}