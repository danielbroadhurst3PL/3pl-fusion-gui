function validateInputField(elementInformation) {
    var state = true;

    if (!(elementInformation instanceof Array)) {
        return false;
    } else {
        for (var i = 0; i < elementInformation.length; i++) {
            if (
                $("#" + elementInformation[i].id).val() === undefined ||
                $("#" + elementInformation[i].id).val() === "") {

                alertCustomError(elementInformation[i].title + " is required");

                state = false;

                break;
            }
        }
    }
    return state;
}

function validateSelectField(elementInformation) {
    var state = true;

    if (!(elementInformation instanceof Array)) {
        return false;
    } else {
        for (var i = 0; i < elementInformation.length; i++) {
            if (
                $("#" + elementInformation[i].id).val() === undefined ||
                $("#" + elementInformation[i].id).val() === "") {

                alertCustomError(elementInformation[i].title + " is required");

                state = false;

                break;
            }
        }
    }
    return state;
}