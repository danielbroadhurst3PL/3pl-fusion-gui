function hideElement(elementId) {
    $("#" + elementId).css({"display": "none"});

}

function showElement(elementId) {
    $("#" + elementId).css({"display": "block"});
}