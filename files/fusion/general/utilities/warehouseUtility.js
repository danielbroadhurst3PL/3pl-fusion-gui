function get_picking_aisle(apiShowAlert) {
    var api3plCountries = urlApi + "warehouse/picking_aisles" + '?' + apiKey + '=' + key;
	var apiData = "";
	api(false, apiShowAlert, "", api3plCountries, methodGet, apiData, function (getPickingAisle){
		generate_select(pickingAisle, getPickingAisle);
	});
}

function get_bulk_storage(apiShowAlert) {
    var api3plCountries = urlApi + "warehouse/bulk_storage_areas" + '?' + apiKey + '=' + key;
	var apiData = "";
	api(false, apiShowAlert, "", api3plCountries, methodGet, apiData, function (getBulkStorage){
		generate_select(bulkStorage, getBulkStorage);
	});
}