function startButtonLoading(buttonId) {
    disableButtons();
    hideButton(buttonId);
    showLoader(buttonId);
}

function stopButtonLoading(buttonId) {
    if(buttonId !== null) {
        hideLoader(buttonId);
        showButton(buttonId);
        enableButtons();
    }
}

// Disable features
function disableButtons() {
    $("." + buttonToggleable).attr("disabled", true);
}

function disableButton(buttonId) {
    $("#" + buttonId).attr("disabled", true);
}

function hideButton(buttonId) {
    $("#" + buttonId).css({"display": "none"});
}

function showLoader(buttonId) {
    var loaderId = buttonId.replace("button", "loader");
    $("#" + loaderId).css({"display": "inline-block"});
}

function showLoaders() {
    $("." + buttonLoader).css({"display": "none"});
}

// Enabling buttons
function hideLoader(buttonId) {
    var loaderId = buttonId.replace("button", "loader");
    $("#" + loaderId).css({"display": "none"});
}

function enableButtons() {
    $("." + buttonToggleable).attr("disabled", false);
}

function enableButton(buttonId) {
    $("#" + buttonId).attr("disabled", false);
}

function showButton(buttonId) {
    $("#" + buttonId).css({"display": "inline-block"});
}

function hideLoaderById(loadingId) {
    $("#" + loadingId).css({"display": "none"});
}