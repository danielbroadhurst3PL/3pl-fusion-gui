let apiTotalRequests = 0;
let newsApiUrl = `${urlApi}news?${apiKey}=${key}`;
let articles = [];
let pageNumber = 1;
let totalPages = 1;

function getNews() {
  let newsUrl = `${newsApiUrl}&page=${pageNumber}`
  api(false, true, "", newsUrl, methodGet, [], function (data) {
    apiTotalRequests++;
    articles = data.data;
    if (data.results.pages) {
      totalPages = data.results.pages;
    }
    displayArticles();
  })
}

function displayArticles() {
  if (articles.length > 0) {
    $('#newsDiv').html("");
    articles.forEach(article => {
      let unread = "";
      let icon = "";
      let unreadArticle = newsArticles.find(unreadNews => unreadNews.id === article.id);
      if (unreadArticle) {
        unread = 'bg-blue-grey';
        icon = `
        <ul class="header-dropdown m-r-0" id='unread_${article.id}'>
          <li>
            <a href="javascript:void(0);" onclick="markArticleRead('${article.id}');markReadNews('${article.id}')">
                <i class="material-icons">markunread_mailbox</i>
            </a>
          </li>
        </ul>`
      }
      $('#newsDiv').append(`
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
          <div class="header ${unread}" id="article_${article.id}">
            <h2>
              ${article.subject} 
              <small>${article.created_by}</small>
              <small>${article.created_at}</small>
            </h2>
            ${icon}
          </div>
          <div class="body">
            ${article.body}
          </div>
        </div>
      </div>
      `)
    });
  }
}

function markReadNews(id) {
  $('#article_'+id).removeClass('bg-blue-grey');
  $('#unread_'+id).remove();
}

function loadPrev() {
  pageNumber--;
  checkPagination('next')
  checkPagination('prev')
  getNews();
}

function loadNext() {
  pageNumber++;
  checkPagination('prev')
  checkPagination('next')
  getNews();
}

function checkPagination(target) {
  $('#' + target).removeClass('disabled');
  $('#' + target).each(function () {
    this.style.pointerEvents = 'auto';
  });
  if (target == 'prev' && pageNumber == 1) {
    $('#' + target).addClass('disabled');
    $('#' + target).each(function () {
      this.style.pointerEvents = 'none';
    });
  }
  if (target == 'next' && totalPages == pageNumber) {
    $('#' + target).addClass('disabled');
    $('#' + target).each(function () {
      this.style.pointerEvents = 'none';
    });
  }
}

getNews()
checkPagination('prev')