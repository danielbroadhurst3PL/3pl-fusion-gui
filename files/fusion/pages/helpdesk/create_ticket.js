var apiTotalRequests = 0;

$('#area').change(function(e) {
    categories = []
    selectedArea = e.target.id;
    switch (selectedArea) {
        case 'area_Amendment':
            categories = [{'address_amendment': 'Address Amendment'},
                        {'cancel_order':'Cancel Order'},
                        {'amendment__change_request': 'Change Request'},
                        {'delete_order': 'Delete Order'},
                        {'a__despatch_consolidation': 'Despatch Consolidation'},
                        {'order_item_amendment': 'Order Item Amendment'},
                        {'service_amendment': 'Service Amendment'},
                        {'system_amendment': 'System Amendment'}]
            break;
        case 'area_Complaint':
            categories = [{'cross_pack': "Cross-Pack"},
                        {'damage': "Damage"},
                        {'inbound_error': "Inbound Error"},
                        {'inbound_timeliness': "Inbound Timeliness"},
                        {'incorrect_address': "Incorrect Address"},
                        {'incorrect_information': "Incorrect Information"},
                        {'incorrect_service': "Incorrect Service"},
                        {'late_delivery': "Late Delivery"},
                        {'linnworks_stock_levels': "Linnworks Stock Levels"},
                        {'miss_pick': "Miss-Pick"},
                        {'missing_information': "Missing Information"},
                        {'non_delivery': "Non Delivery"},
                        {'outage': "Outage"},
                        {'over_pick': "Over Pick"},
                        {'packaging': "Packaging"},
                        {'quality_issue': "Quality Issue"},
                        {'returns_error': "Returns Error"},
                        {'short_pick': "Short Pick"}]
            break;
        case 'area_Finance':
            categories = [{'finance__invoice_error': 'Invoice Error'},
                        {'finance__overcharge': 'Overcharge'},
                        {'finance__undercharge': 'Undercharge'}]
            break;
        case 'area_Inventory':
            categories = [{'inventory__inbound_file': 'Inbound File'},
                        {'inventory__item_information_check': 'Item Information Check'},
                        {'inventory__item_qty_check': 'Item Quantity Check'},
                        {'inventory__re_work_request':'Re-Work Request'}]            
            break;
        case 'area_PostDespatch':
            categories = [{'enquiry__service_enquiry':'Delivery Service Enquiry'},
                        {'enquiry__tracking':'Delivery Tracking'},
                        {'enquiry__order_status':'Order Status'},
                        {'enquiry__pod_request':'POD Request'},
                        {'enquiry__quote':'Quote'},
                        {'post_despatch_enquiry__returns_request':'Returns Request'}]
            break;
        case 'area_PreDespatch':
            categories = [{'pre_despatch_update___enquiry___quote':'Quote'},
                        {'pre_despatch_update___enquiry__customer_ncr':'Customer NCR'},
                        {'order_instruction__date_requirement':'Date Requirement'},
                        {'order_instruction__delivery_note':'Delivery Note'},
                        {'order_instruction__despatch_consolidate':'Despatch Consolidate'},
                        {'pre_despatch_update___enquiry__internal_check':'Internal Check'},
                        {'order_instruction__invoice':'Invoice'},
                        {'order_instruction__order_file':'Order File'},
                        {'order_update__order_status':'Order Status'},
                        {'order_instruction__packing_instructions':'Packing Instructions'},
                        {'order_instruction__returns_request':'Returns Request'},
                        {'order_instruction__tracker':'Tracker'},
                        {'order_instruction__vendor_booking':'Vendor Booking'},
                        {'order_instruction__vendor_requirements':'Vendor Requirements'}]
            break;
        case 'area_Systems':
            categories = [{'systems__data_request':'Data Request'},
                        {'systems__feature_request':'Feature Request'},
                        {'systems__fusion_query':'Fusion Query'},
                        {'systems__linnworks_query':'Linnworks Query'},
                        {'systems_waysofworking':'Ways Of Working'}]
            break;
    }

    containerDiv = document.createElement('div')
    containerDiv.setAttribute('class', 'col-md-12')
    formGroupDiv = document.createElement('div')
    formGroupDiv.setAttribute('class', 'form-group')
    containerDiv.appendChild(formGroupDiv)
    categoryLabel = document.createElement('label')
    categoryLabel.setAttribute('for', 'area')
    categoryLabelText = document.createTextNode("Category")
    categoryLabel.appendChild(categoryLabelText)
    formGroupDiv.appendChild(categoryLabel)
    buttonGroupDiv = document.createElement('div')
    buttonGroupDiv.setAttribute('class', 'btn-group btn-group-toggle')
    buttonGroupDiv.setAttribute('id', 'category')
    buttonGroupDiv.setAttribute('data-toggle', 'buttons')

    categories.forEach(element => {
        categoryKey = Object.keys(element)
        value = Object.values(element)
        buttonLabel = document.createElement('label')
        buttonLabel.setAttribute('class', 'btn btn-secondary')
        buttonLabel.setAttribute('for', `${categoryKey}`)
        buttonLabelText = document.createTextNode(value)
        buttonLabel.appendChild(buttonLabelText)
        input = document.createElement('input')
        input.setAttribute('name', 'category')
        input.setAttribute('tabindex', "5")
        input.setAttribute('type', 'radio')
        input.setAttribute('id', `${categoryKey}`)
        buttonLabel.appendChild(input)
        buttonGroupDiv.appendChild(buttonLabel)
    });

    formGroupDiv.appendChild(buttonGroupDiv)

    if ($('#category').length) {
        $('#category').closest(".col-md-12").remove()
    }

    $('#area').closest(".col-md-12").after(containerDiv)

    $('#category').click(function () {        
        if ($('#categoryError').length) {
            $('#categoryError').remove();
        }
        categoryCheck = true;
    });
})

var areaCheck = false;
var categoryCheck = false;

$("#" + formTicketCreate).submit(function(e) {    
    e.preventDefault();

    var areaRequired = $('#area' + ' :input');
    var categoryRequired = $('#category' + ' :input');

    for (let i = 0; i < areaRequired.length; i++) {
        const element = areaRequired[i];
        if (element.checked == true) {
            areaCheck = true
        }
    }
    if (areaCheck == false) {
        $('#area').after(`<p class="formError" id="areaError">Please select an area related to the issue you have encountered.</p>`)
        return
    }

    for (let i = 0; i < categoryRequired.length; i++) {
        const element = categoryRequired[i];
        if (element.checked == true) {
            categoryCheck = true
        }
    }
    if (categoryCheck == false) {
        $('#category').after(`<p class="formError" id="categoryError">Please select a category related to the area which you have selected.</p>`)
        return
    }

    alertCustomLoading();
    
    var dataTicketCreateForm = $('#' + formTicketCreate + ' :input');
    
    var apiFormatFormData = zendesk_format_form_data_post(dataTicketCreateForm);
    
    apiData = apiFormatFormData;

    apiData['fullName'] = $('#fullName').text().trim();
    
    $.ajax({
        type: 'POST',
        url: `${baseUrl}helpdesk/create_zendesk_ticket`,
        data: apiData,
        //dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {            
            alertCustomSuccess("POST", response);
        },
        error: function(x, h, r) {
            alertCustomError(x, h, r);
        }
    });    

});

$('#area').click(function () {    
    if ($('#areaError').length) {
        $('#areaError').remove();
    }
    areaCheck = true;
});

$('#priority_Normal').prop("checked", true)
$('#priority_Normal').parents("label").addClass('active')

$('#priority_High').change(function () {    
    alertCustomWarning(`Do you remember the boy who cried wolf? Is this really an urgent request? High priority is to be used when you cannot wait for your agreed ticket response times. Urgent tickets are potentially a chargable exercise.`)
})

$('#orderNumber').val(companyCode)
