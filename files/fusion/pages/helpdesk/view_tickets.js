const ticketDataUrl = `${baseUrl}helpdesk/view_zendesk_tickets`;

// Set Open to Active
createZendeskSettings('open');
$('#type' + " :first-child").addClass('active');

$("#" + type).change(function () {
    var selectedTypeVal = $('input[name=options]:checked').attr('id');
    if (selectedTypeVal == 'closed') {
        createZendeskSettings('closed');
        $('#openTicketsDiv').hide();
        $('#closedTicketsDiv').show();
    } else {
        createZendeskSettings('open');
        $('#openTicketsDiv').show();
        $('#closedTicketsDiv').hide();
    }
});

function createZendeskSettings(ticketType) {
    alertCustomLoading();
    let settings = {
        "url": `${baseUrl}/helpdesk/view_zendesk_tickets?ticketType=${ticketType}`,
        "method": "GET",
        "timeout": 0
    };
    loadTickets(settings);
}

let tableTicketsOpen = null;
let tableTicketsClosed = null;
function loadTickets(settings) {
    $.ajax(settings).done(function (response) {
        let openTickets = new Array;
        let closedTickets = new Array;
        ticketData = response.results;
        ticketData.forEach(singleTicket => {
            if (singleTicket.status == "solved" || singleTicket.status == "closed") {
                closedTickets.push(singleTicket);
            }
            else {
                openTickets.push(singleTicket);
            }
        });
        if (tableTicketsOpen !== null) {
            tableTicketsOpen.destroy();
        }
        tableTicketsOpen = $('#tableTicketsViewOpen').DataTable({
            dom: 'Bfrtip',
            pageLength: 10,
            data: openTickets,
            responsive: true,
            columns: [
                {
                    data: "id", render: function (data) {
                        return `<a href=Javascript:void(0) onclick="viewSingleTicket('${data}')" id="${data}">${data}</a>`;
                    }
                },
                { data: "status" },
                { data: "priority" },
                { data: "subject" },
                {
                    data: "created_at", render: function (data) {
                        let utcdate = new Date(data);
                        return utcdate.toLocaleString();
                    }
                },
                {
                    data: "updated_at", render: function (data) {
                        let utcdate = new Date(data);
                        return utcdate.toLocaleString();
                    }
                },
            ],
            "order": [[5, "asc"]],
            buttons: [
                'copy',
                'csv',
                'excelHtml5',
                'pdfHtml5',
                'print'
            ],
        });
        if (tableTicketsClosed !== null) {
            tableTicketsClosed.destroy();
        }
        tableTicketsClosed = $('#tableTicketsViewClosed').DataTable({
            dom: 'Bfrtip',
            pageLength: 10,
            data: closedTickets,
            responsive: true,
            columns: [
                {
                    data: "id", render: function (data) {
                        return `<a href=Javascript:void(0) onclick="viewSingleTicket('${data}')" id="${data}">${data}</a>`;
                    }
                },
                { data: "status" },
                { data: "priority" },
                { data: "subject" },
                {
                    data: "created_at", render: function (data) {
                        let utcdate = new Date(data);
                        return utcdate.toLocaleString();
                    }
                },
                {
                    data: "updated_at", render: function (data) {
                        let utcdate = new Date(data);
                        return utcdate.toLocaleString();
                    }
                },
            ],
            "order": [[5, "asc"]],
            buttons: [
                'copy',
                'csv',
                'excelHtml5',
                'pdfHtml5',
                'print'
            ],
        });
        closeAllSwalWindows();
    });
}

function viewSingleTicket(ticketID) {
    alertCustomLoading();
    var singleTicketSettings = {
        "url": `${baseUrl}/helpdesk/view_zendesk_ticket?ticketID=${ticketID}`,
        "method": "GET",
        "timeout": 0
    };
    $.ajax(singleTicketSettings).done(function (response) {
        let comments = response.comments;
        createComment(comments, ticketID)
        return
    })
}

async function createComment(comments, ticketID) {
    $('#largeModalMessages').modal().show();
    $('#modalMessages').html("");
    $('#largeModalLabel').html("");
    $('#infoText').html("")
    $('#infoHeader').html("")
    for (let i = 0; i < comments.length; i++) {
        let authorID = comments[i].author_id;
        let commentBody = comments[i].html_body;
        const authorName = await $.getJSON(`${baseUrl}/helpdesk/zendeskID_to_name?authorID=${authorID}`);

        $('#modalMessages').append(`<div><h3>${authorName.user.name}</h3>${commentBody}<div id="commentAttachments${i}"></div></div>`)
        if (comments[i].attachments.length > 0) {
            let fileUrl = comments[i].attachments[0].content_url
            let fileThumbnail = comments[i].attachments[0].thumbnails[0].content_url
            $('#commentAttachments' + i).append(`<a href="${fileUrl}" target="_blank"><img src="${fileThumbnail}"></img></a>`)
        }
    }
    $('#largeModalLabel').html(`Ticket ID: ${ticketID}`)
    closeAllSwalWindows();
    $('#modalMessages').append(`<div id="ticketReply"></div><button id="buttonReplyOpen" type="submit" class="${buttonBlue}" onclick="showReplyInput(${ticketID})">Reply to Ticket</button>`)
}

function showReplyInput(ticketID) {
    $('#buttonReplyOpen').remove();
    $('#ticketReply').append(`<label for="${ticketID}">Enter your response:</label><textarea id="${ticketID}" name="story" rows="5" cols="33"></textarea><p>File <span>(optional)</span></p><input type="file" id="file" class="form-control"><button id="buttonReplySend" class="${buttonBlue}" onclick="submitUpdateToZendesk('${ticketID}')">Reply to Ticket</button>`);
}

function submitUpdateToZendesk(ticketID) {

    alertCustomLoading();

    var dataTicketCreateForm = $('#formTicketUpdate' + ' :input, textarea');

    var apiFormatFormData = zendesk_format_form_data_post(dataTicketCreateForm);

    apiData = apiFormatFormData;

    apiData.append('ticketID', ticketID);

    $.ajax({
        type: 'POST',
        url: `${baseUrl}helpdesk/update_zendesk_ticket`,
        data: apiData,
        //dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function (response) {
            alertCustomSuccess("POST", response);
        },
        error: function (x, h, r) {
            alertCustomError(x, h, r);
        }
    });
};