let apiTotalRequests = 1;
const exceptionListUrl = `${urlApi}exceptions?${apiKey}=${key}`;
const exceptionInfoURL = `${urlApi}exceptions/exceptions_information?${apiKey}=${key}`;
let previousOrderNumber = null;
let previousWidth = null;
let exceptionInfo = null;

let apiData = [];
api(false, true, "", exceptionListUrl, methodGet, apiData, function (getExceptions) {
  console.log(getExceptions);
  exceptionsList = getExceptions.data;
  exceptionsList.forEach(element => {
    $('#exceptionCards').append(`
    <div class="exceptionCard" id="${element.ShipmentId}">
      <div class="card">
        <div class="header">
              <h2 id="returnsTitle">${element.ErrorType} Exception</h2>
        </div>
        <div class="body">
          <div class="exceptionID"><p><strong>Shipment ID:</strong> <a href="Javascript:void(0)" onclick="viewExceptionInformation('${element.ShipmentId}')">${element.ShipmentId}</a></p></div>
          <div id="exceptionInfo${element.ShipmentId}" class="exceptionHide"></div>
        </div>
      </div>
    </div>`)
  });
});

function viewExceptionInformation(orderNumber) {
  if (previousOrderNumber != null) {
    $('#exceptionInfo' + previousOrderNumber).html('')
    $('#' + previousOrderNumber).toggleClass('active')
  }
  let exceptionOrderInfo = `${exceptionInfoURL}&orderNumber=${orderNumber}`;
  let exceptionPromise = new Promise((resolve) => {
    api(false, true, "", exceptionOrderInfo, methodGet, apiData, function (getExceptionInfo) {
      apiTotalRequests += 1;
      resolve(exceptionInfo = getExceptionInfo.data[0]);
    });
  })
  exceptionPromise.then((data) => {
    console.log(data);
    populateCard(orderNumber, data);
    previousOrderNumber = orderNumber;
  })
}

function populateCard(orderNumber, exceptionInfo) {
  $(`#${orderNumber}`).addClass('active')
  if (exceptionInfo != undefined) {    
    $('#exceptionInfo' + orderNumber).append(`
    <p>${check_order_edit(statusBack, "button", orderNumber)}</p>
    <p><strong>Type:</strong> ${exceptionInfo.Type}</p>
    <p><strong>Error Area:</strong> ${exceptionInfo['Error Area']}</p>
    <p><strong>Key:</strong> ${exceptionInfo.Key}</p>
    <p><strong>Message Contents:</strong> ${exceptionInfo.MessageContents}</p>
    <p><strong>Errors:</strong> ${exceptionInfo.Errors}</p>
    <p><strong>Date Created:</strong> ${exceptionInfo.DateCreated}</p>
    <p><strong>This order will need to be Deleted and re-submitted to the system with the Error's fixed</strong></p>`);
  } else {
    $('#exceptionInfo' + orderNumber).append(`
    <p>Unfortunately we cannot find the error information for this order. Please raise a ticket with <a href="${baseUrl}helpdesk/create_ticket">Customer Service</a>.</p>`);
  }
}
