var apiTotalRequests = 1;

get_countries(true);
get_picking_aisle(true);
get_bulk_storage(true);

$("#" + formCompanyCreate).submit(function(e) {
	e.preventDefault();
	var dataCreateCreateForm = $('#' + formCompanyCreate + ' :input');

	var apiFormatFormData = api_format_form_data(dataCreateCreateForm);
	submit_company(apiFormatFormData);
});

function submit_company(apiData)
{
	alertCustomCreating("Creating Company " + apiData['companyName']);
	var api3plCompany = urlApi + urlApiCompanies;
	api(false, true, apiData[customerCode], api3plCompany, methodPost, apiData, function (createCompany){  
		$('#' + formCompanyCreate).trigger("reset");
		alertCustomSuccess(methodPost, apiData['companyName']);
	});
}

/**
 * Populates the sidebarinfomation area with info about the selected card.
 * Animates the switching of card info so sidebar can remain open.
 * @param {ID from the Form} formID 
 */
function formInformation(formID) {
	// Fade information out whilst we swtich information
	$('#infoText,#infoHeader').animate({opacity: 0},150, function () {
		// Re-write the Header and clear the text area
		$('#infoText').html("")
		$('#infoHeader').html("")
		infoTitle = formID.replace(/([A-Z])/g, ' $1').trim();
		$('#infoHeader').html(`${infoTitle}`)
		// Populate Data for the selected card
		switch (formID) {
			case "CompanyAddress":
				$('#infoText').append(`<p><strong>Company:</strong><br />Insert company name.</p>`)
				$('#infoText').append(`<p><strong>Company Code:</strong><br />3 digits which represent the Company.</p>`)
				$('#infoText').append(`<p><strong>Address Line 1:</strong><br />First line of company address.</p>`)
				$('#infoText').append(`<p><strong>Address Line 2:</strong><br />Second line of company address.</p>`)
				$('#infoText').append(`<p><strong>Town:</strong><br />Enter the company town.</p>`)
				$('#infoText').append(`<p><strong>Region:</strong><br />Enter the county of the company.`)
				$('#infoText').append(`<p><strong>Postcode:</strong><br />Must be a valid Postcode.`)
				$('#infoText').append(`<p><strong>Country:</strong><br />Select the country from the dropdown menu.`)
				break;
			case "CompanyEmail":
				$('#infoText').append(`<p><strong>3PL Email:</strong><br />Customer email address which is hosted by 3PL.</p>`)
				$('#infoText').append(`<p><strong>Primary Email:</strong><br />Main contact from the Company.</p>`)
				$('#infoText').append(`<p><strong>Secondary Email:</strong> (optional)<br />Optional field which can be a secondary contact from the company.</p>`)
				break;	
			case "StockArea":
				$('#infoText').append(`<p><strong>Picking Aisle:</strong><br />Warehouse aisle where company stock will be stored.</p>`)
				$('#infoText').append(`<p><strong>Bulk Storage:</strong><br />Bulk storage area for the company stock.</p>`)
				break;	
			case "Extras":
				$('#infoText').append(`<p><strong>Active Account:</strong><br />Select if the account is active.</p>`)
				$('#infoText').append(`<p><strong>Back Order Email:</strong><br />Select if company have requested a back order email service.</p>`)
				$('#infoText').append(`<p><strong>Email Tracking:</strong><br />Select if company have requested tracking information to be sent out to customers.</p>`)
				$('#infoText').append(`<p><strong>Licences:</strong><br />Enter how many licences the company have requested.</p>`)
				$('#infoText').append(`<p><strong>Stock Level and Processed Orders File:</strong><br />Select if the company have requested stock level and processed order files.</p>`)
				$('#infoText').append(`<p><strong>Linnworks:</strong><br />Choose what type of Linnworks account the company have.</p>`)
				break;	
			case "Notes":
				$('#infoText').append(`<p><strong>Notes: (optional)</strong><br />Optional field where any notes about the company can be added.</p>`)
				break;			
			default:
				break;
		}
		// Re-animate now the data opacity has been populated
		$('#infoText,#infoHeader').animate({opacity: 1},450, function () {});
	})
}
