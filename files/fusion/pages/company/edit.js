const COMPANY_URL = `${urlApi}companies?${apiKey}=${key}`;
const COMPANY_TYPES_URL = `${urlApi}companies/types?${apiKey}=${key}`;
const LINNWORKS_DELIVERY = `${urlApi}companies/linnworks_delivery_services?${apiKey}=${key}`;
const UPDATE_DELIVERY_NOTE = `${urlApi}deliverynotes/update?${apiKey}=${key}`;

let apiTotalRequests = 2;

let selectId = $("#companySettingsDropdown");
let delNoteSelectId = $("#deliveryNote");
let typesId = $("#companyType");
let apiData = "";
let selectedCompany = null;
let deliveryNotes = null;
let companySettingsArray = null;

getCompanies()

async function getCompanies() {
  api(false, true, "", COMPANY_URL, methodGet, apiData, function (getCompanies) {
    companySettingsArray = getCompanies.data;
    companySettingsArray = companySettingsArray.sort(sortByName);
    selectedCompany = companySettingsArray.find(company => company.id === parseInt(userCompany));
    for (let i = 0; i < companySettingsArray.length; i++) {
      const element = companySettingsArray[i];
      if (userCompany == element.id) {
        $("#companySettingsDropdown").append("<option selected value=" + element.id + ">" + element.name + " " + "(" + element.code + ") " + "</option>")
      } else {
        $("#companySettingsDropdown").append("<option value=" + element.id + ">" + element.name + " " + "(" + element.code + ") " + "</option>")
      }
    }
    selectId.selectpicker("refresh");
    showElement(divCompanyList);
    createTempSettings(selectedCompany)
    addLinnworksDeliveryButton(selectedCompany.linnworks.token)
    getDeliveryNotesPromise(true, selectedCompany.id)
      .then(results => {
        deliveryNotes = results;
        createDeliveryNoteTempSettings(deliveryNotes[0])
      });
  });
}

api(false, false, "", COMPANY_TYPES_URL, methodGet, apiData, function (getCompanyTypes) {
  let companyTypes = getCompanyTypes.data;
  for (let i = 0; i < companyTypes.length; i++) {
    const element = companyTypes[i];
    $("#companyType").append("<option value=" + element.id + ">" + element.value + "</option>")
  }
  typesId.selectpicker("refresh");
  showElement(divCompanyList);
});

$(selectId).change(function () {
  selectedCompany = companySettingsArray.find(company => company.id === parseInt(selectId.val()))
  createTempSettings(selectedCompany)
  addLinnworksDeliveryButton(selectedCompany.linnworks.token);
  getDeliveryNotesPromise(true, selectedCompany.id)
    .then(results => {
      deliveryNotes = results;
      createDeliveryNoteTempSettings(deliveryNotes[0])
    });
})

$(delNoteSelectId).change(function () {
  let selectedDelNote = deliveryNotes.find(notes => notes.id === parseInt(delNoteSelectId.val()))  
  createDeliveryNoteTempSettings(selectedDelNote)
})

function createTempSettings(company) {
  let companySettings = formatCompanySettings(company);
  for (const key in companySettings) {
    if (companySettings.hasOwnProperty(key)) {
      const value = companySettings[key];
      $('#' + key).val(value);
      if (typeof (value) === 'boolean') {
        $('#' + key).prop('checked', value);
      }
      if (key === 'warehouse') {
        let warehouse = null;
        switch (value) {
          case 'WAREHOUSE1':
            warehouse = 'warehouse_TheFulfilmentHub'
            break;
          case 'WAREHOUSE2':
            warehouse = 'warehouse_RetailDistributionCentre'
            break;
          default:
            $('#warehouse').children().removeClass('active')
            $('#warehouse').find('input').attr('checked', false)
            break;
        }
        if (value) {
          $('#warehouse').children().removeClass('active')
          $('#warehouse').find('input').attr('checked', false)
          $('#' + warehouse).closest('label').addClass(value ? 'active' : '')
          $('#' + warehouse).attr('checked', value)
        }
      }
      if (key === 'companyType') {
        $('#companyType').val(value)
        typesId.selectpicker("refresh");
      }
    }
  }
}

function createDeliveryNoteTempSettings(deliveryNote) {
  addSaveDeliveryNoteButton(deliveryNote.id)
  let delNoteSettings = formatCompanyDeliveryNote(deliveryNote);
  for (const key in delNoteSettings) {
    if (delNoteSettings.hasOwnProperty(key)) {
      const value = delNoteSettings[key];
      if (typeof (value) === 'boolean') {
        $('#' + key).prop('checked', value);
      } else if (key === 'deliveryNoteType') {
        let delNoteType = null;
        switch (value) {
          case 'DEFAULT':
            delNoteType = 'deliveryNoteType_Default'
            break;
          case 'CUSTOM':
            delNoteType = 'deliveryNoteType_Custom'
            break;
          default:
            $('#deliveryNoteType').children().removeClass('active')
            $('#deliveryNoteType').find('input').attr('checked', false)
            break;
        }
        if (value) {
          $('#deliveryNoteType').children().removeClass('active')
          $('#deliveryNoteType').find('input').attr('checked', false)
          $('#' + delNoteType).closest('label').addClass(value ? 'active' : '')
          $('#' + delNoteType).attr('checked', value)
        }
      } else if (key === 'deliveryNoteClass') {
        let delNoteClass = null;
        switch (value) {
          case 'VENDOR':
            delNoteClass = 'deliveryNoteClass_Vendor'
            break;
          case 'TRADE':
            delNoteClass = 'deliveryNoteClass_Trade'
            break;
          case 'B2C':
            delNoteClass = 'deliveryNoteClass_B2C'
            break;
          default:
            $('#deliveryNoteClass').children().removeClass('active')
            $('#deliveryNoteClass').find('input').attr('checked', false)
            break;
        }
        if (value) {
          $('#deliveryNoteClass').children().removeClass('active')
          $('#deliveryNoteClass').find('input').attr('checked', false)
          $('#' + delNoteClass).closest('label').addClass(value ? 'active' : '')
          $('#' + delNoteClass).attr('checked', value)
        }
      } else {
        $('#' + key).val(value);
      }
    }
  }
}


$('#formCompanyEdit').submit(function (e) {
  e.preventDefault()
  let formData = $('#formCompanyEdit :input');
  let apiFormatFormData = api_format_form_data(formData);
  let initCompanySettings = companySettingsArray.find(company => company.id === parseInt(selectId.val()))
  initCompanySettings = formatCompanySettings(initCompanySettings)
  let updatedValues = checkSettingsForUpdates(apiFormatFormData, initCompanySettings);
  console.log(apiFormatFormData, updatedValues, 'uV');
  if (Object.keys(updatedValues).length > 0) {
    let updateData = {
      'API-KEY': key,
      'companyId': apiFormatFormData.companySettingsDropdown
    }
    updateData = { ...updateData, ...updatedValues };
    alertCustomCreating("Updating Company: " + selectedCompany.name);
    var api3plCompany = urlApi + urlApiCompanies;
    api(false, false, apiData[customerCode], api3plCompany, methodPut, updateData, function (updatedCompany) {
      alertCustomSuccess("Updated Company: " + selectedCompany.name);
      getCompanies()
      apiTotalRequests = apiTotalRequests + 1;
    });
  } else {
    alertCustomError('No settings have been changed for .' + selectedCompany.name)
  }
})

function checkSettingsForUpdates(updatedSettings, initSettings) {
  let updatedValues = {};
  for (const key in updatedSettings) {
    if (updatedSettings.hasOwnProperty(key)) {
      const value = updatedSettings[key];
      if (initSettings.hasOwnProperty(key)) {
        const initValue = initSettings[key];
        if (key === 'linnworksToken') {
          console.log(value, initValue, (initValue == null && (value == null || value == "")));
        }
        if (typeof (initValue) == 'number') {
          if (parseInt(value) !== initValue) {
            updatedValues[key] = value;
          }
        } else {
          if (value !== initValue) {
            if (!(initValue == null && (value == null || value == ""))) {
              updatedValues[key] = value;
            }
          }
        }
      }
    }
  }
  return updatedValues;
}

function addLinnworksDeliveryButton(token) {
  $('#linnworksDeliveryServices_formDiv').remove();
  if (token != "") {
    $('#fullStockCSV_formDiv').after(`<div class="col-md-4" id="linnworksDeliveryServices_formDiv">                                                                                                               
    <div class="form-group">
      <div class="demo-checkbox">
        <button id="linnworksDeliveryServices" type="button" onclick="createDeliveryServices('${token}')" class="btn btn-primary waves-effect">Create Linnworks Delivery Services</button>
        </label>
      </div>
    </div>
  </div>`)
  }
}

function addSaveDeliveryNoteButton(id) {
  $('#saveDeliveryNoteSettings_formDiv').remove();
  if (id != "") {
    $('#special_formDiv').after(`<div class="col-md-4" id="saveDeliveryNoteSettings_formDiv">                                                                                                               
    <div class="form-group">
      <div class="demo-checkbox">
        <button id="saveDeliveryNoteSettings" type="button" onclick="updateDeliveryNote('${id}')" class="btn btn-primary waves-effect">Save Delivery Note Settings</button>
        </label>
      </div>
    </div>
  </div>`)
  }
}

function updateDeliveryNote() {
  let formData = $('#DeliveryNoteSettings :input');
  let apiFormatFormData = api_format_form_data(formData);
  let initDeliveryNoteSettings = deliveryNotes.find(notes => notes.id === parseInt(delNoteSelectId.val()));
  let formattedDeliveryNoteSettings = formatCompanyDeliveryNote(initDeliveryNoteSettings);
  let updatedValues = checkSettingsForUpdates(apiFormatFormData, formattedDeliveryNoteSettings);
  console.log(apiFormatFormData, updatedValues, 'uV');
  if (Object.keys(updatedValues).length > 0) {
    let updateData = {
      'API-KEY': key,
      'companyId': selectedCompany.id,
      'noteId': apiFormatFormData.deliveryNote
    }
    updateData = { ...updateData, ...updatedValues };
    alertCustomCreating("Updating Delivery Note: " + initDeliveryNoteSettings.name);
    api(false, false, "", UPDATE_DELIVERY_NOTE, methodPut, updateData, function (updatedCompany) {
      alertCustomSuccess("Updated Delivery Note: " + initDeliveryNoteSettings.name);
      getDeliveryNotesPromise(true, selectedCompany.id)
      .then(results => {
        deliveryNotes = results;
        let selectedDelNote = deliveryNotes.find(notes => notes.id === parseInt(apiFormatFormData.deliveryNote))  
        $('#deliveryNote').val(selectedDelNote.id)
        delNoteSelectId.selectpicker("refresh");
        createDeliveryNoteTempSettings(selectedDelNote)
      });
    });
  } else {
    alertCustomError('No settings have been changed for .' + initDeliveryNoteSettings.name)
  }
}

function createDeliveryServices(token) {
  let apiData = {
    'API-KEY': key,
    customer_ds: token
  }
  alertCustomCreating("Creating Linnworks Delivery Services for: " + selectedCompany.name);
  api(false, false, "", LINNWORKS_DELIVERY, methodPost, apiData, function (response) {
    alertCustomSuccess("Created Linnworks Delivery Services for: " + selectedCompany.name);
    apiTotalRequests = apiTotalRequests + 1;
  });
}

function getDeliveryNotesPromise(apiShowAlert, filterCompanyId) {
  return new Promise(resolve => {
    var api3plDeliveryNotes = urlApi + urlApiDeliveryNotes + '?' + apiKey + '=' + key;
    if (filterCompanyId !== null) {
      api3plDeliveryNotes = api3plDeliveryNotes + "&companyId=" + filterCompanyId;
    }
    api(false, apiShowAlert, "", api3plDeliveryNotes, methodGet, apiData, function (getDeliveryNotes) {
      generate_select('deliveryNote', getDeliveryNotes);
      resolve(getDeliveryNotes.data)
      apiTotalRequests = apiTotalRequests + 1;
    });
  })
}

function formatCompanySettings(company) {
  return companySettings = {
    active: company.settings.active ? company.settings.active : false,
    backOrders: company.email.backOrders ? company.email.backOrders : false,
    companyType: company.type.id ? company.type.id : null,
    dailySummary: company.email.dailySummary ? company.email.dailySummary : false,
    ftpPassword: company.ftp.password != " " ? company.ftp.password : null,
    ftpUrl: company.ftp.url != " " ? company.ftp.url : null,
    ftpUsername: company.ftp.username != " " ? company.ftp.username : null,
    fullStockCSV: company.ftp.csv ? company.ftp.csv : false,
    internalEmail: company.email.customerCare ? company.email.customerCare : null,
    licences: company.settings.licenses ? company.settings.licenses : 0,
    linnworksFC: company.linnworks.fulfilmentCenter ? company.linnworks.fulfilmentCenter : null,
    linnworksOrder: company.linnworks.active ? company.linnworks.active : false,
    linnworksToken: company.linnworks.token ? company.linnworks.token : null,
    onStop: company.settings.onStop ? company.settings.onStop : false,
    primaryEmail: company.email.primary ? company.email.primary : null,
    printDeliveryNote: company.print.deliveryNote ? company.print.deliveryNote : false,
    rulesEngine: company.rulesEngine ? company.rulesEngine : false,
    secondaryEmail: company.email.secondary ? company.email.secondary : null,
    snapCustomer: company.snap.username ? true : false,
    snapPassword: company.snap.password ? company.snap.password : null,
    snapUsername: company.snap.username ? company.snap.username : null,
    tradeTriggerQty: company.quantity.trade ? company.quantity.trade : 0,
    warehouse: company.snap.warehouse ? company.snap.warehouse : null,
  }
}

function formatCompanyDeliveryNote(deliveryNote) {
  return companySettings = {
    deliveryNoteType: deliveryNote.type.name ? deliveryNote.type.name : null,
    deliveryNoteClass: deliveryNote.class.name ? deliveryNote.class.name : null,
    print: deliveryNote.print ? deliveryNote.print : false,
    file: deliveryNote.file ? deliveryNote.file : false,
    embroid: deliveryNote.embroid ? deliveryNote.embroid : false,
    dropship: deliveryNote.dropship ? deliveryNote.dropship : false,
    emailCompany: deliveryNote.email.company ? deliveryNote.email.company : "",
    special: deliveryNote.special ? deliveryNote.special : "",
  }
}