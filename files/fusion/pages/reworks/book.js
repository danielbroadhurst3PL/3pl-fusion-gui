const reworkURL = `${urlApi}reworks/`;

$('#description').prop('rows', 8);
$('#description').toggleClass('textarea-autosize');
$('#description').prop('placeholder', "Please supply as much information as possible about the requested rework. This will help speed up the rework request process.");
let label = $('#description')[0].nextElementSibling;
$(label).css('top', '-10px');

$("#reworkType").change(function () {
  var selectedReworkType = $('input[name=reworkType]:checked').prop('id');
  if (selectedReworkType == 'reworkType_Other') {
    displayOtherField();
  } else {
    removeOtherField();
  }
});

function displayOtherField() {
  if ($('#otherReworkType_formDiv').length == 0) {
    $('#reworkType_formDiv').after(`
    <div class="col-md-12" id="otherReworkType_formDiv">                               
      <div class="form-group">
          <div class="form-line">
              <textarea tabindex="8" placeholder="Please enter more information about the Rework Type. " id="otherReworkType" rows="1" class="form-control textarea-autosize" required=""></textarea>
              <label for="otherReworkType">More information.</label>
          </div>
      </div>
    </div>`)
  }
}

function removeOtherField() {
  $('#otherReworkType_formDiv').remove();
}

setDateTo14Days();

$('#formReworkBook').submit(function (e) {
  e.preventDefault();
  let formValues = $('#formReworkBook' + ' :input');
  let apiFormValues = api_format_form_data(formValues);
  let formData = new FormData;
  for (const key in apiFormValues) {
    if (apiFormValues.hasOwnProperty(key)) {
      const value = apiFormValues[key];
      formData.append(key, value)
    }
  }
  let files = $('#file')[0].files;
  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    formData.append('file[]', file, file.name)
  }
  apiFilesUpload(reworkURL, methodPost, formData, function (response) {
    redirectToView()
  })
});
function setDateTo14Days() {
  let disabledDays = [6, 7];
  let date = new Date();
  let tempDate = new Date(date);
  $("#requiredByDate").bootstrapMaterialDatePicker({
    format: 'dddd DD MMMM YYYY',
    clearButton: true,
    weekStart: 1,
    time: false,
    minDate: new Date(),
    disabledDays: disabledDays
  });
  tempDate.setDate(tempDate.getDate() + 14);
  $('#requiredByDate').bootstrapMaterialDatePicker('setMinDate', moment(tempDate));
}

function redirectToView() {
  swal({
      title: "Rework Request Sent?",
      text: "Your rework request has been sent to our operations team in order to complete a time study, confirm cost and an expected completion date. Once the above is completed you will receive a quote for sign off before the work commences.",
      type: "success",
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "OK!",
      closeOnConfirm: false,
      closeOnCancel: false
  }, function (isConfirm) {
      if (isConfirm) {
        window.location = baseUrl + "reworks/view";
      }
  });
}