var apiTotalRequests = 1;
const reworksUrl = `${urlApi}reworks/`;

get_reworks_customer(true, `${reworksUrl}?${apiKey}=${key}`);

/**
 * Displays alert modal to ask user to confirm they wish to delete the order.
 * 
 * @param {String} id - Order Number which will be deleted
 */
function deleteRework(id) {
  swal({
      title: "Are you sure?",
      text: "You will not be able to recover this Rework Request!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, cancel it!",
      cancelButtonText: "Back",
      closeOnConfirm: false,
      closeOnCancel: false
  }, function (isConfirm) {
      if (isConfirm) {
          alertCustomLoading()
          deleteReworkAPI(id)
      } else {
          swal("Cancelled", "Your Rework Request will continue being processed :)", "error");
      }
  });
}

/**
* Calls the delete order endpoint with an order number and deletes from system.
* 
* @param {String} id - Order number which will be deleted
*/
function deleteReworkAPI(id) {
  apiReworksDelete = `${reworksUrl}`;
  apiData = {'rework_id': id, ["API-KEY"]:key}
  api(false, true, "", apiReworksDelete, methodDelete, apiData, function (deleteOrder) {      
    if (deleteOrder.status == 200) {
      get_reworks_customer(true, `${reworksUrl}?${apiKey}=${key}`);
      swal("Cancelled!", "Your Rework Request has been cancelled.", "success");
    } else {
      swal("Error!", "Your Rework Request was not cancelled, please contact customer services.", "error");
    }
  })
}

function viewMessages(reworkId) {      
  alertCustomLoading();
  var singleTicketSettings = {
      "url": `${reworksUrl}messages?${apiKey}=${key}&markRead=${reworkId}`,
      "method": "GET",
      "timeout": 0
  };
  $.ajax(singleTicketSettings).done(function (response) {
      let messages = response.data;
      showMessages(messages)
      return
  })   
}

async function showMessages(messages) {  
  $('#largeModalMessages').modal().show();
  $('#modalMessages').html("");
  // $('#infoText').html("")
  $('#largeModalLabel').html("")
  for (let i = 0; i < messages.length; i++) {
      $('#modalMessages').append(`<div class>
                              <h3>${messages[i].subject}</h3>
                              <p>Sent: ${messages[i].createdAt}</p>
                              <h4>Message:</h4>
                              <p>${messages[i].body}</p>
                              <div id="commentAttachments${i}">
                              </div>
                            </div>`)
  }
  $('#largeModalLabel').html(`Rework ID: ${messages[0].rework_id}`)
  closeAllSwalWindows();
  $('#modalMessages').append(`<div id="ticketReply"></div><button id="buttonReplyOpen" type="submit" class="${buttonBlue}" onclick="showReplyInput('${messages[0].rework_id}')">Reply to Message</button>`)
}

function showReplyInput(rework_id) {    
  $('#buttonReplyOpen').remove();
  $('#ticketReply').append(`<div id="messageForm"><label for="${rework_id}">Enter your response:</label>
                            <textarea id="${rework_id}_responseText" name="story" rows="5" cols="33"></textarea>
                            <p>File <span>(optional)</span></p><input type="file" id="${rework_id}_file" multiple class="form-control">
                            <button id="buttonReplySend" class="${buttonBlue}" onclick="submitResponse('${rework_id}')">Reply with Additional Information</button></div>`);
}

function submitResponse(rework_id) {
  let body = $('#'+rework_id+'_responseText').val();
  let files = $('#'+rework_id+'_file')[0].files;
  let formData = new FormData;
  formData.append('API-KEY', key);
  formData.append('rework_id', rework_id);
  formData.append('body', body);
  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    formData.append('file[]', file, file.name)
  }
  let messageCreateUrl = `${reworksUrl}messages`
  apiFilesUpload(messageCreateUrl, methodPost, formData, function (response) {
    alertCustomSuccess('Success', 'The additonal information has been sent to the operations team.');
    $('#largeModalMessages').modal('hide');
    $('#largeModal').modal('hide');
    get_reworks_customer(true, `${reworksUrl}?${apiKey}=${key}`);
    apiTotalRequests += 1;
  })
}

function viewTimeStudy(reworkId) {
  $('#largeModalLabel').html(`Time Study for Rework: ${reworkId}`);
  let timeStudy = timeStudies.find(study => study.rework_id === reworkId);  
  $('#reworkModalBody').html(`
  <h2>Cost for Rework:</h2>
  <p>£${timeStudy.cost}</p>
  <h3>Proposed Completion Date</h3>
  <p>${timeStudy.proposed_date}</p>
  <h3>Additional Notes</h3>
  <p>${timeStudy.additional_notes != null ? timeStudy.additional_notes : ""}</p>
  <div id="timeStudyFiles">
    <h3>Files</h3>
  </div>
  <div id="ticketReply"></div><button id="buttonReplyOpen" type="submit" class="${buttonBlue}" onclick="showReplyInput('${reworkId}')">Reply to Message</button>`);
  $('#reworkModalConfirmButton').val(reworkId);
  if (Object.keys(timeStudy.files).length > 0) {
    for (const key in timeStudy.files) {
      if (timeStudy.files.hasOwnProperty(key)) {
        const file = timeStudy.files[key];        
        let fileName = file.split('/');
        fileName = fileName[fileName.length - 1];
        $('#timeStudyFiles').append(`
        <a href="${file}" target="_blank">
          <i class="material-icons">
            description
          </i>
          ${fileName.toUpperCase()}
        </a>
        `)    
      }
    }
  }
}

$('#reworkModalConfirmButton').click(function () {
  let reworkId = $('#reworkModalConfirmButton').val();  
  apiTimeStudyComplete = `${reworksUrl}time_study?${apiKey}=${key}&markConfirmed=${reworkId}`;
  api(false, true, "", apiTimeStudyComplete, methodGet, "", function (markComplete) {      
    if (markComplete.status == 200) {
      get_reworks_customer(true, `${reworksUrl}?${apiKey}=${key}`);
      swal("Success!", "Your Rework Request Time Study has been confirmed.", "success");
      $('#largeModal').modal('hide');
    } else {
      swal("Error!", "Your Rework Request Time Study was not confirmed, please contact customer services.", "error");
    }
  })
})