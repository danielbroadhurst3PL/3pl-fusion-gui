$(function() {

	const API_BULK_UPLOAD_TEMPLATE = urlApi + "/grns/bulk_grns_template?API-KEY=" + key; // GET
	const API_BULK_UPLOAD_ISSUES = urlApi + "/grns/bulk_products_issues?API-KEY=" + key;
	const API_BULK_UPLOAD = urlApi + "/grns/bulk_grns";

	$(document).on('click', '#bulkGrnDownload', function() {
		window.location=API_BULK_UPLOAD_TEMPLATE
	});

	$(document).on('click', '#issueProductsDownload', function() {
		window.location=API_BULK_UPLOAD_ISSUES
	});
	
	$(document).on('click', '#bulkGrnUpload', function() {
		var form = new FormData(); 
		if ($("#bulkFiles")[0].files.length === 0) {
			$("#fileValidate").html("Please attach your Bulk Upload File above before Uploading.")
			return false;
		}
		form.append("file", $("#bulkFiles")[0].files[0]);
		form.append("API-KEY", key);
		alertCustomCreating("Uploading bulk grns. Please wait this may take a while");

		$.ajax({
    		type: 'POST',
    		url: API_BULK_UPLOAD,
    		data: form,
    		//dataType: "json",
    		cache: false,
    		contentType: false,
    		processData: false,
    		success: function(response) {					
				downloadPdf(response);
			},
    		error: function(x, h, r) {
                console.log(x,h,r)
				alertCustomError("Failed to upload bulk grn file");
    		}
		});
	});
});

// Creates Bulk Upload and dynamically adds to the GRN page
var $input = $(`<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<div class="card">
						<div class="header">
							<h2>Bulk Upload</h2>
							<ul class="header-dropdown m-r--5">
								<li>
									<a href="javascript:void(0);" onclick="toggleInfoBar(BulkUpload)"  id="BulkUpload">
										<i class="material-icons info">info</i>
									</a>
								</li>
							</ul>
						</div>
						<div class="body">
							<div class="row clearfix">
								<div class="col-md-12">
									<div class="form-group">
										<input name="file" type="file" id="bulkFiles">
									</div>
									<div id="fileValidate"></div>
									<div class="button-demo">
										<button id="bulkGrnUpload" type="button" class="btn btn-primary waves-effect">Upload Bulk GRNs</button>
										<button id="bulkGrnDownload" type="button" class="btn btn-warning waves-effect">Download Template</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>`);
$div = document.querySelector("#formGrnCreate > div:nth-child(2)")
$input.insertBefore($div);
