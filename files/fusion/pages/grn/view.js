var apiTotalRequests = 0;
var grnArray = [];

hideElement(divFilterStatus);
hideElement(divFilterCustomerRef);
dataOrderViewForm = [];


$( "#" + type ).change(function() {
    var selectedOrderTypeVal = $('input[name=options]:checked').attr('id');
    $("#viewGrnProducts").hide();
	$('#' + formOrderView).trigger("reset");
	$('#' + type).val(selectedOrderTypeVal);
    $('#' + type ).selectpicker("refresh");
    if (selectedOrderTypeVal != "grnReference") {
        hideElement(divFilterCustomerRef);
        $("#" + formOrderView).submit();
    } else {
        showElement(divFilterCustomerRef);
    }

    $('#' + status ).selectpicker("refresh");
});

$("#" + formOrderView).submit(function(e) {    
    e.preventDefault();
    let apiFormatFormData = new Object();
    apiFormatFormData[apiKey] = key;
    apiFormatFormData['referenceCustomer'] = "";
/*     apiFormatFormData['dateShippedFrom'] = "";
    apiFormatFormData['dateShippedTo'] = ""; */
    if (e.target.querySelector('.active > input').id != 'grnReference') {
        apiFormatFormData['status'] = e.target.querySelector('.active > input').id;
    }
    
    var dataOrderViewForm = $('#' + formOrderView + ' :input');
    for (const property in dataOrderViewForm) {
        if (dataOrderViewForm.hasOwnProperty(property)) {
            const element = dataOrderViewForm[property];
            if (element.type === 'radio' && element.checked === true) {
                apiFormatFormData['status'] = element.id;
            }
/*             if (element.value !== null && element.value !== "" && element.id === 'dateShippedFrom') {
                apiFormatFormData['dateShippedFrom'] = element.value;
                $("#divFilterShippedFromValidate").html('')
            }
            if (element.value !== null && element.value !== "" && element.id === 'dateShippedTo' ) {
                apiFormatFormData['dateShippedTo'] = element.value;
                $("#divFilterShippedToValidate").html('')
            } */
            if (element.value !== null && element.id === 'grnReference') {
                apiFormatFormData['referenceCustomer'] = element.value;
            }
        }
    }        
    apiTotalRequests = apiTotalRequests + 1;
    get_grns(true, apiFormatFormData);
});


// Submit Button Diplay on Select or KeyUp
$('#type').change(function() {    
    $("#button-div").hide()
    $('#filterType>label').removeClass('active')
    $('#divFilterOrderNumberNotFound').html('')
});

$('input#grnReference').bind("change keyup input",function() { 
    $("#button-div").show()
});