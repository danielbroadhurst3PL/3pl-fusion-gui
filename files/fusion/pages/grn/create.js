var apiTotalRequests = 1;
get_products(true, buttonAdd, true);

$('#referenceCustomer').attr('pattern', "[a-zA-Z0-9 -]+")
$('#referenceCustomer').attr('title', "Reference can only contain alphanumeric character. Backslashes are not allowed.")
$('#referenceCustomer').attr('maxlength', 17)

$("#" + formGrnCreate).submit(function(e) {
    e.preventDefault();
    var dataGrnCreateForm = $('#' + formGrnCreate + ' :input');
    var dataGrnCreateItems = tableItemsViewSettings.rows().data();

    var apiFormatFormData = api_format_form_data(dataGrnCreateForm);

    var tempItems = new Object;

    dataGrnCreateItems.each(function (value, index) {
        tempItems[index] = new Object;
        var tempItemsSku = value[0];
        tempItemsSku = tempItemsSku.substring(tempItemsSku.indexOf(">") + 1);
        tempItemsSku = tempItemsSku.replace("</a>", "");
        tempItems[index].sku = tempItemsSku;
        tempItems[index].quantity = document.getElementById(tableItemQuantityTextbox + "_" + btoa(JSON.stringify(value[0]))).value;
    });

    apiFormatFormData["items"] = tempItems;
    if (Object.keys(apiFormatFormData["items"]).length < 1) return alertCustomError("Please add items to GRN") 
    submit_grn(apiFormatFormData);
});

function submit_grn(apiData)
{
    alertCustomCreating("Creating Goods in Notification");

    referenceCustomer = apiData['referenceCustomer'].replace(/\s/g, "");
    apiData['referenceCustomer'] = referenceCustomer;

    var api3plGrns = urlApi + urlApiGrns;

    api(false, false, "", api3plGrns, methodPost, apiData, function (createGrn, error){  
        if (error) {            
            return alertCustomError(error[0])
        }        
        $('#' + formGrnCreate).trigger("reset");
        datatable_clear(tableItemsViewSettings);
        get_products(true, buttonAdd, true);
        downloadPdf(createGrn);        
    });
}

// Dynamically add info buttons to items and products sections

createInfoIcon("Items", "info", 2);
createInfoIcon("Products", "info", 3);

function downloadPdf(createGrn) {
    if (createGrn.success == 1) {
        swal({
            title: "Download Inbound Booking Receipt",
            text: "Please find below download options for retrieving your inbound paperwork. This paperwork should be attached to the goods prior to delivery to 3PL!",
            type: "success",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, download it!",
            cancelButtonText: "View in new Window",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                window.location = `${urlApi}welcome/customer_file?API-KEY=${key}&grnReceipts=${createGrn.data[0].file}&delivery=download`;
                alertCustomRedirectSuccess("Please create an Inbound Booking, you will be redirected in 3 seconds.");
                setTimeout(function () {
                    window.location = baseUrl + "inbounds/create";
                }, 3000);
            }
            else {
                let url = `${urlApi}welcome/customer_file?API-KEY=${key}&grnReceipts=${createGrn.data[0].file}&delivery=view`;
                var win = window.open(url, '_blank');
                win.focus();
                alertCustomRedirectSuccess("Please create an Inbound Booking, you will be redirected in 3 seconds.");
                setTimeout(function () {
                    window.location = baseUrl + "inbounds/create";
                }, 3000);
            }
        });
    } else {
        alertCustomError(createGrn.errorMessage[0])
    }
}

function createInfoIcon(id, info, divSelector) {
    ul = document.createElement("ul");
    ul.setAttribute("class", "header-dropdown m-r--5");
    li = document.createElement("li");
    aLink = document.createElement("a");
    aLink.href = "javascript:void(0);";
    aLink.setAttribute("onclick", "toggleInfoBar(" + id + ")");
    aLink.setAttribute("id", id);
    icon = document.createElement("i");
    icon.setAttribute("class", "material-icons " + info);
    icon.innerHTML = info;
    aLink.appendChild(icon);
    li.appendChild(aLink);
    ul.appendChild(li);
    document.querySelector("#formGrnCreate > div:nth-child("+divSelector+") > div > div.header").appendChild(ul);
}

/**
 * Populates the sidebarinfomation area with info about the selected card.
 * Animates the switching of card info so sidebar can remain open.
 * @param {ID from the Form} formID 
 */
function formInformation(formID) {
	// Fade information out whilst we swtich information
	$('#infoText,#infoHeader').animate({opacity: 0},150, function () {
		// Re-write the Header and clear the text area
		$('#infoText').html("")
		$('#infoHeader').html("")
		infoTitle = formID.replace(/([A-Z])/g, ' $1').trim();
		$('#infoHeader').html(`${infoTitle}`)
		// Populate Data for the selected card
		switch (formID) {
			case "References":
				$('#infoText').append(`<p><strong>Customer Reference:</strong><br />Enter a reference for the Goods in Notification which will be used to identify your incoming shipments, the reference can contain Letters, Numbers and Hypens only.</p>`)
				break;
			case "BulkUpload":
                $('#infoText').append(`<p><strong>Upload Bulk GRNs:</strong><br />Please upload completed Bulk GRN files. Files can contain different ASN Refernces but all products must be correctly registered previously within 3PL Fusion.</p>`)
				$('#infoText').append(`<p><strong>Download Template:</strong><br />Please download GRN Bulk Upload template and use to complete a bulk upload. Files which are upload not using the template may fail causing issues with your goods in notifications.</p>`)
				break;	
			case "Items":
				$('#infoText').append(`<p><strong>Items:</strong><br />Add Products which are located in the table below the Items table. Once you have added products, enter the quantity which will be being sent as part of the GRN.</p>`)
				break;	
			case "Products":
				$('#infoText').append(`<p><strong>Products:</strong><br />Choose products by clicking the Add button which are part of the Goods in Notification. Products which are selected will be transferred to items table.</p>`)
				break;			
			default:
				break;
		}
		// Re-animate now the data opacity has been populated
		$('#infoText,#infoHeader').animate({opacity: 1},450, function () {});
	})
}
