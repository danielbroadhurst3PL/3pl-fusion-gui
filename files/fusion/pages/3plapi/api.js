function api(apiIgnoreTotalRequests, apiShowAlert, apiFilter, apiUrl, apiType, apiData, _callback, apiSuccessOperation) {
  if (apiData.hasOwnProperty('dateDespatch')) {
    apiData['dateDespatch'] = api_date(apiData['dateDespatch']);
  }

  if (apiType === methodGet) {
    if (apiShowAlert === true) {
      alertCustomLoading();
    }
  }

  $.ajax({
    url: apiUrl,
    type: apiType,
    dataType: "json",
    data: apiData,
    success: function (data) {
      if (data.status === 201) {
        _callback(data);
        switch (apiSuccessOperation) {
          case 1:
            remove_bulk_order_from_table();
            break;
        }

        if (apiUrl === urlApi + urlApiOrders & apiType === methodPost) {
          apiFilter = data.data[0].orderNumber;
        }

        if (apiUrl === urlApi + urlApiGrns & apiType === methodPost) {
          apiFilter = data.data[0].reference.customer;
        }

        if (apiShowAlert === true) {
          alertCustomSuccess(apiType, apiFilter);
        }
      } else if (data.status === 200) {
        if (apiType === methodPut) {
          _callback(data);
          switch (apiSuccessOperation) {
            case 1:
              remove_bulk_order_from_table();
              break;
          }

          if (apiShowAlert === true) {
            alertCustomSuccess(apiType, apiFilter);
          }
        } else {
          _callback(data);

          if (apiIgnoreTotalRequests === false) {
            apiCompletedRequests = apiCompletedRequests + 1;

            if (apiCompletedRequests === apiTotalRequests) {
              closeAllSwalWindows();
            }
          }
        }

      } else if (data.status === 202) {
        _callback(null, data.errorMessage);
        api_error(data.errorMessage, apiShowAlert);
      } else {
        api_error(request.responseJSON.errorMessage, apiShowAlert);
      }
    },
    error: function (request, status, error) {
      if (request.responseJSON) {
        if (request.responseJSON.errorMessage) {

          if (apiType === methodGet) {
            apiCompletedRequests = apiCompletedRequests + 1;
          }

          api_error(request.responseJSON.errorMessage, apiShowAlert);
        } else {
          api_error(request.responseJSON.error, apiShowAlert);
        }
      } else {
        if (apiShowAlert === true) {
          alertCustomError(error);
        }
      }
    },
  });
}

function apiFilesUpload(apiUrl, apiType, apiData, _callback) {
  alertCustomLoading();
  $.ajax({
    url: apiUrl,
    type: apiType,
    data: apiData,
    contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
    processData: false, // NEEDED, DON'T OMIT THIS
    success: function (data) {
      return _callback(data);
    },
    error: function (request, status, error) {
      if (request.responseJSON) {
        if (request.responseJSON.errorMessage) {

          if (apiType === methodGet) {
            apiCompletedRequests = apiCompletedRequests + 1;
          }

          api_error(request.responseJSON.errorMessage, true);
        } else {
          api_error(request.responseJSON.error, true);
        }
      } else {
        alertCustomError(error);
      }
      return request;
    }
  });
}

// -----------------------------> API Calls  <-----------------------------

/**
 * Returns a Log String which can populate modal.
 * 
 * @param {Array} logs - Contains API data containing log information
 */
function api_logs(logs) {
  var tempHtmlLog = "";
  $.each(logs, function (logKey, val) {
    var tempHtmlMessage = val.message;
    tempHtmlMessage = tempHtmlMessage.substr(0, 1).toUpperCase() + tempHtmlMessage.substr(1);
    tempHtmlMessage = api_replace_fields(tempHtmlMessage);
    tempHtmlLog += "<b>" + val.dateTime + "</b><br><i>" + val.user.name.first + " " + val.user.name.last + "</i><br>" + tempHtmlMessage + "<br><hr>";
  });
  tempHtmlLog = tempHtmlLog.slice(0, -8);
  return tempHtmlLog;
}

/**
 * Populates Product Datatable.
 * 
 * @param {Boolean} apiShowAlert - Displays Alert Loading
 * @param {String} valueButton - Switches type of button between Add / Edit
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
var tableProductsViewSettings = null;
function get_products(apiShowAlert, valueButton, valueModal) {
  var api3plProducts = `${urlApi + urlApiProducts}/products_fusion?${apiKey}=${key}`;
  var apiData = "";
  api(false, apiShowAlert, "", api3plProducts, methodGet, apiData, function (getProducts) {
    if (tableProductsViewSettings !== null) {
      tableProductsViewSettings.destroy();
    }
    let productsData = getProducts.data;
    tableProductsViewSettings = $('#' + tableProductsView).DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: productsData,
      responsive: true,
      columns: [
        {
          data: "SKUId", render: function test(data) {
            return valueModalLink(valueModal, modalProduct, data);
          }
        },
        { data: "Description" },
        { data: "Text" },
        { data: "Barcode" },
        { data: "Qty" },
        { data: "QtyAllocated" },
        { data: "QtyAssigned" },
        { data: "QtyDueIn" },
        { data: "QtyDueOut" },
        {
          data: "Status", render: function (data) {
            switch (data) {
              case '01':
                data = 'Held Stock'
                break;
              case '00':
                data = 'Good Stock'
                break;
              default:
                break;
            }
            return data;
          }
        },
        {
          data: "SKUId", render: function (data) {
            return valueButtonHTML(valueButton, urlPageProductEdit, data);
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return;
  });
};
/**
 * Populates product modal and edit pages.
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {String} valueSku - SKU of Product
 * @param {Boolean} populateModal - Populates Product modal
 * @param {Boolean} populatePage - Populates Edit page
 * @param {Boolean} getStock - Gets product stock level
 */
function get_product(apiShowAlert, valueSku, populateModal, populatePage, getStock) {
  if (populateModal === true) {
    clear_modal_product();
  }
  if (populatePage === true) {
    $('#' + formProductEdit).trigger("reset");
  }
  var api3plProducts = urlApi + urlApiProducts + '?' + apiKey + '=' + key + '&' + sku + '=' + valueSku;
  if (getStock === true) {
    api3plProducts = api3plProducts + '&' + stock + '=' + "true";
  }
  var apiData = "";
  api(false, apiShowAlert, "", api3plProducts, methodGet, apiData, function (getProducts) {
    var getProduct = getProducts.data[0];
    if (populateModal === true) {
      model_product_misc(getProduct.sku);
      model_product_stock(getProduct.stock);
      model_product_view(getProduct);
      model_product_logs(getProduct.logs);
    }
    if (populatePage === true) {
      populate_edit_product_form(getProduct);
    }
  });
}

/**
 * Populates Stock Levels Datatable
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
function get_stock(apiShowAlert, valueModal) {
  var api3plStock = `${urlApi + urlApiProducts + stock}?${apiKey}=${key}`;
  var apiData = "";
  api(false, apiShowAlert, "", api3plStock, methodGet, apiData, function (getStock) {
    let stockLevelsData = getStock.data;
    $('#' + tableStockView).DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: stockLevelsData,
      responsive: true,
      columns: [
        {
          data: "sku", render: function (data) {
            return valueModalLink(valueModal, modalProduct, data);
          }
        },
        { data: "quantity" },
        { data: "allocated" },
        { data: "assigned" },
        { data: "orderIn" },
        { data: "dueIn" },
        { data: "orderOut" },
        { data: "dueOut" },
        {
          data: "status", render: function (data) {
            switch (data) {
              case '01':
                data = 'Held Stock'
                break;
              case '00':
                data = 'Good Stock'
                break;
              case '10':
                data = 'Damaged Stock'
                break;
              case '80':
                data = 'Missing Stock'
                break;
              case null:
                data = 'Out of Stock'
                break;
              default:
                break;
            }
            return data;
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  });
}

/**
 * Populates Stock Breakdown Datatable
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
function get_stock_breakdown(apiShowAlert, valueModal) {
  var api3plStockBreakdown = `${urlApi + urlApiProducts}stock_breakdown?${apiKey}=${key}`
  var apiData = "";
  api(false, apiShowAlert, "", api3plStockBreakdown, methodGet, apiData, function (getStockBreakdown) {
    let stockBreakdownData = getStockBreakdown.data;
    $('#' + tableStockBreakdownView).DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: stockBreakdownData,
      responsive: true,
      columns: [
        {
          data: "SKUId", render: function (data) {
            return valueModalLink(valueModal, modalProduct, data);
          }
        },
        { data: "Title" },
        { data: "Long Description" },
        { data: "StockLevel" },
        {
          data: "Status", render: function (data) {
            switch (data) {
              case '01':
                data = 'Held Stock'
                break;
              case '00':
                data = 'Good Stock'
                break;
              case '10':
                data = 'Damaged Stock'
                break;
              case '80':
                data = 'Missing Stock'
                break;
              default:
                break;
            }
            return data;
          }
        },
        { data: "DateExpiry" },
        { data: "BatchId" },
        {
          data: "AssignedTo", render: function (data) {
            return valueOrderLink(data);
          }
        },
        { data: "GRN" }
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  })
}

/**
 * Populates Stock Breakdown Datatable
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
let tableMovementBySkuTable = null;
function get_stock_by_movement(apiShowAlert, url) {
  var apiData = "";
  api(false, apiShowAlert, "", url, methodGet, apiData, function (getStockByMovement) {
    let movementByStockData = getStockByMovement.data;
    if (tableMovementBySkuTable !== null) {
      tableMovementBySkuTable.destroy();
    }
    tableMovementBySkuTable = $('#movementBySkuTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: movementByStockData,
      responsive: true,
      columns: [
        {
          data: "SKUId", render: function (data) {
            return valueModalLink(true, modalProduct, data);
          }
        },
        { data: "Text" },
        { data: "TimesOrdered" },
        { data: "AvgShipped" },
        { data: "TotalShipped" },
        { data: "BoxQty" },
        { data: "ReplenQty" },
        { data: "ReplenArea" }
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  })
}

/**
 * Populates Stock Breakdown Datatable
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
let tableClientServiceMapping = null;
let clientServiceData = null;
function get_client_service_mapping(apiShowAlert, url, data = false) {
  var apiData = "";
  api(false, apiShowAlert, "", url, methodGet, apiData, function (getClientServiceMapping) {
    clientServiceData = getClientServiceMapping.data;
    if (data) {
      return clientServiceData;
    } else {
      if (tableClientServiceMapping !== null) {
        tableClientServiceMapping.destroy();
      }
      tableClientServiceMapping = $('#tableClientServiceMappings').DataTable({
        dom: 'Bfrtip',
        pageLength: 10,
        data: clientServiceData,
        responsive: true,
        columns: [
          {
            data: "clientService",
          },
          { data: "priority" },
          { data: "region" },
          { data: "appliesTo" },
          { data: "deliveryNote" },
          { data: "serviceType" },
          { data: "generalService" },
          { data: "serviceName" },
          {
            data: "id", render: function (data) {
              data = data.replace(/\//g, "-")
              return `<a href="Javascript:void(0)" onclick="editClientMapping('${data}')" class="${buttonBlue}">Edit</a><a href="Javascript:void(0)" onclick="changePriority('${data}')" class="btn btn-success waves-effect">Change Priority</a><a href="Javascript:void(0)" id='button_${data}' onclick="deleteClientMapping('${data}')" class="${buttonRed}">Delete</a>`;
            }
          },
        ],
        buttons: [
          'copy',
          'csv',
          'excelHtml5',
          'pdfHtml5',
          'print'
        ],
      });
      return
    }
  })
}

/**
 * Populates Customer Inbound Bookings Table
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
let tableInboundBookings = null;
function get_inbound_bookings(apiShowAlert, url) {
  var apiData = "";
  api(false, apiShowAlert, "", url, methodGet, apiData, function (getStockByMovement) {
    let movementByStockData = getStockByMovement.data;
    if (tableInboundBookings !== null) {
      tableInboundBookings.destroy();
    }
    tableInboundBookings = $('#inboundBookingsTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: movementByStockData,
      responsive: true,
      order: [[6, "desc"]],
      columns: [
        { data: "po_number" },
        { data: "carrier_name" },
        { data: "number_cartons" },
        { data: "number_pallets" },
        { data: "inbound_service" },
        { data: "container_type" },
        { data: "requested_date" },
        { data: "time_slot" },
        { data: "additional_costs" },
        { data: "current_status" },
        {
          data: "po_number", render: function (data) {
            data = data.replace(/\//g, "-")
            return `<a href="${urlApi}welcome/customer_file?API-KEY=${key}&grnReceipts=${data}.pdf&delivery=view" target="_blank" class="${buttonBlue}">View Receipt</a><a href="Javascript:void(0)" id='button_${data}' onclick="deleteInboundBooking('${data}')" class="${buttonRed}">Cancel</a>`;
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
      "initComplete": function () {
        let todaysDate = new Date();
        movementByStockData.forEach(data => {
          let po_numberReplace = data.po_number.replace(/\//g, "-")
          let inboundDate = new Date(data.requested_date);
          const diffTime = Math.abs(inboundDate - todaysDate);
          const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24));
          if (diffDays <= 1) {
            $("#button_" + po_numberReplace).prop('onclick', null);
            $('#button_' + po_numberReplace).removeClass('btn-danger');
            $('#button_' + po_numberReplace).addClass('bg-grey');
            $('#button_' + po_numberReplace).prop('title', 'To cancel this Inbound please contact Op Support on 01942 720 077 – Option 5');
          }
        });
      },
    });
    return
  })
}

/**
 * Populates Customer Reworks Table
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
let tableReworksCustomer = null;
function get_reworks_customer(apiShowAlert, url) {
  var apiData = "";
  api(false, apiShowAlert, "", url, methodGet, apiData, function (reworks) {
    let reworksData = reworks.data;
    if (tableReworksCustomer !== null) {
      tableReworksCustomer.destroy();
    }
    tableReworksCustomer = $('#currentReworks').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: reworksData,
      responsive: true,
      order: [[6, "desc"]],
      columns: [
        { data: "customerRef" },
        { data: "rework_id" },
        { data: "requiredByDate" },
        { data: "proposedCompletionDate" },
        { data: "reworkType" },
        { data: "extraInfo" },
        { data: "progress", render: function (data) {
            return `${data}%`
          } 
        },
        { data: "status" },
        {
          data: "rework_id", render: function (data) {
            data = data.replace(/\//g, "-")
            return `<a href="Javascript:void(0)" onclick="deleteRework('${data}')" class="${buttonRed}">Cancel</a>`;
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
      "createdRow": function (row, data, dataIndex) {
        let unreadMessages = data.unreadMessages;
        let messageString = "";
        let buttonColour = buttonBlue;
        if (unreadMessages > 0) {
          messageString = `${unreadMessages} New Message(s)`;
          buttonColour = "btn btn-warning waves-effect";
        } else {
          messageString = 'Messages'
        }
        cell = $(row)[0].cells[8]
        if (data.status === "Awaiting Completion") {
          return cell.innerHTML = `In Progress`;
        } else if (data.status === "Awaiting Client Sign Off") {
          return cell.innerHTML = `<a href="" data-toggle="modal" data-target="#largeModal" onclick="viewTimeStudy('${data.rework_id}')" class="btn bg-green waves-effect">View Time Study</a><a href="Javascript:void(0)" id="messages_${data.rework_id}" onclick="viewMessages('${data.rework_id}')" class="${buttonColour}">${messageString}</a>` + cell.innerHTML;
        } else if (data.status === "Awaiting Completion" || data.status === "Rework Complete") {
          return cell.innerHTML = `<a href="Javascript:void(0)" id="messages_${data.rework_id}" onclick="viewMessages('${data.rework_id}')" class="${buttonColour}">${messageString}</a>`;
        } else if (data.status === "Require Additional Information") {
          return cell.innerHTML = `<a href="Javascript:void(0)" id="messages_${data.rework_id}" onclick="viewMessages('${data.rework_id}')" class="${buttonColour}">${messageString}</a><a href="Javascript:void(0)" onclick="deleteRework('${data.rework_id}')" class="${buttonRed}">Cancel</a>`
        } else {
          return cell.innerHTML = `<a href="Javascript:void(0)" id="messages_${data.rework_id}" onclick="viewMessages('${data.rework_id}')" class="${buttonColour}">${messageString}</a><a href="Javascript:void(0)" onclick="deleteRework('${data.rework_id}')" class="${buttonRed}">Cancel</a>`
        }
      }
    });
    return
  })
}

/**
 * Populates Internal Inbound Bookings
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
let tableInternalInboundBookings = null;
function get_inbound_bookings_internal(apiShowAlert, url) {
  var apiData = "";
  api(false, apiShowAlert, "", url, methodGet, apiData, function (getInternalInbounds) {
    let internalInbounds = getInternalInbounds.data;
    if (tableInternalInboundBookings !== null) {
      tableInternalInboundBookings.destroy();
    }
    tableInternalInboundBookings = $('#internalInboundTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: internalInbounds,
      responsive: true,
      columns: [
        { data: "po_number" },
        { data: "companyCode" },
        { data: "site_id" },
        { data: "requested_date" },
        { data: "time_slot" },
        { data: "transit_type" },
        { data: "container_type" },
        { data: "inbound_service" },
        { data: "number_cartons" },
        { data: "number_pallets" },
        { data: "Lines" },
        { data: "LineQty" },
        { data: "Volume" },
        { data: "Weight" },
        { data: "carrier_name" },
        {
          data: "additional_costs", render: function (data) {
            if (data) {
              return `£${data}`;
            } else {
              return `£0`;
            }
          }
        },
        { data: "name" },
        { data: "current_status" },
        {
          data: "created_at", render: function (data) {
            return data;
            let split = data.split('T');
            return `${split[0]} ${split[1].split('+')[0]}`;
          }
        },
        {
          data: "po_number", render: function (data) {
            data = data.replace(/\//g, "-")
            return `<a href="${urlApi}welcome/customer_file?API-KEY=${key}&grnReceipts=${data}.pdf&delivery=view&companyCode=${data.substr(0, 3)}" target="_blank" class="${buttonBlue}">View</a>
                    <button id="cancel" class="btn btn-danger waves-effect" onclick="cancelInboundBooking('${data}')">Cancel Inbound</button>`;
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  })
}

/**
 * Populates Unknown Inbound Bookings
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
let tableUnknownInboundBookings = null;
function get_unknown_inbound_bookings(apiShowAlert, url) {
  console.log(url);

  var apiData = "";
  api(false, apiShowAlert, "", url, methodGet, apiData, function (getUnknownInbounds) {
    let unknownInbounds = getUnknownInbounds.data;
    if (tableUnknownInboundBookings !== null) {
      tableUnknownInboundBookings.destroy();
    }
    tableUnknownInboundBookings = $('#unknownInboundTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: unknownInbounds,
      responsive: true,
      columns: [
        { data: "po_number" },
        { data: "carrier_name" },
        { data: "number_cartons" },
        { data: "number_pallets" },
        { data: "inbound_service" },
        { data: "container_type" },
        { data: "current_status" },
        {
          data: "po_number", render: function (data) {
            data = data.replace(/\//g, "-")
            return `<a href="Javascript:void(0)" onclick="showGRNs('${data}')" class="${buttonBlue}">Link GRN</a>`;
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  })
}

/**
 * Populates order tables depending on which status is requested.
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Array} apiData - Form Data which populates query.
 */
var tableOrdersViewSettings = null;
var tableOrdersShippedViewSettings = null;
function get_orders(apiShowAlert, apiData, valueModal) {

  var api3plOrders = urlApi + 'orders/fusion_orders/?';
  var statusStage = "";
  $.each(apiData, function (key, val) {
    if (val !== "") {
      if (key === 'dateShippedTo') {
        val = api_date(val);
      }
      if (key === 'dateShippedFrom') {
        val = api_date(val);
      }
      if (key === 'status') {
        statusStage = val;
      }
      api3plOrders += key + '=' + val + '&';
    }
  });

  var apiData = "";
  api(false, apiShowAlert, "", api3plOrders, methodGet, apiData, function (getOrders) {
    // Deal with results which return 0 results.
    if (getOrders.results.total == 0 && urlParam('orderNumber') > 0) {
      urlParam = function (name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results !== null && results !== '') {
          return results[1] || '';
        }
        var orderNumberSearch = urlParam('orderNumber');
        $('#divFilterOrderNumberNotFound').html(`Sorry, the Order Number <strong>${unescape(orderNumberSearch)}</strong> was not found. Please use the Order Search form above to filter through orders.`);
      }
    } else if (getOrders.results.total == 0) {
      var replacedStatus = api_replace_status(statusStage);
      $('#divFilterOrderNumberNotFound').html(`Sorry, no orders are currently in status <strong>${replacedStatus}</strong>. Please use the Order Search form above to filter through orders.`);
      if (tableOrdersViewSettings !== null) {
        tableOrdersViewSettings.clear().draw();
      }
      if (tableOrdersShippedViewSettings !== null) {
        tableOrdersShippedViewSettings.clear().draw();
      }
    }
    if (getOrders.results.total > 0) {
      // Register API Data
      let ordersData = getOrders.data;
      if (statusStage == "status") {
        stage = ordersData[0].Stage;
        variance = ordersData[0].Variance;
        statusStage = replace_stage(stage, variance);
      }
      let statusTitle = api_replace_status(statusStage);
      // Send data to the correct tables depending on status of orders.
      if (statusStage == 'BACK' || statusStage == 'NEXTFLUSH' || statusStage == 'AWAITINGPICK' || statusStage == 'AWAITINGPACK') {
        // If the table exists destroy the contents so it can be re-populated.
        if (tableOrdersViewSettings !== null) {
          tableOrdersViewSettings.destroy();
        }
        tableOrdersViewSettings = $('#' + tableOrdersView).DataTable({
          dom: 'Bfrtip',
          pageLength: 10,
          data: ordersData,
          responsive: true,
          columns: [
            {
              data: "ConsignmentId", render: function (data) {
                return valueModalLink(valueModal, modalOrder, data);
              }
            },
            { data: "Name" },
            { data: "Line1" },
            { data: "CustomerRef" },
            { data: "ASN" },
            {
              data: "ShippingMethod", render: function (data) {
                let shippingMethod = replace_shipping_method(data);
                if (shippingMethod != undefined) {
                  return shippingMethod;
                }
                return data;
              }
            },
            {
              data: "DateShipment", render: function (data) {
                return data.split(' ')[0];
              }
            },
            {
              data: "Stage", render: function (data) {
                return statusTitle;
              }
            },
            { data: "CustomerName" },
            {
              data: "ConsignmentId", render: function (data) {
                let orderButtons = check_order_edit(statusStage, "button", data);
                return orderButtons;
              }
            },
          ],
          buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
          ],
        });
        $('#viewOrder').show();
        $('#shippedOrder').hide();
        return;
      }
      // If the table exists destroy the contents so it can be re-populated.
      if (tableOrdersShippedViewSettings !== null) {
        tableOrdersShippedViewSettings.destroy();
      }

      tableOrdersShippedViewSettings = $('#' + tableOrdersShippedView).DataTable({
        dom: 'Bfrtip',
        pageLength: 10,
        data: ordersData,
        responsive: true,
        columns: [
          {
            data: "ConsignmentId", render: function (data) {
              return valueModalLink(valueModal, modalOrder, data);
            }
          },
          { data: "Name" },
          { data: "Line1" },
          { data: "CustomerRef" },
          { data: "ASN" },
          { data: "CarrierId" },
          {
            data: "Stage", render: function (data) {
              return switchStage(data)
            }
          },
          {
            data: "DateShipment", render: function (data) {
              return data.split(' ')[0];
            }
          },
          { data: "DateClosed" },
          { data: "CarrierTrackingNumber" },
          { data: "CustomerName" }
        ],
        buttons: [
          'copy',
          'csv',
          'excelHtml5',
          'pdfHtml5',
          'print'
        ],
        "createdRow": function (row, data, dataIndex) {
          trackingLink = carrierId = trackingNumber = "";
          trackingNumber = data['CarrierTrackingNumber'];
          if (trackingNumber !== null) {
            trackingNumber = trackingNumber.trim();
            if (trackingNumber !== "") {
              carrierId = data['CarrierId'];
              triackingLnk = tackingNumberLink(trackingNumber, carrierId);
              cell = $(row)[0].cells[7]
              cell.innerHTML = trackingLink;
            }
          }
        }
      });
      $('#viewOrder').hide();
      $('#shippedOrder').show();
      return;
    }
  });
}

/**
 * Returns information about a specific order number and populates pages and modal.
 * 
 * @param {Boolean} apiShowAlert - Displays the alert modal
 * @param {String} valueOrderNumber - Order Number for search query
 * @param {Boolean} populateModal - Sends data to order page
 * @param {Boolean} populatePage - Sends data to order page
 */
function get_order(apiShowAlert, valueOrderNumber, populateModal, populatePage) {
  if (populateModal === true) {
    clear_modal_order(modalOrderLabel);
  }

  if (populatePage === true) {
    $('#' + formOrderEdit).trigger("reset");
  }

  var api3plOrder = urlApi + urlApiOrders + '?' + apiKey + '=' + key + '&' + orderNumber + '=' + valueOrderNumber;

  var apiData = "";
  api(false, apiShowAlert, "", api3plOrder, methodGet, apiData, function (getOrder) {
    var getOrder = getOrder.data[0];
    if (populateModal === true) {
      $('.nav-tabs>li.active').removeClass('active')
      $('#shippingLabel').removeClass('active in')
      $('#modalOrderItem').removeClass('active in')
      $('#modalOrderLogs').removeClass('active in')
      $('#modalOrderPacking').removeClass('active in')
      $('#modalOrderNotes').removeClass('active in')
      $('#modalOrderView').addClass('active in')
      $('#modelView').addClass('active')
      model_order_misc(getOrder.orderNumber);
      model_order_view(getOrder);
      model_order_item(getOrder);
      model_order_notes(getOrder.orderNumber);

      statusStage = replace_stage(getOrder.status, 0);

      if (statusStage == 'SHIPPED' || statusStage == 'AWAITINGSHIP') {
        $('#modalPacking').show()
        model_order_packing_list(getOrder.orderNumber);
        if (statusStage == 'SHIPPED') {
          $('#modalShipping').show()
          //$('#modalBilling').show()
          modal_shipping_label(getOrder.orderNumber);
          //modal_billing_information(getOrder.orderNumber);
        } else {
          $('#modalShipping').hide()
        }
      } else {
        $('#modalShipping').hide()
        $('#modalPacking').hide()
      }
    }

    if (populatePage === true) {
      populate_edit_order_form(getOrder);
    }
    
  });
}

let deliveryNoteTableSetting = null;
let deliveryNoteData = [];
/**
 * Gets the Delivery Note from Sorted
 * 
 * @param {Boolean} apiShowAlert - Displays the alert modal
 */
function get_delivery_notes(apiShowAlert) {
  var apiDeliveryNote = `${urlApi}deliverynotes/check?${apiKey}=${key}&companyCode=${companyCode}`;
  var apiData = "";
  api(false, apiShowAlert, "", apiDeliveryNote, methodGet, apiData, function (getDeliveryNote) {
    deliveryNoteData = getDeliveryNote.data;
    if (deliveryNoteTableSetting !== null) {
      deliveryNoteTableSetting.destroy();
    }
    deliveryNoteTableSetting = $('#deliveryNoteTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: deliveryNoteData,
      responsive: true,
      columns: [
        { data: "name" },
        { data: "type.name" },
        { data: "class.name" },
        {
          data: "id", render: function (data) {
            return `<a href="${url3PLDesigner}?${apiKey}=${key}&id=${data}" class="${buttonBlue}" target="_blank">Edit</a>`
          }
        }
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    deliveryNoteData.forEach(option => {
      $('#id').append(`<option value=${option.id}>${option.name}</option>`)
    });
    $('#id').selectpicker("refresh");
  })
}

/**
 * Displays alert modal to ask user to confirm they wish to delete the order.
 * 
 * @param {String} order - Order Number which will be deleted
 */
function deleteOrder(order) {
  swal({
    title: "Are you sure?",
    text: "You will not be able to recover this order!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, cancel it!",
    cancelButtonText: "Back",
    closeOnConfirm: false,
    closeOnCancel: false
  }, function (isConfirm) {
    if (isConfirm) {
      alertCustomLoading()
      deleteOrderApi(order)
    } else {
      swal("Cancelled", "Your order will continue being processed :)", "error");
    }
  });
}

/**
 * Calls the delete order endpoint with an order number and deletes from system.
 * 
 * @param {String} order - Order number which will be deleted
 */
function deleteOrderApi(order) {
  apiOrdersDelete = `${urlApi + urlApiOrders}cancel_order`;
  apiData = { 'ShipmentId': order, ["API-KEY"]: key }
  api(false, true, "", apiOrdersDelete, methodDelete, apiData, function (deleteOrder) {
    swal("Cancelled!", "Your Order has been cancelled.", "success");
  })
}

/**
 * Populates the Goods In Notification table. Adds link to populate
 * secondary table of items in the GRN.
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Array} apiData - Array of API queries
 */
var tableGrnViewSettings = null
var grnArray = null;
function get_grns(apiShowAlert, apiData) {
  grnArray = [];
  let itemKey = 0;
  var api3plGrns = urlApi + 'grns?';
  /* var status = ""; */
  $.each(apiData, function (key, val) {
    if (val !== "") {
      if (key === 'status') {
        status = val;
      }
      if (key === 'grnReference') {
        grnReference = val;
      }
      api3plGrns += key + '=' + val + "&";
    }
  });
  var apiData = "";
  api(false, apiShowAlert, "", api3plGrns, methodGet, apiData, function (getGrns) {
    if (tableGrnViewSettings !== null) {
      tableGrnViewSettings.destroy();
    }
    grnArray = getGrns.data;
    tableGrnViewSettings = $('#' + tableGrnView).DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: grnArray,
      responsive: true,
      columns: [
        {
          data: "reference.customer", render: function (data) {
            let link = `<a href="javascript:void(0)" onclick="displayGrnItems('${itemKey}')">${data}</a>`;
            itemKey += 1;
            return link;
          }
        },
        { data: "status" },
        { data: "dateTime.created.date" },
        { data: "dateTime.created.time" }
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    $([document.documentElement, document.body]).animate({
      scrollTop: $("#grnList").offset().top - 100
    }, 2000);
    return
  })
}

/**
 * Populates items table corresponding to Customer GRN Reference.
 * 
 * @param {Integer} key - ID of GRN which is held in grnArray
 */
var tableGrnsViewItemsSettings = null
function displayGrnItems(key) {
  $("#viewGrnProducts").show();
  items = grnArray[key].items;
  if (tableGrnsViewItemsSettings !== null) {
    tableGrnsViewItemsSettings.destroy();
  }
  tableGrnsViewItemsSettings = $('#' + tableGrnsViewItems).DataTable({
    dom: 'Bfrtip',
    pageLength: 10,
    data: items,
    responsive: true,
    columns: [
      { data: "lineId" },
      { data: "sku" },
      { data: "quantity.dueIn" },
      { data: "quantity.received" }
    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ],
  });
  $('#grnItemsTitle').html(`Items on Goods In ${grnArray[key].reference.customer}`)
  $([document.documentElement, document.body]).animate({
    scrollTop: $("#grnProducts").offset().top - 100
  }, 2000);
  return;
}

/**
 * Populates Returns datatable.
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Creates modal link for SKU
 */
var tableReturnsViewSettings = null;
function get_returns(apiShowAlert, valueModal, dateClosedFrom, dateClosedTo) {
  if (dateClosedFrom == null && dateClosedTo == null) {
    var date = new Date();
    var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    dateClosedFrom = formatJSDate(firstDay);
    dateClosedTo = formatJSDate(date);
  }
  var api3plReturns = `${urlApi}returns?${apiKey}=${key}&dateClosedFrom=${dateClosedFrom}&dateClosedTo=${dateClosedTo}`;
  var apiData = "";
  api(false, apiShowAlert, "", api3plReturns, methodGet, apiData, function (getReturns) {
    let returnsData = getReturns.data;
    tableReturnsViewSettings = $('#' + tableReturnsView).DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: returnsData,
      responsive: true,
      columns: [
        {
          data: "orderNumber", render: function (data) {
            if (data) {
              return valueModalLink(valueModal, modalOrder, data);
            }
            return 'N/A';
          }
        },
        { data: "reference" },
        {
          data: "sku", render: function (data) {
            return valueModalLink(valueModal, modalProduct, data);
          }
        },
        { data: "quantity.actioned" },
        { data: "stock.status" },
        { data: "return.reason" },
        {
          data: "dateTime.closed", render: function (data) {
            return `${data.date} ${data.time.slice(0, -4)}`;
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  });
}

/**
 * Populates tableFilesViewDownload with files which are stored in requested
 * folder.
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {String} folder - Sets folder which API will query
 */
function get_files(apiShowAlert, folder) {
  api3plFiles = `${urlApi}welcome/customer_files?API-KEY=${key}&documentArea=${folder}`;
  apiData = "";
  api(false, apiShowAlert, "", api3plFiles, methodGet, apiData, function (getFiles) {
    let filesData = getFiles.data;
    $('#' + tableFilesViewDownload).DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: filesData,
      responsive: true,
      columns: [
        { data: folder },
        { data: "dateCreated" },
        {
          data: folder, render: function (data) {
            return `<a href="${urlApi}welcome/customer_file?API-KEY=${key}&invoiceFilename=${data}&delivery=view" target="_blank"><i class="material-icons">open_in_browser</i> View (Opens in New Window)</a>`;
          }
        },
        {
          data: folder, render: function (data) {
            return `<a href="${urlApi}welcome/customer_file?API-KEY=${key}&invoiceFilename=${data}&delivery=download"><i class="material-icons">cloud_download</i> Download File</a>`;
          }
        },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  })
}

/**
 * Populates Users table with current users which hold account with 3PL
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 */
function get_current_users(apiShowAlert) {
  apiUsers = `${urlApi}fusion_dashboard/current_users?${apiKey}=${key}`;
  apiData = "";
  api(false, apiShowAlert, "", apiUsers, methodGet, apiData, function (getUsers) {
    let users = getUsers.data;
    $('#viewUsersTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: users,
      responsive: true,
      columns: [
        { data: "username" },
        { data: "email" },
        { data: "companyCode" },
        {
          data: "name", render: function (data) {
            return data.charAt(0).toUpperCase() + data.slice(1);
          }
        },
        {
          data: "account_active", render: function (data) {
            switch (data) {
              case "1":
                return `<i class="material-icons" style="color: green">done</i>`
              case "0":
                return `<i class="material-icons" style="color: red">clear</i>`
              default:
                break;
            }
          }
        }
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    return
  })
}

// -----------------------------> API Errors  <-----------------------------

/**
 * 
 * @param {Array} errorMessageApi - Contains error message from API
 * @param {Boolean} apiShowAlert - Displays alert modal
 */
function api_error(errorMessageApi, apiShowAlert) {
  var alertErrorMessage = "";

  if (jQuery.type(errorMessageApi) === "array") {
    $.each(errorMessageApi, function (errorKey, val) {
      alertErrorMessage = alertErrorMessage + api_replace_fields(val) + "\n";
    });
  } else {
    alertErrorMessage = api_replace_fields(errorMessageApi);
  }

  if (apiShowAlert === true) {
    alertCustomError(alertErrorMessage);
  }
}

/**
 * 
 * @param {Array} errorMessageApi - Replaces array key with title.
 */
function api_replace_fields(errorMessageApi) {
  errorMessageApi = errorMessageApi.replace(asin, titleAsin);
  errorMessageApi = errorMessageApi.replace(batch, titleBatch);
  errorMessageApi = errorMessageApi.replace(barcode, titleBarcode);
  errorMessageApi = errorMessageApi.replace(commodityCode, titleCommodityCode);
  errorMessageApi = errorMessageApi.replace(country, titleCountry);
  errorMessageApi = errorMessageApi.replace(currency, titleCurrency);
  errorMessageApi = errorMessageApi.replace(dateExpiry, titleExpiryDate);
  errorMessageApi = errorMessageApi.replace(description, titleDescription);
  errorMessageApi = errorMessageApi.replace(depth, titleDepth);
  errorMessageApi = errorMessageApi.replace(dgnDetails, titleDgnDetails);
  errorMessageApi = errorMessageApi.replace(dgnType, titleDgnType);
  errorMessageApi = errorMessageApi.replace(height, titleHeight);
  errorMessageApi = errorMessageApi.replace(instructionsPicking, titlePickingInstructions);
  errorMessageApi = errorMessageApi.replace(quantityInner, titleInnerQuantity);
  errorMessageApi = errorMessageApi.replace(quantityMasterCarton, titleMasterCartonQuantity);
  errorMessageApi = errorMessageApi.replace(quantityPallet, titlePalletQuantity);
  errorMessageApi = errorMessageApi.replace(serial, titleSerial);
  errorMessageApi = errorMessageApi.replace(sku, titleSku);
  errorMessageApi = errorMessageApi.replace(value, titleValue);
  errorMessageApi = errorMessageApi.replace(weight, titleWeight);
  errorMessageApi = errorMessageApi.replace(width, titleWidth);
  errorMessageApi = errorMessageApi.replace(postCode, titlePostcode);
  errorMessageApi = errorMessageApi.replace(title, titleTitle);
  errorMessageApi = errorMessageApi.replace(statusBack, titleBackOrders);
  errorMessageApi = errorMessageApi.replace(statusNextFlush, titleNextFlush);
  errorMessageApi = errorMessageApi.replace(statusAwaitingPick, titleAwaitingPick);
  errorMessageApi = errorMessageApi.replace(statusAwaitingPack, titleAwaitingPack);
  errorMessageApi = errorMessageApi.replace(statusAwaitingShip, titleAwaitingShip);
  errorMessageApi = errorMessageApi.replace(statusShipped, titleShipped);
  errorMessageApi = errorMessageApi.replace(dateShippedTo, titleDateShippedTo);
  errorMessageApi = errorMessageApi.replace(dateShippedFrom, titleDateShippedFrom);

  return errorMessageApi;
}

// -----------------------------> Zendesk Functions <-----------------------------

/**
 * Takes inputs from Create Ticket form and creates Form Data ready to send
 * to Zendesk.
 * 
 * @param {Array} dataFormPostPut - Form Data
 */
function zendesk_format_form_data_post(dataFormPostPut) {
  var form = new FormData();
  var fullName = document.getElementById('fullName').innerHTML.trim();
  form.append('companyCode', companyCode);
  form.append('fullName', fullName);
  $.each(dataFormPostPut, function (arrayKey, val) {
    switch (val.id) {
      case 'subject':
        form.append('subject', val.value);
        break;
      case 'description':
        form.append('description', val.value);
        break;
      case 'orderNumber':
        if (val.value != companyCode) {
          form.append('orderNumber', val.value);
        }
        break;
    }
    if (val.type == 'radio' && val.checked && val.name == 'priority') {
      value = val.id.split('_')[1].toLowerCase();
      form.append('priority', value);
    }
    if (val.type == 'radio' && val.checked && val.name == 'area') {
      areaValue = val.id.split('_')[1].toLowerCase();
      switch (areaValue) {
        case 'complaint':
          areaValue = 'complaint2';
          break;
        case 'postdespatch':
          areaValue = 'transport_enquiry';
          break;
        case 'predespatch':
          areaValue = 'order_instruction';
          break;
        case 'systems':
          areaValue = 'systems1';
          break;
        default:
          break;
      }
      form.append('area', areaValue);
    }
    if (val.type == 'radio' && val.checked && val.name == 'category') {
      categoryValue = val.id;
      form.append('category', categoryValue);
    }
    if ($("#file")[0].files.length > 0) {
      form.append("file", $("#file")[0].files[0]);
    }
    if (val.type == 'textarea') {
      form.append('description', val.value);
    }
  });
  return form;
}

// -----------------------------> Datatable Functions <-----------------------------

// Create Edit & Add Buttons
function valueButtonHTML(valueButton, page, value) {
  switch (valueButton) {
    case buttonAdd:
      var valueButtonProductsTable = `<button type="button" class="${buttonBlue} ${buttonAdd}" value="${value}">Add</button>`;
      break;
    case buttonEdit:
      var valueButtonProductsTable = `<a href="${baseUrl + page}/${value}" class="${buttonBlue}">Edit</a>`;
      break;
  }
  return valueButtonProductsTable;
}

// Create Modal Links
function valueModalLink(valueModal, modalType, value) {
  switch (valueModal) {
    case true:
      var valueSkuProductsTable = `<a class="${modalType}" data-toggle="modal" data-target="#${modalType}">${value}</a>`;
      break;
    default:
      var valueSkuProductsTable = value;
  }
  return valueSkuProductsTable;
}

// Create Order View Link
function valueOrderLink(value) {
  var valueOrderLink = value;
  if (value.substring(0, 3) === companyCode) {
    valueOrderLink = `<a href="${baseUrl + urlPageOrdersView}?orderNumber=${value}">${value}</a>`;
  }
  return valueOrderLink;
}

// Time Formatting Unix
function formatTime(unix_timestamp) {
  // Create a new JavaScript Time object based on the timestamp
  var date = new Date(unix_timestamp * 1000);
  var hours = date.getHours();
  var minutes = "0" + date.getMinutes();
  var seconds = "0" + date.getSeconds();
  // Will display time in 10:30:23 format
  var formattedTime = hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  return formattedTime
}

// Date Formatting Unix
function formatDate(unix_timestamp) {
  // Create a new JavaScript Date object based on the timestamp
  var date = new Date(unix_timestamp * 1000);
  var day = ("0" + date.getDate()).slice(-2);
  var month = ("0" + (date.getMonth() + 1)).slice(-2);
  var year = date.getFullYear();
  // Will display date in MM/DD/YYYY format
  var formattedDate = month + '/' + day + '/' + year;
  return formattedDate
}

function formatJSDate(date) {
  var day = ("0" + date.getDate()).slice(-2);
  var month = ("0" + (date.getMonth() + 1)).slice(-2);
  var year = date.getFullYear();
  // Will display date in YYYY/MM/DD format
  var formattedDate = year + '-' + month + '-' + day;
  return formattedDate
}

function formatEditDate(date) {
  var day = ("0" + date.getDate()).slice(-2);
  var month = ("0" + (date.getMonth() + 1)).slice(-2);
  var year = date.getFullYear();
  // Will display date in YYYY/MM/DD format
  var formattedDate = year + '-' + day + '-' + month;
  return formattedDate
}

// File Size Format
function bytesToSize(bytes) {
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
  if (bytes == 0) return '0 Byte';
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
}

// Create Edit & Delete buttons for orders
function check_order_edit(dataStatus, action, dataOrderNumber) {  
  let buttonHtml = '';
  if (dataStatus < 49) {
    if (action === "button") {
      buttonHtml = '';
      if (dataStatus <= 11) {
        buttonHtml += `<a href="${baseUrl + urlPageOrderEdit}/${dataOrderNumber}" class="${buttonBlue}">Edit</a>`;
      }
      if (dataStatus < 49) {
        buttonHtml += `<a href="${baseUrl}/orders/address_service_update/${dataOrderNumber}" class="${buttonBlue}">Update Address / Service</a>`;
      }    
      if (dataStatus <= 20) {
        buttonHtml += `<a href="JavaScript:void(0)" class="${buttonRed}" onclick="deleteOrder('${dataOrderNumber}')">Cancel</a>`;
      }
      return buttonHtml
    } else {
      return true;
    }
  } else {
    if (action === "button") {
      return "";
    } else {
      apiCompletedRequests = apiCompletedRequests + 1;
      alertCustomRedirect(dataOrderNumber + " cannot be edited as it is now in the warehouse, you will be redirected to the orders page.");
      setTimeout(function () {
        window.location = baseUrl + urlPageOrdersView;
      }, 5000);
    }
  }
}

// Adds tracking Link to a value if the carrier ID is Hermes, Royal Mail, Parcelforce
function tackingNumberLink(trackingNumber, carrierId) {
  if (carrierId == 'HERMES ROTW' || carrierId == 'HERMES') {
    trackingNumber = `<a href="https://new.myhermes.co.uk/track.html#/parcel/${trackingNumber}" target="_blank">${trackingNumber}</a>`;
  }
  if (carrierId == 'ROYAL MAIL') {
    trackingNumber = `<a href="https://www.royalmail.com/track-your-item#/tracking-results/${trackingNumber}" target="_blank">${trackingNumber}</a>`;
  }
  if (carrierId == 'PARCELFORCE' || carrierId == 'PARCELFORCE INT') {
    trackingNumber = `<a href="https://www.parcelforce.com/track-trace?trackNumber=${trackingNumber}" target="_blank">${trackingNumber}</a>`;
  }
  if (carrierId == 'DPD' || carrierId == 'DPD INT') {
    trackingNumber = `<a href="https://www.dpd.co.uk/apps/tracking/?reference=${trackingNumber}" target="_blank">${trackingNumber}</a>`;
  }
  if (carrierId == 'DPD LOCAL') {
    trackingNumber = `<a href="http://www.dpdlocal.co.uk/apps/tracking/?reference=${trackingNumber}" target="_blank">${trackingNumber}</a>`;
  }
  if (carrierId == 'DHL') {
    trackingNumber = `<a href="http://www.dhl.co.uk/content/gb/en/express/tracking.shtml?brand=DHL&AWB=${trackingNumber}" target="_blank">${trackingNumber}</a>`;
  }
  return trackingNumber;
}

/**
 * Replaces string with shipping method.
 * 
 * @param {String} shippingMethod - Types of shipping method.
 */
function replace_shipping_method(shippingMethod) {
  if (shippingMethod !== null) {
    if (shippingMethod.length == 3 && shippingMethod != 685) {
      changeService = serviceShippingMethods.find(service => service.type === shippingMethod);
      if (changeService != null) {
        return changeService.name;
      }
    } else if (shippingMethod.length > 3) {
      changeService = serviceShippingMethods.find(service => service.code === shippingMethod);
      if (changeService != null) {
        return changeService.name;
      }
    } else if (shippingMethod == '685' || shippingMethod == '684') {
      switch (shippingMethod) {
        case '685':
          shippingMethod = 'DROPSHIP';
          return shippingMethod;
        case '684':
          shippingMethod = 'EMBROID';
          return shippingMethod;

        default:
          return shippingMethod;
      }
    }
  }
  else {
    return shippingMethod;
  }
}

/**
 * Replaces the API status which a readable string to populate table.
 * 
 * @param {Integer} stage - Stage which order is currently in.
 * @param {Boolean} variance - Boolean Number which relates to variance.
 */
function replace_stage(stage, variance) {
  if (replacedStatus == null || replacedStatus == '') {
    if (stage >= 30 && stage <= 31) {
      replacedStatus = 'Awaiting Packing'
    } else if (stage >= 16 && stage <= 20) {
      return replacedStatus = 'Awaiting Picking'
    } else if (stage >= 00 && stage <= 15) {
      if (variance == 0) {
        replacedStatus = 'Next Flush'
        status = 'NEXTFLUSH'
      }
      replacedStatus = 'Back Orders'
      status = 'BACK'
    }
  }
}

function replace_stage(stage, variance) {
  if (stage >= 0 && stage <= 16 && variance == 1) {
    status = 'BACK';
  } else if (stage >= 10 && stage <= 16) {
    status = 'NEXTFLUSH';
  } else if (stage >= 17 && stage <= 21) {
    status = 'AWAITINGPICK';
  } else if (stage >= 30 && stage <= 31) {
    status = 'AWAITINGPACK';
  } else if (stage >= 39 && stage <= 69) {
    status = 'AWAITINGSHIP';
  } else if (stage >= 70 && stage <= 90) {
    status = 'SHIPPED';
  }
  return status;
}

// -----------------------------> Data Functions <-----------------------------


/**
 * Convert String to Camel Case
 */
function stringToCamelCase(string, delim = ' ') {
  let split = string.split(delim);
  let returnString = "";
  for (let i = 0; i < split.length; i++) {
    let word = split[i];
    if (i != 0) {
      returnString += word.toLowerCase().charAt(0).toUpperCase() + word.slice(1)
    } else {
      returnString += word.toLowerCase();
    }
  }
  return returnString;
}

/**
 * Sorts an array of objects by column/property.
 * @param {Array} array - The array of objects.
 * @param {object} sortObject - The object that contains the sort order keys with directions (asc/desc). e.g. { age: 'desc', name: 'asc' }
 * @returns {Array} The sorted array.
 */
function multiSort(array, sortObject = {}) {
  const sortKeys = Object.keys(sortObject);
  // Return array if no sort object is supplied.
  if (!sortKeys.length) {
    return array;
  }
  // Change the values of the sortObject keys to -1, 0, or 1.
  for (let key in sortObject) {
    sortObject[key] = sortObject[key] === 'desc' || sortObject[key] === -1 ? -1 : (sortObject[key] === 'skip' || sortObject[key] === 0 ? 0 : 1);
  }
  const keySort = (a, b, direction) => {
    direction = direction !== null ? direction : 1;
    if (a === b) { // If the values are the same, do not switch positions.
      return 0;
    }
    // If b > a, multiply by -1 to get the reverse direction.
    return a > b ? direction : -1 * direction;
  };
  return array.sort((a, b) => {
    let sorted = 0;
    let index = 0;
    // Loop until sorted (-1 or 1) or until the sort keys have been processed.
    while (sorted === 0 && index < sortKeys.length) {
      const key = sortKeys[index];
      if (key) {
        const direction = sortObject[key];

        sorted = keySort(a[key], b[key], direction);
        index++;
      }
    }
    return sorted;
  });
}

// Format Charge Codes
function format_charge_codes(code) {
  switch (code) {
    case '4003':
      return 'Inbound'
    case '4004':
      return 'Storage'
    case '4005':
      return 'Pick Pack'
    case '4006':
      return 'Administration & Services'
    case '4007':
      return 'Consumables, Waste and Other'
    case '4010':
      return 'Parcel Distribution'
    case '4011':
      return 'Pallet Distribution'
    case '4012':
      return 'Freight'
    default:
      break;
  }
}

// Format Form Data
function api_format_form_data(dataFormPostPut) {
  var objTempData = {};
  $.each(dataFormPostPut, function (dataKey, val) {
    if (val.id == serial | val.id == batch | val.id == dateExpiry) {
      objTempData[val.id] = val.checked;
    } else if (val.type == 'radio' && val.checked) {
      radioID = val.id.split('_');
      if (radioID[0] == 'accountType') {
        switch (radioID[1]) {
          case 'Customer':
            objTempData['groupId'] = 2;
            break;
          case 'Admin':
            objTempData['groupId'] = 4;
            break;
        }
      } else if (radioID[0] == 'transitType' || radioID[0] == 'inboundService' || radioID[0] == 'timeSlot' || radioID[0] == 'siteId') {
        return objTempData[radioID[0]] = radioID[1];
      } else if (radioID[0] == 'containerTypeLoose' || radioID[0] == 'containerTypePallets') {
        return objTempData['containerType'] = radioID[1];
      } else {
        return objTempData[radioID[0]] = radioID[1];
      }
    } else if (val.type == 'radio') {
      return
    } else if (val.id == 'numberCartons' || val.id == 'numberPallets') {
      if (val.value !== "0") {
        objTempData[val.id] = val.value;
      }
      return
    } else if (val.id == 'dateShippedTo' || val.id == 'dateShippedFrom' || val.id == "requestedDate" || val.id == "requiredByDate") {
      objTempData[val.id] = api_date(val.value)
    } else if (val.id == 'company') {
      objTempData['companyId'] = val.value;
    } else {
      objTempData[val.id] = val.value;
    }
    if (val.type == 'checkbox') {
      objTempData[val.id] = val.checked;
    }
    if (val.type == 'file') {
      objTempData[val.id] = [];
    }
  });
  objTempData[apiKey] = key;
  return objTempData;
}

// Rename Status for API
function api_replace_status(apiStatus) {
  switch (apiStatus) {
    case statusShipped:
      return titleShipped;
      break;
    case statusBack:
      return titleBackOrders;
      break;
    case statusNextFlush:
      return titleNextFlush;
      break;
    case statusAwaitingPick:
      return titleAwaitingPick;
      break;
    case statusAwaitingPack:
      return titleAwaitingPack;
      break;
    case statusAwaitingShip:
      return titleAwaitingShip;
      break;
    default:
      return "";
  }
}

// Format Form Date value into API 
function api_date(dateCurrent) {
  var dateSplit = dateCurrent.split(" ");
  if (dateSplit.length > 1) {
    return dateSplit[3] + "-" + api_month(dateSplit[2]) + "-" + dateSplit["1"];
  } else {
    return dateCurrent;
  }
}

// Swtich month String to number for API
function api_month(monthCurrent) {
  switch (monthCurrent) {
    case "January":
      return "01";
      break;
    case "February":
      return "02";
      break;
    case "March":
      return "03";
      break;
    case "April":
      return "04";
      break;
    case "May":
      return "05";
      break;
    case "June":
      return "06";
      break;
    case "July":
      return "07";
      break;
    case "August":
      return "08";
      break;
    case "September":
      return "09";
      break;
    case "October":
      return "10";
      break;
    case "November":
      return "11";
      break;
    case "December":
      return "12";
      break;
  }
}

function switchStage(stage) {
  switch (stage) {
    case '00':
      return 'Due Out'
      break;
    case '00':
      return 'Due Out'
      break;
    case '10':
      return 'Allocate'
      break;
    case '11':
      return 'Allocated'
      break;
    case '15':
      return 'Release'
      break;
    case '16':
      return 'Released'
      break;
    case '20':
      return 'Pick'
      break;
    case '21':
      return 'Picking'
      break;
    case '29':
      return 'Picked'
      break;
    case '30':
      return 'Pack'
      break;
    case '31':
      return 'Packing'
      break;
    case '39':
      return 'Packed'
      break;
    case '40':
      return 'Marshall'
      break;
    case '41':
      return 'Marshalling'
      break;
    case '49':
      return 'Ready to Ship'
      break;
    case '50':
      return 'Packing'
      break;
    case '60':
      return 'Load'
      break;
    case '61':
      return 'Loading'
      break;
    case '69':
      return 'Loaded'
      break;
    case '70':
      return 'Shipped'
      break;
    case '90':
      return 'Shipped'
    default:
      return stage;
      break;
  }
}

// -----------------------------> PDF Functions <-----------------------------

// The Base64 string of a simple PDF file
function createPdfDownload(data, orderNumber, div, download) {
  var b64 = data;
  // Embed the PDF into the HTML page and show it to the user
  var obj = document.createElement('object');
  obj.style.width = '100%';
  obj.style.height = '70vh';
  obj.type = 'application/pdf';
  obj.data = "data:application/pdf;base64," + b64;
  $('#' + div).html('');
  $('#' + div).append(obj);
  // Insert a link that allows the user to download the PDF file
  if (download == true) {
    var link = document.createElement('a');
    link.setAttribute('class', 'btn btn-success')
    link.innerHTML = 'Download PDF file';
    link.download = `${orderNumber}.pdf`;
    link.href = 'data:application/octet-stream;base64,' + b64;
    $('#' + div).append(link)
  }
  closeAllSwalWindows();
}

