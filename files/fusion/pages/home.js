let apiTotalRequests = 1;
let orders = [];
let exceptions = [];
let dataReceived = [];
let awaitingPick = [];
let beingPicked = [];
let awaitingPack = [];
let courierException = [];
let awaitingBespokeProcess = [];
let awaitingShipping = [];
let collectionArea = [];
let serviceShippingMethods = [];
let shipped = [];
let linnworksData = [];
let fullData = [];
let dashboardData = [];

// Creates an array of Services which are offered.
var servicesPromise = new Promise(function (resolve, reject) {
  url = `${urlApi}services?API-KEY=${key}`;
  api(true, false, "", url, methodGet, "", function (getServices){
      serviceShippingMethods = getServices.data;
      resolve(serviceShippingMethods);        
  })
})

getHomepageData();

function getHomepageData() {
  const apiFusionOrders = `${urlApi}orders/fusion_orders_breakdown?${apiKey}=${key}`;
  api(false, false, "", apiFusionOrders, methodGet, [], function (getOrders) {
    orders = getOrders.data;
    dataSplitter()
  });
  let date = new Date;
  dateFormatted = formatJSDate(date);  
  let apiFusionOrdersShipped = `${apiFusionOrders}&shipped=${dateFormatted}`
  api(false, false, "", apiFusionOrdersShipped, methodGet, [], function (getShippedOrders) {
    if (getShippedOrders.results.total > 0) {
      shipped = getShippedOrders.data;
    }
  });
  let apiFusionOrderBreakdown = `${urlApi}fusion_dashboard/orders_breakdown?${apiKey}=${key}`
  api(false, false, "", apiFusionOrderBreakdown, methodGet, [], function (getOrderBreakdown) {
    if (getOrderBreakdown.results.total > 0) {
      dashboardData = getOrderBreakdown.data;
    }
    let apiFusionOrderBreakdownShipped = `${apiFusionOrderBreakdown}&shipped=${dateFormatted}`;
    api(false, false, "", apiFusionOrderBreakdownShipped, methodGet, [], function (getOrdersShipped) {
      if (getOrdersShipped.results.total > 0) {
        dashboardData.push(getOrdersShipped.data[0])
      }
      orderBreakdownDisplay()
    });
  });
  let apiFusionLinnworks = `${urlApi}orders/linnworks_order_import_errors?${apiKey}=${key}`
  api(false, false, "", apiFusionLinnworks, methodGet, [], function (getLinnworks) {
    linnworksData = getLinnworks.data;
    linnworksDisplay()
  });
}

function dataSplitter() {
  exceptions = orders.filter(order => order.fusionStage === "Exception")
  exceptionsDisplay()
  courierException = orders.filter(order => order.fusionStage === "Courier Exception")
  courierExceptionDisplay()
  dataReceived = orders.filter(order => order.fusionStage === "Data Received")
  fullData.push(dataReceived)
  awaitingPick = orders.filter(order => order.fusionStage === "Awaiting Pick")
  fullData.push(awaitingPick)
  beingPicked = orders.filter(order => order.fusionStage === "Being Picked")
  fullData.push(beingPicked)
  awaitingPack = orders.filter(order => order.fusionStage === "Awaiting Pack")
  fullData.push(awaitingPack)
  awaitingBespokeProcess = orders.filter(order => order.fusionStage === "Awaiting BeSpoke Process")
  fullData.push(awaitingBespokeProcess)
  awaitingShipping = orders.filter(order => order.fusionStage === "Awaiting Shipping")
  fullData.push(awaitingShipping)
  collectionArea = orders.filter(order => order.fusionStage === "Awaiting Collection")
  fullData.push(collectionArea)
  shippingData(fullData)
}

function orderBreakdownDisplay() {    
  dashboardData.forEach(stage => {
    switch (stage.fusionStage) {
      case "Data Received":
      fusionStage = "dataReceived";
      break;
      case "Awaiting Pick":
      fusionStage = "awaitingPick";
      break;
      case "Being Picked":
      fusionStage = "beingPicked";
      break;
      case "Awaiting Pack":
      fusionStage = "awaitingPack";
      break;
      case "Awaiting Bespoke Process":
      fusionStage = "awaitingBespokeProcess";
      break;
      case "Awaiting Shipping":
      fusionStage = "awaitingShipping";
      break;
      case "Awaiting Collection":
      fusionStage = "collectionsArea";
      break;
      case "Shipped":
      fusionStage = "shippedOrders";
      break;
      default:
        return;
    }
    $('#' + fusionStage).html(`Orders: ${stage.orders}<div class="fusionHomeSplit"><div class="fusionHomeLines">Lines: ${stage.lines}</div><div class="fusionHomePieces">Pieces: ${stage.pieces}</div></div>`)
  });
}

// <td>${check_order_edit(statusBack, "button", exception.ShipmentId)}</td>
function exceptionsDisplay() {
  if (exceptions.length > 0) {
    $('#exceptions').html(`${exceptions.length}`)
    exceptions.forEach(exception => {
      $('#exceptionListBody').append(`<tr><td>${valueModalLink(true, 'modalOrder', exception.ShipmentId)}</td><td>${exception.fusionSubstage}</td></tr>`)
    });
  } else {
    $('#exceptionsBlank').hide();
  }
}

// <td>${check_order_edit(statusBack, "button", courierException.ShipmentId)}</td>
function courierExceptionDisplay() {
  //console.log(courierException, 'courierException');
  if (courierException.length > 0) {
    $('#courierException').html(`${courierException.length}`)
    courierException.forEach(courierException => {
      $('#courierExceptionList').append(`<tr><td>${valueModalLink(true, 'modalOrder', courierException.ShipmentId)}</td><td>${courierException.fusionSubstage}</td></tr>`)
    });
  } else {    
    $('#courierExceptionBox').hide();
  }
}

function linnworksDisplay() {  
  if (linnworksData.length > 0) {
    $('#linnworksExceptionBox').show();
    $('#linnworksException').html(`${linnworksData.length}`)
    linnworksData.forEach(linnwork => {
      $('#linnworksExceptionList').append(`<tr><td><a href="/orders/linnworks_import_errors">${linnwork.reference}</a></td></tr>`)
    });
  }
}

function closeTable(tableId) {
  $( "#" + tableId ).animate({
      width: "100%",
      opacity: 0,
      left: "-1000px",
    }, 500 );
  $("#" + tableId).toggle()
}

var fusionHomePageSettings = null
function ordersClicked(dataSelected) {
  let tempStage = ''; 
  data = dataSelected;
  if (fusionHomePageSettings !== null) {
      fusionHomePageSettings.destroy();
  }
  if (dataSelected == shipped) {
    $('#status').html('Tracking')
  } else {
    $('#status').html('Status')
  }
  if ((dataSelected == shipped) == true || tempStage == "Shipped") {        
    $('#trackingShipped').html("Tracking");
    $('#tracking').html('Shipped');
    columnData = "DateClosed";
  } else {    
    $('#trackingShipped').html("Service");
    $('#tracking').html('Dispatch By')
    columnData = "DateDueOut";
  }
  fusionHomePageSettings = $('#fusionHomePageTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 5,
      data: data,
      responsive: true,
      columns: [
          {data: "ShipmentId", render: function (data) {
              return valueModalLink(true, modalOrder, data);
          }},
          {data: "fusionStage" },
          {data: "fusionSubstage"},
          {data: "Name"},
          {data: "Line1"},
          {data: "CustomerRef"},
          {data: "ASN"},
          {data: "ShippingMethod", render: function (data) {                            
            return replace_shipping_method(data) ? replace_shipping_method(data) : null;
          }},
          {data: columnData, render: function (data) {
            return data.split(' ')[0]
          }},
          {data: "CustomerName"},
          {data: "ShipmentId"}
      ],
      "createdRow": function( row, data, dataIndex ) {
        if (data.Stage < 49) {
          let cellEdit = $(row)[0].cells[10];
          let orderButtons = check_order_edit(data.Stage, "button", data.ShipmentId);
          cellEdit.innerHTML = orderButtons;
        } else {
          let cellEdit = $(row)[0].cells[10];          
          cellEdit.innerHTML = "";
        }
        if (data.fusionSubstage == 'Fulfilled') {
          trackingLink = carrierId = trackingNumber = "";
          trackingNumber = data['CarrierTrackingNumber'];
          if (trackingNumber !== null) {
            trackingNumber = trackingNumber.trim();
            if (trackingNumber !== "") {
              carrierId = data['CarrierId'];
              triackingLnk = tackingNumberLink(trackingNumber, carrierId);
              let cellEdit = $(row)[0].cells[7];
              cellEdit.innerHTML = triackingLnk;
            }
          }
        }
      },
      buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
      ],
  });
  if($('#orderTableHome').is(':hidden')) {
    $("#orderTableHome").toggle();
    $( "#orderTableHome" ).animate({
      width: "100%",
      opacity: 1,
      left: "0",
    }, 500 );
  }
  $([document.documentElement, document.body]).animate({
    scrollTop: $("#fusionHomePageTable").offset().top - 200
}, 1500);
}


function cardDetails(dataArray) {    
  dataArray.forEach(element => {
      document.querySelector("#" + element.selector).innerHTML = element.count.toLocaleString();
  });
}

/**
 * Orders Shipped in Past 7 Days Graph API Call and Chart.js Functions
 * API Call = https://${urlApi}orders/?type=status&status=BACK&API-KEY=
 */
shippedLastWeek();
let shippedDataArray = [];
dateShippedArray = testData = [];
success = '';
complete = 0;
function shippedLastWeek() {
    let url = `${urlApi}${urlApiOrders}fusion_shipped_orders_data?${apiKey}=${key}`
    apiData = "";
    api(true, false, "", url, methodGet, apiData, function (getShippedData) {        
        let shippedData = getShippedData.data;        
        let j = 0;
        let h = 0;
        for (let i = 0; i < shippedData.length; i++) {
            const element = shippedData[i];                        
            if (element.day === 'day' && element.day != null) {                
                let key = Object.keys(element)[0];
                let date = Object.keys(element)[1];
                dateShippedArray[j] = {'date': date, 'count': parseInt(element.Total), 'selector': key}
                j += 1;
            } else {
                let keySelector = Object.keys(element)[0];
                shippedDataArray[h] = {'name':keySelector, 'count': parseInt(element.Total), 'selector': keySelector};
                h += 1;
            }
        }
        graphPastWeek(dateShippedArray);  
        cardDetails(shippedDataArray)      
    })
    return
}
// Orders Shipped in Past 7 Days
function graphPastWeek(data) {    
  document.querySelector("#shipped-data > div.demo-preloader").innerHTML = "";
  keys = [];
  values = [];
  reversedArray = data.reverse()
  reversedArray.forEach(element => {
      keys.push(element.date)
      values.push(element.count)
  });
  keys.reverse()
  values.reverse();
  
  var ctx = document.getElementById('myChartShipped').getContext('2d');
  window.myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: keys,
          datasets: [
              {
                  label: 'Shipped Orders',
                  data: values,
                  backgroundColor: ['rgb(177, 177, 177)','rgb(0, 147, 201)','rgb(255, 198, 41)','rgb(84, 86, 90)','rgb(255, 198, 41)','rgb(213, 0, 87)','rgb(0, 147, 201)'],
              }
          ]
        },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          },
          tooltips:{
              callbacks:{
                  title: ()=>{}
              }
          },
          legend: {
              display: false
          },
      }
  });
  divHeight = $('#ordersShipped').height()
  $('#shippingMethods').css('height', divHeight + "px")
}


let shipMethod = []
function shippingData(fullData) {
    fullData.forEach(fullDataElement => {      
        fullDataElement.forEach(element => {          
            count = 1
            tempShipMethod = element.ShippingMethod;            
            if (tempShipMethod == '685' || tempShipMethod == '684') {
                switch (tempShipMethod) {
                    case '685':
                        tempShipMethod = 'DROPSHIP';
                        break;
                    case '684':
                        tempShipMethod = 'EMBROID';
                        break;
                }
            }
            if (shipMethod.hasOwnProperty(tempShipMethod)) {
                count = shipMethod[tempShipMethod] + 1
                shipMethod[tempShipMethod] = count
            } else {
                shipMethod[tempShipMethod] = count
            }
        });
        
    });
    pieChartShipping(shipMethod)
}

function pieChartShipping(shippingDataChart) {
    keys = [];
    values = [];
    keys = Object.keys(shippingDataChart)
    values = Object.values(shippingDataChart)

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: values,
                backgroundColor: ['rgb(213, 0, 87)','rgb(177, 177, 177)','rgb(0, 147, 201)','rgb(255, 198, 41)','rgb(84, 86, 90)','rgb(213, 0, 87)','rgb(0, 147, 201)','rgb(213,0,87)'],
                label: 'Shipping Methods for Current Orders'
            }],
            labels: keys
        },
        options: {
            responsive: true
        }
    };

    var ctx = document.getElementById('chart-area').getContext('2d');
    window.myPie = new Chart(ctx, config);
}
