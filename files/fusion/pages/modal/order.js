$(document).on("click", "." + modalOrder, function () {
    get_order(true, $(this).text(), true, false);
    apiTotalRequests = apiTotalRequests + 1;
});

function clear_modal_order(modalLabel)
{
    $( "#" + modalLabel).html("");
    $( "#" + modalOrderFirstName).html("");
    $( "#" + modalOrderLastName).html("");
    $( "#" + modalOrderCompany).html("");
    $( '#' + modalOrderAddressLine1).html("");
    $( '#' + modalOrderAddressLine2).html("");
    $( "#" + modalOrderTown).html("");
    $( '#' + modalOrderRegion).html("");
    $( '#' + modalOrderPostcode).html("");
    $( '#' + modalOrderCountry).html("");
    $( '#' + modalOrderPhoneNumber).html("");
    $( '#' + modalOrderEmail).html("");
    $( '#' + modalOrderService).html("");
    $( '#' + modalOrderReference).html("");
    $( '#' + modalOrderPurchaseOrder).html("");
    $( '#' + modalOrderCurrency).html("");
    $( '#' + modalOrderDispatchDate).html("");
    $( '#' + modalOrderDeliveryNote).html("");
    $( '#' + modalOrderDateCreated).html("");
    $( '#' + modalOrderInstructionsPacking).html("");
    $( '#' + modalOrderTrackingNumber).html("");
    $( '#' + modalOrderCarrier).html("");
    $( '#modalOrderTable' ).html("");
}

function model_order_misc(orderNumber)
{
    $( "#" + modalOrderLabel).html(orderNumber);
    $( "#" + modalOrderEdit).val(orderNumber);
}

function model_order_notes(orderNumberNotes) {    
    $('#modalOrderNotes').html("")
    //url = `${urlApi}orders/notes?${apiKey}=${key}&orderNumber=${orderNumber}`;
    urlOrderNotes = `${urlApi}orders/notes?API-KEY=${key}&orderNumber=${orderNumberNotes}`;
    $.ajax({
        type: 'GET',
        url: urlOrderNotes,
        processData: false,
        success: function(response) {  
            let notes = response.data;
            notes.forEach(note => {
                let user =  `${note.first_name} ${note.last_name}`;
                let date = new Date(note.date_time)
                $('#modalOrderNotes').append(`<div class="modalNotesBody"><h3>${note.note_body}</h3></div>`)
                if (note.note_image != null) {
                    $('#modalOrderNotes').append(`<a href="${note.note_image}" target="_blank"><img src="${note.note_image}" class="modalImage"></a><i>Click to view full size. (Opens in new window)</i>`)
                }
                $('#modalOrderNotes').append(`<div class="modalNotesCreated">Created by: ${user} - ${date.toDateString()} at ${date.toLocaleTimeString()}</div>`)
            });
        }
    });
}

function model_order_view(order) {    
    closeAllSwalWindows();
    statusStage = replace_stage(order.status, 0);
    if (statusStage == 'BACK' || statusStage == 'NEXTFLUSH') {
        $( "#" + modalOrderEdit).attr("disabled", false);
    } else {
        $( "#" + modalOrderEdit).attr("disabled", true);
    }
    $( "#" + modalOrderFirstName).html(order.delivery.contact.name.first);
    $( "#" + modalOrderLastName).html(order.delivery.contact.name.last);
    $( "#" + modalOrderCompany).html(order.delivery.address.company);
    if (order.delivery.address != null && !order.delivery.address.hasOwnProperty('id')) $( '#' + modalOrderAddressLine1).html(order.delivery.address.line[1]);
    if (order.delivery.address != null && !order.delivery.address.hasOwnProperty('id')) $( '#' + modalOrderAddressLine2).html(order.delivery.address.line[2]);
    if (order.delivery.address != null && !order.delivery.address.hasOwnProperty('id')) $( "#" + modalOrderTown).html(order.delivery.address.town);
    if (order.delivery.address != null && !order.delivery.address.hasOwnProperty('id')) $( '#' + modalOrderRegion).html(order.delivery.address.region);
    if (order.delivery.address != null && !order.delivery.address.hasOwnProperty('id')) $( '#' + modalOrderPostcode).html(order.delivery.address.postCode);
    if (order.delivery.address != null && !order.delivery.address.hasOwnProperty('id')) $( '#' + modalOrderCountry).html(order.delivery.address.country.name);
    $( '#' + modalOrderPhoneNumber).html(order.delivery.contact.telephone);
    $( '#' + modalOrderEmail).html(order.delivery.contact.email);
    $( '#' + modalOrderService).html(order.service.name);
    $( '#' + modalOrderReference).html(order.reference.customer);
    $( '#' + modalOrderPurchaseOrder).html(order.reference.purchaseOrder);
    $( '#' + modalOrderCurrency).html(order.currency.name);
    $( '#' + modalOrderDispatchDate).html(order.dateTime.despatch.requested.date);
    $( '#' + modalOrderDateCreated).html(`${order.dateTime.created.date} ${order.dateTime.created.time.slice(0, -4)}`)
    if (order.deliveryNote) $( '#' + modalOrderDeliveryNote).html( order.deliveryNote.value ? order.deliveryNote.name : '');
    if (order.instructions) $( '#' + modalOrderInstructionsPacking).html(order.instructions.packing.value ? order.instructions.packing.value : '');        
    if (order.trackingNumbers && Object.keys(order.trackingNumbers).length >= 1) {
        for (const key in order.trackingNumbers) {
            if (order.trackingNumbers.hasOwnProperty(key)) {
                const trackingDetails = order.trackingNumbers[key];
                let trackingLink = tackingNumberLink(trackingDetails, order.delivery.details.carrier);
                $( '#' + modalOrderTrackingNumber).append(trackingLink);
                $( '#' + modalOrderTrackingNumber).append('<br />');
            }
        }
    } else {
        let trackingLink = tackingNumberLink(order.delivery.details.trackingNumber, order.delivery.details.carrier);
        $( '#' + modalOrderTrackingNumber).html(trackingLink);
    } 
    $( '#' + modalOrderCarrier).html(order.delivery.details.carrier);
}

function model_order_item(order)
{
  let orderItemURL = `${urlApi}orders/items/?API-KEY=${key}&orderNumber=${order.orderNumber}`;
  table = $('#modalItemTable').DataTable({
    destroy: true,
    dom: 'Bfrtip',
    responsive: true,
    ordering: false,
    autoWidth: false,
    ajax: orderItemURL,
    columns: [
      {data: "ShipmentId"},
      {data: "SOLineId"},
      {data: "SKUId"},
      {data: "Text"},
      {data: "QtyRequired"},
      {data: "QtyShipped"},
      {data: "StockStatus"},
    ]
  })
}
let packing_box_data = null;
let boxListTableSettings = null;
let packingListTableSettings = null;
function model_order_packing_list(order) {
    if (boxListTableSettings !== null) {
        boxListTableSettings.destroy();
    }
    if (packingListTableSettings !== null) {
      packingListTableSettings.destroy();
    }
    let url = `${urlApi}welcome/db_packing_list_report?${apiKey}=${key}&orderNumber=${order}`;
    $.ajax({
        type: 'GET',
        url: url,
        processData: false,
        success: function(response) {   
            packing_box_data = response.data;
            boxListTableSettings = $('#boxInfoTable').DataTable({
                dom: 'Bfrtip',
                pageLength: 5,
                data: packing_box_data.boxInfo,
                responsive: true,
                columns: [
                    { data: "BoxRef" },
                    { data: "WEIGHT" },
                    { data: "DIMS" },
                    { data: "BoxQty" },
                ],
                buttons: [
                ],
                "initComplete": function(settings, json) {
                    closeAllSwalWindows();
                }
            });
            packingListTableSettings = $('#packingListTable').DataTable({
                dom: 'Bfrtip',
                pageLength: 5,
                data: packing_box_data.packingList,
                responsive: true,
                columns: [
                    { data: "ConsignmentId" },
                    { data: "BoxRef" },
                    { data: "Prod" },
                    { data: "BatchId" },
                    { data: "Description" },
                    { data: "BoxQty" }
                ],
                buttons: [
                'copy',
                'csv',
                'excelHtml5',
                'pdfHtml5',
                'print'
                ],
                "initComplete": function(settings, json) {
                    closeAllSwalWindows();
                }
            });
        }
    });
}

/* function modal_billing_information(orderNumber) {
    url = `https://api-staging.3p-logistics.co.uk/company_billing/order_number?${apiKey}=5a539e58-28e2-40f5-94c0-06fe7c4f15eb
    &orderNumber=${orderNumber}`;
    table = $('#modalBillingTable').DataTable({
      destroy: true,
      dom: 'Bfrtip',
      responsive: true,
      ordering: false,
      autoWidth: true,
      ajax: url,
      columns: [
        {data: "shipment_id"},
        {data: "delivery_address"},
        {data: "pieces"},
        {data: "delivery_service"},
        {data: "pick_cost", render: function (data) {
          return `£${data}`
        }},
        {data: "charge_code", render: function (data) {
          return format_charge_codes(data)
        }},
      ]
    })
} */

function modal_shipping_label(order) {
    $('#shippingLabel').html(`<div class="preloader pl-size-xl">
        <div class="spinner-layer">
            <div class="circle-clipper left">
                <div class="circle"></div>
            </div>
            <div class="circle-clipper right">
                <div class="circle"></div>
            </div>
        </div>
    </div>`)
    let url = `${urlApi}orders/fusion_label/?API-KEY=${key}&orderNumber=${order}`
    var apiData = "";
    api(true, true, "", url, methodGet, apiData, function (getShippingLabel) {
        shippingLabelData = getShippingLabel.data;
        let document = shippingLabelData.docs[0].data;
        createPdfDownload(document, order, 'shippingLabel', false)
    })
}

$(document).on("click", "#" + modalOrderEdit, function () {
	window.location.href = baseUrl + urlPageOrderEdit + "/" + $( "#" + modalOrderEdit).val();
});