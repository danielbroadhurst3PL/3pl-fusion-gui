$(document).on("click", "." + modalCompany, function () {
    get_company(true, this.id, true, false);
    apiTotalRequests = apiTotalRequests + 1;
});

function clear_modal_company(modalLabel)
{
    $( "#" + modalLabel).html("");
}

function modal_company_misc(companyName)
{
    $( "#" + modalCompanyLabel).html(companyName.name);
    $( "#" + modalCompanyEdit).val(companyName.id);
}

function modal_company_view(company)
{
    $( "#" + modalCompanyID).html(company.id);
    $( "#" + modalCompanyTitle).html(company.name);
    $( "#" + modalCompanyCode).html(company.code);
    $( "#" + modalCompanyEmail).html(company.email.customerCare);
    $( "#" + modalCompanyprimaryEmail).html(company.email.primary);
    $( "#" + modalCompanysecondaryEmail).html(company.email.secondary);
    $( "#" + modalCompanyAddressLine1).html(company.address.line[1]);
    $( "#" + modalCompanyAddressLine2).html(company.address.line[2]);
    $( "#" + modalCompanyTown).html(company.address.town);
    $( "#" + modalCompanyRegion).html(company.address.region);

    $( '#' + modalCompanyLogs).html("");
}

function model_company_logs(logs)
{
    logs = api_logs(logs);

    $( '#' + modalCompanyLogs).html(logs);
}

$(document).on("click", "#" + modalCompanyEdit, function () {
	window.location.href = baseUrl + urlPageCompanyEdit + "/" + $( "#" + modalCompanyEdit).val();
});