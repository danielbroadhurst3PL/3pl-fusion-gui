$(document).on("click", "." + modalProduct, function () {    
    get_product(true, $(this).text(), true, false, true);
    apiTotalRequests = apiTotalRequests + 1;
});

function clear_modal_product()
{
    $( "#" + modalProductLabel).html("");
    $( "#" + modalProductEdit).val("");

    $( "#" + modalProductQuantity).html("");
    $( "#" + modalProductAllocated).html("");
    $( "#" + modalProductAssigned).html("");
    $( "#" + modalProductOrderIn).html("");
    $( "#" + modalProductDueIn).html("");
    $( "#" + modalProductOrderOut).html("");
    $( "#" + modalProductDueOut).html("");

    $( "#" + modalProductTitle).html("");
    $( "#" + modalProductDescription).html("");
    $( "#" + modalProductBarcode).html("");
    $( "#" + modalProductASIN).html("");
    $( "#" + modalProductDGNType).html("");
    $( "#" + modalProductDGNDetails).html("");
    $( "#" + modalProductCountry).html("");
    $( '#' + modalProductCommodityCode).html("");
    $( '#' + modalProductHeight).html("");
    $( '#' + modalProductWidth).html("");
    $( '#' + modalProductDepth).html("");
    $( '#' + modalProductWeight).html("");
    $( '#' + modalProductCurrency).html("");
    $( '#' + modalProductValue).html("");
    $( '#' + modalProductInnerQuantity).html("");
    $( '#' + modalProductMasterCartonQuantity).html("");
    $( '#' + modalProductPalletQuantity).html("");
    $( '#' + modalProductDateExpiry).html("");
    $( '#' + modalProductSerial).html("");
    $( '#' + modalProductBatch).html("");

    $( '#' + modalProductLogs).html("");
}

function model_product_logs(logs)
{
    logs = api_logs(logs);

    $( '#' + modalProductLogs).html(logs);
}

function model_product_misc(sku)
{
    $( "#" + modalProductLabel).html(sku);
    $( "#" + modalProductEdit).val(sku);
}

function model_product_stock(stock)
{
    $( "#" + modalProductQuantity).html(stock.quantity);
    $( "#" + modalProductAllocated).html(stock.allocated);
    $( "#" + modalProductAssigned).html(stock.assigned);
    $( "#" + modalProductOrderIn).html(stock.orderIn);
    $( "#" + modalProductDueIn).html(stock.dueIn);
    $( "#" + modalProductOrderOut).html(stock.orderOut);
    $( "#" + modalProductDueOut).html(stock.dueOut);
}

function model_product_view(product)
{
    console.log(product);
    
    $( "#" + modalProductTitle).html(product.title);
    $( "#" + modalProductDescription).html(product.description);
    $( "#" + modalProductBarcode).html(product.alternates ? product.alternates.barcode.value : "");
    $( "#" + modalProductASIN).html(product.alternates ? product.alternates.asin.value: "");
    $( "#" + modalProductDGNType).html(product.dgn.type.name);
    $( "#" + modalProductDGNDetails).html(product.dgn.details.name);
    $( "#" + modalProductCountry).html(product.country ? product.country.name : "");
    $( '#' + modalProductCommodityCode).html(product.commodityCode);
    $( '#' + modalProductHeight).html(product.dimensions.height);
    $( '#' + modalProductWidth).html(product.dimensions.width);
    $( '#' + modalProductDepth).html(product.dimensions.depth);
    $( '#' + modalProductWeight).html(product.weight.value);
    $( '#' + modalProductCurrency).html(product.price ? product.price.currency.name : "");
    $( '#' + modalProductValue).html(product.price ? product.price.value : "");
    $( '#' + modalProductInnerQuantity).html(product.quantity ? product.quantity.inner : "");
    $( '#' + modalProductMasterCartonQuantity).html(product.quantity ? product.quantity.masterCarton : "");
    $( '#' + modalProductPalletQuantity).html(product.quantity ? product.quantity.pallet : "");
    $( '#' + modalProductDateExpiry).html((product.date.expiry.toString()));
    $( '#' + modalProductSerial).html((product.capture.serial.toString()));
    $( '#' + modalProductBatch).html((product.capture.batch.toString()));
}

$(document).on("click", "#" + modalProductEdit, function () {
    window.location.href = baseUrl + urlPageProductEdit + "/" + $(this).val();
});