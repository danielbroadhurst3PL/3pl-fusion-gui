var apiTotalRequests = 4;
let companyData = null;
let viewMappingUrl = `${urlApi}services/mapping?${apiKey}=${key}&companyId=${userCompany}`
let deliveryNoteelect = $("#deliveryNote");
let serviceSelect = $("#service");
let editData = null;

get_service(true);
get_delivery_notes_select(true, userCompany);
get_company_promise(false, userCompany)
  .then(response => {
    companyData = response;
    showHideServiceType(companyData.rulesEngine);
  });
get_client_service_mapping(true, viewMappingUrl)

// Criteria Section 
$('#edit_formDiv').hide();
$('#deliveryNote_formDiv').hide();

$('#appliesTo_formDiv').change(function (e) {
  let appliesTo = $('#appliesTo input:radio:checked').attr('id') ? $('#appliesTo input:radio:checked').attr('id').split('_')[1] : null;
  if (appliesTo == 'DeliveryNote') {
    $('#deliveryNote_formDiv').show();
  } else {
    $('#deliveryNote_formDiv').hide();
  }
})

// Maps To Section
$('#service_formDiv').hide();
$('#generalService_formDiv').hide();

$('#serviceType_formDiv').change(function (e) {
  resetRadioService('generalService')
  let serviceType = $('#serviceType input:radio:checked').attr('id') ? $('#serviceType input:radio:checked').attr('id').split('_')[1] : null;
  if (serviceType == 'ServiceGroup') {
    $('#service_formDiv').show();
    $('#generalService_formDiv').hide();
  } else {
    $('#generalService_formDiv').show();
    $('#service_formDiv').hide();
  }
})

// Submit Form
$('#formServiceMappingTool').submit(function (e) {
  e.preventDefault();
  let method = methodPost;
  let viewMappingUrlPost = viewMappingUrl.split('?')[0];
  let serviceMappingData = $('#formServiceMappingTool :input');
  let apiFormatFormData = api_format_form_data(serviceMappingData);
  apiFormatFormData['companyId'] = userCompany;
  apiFormatFormData['createdBy'] = user;
  if (apiFormatFormData['edit']) {
    method = methodPut;
    apiFormatFormData['id'] = editData.id;
  }
  if (apiFormatFormData['appliesTo'] == 'WholeAccount') {
    apiFormatFormData['deliveryNote'] = null
  }
  if (apiFormatFormData['serviceType'] == 'GeneralService') {
    apiFormatFormData['service'] = null;
  }
  if (apiFormatFormData['serviceType'] == 'ServiceGroup') {
    apiFormatFormData['generalService'] = null;
  }
  api(false, true, "", viewMappingUrlPost, method, apiFormatFormData, function (getClientServiceMapping) {
    apiTotalRequests += 1;
    resetForm()
    get_client_service_mapping(false, viewMappingUrl);
    alertCustomSuccess('POST', 'Created')
    $('#edit').prop('checked', false);
    $('#buttonServiceMappingCreate').html('Create Service Mapping')
  })
})

// Edit Service Mapping
function editClientMapping(id) {
  let serviceMapping = clientServiceData.find(mapping => mapping.id === id)
  if (serviceMapping.id === id) {
    editData = serviceMapping;
    populateForm(serviceMapping)
  } else {
    alertCustomError("The ID's do not match - contact customer services.")
  }
}

function populateForm(data) {
  $('#edit').prop('checked', true);
  $('#buttonServiceMappingCreate').html('Update Service Mapping')
  for (const key in data) {
    if (data.hasOwnProperty(key)) {
      const value = data[key];
      $('#' + key).val(value);
      if (key == 'region' || key == 'appliesTo' || key == 'serviceType' || key == 'generalService') {
        let selected = `${key}_${value}`;
        setRadioValue(key, selected, value)
        if (value == 'DeliveryNote') {
          $('#deliveryNote_formDiv').show();
        }
        if (value == 'ServiceGroup') {
          $('#service_formDiv').show();
          $('#generalService_formDiv').hide();
        }
        if (value == 'GeneralService') {
          $('#service_formDiv').hide();
          $('#generalService_formDiv').show();
        }
      }
      if (key == 'deliveryNote') {
        let serviceId = $('#deliveryNote option:contains("'+value+'")').val();
        $('#deliveryNote').val(serviceId)
        deliveryNoteelect.selectpicker("refresh");
      }
      if (key == 'serviceName') {
        let serviceId = $('#service option:contains("'+value+'")').val();
        $('#service').val(serviceId)
        serviceSelect.selectpicker("refresh");
      }
    }
  }
}

function setRadioValue(id, selected, value) {
  $('#'+id).children().removeClass('active')
  $('#'+id).find('input').prop('checked', false)
  $('#'+selected).closest('label').addClass(value ? 'active' : '')
  $('#'+selected).prop('checked', true)
}

// Edit Priority
function changePriority(id) {
  let serviceMapping = clientServiceData.find(mapping => mapping.id === id)
  $('#editID').val(id);
  if (serviceMapping.priority !== null) {
    $('#priority').val(serviceMapping.priority);
  }
  $('#smallModalMessages').modal().show();
}

function savePriority() {
  alertCustomLoading();
  let viewMappingUrlPut = viewMappingUrl.split('?')[0];
  let priority = $('#priority').val();
  let id = $('#editID').val();
  if (priority < 1) {
    alertCustomError('Please Enter Priority')
  }
  let apiData = {
    'API-KEY': key,
    'id': id,
    'priority': priority
  }
  api(false, true, "", viewMappingUrlPut, methodPut, apiData, function (getClientServiceMapping) {
    apiTotalRequests += 1;
    get_client_service_mapping(false, viewMappingUrl)
    $('#smallModalMessages').modal('hide');
  })
}

// Delete Service Mapping
function deleteClientServiceApi(id) {
  let viewMappingUrlDel = viewMappingUrl.split('?')[0];
  let apiData = {
    'API-KEY': key,
    'id': id
  }
  api(false, true, "", viewMappingUrlDel, methodDelete, apiData, function (getClientServiceMapping) {
    apiTotalRequests += 2;
    get_client_service_mapping(false, viewMappingUrl);
  })
}

/**
 * Displays alert modal to ask user to confirm they wish to delete the Client Service Mapping.
 * 
 * @param {String} id - Client Service Mapping which will be deleted
 */
function deleteClientMapping(id) {
  swal({
    title: "Are you sure?",
    text: "You will not be able to recover this service mapping!",
    type: "warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Yes, delete it!",
    cancelButtonText: "Back",
    closeOnConfirm: false,
    closeOnCancel: false
  }, function (isConfirm) {
    if (isConfirm) {
      alertCustomLoading()
      deleteClientServiceApi(id)
    } else {
      swal("Cancelled", "Your service mapping will continue being implemented :)", "error");
    }
  });
}

// Reset Functions
function resetRadioService(id) {
  $('#' + id).children().removeClass('active');
  $('#' + id).find('input').prop('checked', false);
}

function resetForm() {
  $('#formServiceMappingTool').trigger("reset");
  resetRadioService('generalService');
  resetRadioService('serviceType');
  resetRadioService('appliesTo');
  resetRadioService('region');
  $('#deliveryNote_formDiv').hide();
  $('#service_formDiv').hide();
  $('#generalService_formDiv').hide();
  showHideServiceType(companyData.rulesEngine);
}

function showHideServiceType(rulesEngine) {
  if (!rulesEngine) {
    $('#serviceType_ServiceGroup').prop('checked', true);
    $('#serviceType_ServiceGroup')[0].labels[0].classList.add('active');
    $('#serviceType_formDiv').hide();
    $('#service_formDiv').show();
  }
}