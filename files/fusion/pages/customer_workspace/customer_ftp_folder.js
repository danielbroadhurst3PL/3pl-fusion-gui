apiTotalRequests = 0;

var tableCustomerFTPSettings = $("#tableCustomerFTP").DataTable({
    dom: 'Bfrtip',
    responsive: false,
    autoWidth: false,
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ]
});

customerFTPCheck(userCompany).then(function (FTPdata) {    
    if (!FTPdata[0].ftp.url) {
        $('#type').remove()
        $('#grnList').remove()
        $('#divFilterType').attr('class', 'col-lg-12')
        $('#divFilterType').html(`<p>Unfortunately your account does not have a FTP Folder. Please speak with customer service if you require an FTP Account.`)
    }
})

$( "#" + type ).change(function(e) {
    $('#typeFolder').remove();
    $('#typeFolderNext').remove();    
    clicked = e.target.id;
    folderPromise(clicked).then(function (importData) {
        tableCustomerFTPSettings.clear()
        importData.forEach(root => {            
            time = formatTime(root.atime)
            date = formatDate(root.atime)
            size = bytesToSize(root.size)
            if (!root.filename.includes('.') && root.filename.length > 2) {
                // This deals with Folders.
                $('#type').after(`
                <div class="btn-group btn-group-toggle" data-toggle="buttons" id="typeFolder">
                    <label class="btn btn-secondary">
                    <input type="radio" name="options" id="${root.filename}" autocomplete="off" class="switch-input">${root.filename}
                    </label>
                </div>`)
            } else if (root.filename.length > 2) {
                tableCustomerFTPSettings.row.add([
                    root.filename,
                    date,
                    time,
                    size
                ]);     
            } 
        });        
        tableCustomerFTPSettings.order([1,'asc'])
        tableCustomerFTPSettings.draw()
        $( "#typeFolder" ).change(function(e) {
            $('#typeFolderNext').remove();
            str = "";
            folderArray =  $('.active' + ' :input');
            $.each(folderArray, function (key, val) {
                str += `${val.id}/`
            })            
            changeFolder(str);
        })
    })
})

function changeFolder(clicked) {
    folderPromise(`${clicked}`).then(function (importData) {
        tableCustomerFTPSettings.clear()
        importData.forEach(root => {            
            time = formatTime(root.atime)
            date = formatDate(root.atime)
            size = bytesToSize(root.size)
            if (!root.filename.includes('.') && root.filename.length > 2) {
                // This deals with Folders.
                $('#typeFolder').after(`
                <div class="btn-group btn-group-toggle" data-toggle="buttons" id="typeFolderNext">
                    <label class="btn btn-secondary">
                    <input type="radio" name="options" id="${root.filename}" autocomplete="off" class="switch-input">${root.filename}
                    </label>
                </div>`)
            } else if (root.filename.length > 2) {
                tableCustomerFTPSettings.row.add([
                    root.filename,
                    date,
                    time,
                    size
                ]);     
            } 
        });
        tableCustomerFTPSettings.order([1,'asc'])
        tableCustomerFTPSettings.draw()
        $( "#typeFolderNext" ).change(function(e) {
            str = "";
            folderArray =  $('.active' + ' :input');
            $.each(folderArray, function (key, val) {
                str += `${val.id}/`
            })            
            changeFolder(str);
        })
    })
}

function folderPromise(folder) {
    return new Promise(function (resolve, reject) {
        url = `${urlApi}welcome/ftp_folder?API-KEY=${key}&folder=${folder}`;
        api(false, true, "", url, methodGet, "", function (getServices) {
            serviceShippingMethods = getServices.data;
            resolve(serviceShippingMethods);
        });
        apiTotalRequests += 1;
    });
}

function customerFTPCheck(id) {
    return new Promise(function (resolve, reject) {
        url = `${urlApi}companies?API-KEY=${key}&id=${id}`;
        api(false, true, "", url, methodGet, "", function (getCompany) {
            customerFTP = getCompany.data;
            resolve(customerFTP);
        });
        apiTotalRequests += 1;
    });
}