let apiTotalRequests = 0;
let viewRules = `${urlApi}services/rules_engine?${apiKey}=${key}&companyId=${userCompany}`
let rules = [];
let regionSelected = null;

$('#international').hide();
$('#uk').hide();

if ($('#destinationSwtich').prop('checked')) {
  getServices(true, 'uk');
} else {
  getServices(false, 'int');
}

function getRegion() {
  if ($('#destinationSwtich').prop('checked')) {
    return 'uk';
  } else {
    return 'int';
  }

}

function getServices(uk, regionSelected) {
apiTotalRequests += 2;
  get_service_promise(true, uk)
    .then(res => {
      if (res) {
        if (uk) {
          getCompanyRules(regionSelected);
          $('#uk').show();
        } else {
          getCompanyRules(regionSelected);
          $('#international').show();
        }
        if (window.innerWidth > 770) {
          draw(regionSelected);
        }
      }
    })
    .catch(error => {
      alertCustomError(error)
    });
}


$('#destinationSwtich').click(function () {
  removeLines();
  $('#international').hide();
  $('#uk').hide();
  if ($('#destinationSwtich').prop('checked')) {
    regionSelected = 'uk';
    getServices(true, regionSelected)
  } else {
    regionSelected = 'int';
    getServices(false, regionSelected)
  }
})

function getCompanyRules(regionSelected) {
  api(false, false, "", viewRules, methodGet, "", function (storedRules) {
    if (storedRules.data.length > 0) {
      rules = storedRules.data;
      populateRules(rules, regionSelected);
    }
  })
}

function populateRules(rules, regionSelected) {
  console.log(rules, regionSelected);
  let destination = null;
  if (regionSelected == 'uk') {
    destination = 'UK';
  } else {
    destination = 'INTERNATIONAL';
  }
  let regionRules = rules.filter(rule => rule.destination === destination);
  regionRules.forEach(rule => {
    if (rule.order_value) {
      let id = `${rule.general_service}-${rule.mail_format}-order-value-${regionSelected}`;
      $('#' + id).html(`Order Value: £${rule.order_value}`);
    }
    if (rule.service_rule && rule.service_rule === "equalOrGreaterThan") {
      let id = `${rule.general_service}-${rule.mail_format}-serviceAbove-${regionSelected}`;
      console.log(id, rule.service_id);
      let service = servicesArray.data.find(service => service.id == rule.service_id);
      $('#' + id).html(`${service.name}`);
    }
    if (rule.service_rule && rule.service_rule === "lessThan") {
      let id = `${rule.general_service}-${rule.mail_format}-serviceBelow-${regionSelected}`;
      let service = servicesArray.data.find(service => service.id == rule.service_id);
      $('#' + id).html(`${service.name}`);
    }
    if (rule.general_service === 'oversize') {
      let id = `${rule.general_service}-service-${regionSelected}`;
      let service = servicesArray.data.find(service => service.id == rule.service_id);
      $('#' + id).html(`${service.name}`);
    }
    if (rule.mail_format === 'dangerous') {
      let id = `${rule.general_service}-dangerous-service-${regionSelected}`;
      let service = servicesArray.data.find(service => service.id == rule.service_id);
      $('#' + id).html(`${service.name}`);
    }
  });
}

function setOrderValue(id) {
  $('#orderValue').val(null);
  $('#orderValueModal').modal().show();
  $('#editID').val(id);
  $("#orderValue").focus().blur(function () {
    $("#orderValue").focus()
  });
}

function storeOrderValue() {
  let rulesUrlPost = viewRules.split('?')[0];
  let orderValue = $('#orderValue').val();
  let apiData = createData($('#editID').val());
  apiData.orderValue = orderValue;
  delete apiData.serviceId;
  delete apiData.serviceRule;
  if (regionSelected == 'uk') {
    apiData.destination = 'UK';
  } else {
    apiData.destination = 'INTERNATIONAL';
  }
  $('#orderValueModal').modal('hide');
  $('#' + $('#editID').val()).html(`Order Value: £${orderValue}`);
  removeLines();
  if (getRegion() == 'uk') {
    drawLines('uk');
  } else {
    drawLines('int');
  }
  api(false, false, "", rulesUrlPost, methodPost, apiData, function (storedRules) {
    //apiTotalRequests += 1;
    if (storedRules.data.length > 0) {
      rules = storedRules.data;
      populateRules(rules, getRegion());
    }
  })
}

function setService(id) {
  let currentValue = $('#' + id)[0].innerText;
  let mailFormat = id.split('-')[1];
  let searchVal = null;
  switch (mailFormat) {
    case 'letter':
      searchVal = 2;
      break;
    case 'packet':
      searchVal = 3;
      break;
    case 'parcel':
      searchVal = 4;
      break;
    case 'pallet':
      searchVal = 5;
      break;
    default:
      searchVal = 0;
      break;
  }
  let potentialServices = servicesArray.data.filter(service => {
    return service.parcelFormat.id >= searchVal
  });
  potentialServices.data = potentialServices;
  generate_select(service, potentialServices);
  $('#selectServiceModal').modal().show();
  $('#editServiceID').val(id);
  if (currentValue != 'Service Below' && currentValue != 'Service Equal or Above') {
    let service = servicesArray.data.find(service => service.name == currentValue);
    if (service) {
      $("#service option[value=" + service.id + "]").prop('selected', true);
      $('#service').selectpicker('refresh', true)
    }
  }
}

function storeService() {
  let rulesUrlPost = viewRules.split('?')[0];
  let serviceValue = $('#service').val();
  let apiData = createData($('#editServiceID').val());
  apiData.serviceId = serviceValue;
  delete apiData.orderValue;
  if (regionSelected == 'uk') {
    apiData.destination = 'UK';
  } else {
    apiData.destination = 'INTERNATIONAL';
  }
  let chosenService = servicesArray.data.find(service => service.id == serviceValue);
  $('#selectServiceModal').modal('hide');
  $('#' + $('#editServiceID').val()).html(`${chosenService.name}`);
  api(false, false, "", rulesUrlPost, methodPost, apiData, function (storedRules) {
    //apiTotalRequests += 1;
    if (storedRules.data.length > 0) {
      rules = storedRules.data;
      populateRules(rules, getRegion());
    }
  })
}

function createData(id) {
  let data = {
    'API-KEY': key,
    companyId: userCompany,
    destination: $('#destinationSwtich').prop('checked') == 'on' ? 'UK' : 'INTERNATIONAL',
    generalService: null,
    mailFormat: null,
    orderValue: null,
    serviceId: null,
    serviceRule: null,
    createdBy: user
  }
  let split = id.split('-');
  split.forEach(value => {
    switch (value) {
      case 'economy':
        data.generalService = 'economy';
        break;
      case 'standard':
        data.generalService = 'standard';
        break;
      case 'express':
        data.generalService = 'express';
        break;
      case 'oversize':
        data.generalService = 'oversize';
        data.mailFormat = 'oversize';
        data.serviceRule = 'equals';
        break;

      case 'dangerous':
        data.mailFormat = 'dangerous';
        break;
      case 'letter':
        data.mailFormat = 'letter';
        break;
      case 'packet':
        data.mailFormat = 'packet';
        break;
      case 'parcel':
        data.mailFormat = 'parcel';
        break;
      case 'pallet':
        data.mailFormat = 'pallet';
        break;

      case 'serviceAbove':
        data.serviceRule = 'equalOrGreaterThan';
        break;
      case 'serviceBelow':
        data.serviceRule = 'lessThan';
        break;

      default:
        break;
    }
  });
  return data;
}

// Leader Lines
function draw(regionSelected) {
  $(document).ready(function () {
    setTimeout(() => {
      drawLines(regionSelected);
    }, 350);
  });
}
// Leaderline Options
let options = {
  color: '#000',
  size: 2,
  startSocketGravity: 100,
  endSocket: 'left',
};

function drawLinesUK(regionSelected) {
  // Eco UK 
  new LeaderLine(
    document.getElementById('economy' + '-' + regionSelected),
    document.getElementById('economy-sm-letter' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-sm-letter' + '-' + regionSelected),
    document.getElementById('economy-letter-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-letter-order-value' + '-' + regionSelected),
    document.getElementById('economy-let-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-letter-order-value' + '-' + regionSelected),
    document.getElementById('economy-let-sB' + '-' + regionSelected),
    options
  );
  // Standard UK
  new LeaderLine(
    document.getElementById('standard' + '-' + regionSelected),
    document.getElementById('standard-sm-letter' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-sm-letter' + '-' + regionSelected),
    document.getElementById('standard-letter-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-letter-order-value' + '-' + regionSelected),
    document.getElementById('std-let-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-letter-order-value' + '-' + regionSelected),
    document.getElementById('std-let-sB' + '-' + regionSelected),
    options
  );
  // Express UK
  new LeaderLine(
    document.getElementById('express' + '-' + regionSelected),
    document.getElementById('express-sm-letter' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-sm-letter' + '-' + regionSelected),
    document.getElementById('express-letter-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-letter-order-value' + '-' + regionSelected),
    document.getElementById('exp-let-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-letter-order-value' + '-' + regionSelected),
    document.getElementById('exp-let-sB' + '-' + regionSelected),
    options
  );
}

function drawLinesInt(regionSelected) {
  // Eco Dangerous Goods 
  new LeaderLine(
    document.getElementById('economy' + '-' + regionSelected),
    document.getElementById('economy-dangerous' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-dangerous' + '-' + regionSelected),
    document.getElementById('economy-dangerous-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-dangerous-order-value' + '-' + regionSelected),
    document.getElementById('economy-dng-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-dangerous-order-value' + '-' + regionSelected),
    document.getElementById('economy-dng-sB' + '-' + regionSelected),
    options
  );
  // Standard Dangerous Goods 
  new LeaderLine(
    document.getElementById('standard' + '-' + regionSelected),
    document.getElementById('standard-dangerous' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-dangerous' + '-' + regionSelected),
    document.getElementById('standard-dangerous-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-dangerous-order-value' + '-' + regionSelected),
    document.getElementById('std-dng-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-dangerous-order-value' + '-' + regionSelected),
    document.getElementById('std-dng-sB' + '-' + regionSelected),
    options
  );
  // Express Dangerous Goods 
  new LeaderLine(
    document.getElementById('standard' + '-' + regionSelected),
    document.getElementById('express-dangerous' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-dangerous' + '-' + regionSelected),
    document.getElementById('express-dangerous-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-dangerous-order-value' + '-' + regionSelected),
    document.getElementById('exp-dng-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-dangerous-order-value' + '-' + regionSelected),
    document.getElementById('exp-dng-sB' + '-' + regionSelected),
    options
  );
}

function drawLines(regionSelected) {
  if (regionSelected === 'int') {
    drawLinesInt(regionSelected)
  }
  if (regionSelected === 'uk') {
    drawLinesUK(regionSelected)
  }
  // Oversize 
  new LeaderLine(
    document.getElementById('oversize' + '-' + regionSelected),
    document.getElementById('oversize-s' + '-' + regionSelected),
    options
  );
  // Economy
  new LeaderLine(
    document.getElementById('economy' + '-' + regionSelected),
    document.getElementById('economy-packet' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy' + '-' + regionSelected),
    document.getElementById('economy-parcel' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy' + '-' + regionSelected),
    document.getElementById('economy-pallet' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-packet' + '-' + regionSelected),
    document.getElementById('economy-packet-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-parcel' + '-' + regionSelected),
    document.getElementById('economy-parcel-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-pallet' + '-' + regionSelected),
    document.getElementById('economy-pallet-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-packet-order-value' + '-' + regionSelected),
    document.getElementById('economy-pack-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-packet-order-value' + '-' + regionSelected),
    document.getElementById('economy-pack-sB' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-parcel-order-value' + '-' + regionSelected),
    document.getElementById('economy-parc-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-parcel-order-value' + '-' + regionSelected),
    document.getElementById('economy-parc-sB' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-pallet-order-value' + '-' + regionSelected),
    document.getElementById('economy-pal-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('economy-pallet-order-value' + '-' + regionSelected),
    document.getElementById('economy-pal-sB' + '-' + regionSelected),
    options
  );

  // Standard
  new LeaderLine(
    document.getElementById('standard' + '-' + regionSelected),
    document.getElementById('standard-packet' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard' + '-' + regionSelected),
    document.getElementById('standard-parcel' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard' + '-' + regionSelected),
    document.getElementById('standard-pallet' + '-' + regionSelected),
    options
  );

  new LeaderLine(
    document.getElementById('standard-packet' + '-' + regionSelected),
    document.getElementById('standard-packet-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-parcel' + '-' + regionSelected),
    document.getElementById('standard-parcel-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-pallet' + '-' + regionSelected),
    document.getElementById('standard-pallet-order-value' + '-' + regionSelected),
    options
  );

  new LeaderLine(
    document.getElementById('standard-packet-order-value' + '-' + regionSelected),
    document.getElementById('std-pack-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-packet-order-value' + '-' + regionSelected),
    document.getElementById('std-pack-sB' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-parcel-order-value' + '-' + regionSelected),
    document.getElementById('std-parc-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-parcel-order-value' + '-' + regionSelected),
    document.getElementById('std-parc-sB' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-pallet-order-value' + '-' + regionSelected),
    document.getElementById('std-pal-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('standard-pallet-order-value' + '-' + regionSelected),
    document.getElementById('std-pal-sB' + '-' + regionSelected),
    options
  );

  // Express

  new LeaderLine(
    document.getElementById('express' + '-' + regionSelected),
    document.getElementById('express-packet' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express' + '-' + regionSelected),
    document.getElementById('express-parcel' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express' + '-' + regionSelected),
    document.getElementById('express-pallet' + '-' + regionSelected),
    options
  );

  new LeaderLine(
    document.getElementById('express-packet' + '-' + regionSelected),
    document.getElementById('express-packet-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-parcel' + '-' + regionSelected),
    document.getElementById('express-parcel-order-value' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-pallet' + '-' + regionSelected),
    document.getElementById('express-pallet-order-value' + '-' + regionSelected),
    options
  );

  new LeaderLine(
    document.getElementById('express-packet-order-value' + '-' + regionSelected),
    document.getElementById('exp-pack-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-packet-order-value' + '-' + regionSelected),
    document.getElementById('exp-pack-sB' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-parcel-order-value' + '-' + regionSelected),
    document.getElementById('exp-parc-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-parcel-order-value' + '-' + regionSelected),
    document.getElementById('exp-parc-sB' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-pallet-order-value' + '-' + regionSelected),
    document.getElementById('exp-pal-sA' + '-' + regionSelected),
    options
  );
  new LeaderLine(
    document.getElementById('express-pallet-order-value' + '-' + regionSelected),
    document.getElementById('exp-pal-sB' + '-' + regionSelected),
    options
  );
}

function removeLines() {
  let lines = $('svg');
  for (let i = 0; i < lines.length; i++) {
    const element = lines[i];
    $(element).remove();
  }
}