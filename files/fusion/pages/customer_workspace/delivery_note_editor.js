var apiTotalRequests = 1;

get_delivery_notes(true);

$('#deliveryNoteCreate').submit(function (e) {
  e.preventDefault();
  var dataForm = $('#deliveryNoteCreate' + ' :input');
  var apiFormatFormData = api_format_form_data(dataForm);
  delete apiFormatFormData[""];
  delete apiFormatFormData["deliveryNoteForm"];
  create_delivery_note(apiFormatFormData);
})

function create_delivery_note(apiData)
{
  alertCustomCreating(`Creating Delivery Note ${apiData[name]}`);
  var apiCopyDeliveryNote = `${urlApi}deliverynotes/copy`;
	api(false, true, apiData, apiCopyDeliveryNote, methodPost, apiData, function (createDeliveryNote){
    if (createDeliveryNote.success == 1) {
      alertCustomSuccess(methodPost, createDeliveryNote.data.name)
    }      
    $('#deliveryNoteCreate').trigger("reset");
    get_delivery_notes(true);
    apiTotalRequests += 2;
	});
}

// The Base64 string of a simple PDF file
function viewDeliveryNote(id) {
  alertCustomLoading()
  let deliveryNote = deliveryNoteData.find(deliveryNote => deliveryNote.id === id)
  var b64 = deliveryNote.value;
  // Embed the PDF into the HTML page and show it to the user
  var obj = document.createElement('object');
  obj.style.width = '100%';
  obj.style.height = '842pt';
  obj.type = 'application/pdf';
  obj.data = "data:application/pdf;base64," + b64;
  $('.content').append(obj);
  // Insert a link that allows the user to download the PDF file
  var link = document.createElement('a');
  link.setAttribute('class', 'btn btn-success')
  link.innerHTML = 'Download PDF file';
  link.download = `${deliveryNote.name}.pdf`;
  link.href = 'data:application/octet-stream;base64,' + b64;
  $('#debenhamsDeliverNote').append(link)
  closeAllSwalWindows();
}

