const url = `${urlApi}welcome/welspun_billing?API-KEY=${key}`;
let recentlyPackedTableSettings = null;

function populateTable(url) {
    if (recentlyPackedTableSettings !== null) {
        recentlyPackedTableSettings.destroy();
    } 
  recentlyPackedTableSettings = $('#welspunBillingTable').DataTable({
    dom: 'Bfrtip',
    pageLength: 50,
    ajax: url,
    "order": [[ 0, "desc" ]],
    columns: [
        { data: "client" },
        { data: "chargeDate", render: function (data) {
            return data ? data.split(' ')[0] : data;
        } },
        { data: "chargeType" },
        { data: "reference" },
        { data: "clientReference" },
        { data: "UOM" },
        { data: "billableUnits" },
        { data: "tariff" },
        { data: "totalCharge", render: function (data) {              
            if (data.split('.')[1] && data.split('.')[1].length > 2) {
                  data = parseFloat(data).toFixed(2)
              }
              return data
        } },
        { data: "chargeCode" }
    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ]
});
}

populateTable(url);

$('#welspunBillingForm').submit(function (e) {
    e.preventDefault()
    var dataForm = $('#welspunBillingForm' + ' :input');
    var apiFormatFormData = api_format_form_data(dataForm);
    populateTable(`${url}&dateShippedFrom=${apiFormatFormData.dateShippedFrom}&dateShippedTo=${apiFormatFormData.dateShippedTo}`)
})