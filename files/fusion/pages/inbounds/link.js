var apiTotalRequests = 1;

const url = `${urlApi}inbounds?${apiKey}=${key}&status=unknown`

get_unknown_inbound_bookings(true, url);

async function showGRNs(value) {
  let unknownInboundPO = value;
  try {
    let openGRNs = await loadOpenGrns();
    await populateGrnDropdown(openGRNs);
    $('#largeLinkGRN').modal().show();
    closeAllSwalWindows();
    $('#messageBody').html('')
    $('#messageBody').append(`<p class="pad-2">Please select the GRN which you would like to link to the Unknown Inbound <strong>${unknownInboundPO}</strong>, this will allow us to correctly put away your stock.</p>`)
    $('#openGRNS').change(function(e) {      
      let selectedGrn = e.target.value;
      $('#linkGrn').prop('disabled', false)
      $('#linkGrn').attr('onClick', 'linkGrnToUnknown("'+selectedGrn+'", "'+unknownInboundPO+'");');
    })
  } catch (error) {    
    return alertCustomError('No GRNs in a Due In status have been found in the system. Please create a GRN before attempting to link it to an Unknown Inbound.')
  }

}

function loadOpenGrns() {
  alertCustomLoading();
  apiTotalRequests += 1;
  let openGrnsUrl = `${urlApi}grns?${apiKey}=${key}&status=open`;
  return new Promise((resolve, reject) => {
    api(true, true, "", openGrnsUrl, methodGet, "", function (getGrns) {
      resolve(getGrns.data);
    });
  })
}

function populateGrnDropdown(options) {
  return new Promise ((resolve, reject) => {
    poNumbers = options;    
    let selectId = $("#openGRNS").empty();
    selectId.append("<option>Please select a GRN</option>");
    let dueInInbounds = 0;
    for (let i = 0; i < poNumbers.length; i++) {
      const element = poNumbers[i];
      if (element.status == 'DUE IN') {
        dueInInbounds += 1;
        selectId.append("<option value=" + element.reference.customer.replace(/\s/g, "") + ">" + element.reference.customer + " " + "(" + element.status + ") " + "</option>");
      }
    }
    if (dueInInbounds === 0) {
      return reject(false)
    }
    selectId.selectpicker("refresh");
    resolve(true)
  })
}

function linkGrnToUnknown(selectedGrn, unknownInboundPO) {
  alertCustomLoading();
  console.log(selectedGrn, unknownInboundPO);
  let linkUrl = `${urlApi}inbounds/link`
  let requestBody = {
    'API-KEY': key,
    selectedGrn,
    unknownInboundPO
  }
  api(false, false, "", linkUrl, methodPost, requestBody, function (linkResponse) {
    apiTotalRequests += 1;
    alertCustomSuccess('PUT', 'Successfully Linked GRN to Inbound.');
    $('#largeLinkGRN').modal('hide');
    $('#messageBody').html('');
    $("#openGRNS").empty();
    get_unknown_inbound_bookings(true, url);
  })
}