var apiTotalRequests = 1;

const url = `${urlApi}inbounds?${apiKey}=${key}`

get_inbound_bookings(true, url);


/**
 * Displays alert modal to ask user to confirm they wish to delete the order.
 * 
 * @param {String} poNumber - Order Number which will be deleted
 */
function deleteInboundBooking(poNumber) {
  swal({
      title: "Are you sure?",
      text: "You will not be able to recover this Inbound Booking!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, cancel it!",
      cancelButtonText: "Back",
      closeOnConfirm: false,
      closeOnCancel: false
  }, function (isConfirm) {
      if (isConfirm) {
          alertCustomLoading()
          deleteInboundBookingAPI(poNumber)
      } else {
          swal("Cancelled", "Your inbound booking will continue being processed :)", "error");
      }
  });
}

/**
* Calls the delete order endpoint with an order number and deletes from system.
* 
* @param {String} poNumber - Order number which will be deleted
*/
function deleteInboundBookingAPI(poNumber) {
  apiOrdersDelete = `${urlApi}inbounds`;
  apiData = {'poNumber': poNumber, ["API-KEY"]:key}
  api(false, true, "", apiOrdersDelete, methodDelete, apiData, function (deleteOrder) {  
    if (deleteOrder.status == 200) {
      get_inbound_bookings(false, url)
      swal("Cancelled!", "Your Inbound Booking has been cancelled.", "success");
    } else {
      swal("Error!", "Your Inbound Booking was not cancelled, please contact customer services.", "error");
    }
  })
}