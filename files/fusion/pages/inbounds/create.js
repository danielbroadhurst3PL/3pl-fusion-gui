let apiTotalRequests = 1;
// Create Global variables
let poNumbers = null;
let url = `${urlApi}grns?${apiKey}=${key}&status=open`
let disabledDays = [6, 7];
let date = new Date();

$('#buttonInboundCreate').attr('disabled', true)

/**
 * Disable Datepicker Weekends
 */
$("#requestedDate").bootstrapMaterialDatePicker({
  format: 'dddd DD MMMM YYYY',
  clearButton: true,
  weekStart: 1,
  time: false,
  minDate: new Date(),
  disabledDays: disabledDays
});

if ($('#userStaff').length == 0) {
  addTwoBusinessDay()
}

function setMinDate(date) {
  $('#requestedDate').bootstrapMaterialDatePicker('setMinDate', moment(date));
}

function addTwoBusinessDay() {
  if ($('#userStaff').length == 0) {
    let tempDate = new Date(date)
    let today = tempDate.getDay()
    switch (today) {
      case 4:
        tempDate.setDate(tempDate.getDate() + 4)
        break;
      case 5:
        tempDate.setDate(tempDate.getDate() + 5)
        break;
      case 6:
        tempDate.setDate(tempDate.getDate() + 4)
        break;
  
      default:
        tempDate.setDate(tempDate.getDate() + 2)
        break;
    }
    setMinDate(tempDate);
  } else {
    setMinDate(date)
  }
}

function addSevenBusinessDays() {
  if ($('#userStaff').length == 0) {
    let tempDate = new Date(date)
    setMinDate(tempDate.setDate(tempDate.getDate() + 7))
  } else {
    setMinDate(date)
  }
}

$("a").click(function (e) {
  let div = $(e.target).closest('div');
  let upDown = e.currentTarget.dataset.spin;
  let input = $(div[0]).find('input');
  if (input.length > 0) {
    let value = $('#' + input[0].id).val()
    if (upDown === 'up') {
      value = parseInt(value) + 1;
    } else if (upDown === 'down') {
      value = parseInt(value) - 1;
    }
    checkValue(input[0].id, value)
  }
})

// Flow of Form
$('#carrierName_formDiv').hide();
$('#transitType_formDiv').hide();
$('#requestedDate_formDiv').hide();
$('#inboundService_formDiv').hide();
$('#estimatedTime_formDiv').hide();
$('#estimatedTime').attr('required', false);

$('#poNumber').change(function (e) {
  if (e.target.value) {
    $('#carrierName_formDiv').show();
    $('#carrierName').attr('placeholder', 'Please enter Carrier Name, must be more than 3 characters.')
  }
})

$('#carrierName').on('input', function (e) {
  if (e.target.value.length >= 3) {
    $('#transitType_formDiv').show();
  }
})

$('#numberPallets').keyup(function (e) {
  checkValue(e.target.id, parseInt(e.target.value))
})

$('#numberCartons').keyup(function (e) {
  checkValue(e.target.id, parseInt(e.target.value))
})

$('#containerTypePallets').change(function (e) {
  if (e.target.value) {
    $('#numberPallets_formDiv').show();
    if ($('#numberPallets').val() > 1) {
      $('#requestedDate_formDiv').show();
      addSevenBusinessDays()
    }
  }
})

$('#containerTypeLoose').change(function (e) {
  if (e.target.value) {
    addSevenBusinessDays()
    $('#numberCartons_formDiv').show();
    if ($('#numberCartons').val() > 1) {
      $('#requestedDate_formDiv').show();
      addSevenBusinessDays()
    }
  }
})

$('#timeSlot').change(function (e) {
  if (e.target.value) {
    $('#inboundService_formDiv').show();
  }
})

function checkValue(type, value) {
  if (type == 'numberPallets') {
    palletRules(value)
  } else if (type == 'numberCartons') {
    cartonRules(value)
  }
}

function palletRules(value) {
  let transitType = $('input[name=transitType]:checked').attr('id');
  if (transitType === 'transitType_Pallets' && value > 5 && value < 10) {
    $('#requestedDate_formDiv').show();
    $('#estimatedTime_formDiv').show();
    $('#estimatedTime').attr('required', true);
    $('#estimatedTime').parent().children()[0].after('<small>Warehouse Inbound hours are 7am to 6pm</small>')
    $('#estimatedTime').val("")
    $('#requestedDate').val("")
    addSevenBusinessDays()
  } else if (transitType === 'transitType_Pallets' && value > 10) {
    $('#transitType_Pallets').closest('label').removeClass('active')
    $('#containerTypePallets').closest('input').attr('checked', false)
    $('#transitType_ContainerPallets').closest('label').addClass('active')
    $('#transitType_ContainerPallets').closest('input').attr('checked', true)
    $('#containerTypePallets_formDiv').show()
    $('#requestedDate_formDiv').hide();
    $('#estimatedTime_formDiv').hide();
    $('#additionalCharges_formDiv').remove()
    $('#estimatedTime').attr('required', false);
    $('#requestedDate').val("")
    $('#estimatedTime').val("")
    $('#inboundService_formDiv').hide();
    addSevenBusinessDays()
    alertCustomError('Inbounds containing more than 10 pallets must be booked in as a Container.')
  } else if (transitType === 'transitType_Pallets' && value <= 5) {
    $('#requestedDate_formDiv').show();
    $('#estimatedTime_formDiv').show();
    $('#estimatedTime').attr('min', '07:00');$('#estimatedTime').attr('max', '18:00');
    $('#estimatedTime').attr('required', true);
    addTwoBusinessDay()
  }
  if (transitType == 'transitType_ContainerPallets') {
    $('#requestedDate_formDiv').show();
    addSevenBusinessDays()
  }
}

function cartonRules(value) {
  let tempDate = new Date(date)
  if (value > 0) {
    $('#requestedDate_formDiv').show();
    if ($('#userStaff').length > 0) {
      tempDate.setDate(tempDate.getDate())
    }
  } else {
    $('#requestedDate_formDiv').hide();
  }
}

/**
 * Hide Cartons, Pallets and Containers .
 * Fill in Company Code and Disable input.
 */
resetForm();

/**
 * API Call for Open GRNS
 */
api(false, true, "", url, methodGet, "", function (getGrns) {
  let selectId = $("#poNumber");
  poNumbers = getGrns.data;
  selectId.append("<option selected value='default'>Please Choose Inbound PO Number</option>")
  if ($('#userStaff').length > 0) {
    selectId.append("<option value='enterManually'>Enter PO Manually</option>")
  }
  for (let i = 0; i < poNumbers.length; i++) {
    const element = poNumbers[i];
    if (element.status == 'DUE IN') {
      selectId.append("<option value=" + element.reference.customer.replace(/\s/g, "") + ">" + element.reference.customer + " " + "(" + element.status + ") " + "</option>")
    }
  }
  selectId.selectpicker("refresh");
})

/**
 * Show and Hide Cartons, Pallets and Containers
 */
$("#transitType").change(function () {
  var selectedType = event.target.querySelector('input').id
  switch (selectedType) {
    case 'transitType_Cartons':
      addTwoBusinessDay()
      resetContainerSections()
      $('#numberCartons_formDiv').show()
      break;
    case 'transitType_Pallets':
      addTwoBusinessDay()
      resetContainerSections()
      $('#numberPallets_formDiv').show()
      break;
    case 'transitType_ContainerPallets':
      addSevenBusinessDays()
      resetContainerSections()
      $('#containerTypePallets_formDiv').show()
      break;
    case 'transitType_ContainerLoose':
      addSevenBusinessDays()
      resetContainerSections()
      $('#containerTypeLoose_formDiv').show()
      break;
    default:
      break;
  }
});

/**
 * Check if Staff have chosen to Manually Enter PO Number
 */
$('#poNumber').change(function () {
  let poNumber = $('#poNumber').val();
  if (poNumber === 'enterManually') {
    $('#manualPoNumber_formDiv').show()
  } else {
    $('#manualPoNumber_formDiv').hide()
  }
})

/**
 * Check available times when Date Selected
 */
$('#requestedDate').change(function (e) {
  $('#doubleBook').remove()
  $('#doubleBoooking').remove()
  let selectedDate = api_date($('#requestedDate').val())
  let transitType = $('#transitType input:radio:checked').attr('id') ? $('#transitType input:radio:checked').attr('id').split('_')[1] : null;
  if (transitType === null) {
    alertCustomError('Please choose Transit Type before selecting date.')
    $('#requestedDate').val("")
    return
  } else if (transitType === 'Cartons' || transitType === 'Pallets') {
    $('#inboundService_formDiv').show();
    return
  } else {
    let urlTimeSlots = `${urlApi}inbounds/available_times?${apiKey}=${key}&selectedDate=${selectedDate}`;
    apiData = "";
    api(false, true, "", urlTimeSlots, methodGet, apiData, function (getAvailableSlots) {
      $("label[for=timeSlot_7am").show()
      $("label[for=timeSlot_11am").show()
      //$("label[for=timeSlot_2pm").show()
      apiTotalRequests += 1;
      var unavailable = 0;
      var checkDay = new Date(selectedDate).getDay();
      availableSlots = getAvailableSlots.data;
      availableSlots.forEach(slot => {
        if (slot.unavailable) {
          $("label[for=timeSlot_" + slot.unavailable).hide()
          unavailable += 1
        }
        if (slot.available) {
          $("label[for=timeSlot_" + slot.available).show()
          unavailable -= 1
        }
      });
      if (checkDay === 1) {
        $("label[for=timeSlot_7am").hide()
        $("label[for=timeSlot_11am").hide()
        //$("label[for=timeSlot_2pm").hide()
        unavailable += 3
      }
      noSlotsAvailable(unavailable, checkDay);
    });
    $('#timeSlot_formDiv').show()
  }
})

/**
 * Show and Hide Cartons, Pallets and Containers
 */
$("#inboundService").change(function () {
  var selectedType = event.target.querySelector('input').id
  let transitType = $('#transitType input:radio:checked').attr('id') ? $('#transitType input:radio:checked').attr('id').split('_')[1] : null;
  if (transitType === null) {
    $('#requestedDate').val("")
    resetInboundService();
    alertCustomError('Please choose Transit Type before selecting date.')
    return
  }
  switch (selectedType) {
    case 'inboundService_Standard':
      if ($('#additionalCharges_formDiv')) {
        $('#additionalCharges_formDiv').remove()
      }
      $('#buttonInboundCreate').attr('disabled', false)
      break;
    case 'inboundService_Express':
      let poNumber = $('#poNumber').val();
      if (poNumber === 'default') {
        $('#requestedDate').val("")
        resetInboundService();
        alertCustomError('Please choose PO Number which is relevant to the inbound booking.')
        return
      }
      let poItems = new Array();
      let additionalCharge = 0;
      let poNumberData = poNumbers.find(po => po.reference.customer.replace(/\s/g, "") === poNumber);
      if (poNumberData) {
        poItems = poNumberData.items ? poNumberData.items : [];
      }
      if ((transitType === 'ContainerLoose' || transitType === 'ContainerPallets') && timeSlot === null) {
        alertCustomError('Please choose time slot for your inbound booking.')
        return
      } else if ((transitType === 'ContainerLoose' || transitType === 'ContainerPallets') && timeSlot !== null) {
        additionalCharge += 100;
      }

      if (poItems.length <= 60) {
        additionalCharge += 50;
      } else if (poItems.length > 60 && poItems.length < 200) {
        additionalCharge += 150;
      } else {
        additionalCharge += 400;
      }

      alertCustomQuestion(`Booking an Express Inbound Service for ${poNumber} would incur and additional charge of £${additionalCharge}.`)

      $('#additionalCharges_formDiv').remove()
      $('#inboundService_formDiv').after(`
            <div class="col-md-6" id="additionalCharges_formDiv">
                <div class="input-group spinner" data-trigger="spinner">
                    <div class="form-line" style="position:relative;">
                        <label for="inboundService">Additional Charges</label>
                        <span class="input-group-addon" style="position:absolute; top:30px; font-size:1.2em;">£</span>
                        <input style="margin-left:1.2em; text-align:left;" tabindex="4" id="additionalCosts" disabled type="text" class="form-control text-center" value="${additionalCharge}" data-rule="fusionNumber">
                    </div>
                </div>
            </div>`)
      $('#buttonInboundCreate').attr('disabled', false);
      break;
    default:
      break;
  }
});

/**
 * Checks all relevant information is supplied before Submitting the form.
 */
$('#formInboundCreate').submit(function (event) {
  event.preventDefault()
  let timeSlot = $('#timeSlot input:radio:checked').attr('id') ? $('#timeSlot input:radio:checked').attr('id').split('_')[1] : null;
  let inboundService = $('#inboundService input:radio:checked').attr('id') ? $('#inboundService input:radio:checked').attr('id').split('_')[1] : null;
  let transitType = $('#transitType input:radio:checked').attr('id') ? $('#transitType input:radio:checked').attr('id').split('_')[1] : null;
  let poNumber = $('#poNumber').val();
  let poNumberData = poNumbers.find(po => po.reference.customer.replace(/\s/g, "") === poNumber);
  let editPoNumber = false;

  if (poNumber === 'enterManually') {
    editPoNumber = true;
  } else if (!poNumberData) {
    alertCustomError('Please choose PO Number which is relevant to the inbound booking.')
    return
  }
  if ((transitType === 'ContainerLoose' || transitType === 'ContainerPallets') && timeSlot === null) {
    alertCustomError('Please choose time slot for your inbound booking.')
    return
  }
  if (inboundService === null) {
    alertCustomError('Please choose inbound service.')
    return
  }
  if (transitType === 'Cartons' || transitType === 'ContainerLoose') {
    let numberCartons = parseInt($('#numberCartons').val());
    if (numberCartons === 0) {
      alertCustomError('Please enter number of Cartons which are being sent on the inbound.')
      return
    } else if (numberCartons > 50 && transitType === 'Cartons') {
      alertCustomError('Please choose Container Loose as the transit type due to number of cartons.')
      $('#requestedDate').val("")
      return
    }
    if (transitType === 'ContainerLoose') {
      let looseContainerType = $('#containerTypeLoose input:radio:checked').attr('id') ? $('#containerTypeLoose input:radio:checked').attr('id').split('_')[1] : null;
      if (looseContainerType === null) {
        alertCustomError('Please choose the type of Loose Container service which is being booked in.')
        return
      }
    }
  } else {
    let numberPallets = parseInt($('#numberPallets').val());
    if (numberPallets === 0) {
      alertCustomError('Please enter number of Pallets which are being sent on the inbound.')
      return
    } else if (numberPallets > 10 && transitType === 'Pallets') {
      alertCustomError('Please choose Container Pallets as the transit type due to number of pallets.')
      $('#requestedDate').val("")
      return
    }
    if (transitType === 'ContainerPallets') {
      let looseContainerType = $('#containerTypePallets input:radio:checked').attr('id') ? $('#containerTypePallets input:radio:checked').attr('id').split('_')[1] : null;
      if (looseContainerType === null) {
        alertCustomError('Please choose which type of Pallets Container is being booked in.')
        return
      }
    }
  }
  //alertCustomLoading();

  let urlTimeSlots = `${urlApi}inbounds`;
  let dataInboundForm = $('#formInboundCreate :input');
  let apiFormatFormData = api_format_form_data(dataInboundForm);

  apiFormatFormData['companyId'] = userCompany;
  apiFormatFormData['userId'] = user;

  if (editPoNumber) {
    apiFormatFormData['poNumber'] = `${companyCode}${$('#manualPoNumber').val().replace(/\s/g, "")}`
  }
  alertCustomLoading();
  api(false, false, "", urlTimeSlots, methodPost, apiFormatFormData, function (createInbound) {
    if (createInbound.success == 1) {
      resetForm()
      swal({
        title: "Download Inbound Booking Receipt",
        text: "Please find below download options for retrieving your inbound paperwork. This paperwork should be attached to the goods prior to delivery to 3PL!",
        type: "success",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Download PDF now!",
        cancelButtonText: "View in new Window",
        closeOnConfirm: false,
        closeOnCancel: false
      }, function (isConfirm) {
        if (isConfirm) {
          window.location = `${urlApi}welcome/customer_file?API-KEY=${key}&grnReceipts=${apiFormatFormData['poNumber']}.pdf&delivery=download`;
          alertCustomRedirectSuccess(`Thank you for creating an Inbound for ${apiFormatFormData['poNumber']}, you will be redirected to view your current Inbounds in 3 seconds.`);
          setTimeout(function () {
            window.location = baseUrl + "inbounds/view";
          }, 3000);
        }
        else {
          let url = `${urlApi}welcome/customer_file?API-KEY=${key}&grnReceipts=${apiFormatFormData['poNumber']}.pdf&delivery=view`;
          var win = window.open(url, '_blank');
          win.focus();
          alertCustomRedirectSuccess(`Thank you for creating an Inbound for ${apiFormatFormData['poNumber']}, you will be redirected to view your current Inbounds in 3 seconds.`);
          setTimeout(function () {
            window.location = baseUrl + "inbounds/view";
          }, 3000);
        }
      });
    } else {
      alertCustomError(createInbound.errorMessage[0])
    }
  });
})

function noSlotsAvailable(unavailable, checkDay) {
  if (unavailable >= 3) {
    if ($('#userStaff').length > 0) {
      $('#timeSlot_formDiv').show();
      $('#timeSlot_formDiv').append(`<p id="unavailable" class="error">The requested delivery slot is not available, please choose an alternative delivery slot.</p>
      <button id="doubleBook" class="btn btn-danger waves-effect" onclick="enableDoubleBooking()">Double Book Trailer</button>`)
    } else {
      $('#timeSlot_formDiv').append(`
      <p id="unavailable" class="error">There are no available delivery slots on the requested date. Please select an alternative date. <br />
      If no available delivery slots are suitable please contact our inbound bookings team via the below mediums<br />
      For Urgent Enquiries - Telephone : 01942 720 077 – Option 5<br />
      All other enquiries - Email : <a href="mailto:InboundBookings@3p-logistics.co.uk">InboundBookings@3p-logistics.co.uk</a>.</p>`);
      resetTimeSlots()
    }
    $('#inboundService_formDiv').hide();
    $('#additionalCharges_formDiv').remove();
    $('#inboundService').children().removeClass('active');
    $('#inboundService').find('input').attr('checked', false);
    $('#buttonInboundCreate').attr('disabled', true)
  }
  else {
    $('#unavailable').remove();
  }
}

/**
 * Shows Response from API
 */
function enableDoubleBooking(checkDay) {
  $('#doubleBook').remove()
  $('#unavailable').remove()
  $('#timeSlot_formDiv').append(`<p id="doubleBoooking" class="error">Please be aware you are about to double book an inbound trailer.</p>`);
  $("label[for=timeSlot_7am").show()
  $("label[for=timeSlot_11am").show()
  //$("label[for=timeSlot_2pm").show()
}

/**
 * Resets Inbound Service
 */
function resetInboundService() {
  $('#inboundService>label').removeClass('active');
  $('#inboundService_Express')["0"].checked = false;
  $('#inboundService_Standard')["0"].checked = false;
}

/**
 * Resets Time Slots
 */
function resetTimeSlots() {
  $('#timeSlot_formDiv').hide()
  $('#timeSlot>label').removeClass('active');
  $('#timeSlot_7am')["0"].checked = false;
  $('#timeSlot_11am')["0"].checked = false;
  //$('#timeSlot_2pm')["0"].checked = false;
}

/**
 * Reset the Form to Default
 */
function resetForm() {
  $('#formInboundCreate').trigger("reset");
  $('#transitType').children().removeClass('active');
  $('#transitType').find('input').attr('checked', false);
  $('#containerTypeLoose').children().removeClass('active');
  $('#containerTypeLoose').find('input').attr('checked', false);
  $('#containerTypePallets').children().removeClass('active');
  $('#containerTypePallets').find('input').attr('checked', false);
  $('#inboundService').children().removeClass('active');
  $('#inboundService').find('input').attr('checked', false);
  $('#numberCartons_formDiv').hide();
  $('#numberPallets_formDiv').hide();
  $('#containerTypePallets_formDiv').hide();
  $('#containerTypeLoose_formDiv').hide();
  $('#timeSlot_formDiv').hide();
  $('#additionalCharges_formDiv').remove()
  $('#manualPoNumber_formDiv').hide()
  $('#estimatedTime_formDiv').hide();
  $('#estimatedTime').attr('required', false);
  $('#estimatedTime').val("")
  $('#buttonInboundCreate').attr('disabled', true)
}
function resetContainerSections() {
  $('#containerTypePallets_formDiv').hide();
  $('#numberCartons_formDiv').hide()
  $('#numberPallets_formDiv').hide()
  $('#numberCartons').val(0)
  $('#numberPallets').val(0)
  $('#containerTypeLoose').children().removeClass('active')
  $('#containerTypeLoose').find('input').attr('checked', false)
  $('#containerTypePallets').children().removeClass('active')
  $('#containerTypePallets').find('input').attr('checked', false)
  $('#requestedDate').val("")
  $('#inboundService_formDiv').hide();
  $('#additionalCharges_formDiv').remove()
  $('#requestedDate_formDiv').hide();
  $('#containerTypeLoose_formDiv').hide()
  $('#estimatedTime_formDiv').hide();
  $('#estimatedTime').attr('required', false);
  $('#estimatedTime').val("")
  resetTimeSlots()
  resetInboundService();
  $('#buttonInboundCreate').attr('disabled', true)
}