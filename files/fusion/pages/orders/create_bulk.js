$(function() {

	const API_BULK_UPLOAD_TEMPLATE = urlApi + "/orders/bulk_orders_template?API-KEY=" + key; // GET
	const API_BULK_UPLOAD_ISSUES = urlApi + "/orders/bulk_orders_issues?API-KEY=" + key;
	const API_BULK_UPLOAD = urlApi + "/orders/bulk_orders";

	$(document).on('click', '#templateDownload', function() {
		window.location=API_BULK_UPLOAD_TEMPLATE
	});

	$(document).on('click', '#issueOrdersDownload', function() {
		window.location=API_BULK_UPLOAD_ISSUES
	});
	
	$(document).on('click', '#bulkOrderUpload', function() {
		var form = new FormData(); 
		if ($("#bulkFiles")[0].files.length === 0) {
			$("#fileValidate").html("Please attach your Bulk Upload File above before Uploading.")
			return false;
		}
		form.append("file", $("#bulkFiles")[0].files[0]);
		form.append("API-KEY", key);
		alertCustomCreating("Uploading bulk orders. Please wait this may take a while");

		$.ajax({
    		type: 'POST',
    		url: API_BULK_UPLOAD,
    		data: form,
    		//dataType: "json",
    		cache: false,
    		contentType: false,
    		processData: false,
    		success: function(response) {					
					errorString = "";
					if (response.errors > 0) {
						errorString = "\nErrors in File\n"
						errors = response.errorMessage
						errors.forEach(element => {
							errorString = errorString + element + "\n"
						});
						alertCustomError("Bulk Orders File Uploaded Successfully\n" + errorString);
					} else {
						successString = ""
						success = response.successMessage
						success.forEach(element => {
								successString = successString + element + "\n"
						});
						alertCustomSuccess("POST", "Bulk Orders File Uploaded Successfully\n" + successString);
					}
    		},
    		error: function(x, h, r) {
					alertCustomError("Failed to upload bulk order file");
    		}
		});
	});
});