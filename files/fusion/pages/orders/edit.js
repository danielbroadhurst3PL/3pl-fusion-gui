var editPageOrderNum = window.location.href;
editPageOrderNum = editPageOrderNum.split("/");

var apiTotalRequests = 6;

let orderStage = null;

get_countries(true);
get_currency(true);
get_service(true);
get_products(true, buttonAdd, false);
get_delivery_notes_select(true, userCompany);

window.setInterval(function(){
	if (apiCompletedRequests == 5) {
		get_order(true, editPageOrderNum[5], false, true);
		clearInterval();
	}
}, 5000);

// Populate From
function populate_edit_order_form(getOrder)
{
	var checkEditable = check_order_edit(getOrder.status, "edit", getOrder.orderNumber);
	orderStage = getOrder.status;
    if(checkEditable === true) {
		$( "#" + orderNumber).val(getOrder.orderNumber);
		document.getElementById(orderNumber).disabled = true;
		$( "#" + nameFirst).val(getOrder.delivery.contact.name.first);
		$( "#" + nameLast).val(getOrder.delivery.contact.name.last);
		$( "#" + company).val(getOrder.delivery.address != null && !getOrder.delivery.address.hasOwnProperty('id') ? getOrder.delivery.address.company :  "");
		$( "#" + addressLine1).val(getOrder.delivery.address != null && !getOrder.delivery.address.hasOwnProperty('id') ? getOrder.delivery.address.line[1] :  "");
		$( "#" + addressLine2).val(getOrder.delivery.address != null && !getOrder.delivery.address.hasOwnProperty('id') ? getOrder.delivery.address.line[2] :  "");
		$( "#" + town).val(getOrder.delivery.address != null && !getOrder.delivery.address.hasOwnProperty('id') ? getOrder.delivery.address.town :  "");
		$( "#" + region).val(getOrder.delivery.address != null && !getOrder.delivery.address.hasOwnProperty('id') ? getOrder.delivery.address.region :  "");
		$( "#" + postCode).val(getOrder.delivery.address != null && !getOrder.delivery.address.hasOwnProperty('id') ? getOrder.delivery.address.postCode :  "");
		$( "#" + country).val(getOrder.delivery.address != null && !getOrder.delivery.address.hasOwnProperty('id') ? getOrder.delivery.address.country.id :  "").change();
		$( "#" + telephone).val(getOrder.delivery.contact.telephone);
		$( "#" + email).val(getOrder.delivery.contact.email);
		$( "#" + service).val(getOrder.service.id).change();
		$( "#" + reference).val(getOrder.reference.customer);
		$( "#" + purchaseOrder).val(getOrder.reference.purchaseOrder);
		$( "#" + currency).val(getOrder.currency.id).change();
		$( "#" + deliveryNote).val(getOrder.deliveryNote ? getOrder.deliveryNote.id : "").change(); 
		$( "#" + dispatchDate).val(formatJSDate(new Date(getOrder.dateTime.despatch.requested.date)));
		$( "#" + packingInstructions).val(getOrder.instructions != null ? getOrder.instructions.packing.value : ""); // ERROR
		
    	// Populate item table
    	for (i = 0; i < getOrder.items.length; i++) {
      		var tempProductSku = getOrder.items[i].sku;
      		var tempProductQuantity = getOrder.items[i].quantity;
			var tempProductValue = getOrder.items[i].value;
			
			tableProductsViewSettings.rows().every( function ( rowIdx ) {
				var data = this.data();										
        		if (data == null) { return; }
        		if (data['SKUId'] == tempProductSku) {
					var dataJson = btoa(JSON.stringify(data));
        			tableProductsViewSettings
            			.row(rowIdx)
            			.remove()
            			.draw();
					tableItemsViewSettings.row.add([
						data['SKUId'],
						"<input type='number' id=" + tableItemQuantityTextbox + "_" + btoa(JSON.stringify(data["SKUId"])) + " class='form-control' step='1' min='1' value='1'>",
						"<input type='number' id=" + tableItemValueTextbox + "_" + btoa(JSON.stringify(data["SKUId"])) + " class='form-control' step='0.01' min='1' value='1'>",
						"<button type='button' class='"+ buttonRed + " " + buttonRemove + "' value='" + dataJson + "'>REMOVE</button>"
						]);
        		tableItemsViewSettings.draw();
    				document.getElementById(tableItemQuantityTextbox + "_" + btoa(JSON.stringify(data["SKUId"]))).value = tempProductQuantity;
        		document.getElementById(tableItemValueTextbox + "_" + btoa(JSON.stringify(data["SKUId"]))).value = tempProductValue;
        		}
    		});
        }
    }
}

$("#" + formOrderEdit).submit(function(e) {
	e.preventDefault();
	alertCustomLoading();

    var dataOrderEditForm = $('#' + formOrderEdit + ' :input');
    var dataOrderEditItems = tableItemsViewSettings.rows().data();

    var apiFormatFormData = api_format_form_data(dataOrderEditForm);

    var tempItems = new Object;

    dataOrderEditItems.each(function (value, index) {
		
		let tempQuant = value[1].split("id")[1].split(" ")[0].substr(1)
		let tempValue = value[2].split("id")[1].split(" ")[0].substr(1)

        tempItems[index] = new Object;
        tempItems[index].sku = value[0];
        tempItems[index].quantity = document.getElementById(tempQuant).value;
        tempItems[index].value = document.getElementById(tempValue).value;
	});

	apiFormatFormData["items"] = tempItems;

	if (orderStage == "11") {
		console.log(orderStage, "Success 11");
		$(".sa-custom").siblings('p').text("Moving order back to Editable Stage")
		changeStatus("35", apiFormatFormData)
	} else if (orderStage == "00") {
		edit_order(apiFormatFormData)
	}  else if (orderStage == "10") {
		alertCustomError(`Order is currently not editable - Please contact Customer Services`);
	}
});

function changeStatus(status, apiFormatFormData) {
	let api3plStockChange = urlApi + urlApiOrders + "stock_status";
	let apiData = {
		"API-KEY": key,
		"ShipmentId": apiFormatFormData["orderNumber"],
		"Status": status,
	}
	api(false, false, "", api3plStockChange, methodPut, apiData, function (updateStatus){
		if (updateStatus.success === 1) {
			if (updateStatus.status === 200 && updateStatus.data[0].Stage === "00") {
				$(".sa-custom").siblings('p').text("Successfully moved order to Editable Stage")
				edit_order(apiFormatFormData);
			} else if (updateStatus.status === 200 && updateStatus.data[0].Stage === "10") {
				$(".sa-custom").siblings('p').text("Moved order back to Allocating Stage")
				alertCustomSuccess("PUT", editPageOrderNum[5] + " Successfully")
			} 
		} else {
			alertCustomError(updateStatus.data.errorMessage[0]);
		}
	});
}

function edit_order(orderEditData) {
	$(".sa-custom").siblings('p').text("Editing Order " + editPageOrderNum[5])
	var api3plOrders = urlApi + urlApiOrders;
	api(false, false, "", api3plOrders, methodPut, orderEditData, function (editOrder){
		if (editOrder.success === 1) {
			$(".sa-custom").siblings('p').text("Edit Successful")
			if (orderStage == "11") {
				$(".sa-custom").siblings('p').text("Moving order back to Allocate Stage")
				changeStatus("36", {orderNumber: editOrder.data[0].orderNumber});
			} else if (orderStage == "00") {
				alertCustomSuccess("PUT", editPageOrderNum[5] + " Successfully")
			}
		} else {
			alertCustomError(editOrder.data.errorMessage[0]);
		}
	});
}
