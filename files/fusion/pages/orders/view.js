var apiTotalRequests = 2;
const apiFusionOrders = `${urlApi}orders/fusion_orders_breakdown?${apiKey}=${key}`;

hideElement(divFilterStatus);
hideElement(divFilterDateShippedTo);
hideElement(divFilterDateShippedFrom);
hideElement(divFilterOrderNumber);
dataOrderViewForm = [];

$( "#" + type ).change(function() {    
  var selectedOrderTypeVal = $('input[name=options]:checked').attr('id');

	$('#' + formOrderView).trigger("reset");
	$('#' + type).val(selectedOrderTypeVal);
	$('#' + type ).selectpicker("refresh");
    
	switch (selectedOrderTypeVal) {
        case 'orderNumber':
          hideElement(divFilterStatus);
          showElement(divFilterOrderNumber);
          $('#dateShippedFrom').prop('required', false)
          $('#dateShippedTo').prop('required', false)
        break;
        case 'status':            
          hideElement(divFilterOrderNumber);
          showElement(divFilterStatus);
          $('#dateShippedFrom').prop('required', false)
          $('#dateShippedTo').prop('required', false)
        break;
        case 'openOrders':            
            orderBreakdownDisplay("open")
        break;
    }

    hideElement(divFilterDateShippedFrom);
    hideElement(divFilterDateShippedTo);

    $('#' + status ).selectpicker("refresh");
});

$( "#statusType" ).change(function() {
    var selectedOrderStatusVal = $('input[name=statusOptions]:checked').attr('id');

	if(selectedOrderStatusVal == 'shipped')
	{
		showElement(divFilterDateShippedFrom);
        showElement(divFilterDateShippedTo);
	}
	else
	{
		hideElement(divFilterDateShippedFrom);
        hideElement(divFilterDateShippedTo);
	}

	$('#' + dateShippedFrom).val("");
	$('#' + dateShippedTo).val("");
});

$("#" + formOrderView).submit(function(e) {
    e.preventDefault();
    alertCustomLoading()
    let apiFormatFormData = new Object();
    let orderNumberSearch = '';
    apiFormatFormData[apiKey] = key;
    apiFormatFormData['orderNumber'] = "";
    apiFormatFormData['dateShippedFrom'] = "";
    apiFormatFormData['dateShippedTo'] = "";
    apiFormatFormData['status'] = e.target.querySelector('input').id;
    var dataOrderViewForm = $('#' + formOrderView + ' :input');
    for (const property in dataOrderViewForm) {
        if (dataOrderViewForm.hasOwnProperty(property)) {
            const element = dataOrderViewForm[property];
            if (element.type === 'radio' && element.checked === true) {
                apiFormatFormData['status'] = element.id;
            }
            if (element.value !== null && element.value !== "" && element.id === 'dateShippedFrom') {
                apiFormatFormData['dateShippedFrom'] = element.value;
                $("#divFilterShippedFromValidate").html('')
            }
            if (element.value !== null && element.value !== "" && element.id === 'dateShippedTo' ) {
                apiFormatFormData['dateShippedTo'] = element.value;
                $("#divFilterShippedToValidate").html('')
            }
            if (element.value !== null && element.value !== "" && element.id === 'orderNumber' && element.value !== 'on') {
                orderNumberSearch = element.value.toUpperCase()
            }
        }
    }    
    if (apiFormatFormData.dateShippedFrom != null && apiFormatFormData.dateShippedFrom != '' && apiFormatFormData.dateShippedTo != null && apiFormatFormData.dateShippedTo != '') {
      checkDateFrom = api_date(apiFormatFormData.dateShippedFrom);
      checkDateTo = api_date(apiFormatFormData.dateShippedTo);
      if (new Date(checkDateFrom) > new Date(checkDateTo)) {
        $("#divFilterShippedToValidateFuture").html('Date Shipped From cannot be in future... Please reselect dates.')
        return false;
      } else {
        $("#divFilterShippedToValidateFuture").html('')
        statusShippedBreakdown(checkDateFrom, checkDateTo)
        apiTotalRequests = apiTotalRequests + 1;
        return
      }
    } 
    if (orderNumberSearch) {
      searchOrderNumber(orderNumberSearch);
      apiTotalRequests = apiTotalRequests + 1;
    }
    // get_orders(true, apiFormatFormData, true);    
});


// Submit Button Diplay on Select or KeyUp
$('#type').change(function() {
    $("#button-div").hide()
    $('#statusType>label').removeClass('active')
    $('#divFilterOrderNumberNotFound').html('')
});
// Status Selector
$('#statusType').change(function() {
    let selectedStatus = event.target.querySelector('input').id
    if(event.target.querySelector('input').id == 'shipped') {
        $("#button-div").show();
        $("#shippedOrder").css('display', 'contents');
        $('#dateShippedFrom').prop('required', true)
        $('#dateShippedTo').prop('required', true)
        $('#divFilterOrderNumberNotFound').html('')
        orderBreakdownDisplay(selectedStatus)
    } else if (event.target.querySelector('input').id == 'awaitingShip') {
        $("#button-div").hide();
        $("#shippedOrder").css('display', 'contents');
        $('#dateShippedFrom').prop('required', false)
        $('#dateShippedTo').prop('required', false)
        $('#divFilterOrderNumberNotFound').html('')
        orderBreakdownDisplay(selectedStatus)
        //$("#" + formOrderView).submit();
    } else {
        $("#button-div").hide();
        $("#shippedOrder").css('display', 'none');
        $('#dateShippedFrom').prop('required', false)
        $('#dateShippedTo').prop('required', false)
        $('#divFilterOrderNumberNotFound').html('')
        //$("#" + formOrderView).submit();
        orderBreakdownDisplay(selectedStatus)
    }
});

$('input#orderNumber').bind("change keyup input",function() { 
    $("#button-div").show()
});

var servicesPromise = new Promise(function (resolve, reject) {
    url = `${urlApi}services?API-KEY=${key}`;
    api(false, false, "", url, methodGet, "", function (getServices){
        serviceShippingMethods = getServices.data;
        resolve(serviceShippingMethods);
    })
})

servicesPromise.then(function () {
    orderNumberRequest();
})

function orderNumberRequest() {
    urlParam = function(name){
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results !== null && results !== '') {
            return results[1] || '';
        }
    }
    var orderNumberSearch = urlParam('orderNumber');
    if (orderNumberSearch !== null && orderNumberSearch !== '' && orderNumberSearch !== undefined) {
        $('#orderNumber').parent().addClass('active');
        $('#divFilterOrderNumber').show();
        $('input#orderNumber').val(unescape(orderNumberSearch).toUpperCase());
        $("#button-div").show();
        $("#" + formOrderView).submit();
    }
}

statusBreakdown()

let orders = [];
let exceptions = [];
let dataReceived = [];
let awaitingPick = [];
let beingPicked = [];
let awaitingPack = [];
let courierException = [];
let awaitingBespokeProcess = [];
let awaitingShipping = [];
let collectionArea = [];
let shipped = [];
let fullData = [];
let singleOrder = [];

function statusBreakdown() {
  api(false, true, "", apiFusionOrders, methodGet, [], function (getOrders) {
    orders = getOrders.data;
    dataSplitter()
  });
  let date = new Date;
  dateFormatted = formatJSDate(date);  
}
function searchOrderNumber(orderNumber) {
    let tempUrl = `${apiFusionOrders}&orderNumber=${orderNumber}`
    api(false, true, "", tempUrl, methodGet, [], function (getOrders) {
        singleOrder = getOrders.data;
        ordersClicked(singleOrder)
    });
}
function statusShippedBreakdown(dateShippedFrom, dateShippedTo) {
    let apiFusionOrdersShipped = `${apiFusionOrders}&dateShippedFrom=${dateShippedFrom}&dateShippedTo=${dateShippedTo}`
    api(false, false, "", apiFusionOrdersShipped, methodGet, [], function (getShippedOrders) {
      if (getShippedOrders.results.total > 0) {
        shipped = getShippedOrders.data;
        ordersClicked(shipped)
      }
    });
}

// MOVE TO API PAGE
function orderBreakdownDisplay(selected) {    
    switch (selected) {
        case "dataReceived":
        data = dataReceived;
        break;
        case "awaitingPick":
        data = awaitingPick;
        break;
        case "beingPicked":
        data = beingPicked;
        break;
        case "awaitingPack":
        data = awaitingPack;
        break;
        case "awaitingBespokeProcess":
        data = awaitingBespokeProcess;
        break;
        case "awaitingShipping":
        data = awaitingShipping;
        break;
        case "collectionArea":
        data = collectionArea;
        break;
        case "exceptions":
        data = exceptions;
        break;
        case "courierExceptions":
        data = courierException;
        break;
        case "shipped":
        data = shipped;
        break;
        case "open":
        data = orders;
        break;
    };
    ordersClicked(data)
}

function dataSplitter() {
    exceptions = orders.filter(order => order.fusionStage === "Exception")
    fullData.push(exceptions)
    courierException = orders.filter(order => order.fusionStage === "Courier Exception")
    fullData.push(courierException)
    dataReceived = orders.filter(order => order.fusionStage === "Data Received")
    fullData.push(dataReceived)
    awaitingPick = orders.filter(order => order.fusionStage === "Awaiting Pick")
    fullData.push(awaitingPick)
    beingPicked = orders.filter(order => order.fusionStage === "Being Picked")
    fullData.push(beingPicked)
    awaitingPack = orders.filter(order => order.fusionStage === "Awaiting Pack")
    fullData.push(awaitingPack)
    awaitingBespokeProcess = orders.filter(order => order.fusionStage === "Awaiting BeSpoke Process")
    fullData.push(awaitingBespokeProcess)
    awaitingShipping = orders.filter(order => order.fusionStage === "Awaiting Shipping")
    fullData.push(awaitingShipping)
    collectionArea = orders.filter(order => order.fusionStage === "Awaiting Collection")
    fullData.push(collectionArea)
}

var fusionHomePageSettings = null
function ordersClicked(dataSelected) { 
  let tempStage = ''; 
  if (fusionHomePageSettings !== null) {
      fusionHomePageSettings.destroy();
  }  
  if (dataSelected.length > 0) {
    tempStage = dataSelected[0].fusionStage
  }
  if ((dataSelected == shipped) == true || tempStage == "Shipped") {        
    $('#trackingShipped').html("Tracking");
    $('#tracking').html('Shipped');
    columnData = "DateClosed";
  } else {    
    $('#trackingShipped').html("Service");
    $('#tracking').html('Dispatch By')
    columnData = "DateDueOut";
  }
          
  fusionHomePageSettings = $('#tableOrdersView').DataTable({
      dom: 'Bfrtip',
      pageLength: 5,
      data: dataSelected,
      responsive: true,
      columns: [
          {data: "ShipmentId", render: function (data) {
              return valueModalLink(true, modalOrder, data);
          }},
          {data: "fusionStage" },
          {data: "fusionSubstage"},
          {data: "Name"},
          {data: "Line1"},
          {data: "CustomerRef"},
          {data: "ASN"},
          {data: "ShippingMethod", render: function (data) {                            
            let shippingMethod = replace_shipping_method(data);
            if (shippingMethod != undefined) {
                return shippingMethod;
            }
            return data;
          }},
          {data: columnData, render: function (data) {
            return data.split(' ')[0]
          }},
          {data: "CustomerName"},
          {data: "ShipmentId"}
      ],
      "createdRow": function( row, data, dataIndex ) {    
        if (data.Stage < 49) {
          let cellEdit = $(row)[0].cells[10];
          let orderButtons = check_order_edit(data.Stage, "button", data.ShipmentId);
          cellEdit.innerHTML = orderButtons;
        } else {
          let cellEdit = $(row)[0].cells[10];          
          cellEdit.innerHTML = "";
        }
        if (data.fusionSubstage == 'Fulfilled') {
          trackingLink = carrierId = trackingNumber = "";
          trackingNumber = data['CarrierTrackingNumber'];
          if (trackingNumber !== null) {
            trackingNumber = trackingNumber.trim();
            if (trackingNumber !== "") {
              carrierId = data['CarrierId'];
              trackingLink = tackingNumberLink(trackingNumber, carrierId);
              let cellEdit = $(row)[0].cells[7];
              cellEdit.innerHTML = trackingLink;
            }
          }
        }
      },
      buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
      ],
  });
  if($('#orderTableHome').is(':hidden')) {
    $("#orderTableHome").toggle();
    $( "#orderTableHome" ).animate({
      width: "100%",
      opacity: 1,
      left: "0",
    }, 500 );
  }
  $([document.documentElement, document.body]).animate({
    scrollTop: $("#tableOrdersView").offset().top - 200
}, 1500);
}