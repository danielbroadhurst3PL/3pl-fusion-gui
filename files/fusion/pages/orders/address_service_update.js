let windowURL = window.location.href.split('/');
let orderQuery = windowURL[windowURL.length - 1];
let apiTotalRequests = 4;

let orderOriginal = null;
let orderUpdate = {};

get_countries(true);
get_service(true);
get_delivery_notes_select(true, userCompany);

function orderDetails(orderQuery) {
  var api3plOrder = `${urlApi}${urlApiOrders}?${apiKey}=${key}&${orderNumber}=${orderQuery}`;
  var apiData = "";
  api(false, true, "", api3plOrder, methodGet, apiData, function (order) {
    orderOriginal = order.data[0]    
    populateOrderForm(orderOriginal)
  })
}

function populateOrderForm(order) {  
  $( "#" + orderNumber).val(order.orderNumber);
  $( "#" + nameFirst).val(order.delivery.contact.name.first);
  $( "#" + nameLast).val(order.delivery.contact.name.last);
  $( "#" + company).val(order.delivery.address != null && !order.delivery.address.hasOwnProperty('id') ? order.delivery.address.company :  "");
  $( "#" + addressLine1).val(order.delivery.address != null && !order.delivery.address.hasOwnProperty('id') ? order.delivery.address.line[1] :  "");
  $( "#" + addressLine2).val(order.delivery.address != null && !order.delivery.address.hasOwnProperty('id') ? order.delivery.address.line[2] :  "");
  $( "#" + town).val(order.delivery.address != null && !order.delivery.address.hasOwnProperty('id') ? order.delivery.address.town :  "");
  $( "#" + region).val(order.delivery.address != null && !order.delivery.address.hasOwnProperty('id') ? order.delivery.address.region :  "");
  $( "#" + postCode).val(order.delivery.address != null && !order.delivery.address.hasOwnProperty('id') ? order.delivery.address.postCode :  "");
  $( "#" + country).val(order.delivery.address != null && !order.delivery.address.hasOwnProperty('id') ? order.delivery.address.country.id :  "").change();
  $( "#" + telephone).val(order.delivery.contact.telephone);
  $( "#" + email).val(order.delivery.contact.email);
  $( "#" + service).val(order.service.id).change();
  $( "#" + deliveryNote).val(order.deliveryNote != null ? order.deliveryNote.id :  "").change(); 
  $( "#" + dispatchDate).val(formatJSDate(new Date(order.dateTime.despatch.requested.date)));
  orderUpdate = {}
}

$('#formOrderAddress').change(function(e) {
  let updatedItem = e.target.id;
  let updatedValue = e.target.value;
  if (updatedItem === 'dateDespatch') {
    updatedValue = api_date(updatedValue)
  }
  orderUpdate[updatedItem] = updatedValue;
})

$('#formOrderAddress').submit(function(e) {
  alertCustomLoading()
  e.preventDefault();  
  let updateUrl = `${urlApi}${urlApiOrders}address_update`;
  if (Object.entries(orderUpdate).length === 0) {
    return alertCustomError('Nothing has changed')
  }  
  orderUpdate['orderNumber'] = orderOriginal.orderNumber;
  orderUpdate['API-KEY'] = key;  
  $.ajax({
    url: updateUrl,
    type: 'PUT',
    dataType: "json",
    data: orderUpdate,
    success: function (data) {
      let messages = data.successMessage
      redirectUser(messages)
    },
    error: function (request, status, error) {
      if (request.responseJSON) {
        if (request.responseJSON.errorMessage) {
          api_error(request.responseJSON.errorMessage, true);
        } else {
          api_error(request.responseJSON.error, true);
        }
      } else {
        alertCustomError(error);
      }
      return request;
    }
  });
})

function redirectUser(messages) {
  let string = ``
  messages.forEach(message => {
    string += `${jsUcfirst(message)}\n`
  });  
  swal({
      title: "Successfully Updated Order",
      text: string,
      type: "success",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "View Updated Order",
      cancelButtonText: "Close",
      closeOnConfirm: false,
      closeOnCancel: false
  }, function (isConfirm) {
      if (isConfirm) {
          alertCustomRedirectSuccess("You will be redirected to the View Orders page where you can view the the order in more detail.");
          setTimeout(function () {
            window.location.href = `${baseUrl}/orders/view?orderNumber=${orderQuery}`
          }, 3000);
      } else {
        closeAllSwalWindows()
      }
  });
}

function jsUcfirst(string) 
{
    return string.charAt(0).toUpperCase() + string.slice(1);
}

orderDetails(orderQuery)