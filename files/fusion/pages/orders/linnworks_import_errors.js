var apiTotalRequests = 1;

function customerLinnworksAPICheck(id) {
    return new Promise(function (resolve, reject) {
        url = `${urlApi}companies?API-KEY=${key}&id=${id}`;
        api(false, true, "", url, methodGet, "", function (getCompany) {
            customerFTP = getCompany.data;
            resolve(customerFTP);
        });
        apiTotalRequests += 1;
    });
}

customerLinnworksAPICheck(userCompany).then(function (linnworksAPI) {        
    if (linnworksAPI[0].orders.linnworks.api === true) {
        alertCustomLoading();
        populateLinnworksTable();
    } else {
        alertCustomError('Unfortunately, we cannot find your Linnworks credentials. If you believe this is an error then please speak with customer services')
        $('#linnworksImportError').html(`
        <p>Unfortunately, we cannot find your Linnworks credentials. If you believe this is an error then please speak with customer services.</p>
        `)
    }
});

function populateLinnworksTable() {
    url = `${urlApi+urlApiOrders}linnworks_order_import_errors?${apiKey}=${key}`;
    $('#linnworksImportErrorTable').DataTable({
        dom: 'Bfrtip',
        pageLength: 10,
        ajax: url,
        responsive: true,
        columns: [
            {
                data: "reference"
            },
            {
                data: "result"
            },
            {
                data: "error"
            },
            {
                data: "reference", render: function (data) {
                    return `<button type="button" class="${buttonRed} ${buttonAdd}" onclick="deleteRecord('${data}')">DELETE</button>`;
                }
            }
        ],
        buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
        ],
        "initComplete": function(settings, json) {
            closeAllSwalWindows();
          }
    });
};

function deleteRecord(orderReference) {
    swal({
        title: "Are you sure?",
        text: "This will remove the Linnworks Error Log, not fix it!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            alertCustomLoading()
            deleteLinnworksOrder(orderReference)
        } else {
            swal("Cancelled", "The Error Log will not be deleted :)", "error");
        }
    });
}

function deleteLinnworksOrder(orderReference) {
    let formData = {};
    formData['API-KEY'] = key;
    formData['orderReference'] = orderReference;
    $.ajax({
        url: `${urlApi}orders/linnworks_order_import_errors`,
        method: 'DELETE',
        data: formData,
        success: function (data) {
            if (data.success == 1) {
                swal("Deleted!", "Linnworks Error Log has been deleted.", "success");
                $('#apiResponse').empty();
                $('#apiResponse').append(`<h2>${formData['orderReference']} Deleted successfully.</h2>`).show();
            }
        },
        error: function (error) {
            swal("Error!", error.responseJSON.errorMessage[0], "error");
        }
    })
}