$('#deliveryNoteOrderNumber').submit(function (e) {
  e.preventDefault();
  let orderNumber = $('#searchQuery').val()
  populateDeliveryNoteTable(orderNumber);
  alertCustomLoading();
})
let orderDeliveryNoteTableSettings = null;
function populateDeliveryNoteTable(orderNumber) {
  url = `${urlApi}orders/order_deliver_note?${apiKey}=${key}&orderNumber=${orderNumber}`;
  if (orderDeliveryNoteTableSettings !== null) {
    orderDeliveryNoteTableSettings.destroy();
  }
  orderDeliveryNoteTableSettings = $('#orderDeliveryNoteTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      ajax: url,
      responsive: true,
      columns: [
          { data: "ShipmentId" },
          { data: "CustomerRef" },
          { data: "ASN" },
          { data: "OrderClass" },
          { data: "delivery_note" },
      ],
      buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
      ],
      "initComplete": function(settings, json) {
          closeAllSwalWindows();
        }
  });
};