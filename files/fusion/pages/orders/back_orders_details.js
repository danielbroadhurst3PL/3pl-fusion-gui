populateTable()
function populateTable() {
  url = `${urlApi+urlApiOrders}back_order_details?${apiKey}=${key}`;
  $('#backOrderDetails').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      ajax: url,
      responsive: true,
      columns: [
          { data: "ShipmentId" },
          { data: "SKUId" },
          { data: "Text" },
          { data: "QtyOrdered" },
          { data: "QtyAllocated" },
          { data: "QtyTasked" },
          { data: "QtyShort" }
      ],
      buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
      ],
      "initComplete": function(settings, json) {
          closeAllSwalWindows();
        }
  });
};