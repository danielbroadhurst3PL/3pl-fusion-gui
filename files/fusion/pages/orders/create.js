var apiTotalRequests = 5;

get_countries(true);
get_currency(true);
get_service(true);
get_products(true, buttonAdd, true);
get_delivery_notes_select(true, userCompany);

$("#" + formOrderCreate).submit(function (e) {
  e.preventDefault();
  var dataOrderCreateForm = $('#' + formOrderCreate + ' :input');
  var dataOrderCreateItems = tableItemsViewSettings.rows().data();

  var apiFormatFormData = api_format_form_data(dataOrderCreateForm);

  var tempItems = new Object;

  dataOrderCreateItems.each(function (value, index) {
    tempItems[index] = new Object;
    var tempItemsSku = value[0];
    tempItemsSku = tempItemsSku.substring(tempItemsSku.indexOf(">") + 1);
    tempItemsSku = tempItemsSku.replace("</a>", "");
    tempItems[index].sku = tempItemsSku;
    tempItems[index].quantity = document.getElementById(tableItemQuantityTextbox + "_" + btoa(JSON.stringify(value[0]))).value;
    tempItems[index].value = document.getElementById(tableItemValueTextbox + "_" + btoa(JSON.stringify(value[0]))).value;
  });

  if (apiFormatFormData['customService']) {
    apiFormatFormData['service'] = apiFormatFormData['customService']
  }
  apiFormatFormData["items"] = tempItems;
  submit_order(apiFormatFormData);
});

function submit_order(apiData) {
  alertCustomCreating("Creating Order");
  var api3plOrders = urlApi + urlApiOrders;

  api(false, true, "", api3plOrders, methodPost, apiData, function (createOrder) {
    $('#' + formOrderCreate).trigger("reset");
    datatable_clear(tableItemsViewSettings);
    get_products(true, buttonAdd, true);
  });
}

// Automatically set the Despatch Date to be today's date.
$("#dateDespatch").val(moment(new Date()).format('dddd DD MMMM YYYY'));

// Dynamically add info buttons to items and products sections
createInfoIcon("Items", "info", 7);
createInfoIcon("Products", "info", 8);
function createInfoIcon(id, info, divSelector) {
  ul = document.createElement("ul");
  ul.setAttribute("class", "header-dropdown m-r--5");
  li = document.createElement("li");
  aLink = document.createElement("a");
  aLink.href = "javascript:void(0);";
  aLink.setAttribute("onclick", "toggleInfoBar(" + id + ")");
  aLink.setAttribute("id", id);
  icon = document.createElement("i");
  icon.setAttribute("class", "material-icons " + info);
  icon.innerHTML = info;
  aLink.appendChild(icon);
  li.appendChild(aLink);
  ul.appendChild(li);
  document.querySelector("#formOrderCreate > div:nth-child(" + divSelector + ") > div > div.header").appendChild(ul);
}

/**
 * Populates the sidebarinfomation area with info about the selected card.
 * Animates the switching of card info so sidebar can remain open.
 * @param {ID from the Form} formID 
 */
function formInformation(formID) {
  // Fade information out whilst we swtich information
  $('#infoText,#infoHeader').animate({ opacity: 0 }, 150, function () {
    // Re-write the Header and clear the text area
    $('#infoText').html("")
    $('#infoHeader').html("")
    infoTitle = formID.replace(/([A-Z])/g, ' $1').trim();
    $('#infoHeader').html(`${infoTitle}`)
    // Populate Data for the selected card
    switch (formID) {
      case "CustomerContact":
        $('#infoText').append(`<p><strong>First Name:</strong><br />Customers first name, this will be printed on delivery labels.</p>`)
        $('#infoText').append(`<p><strong>Last Name:</strong><br />Customers last name, this will be used on delivery labels.</p>`)
        $('#infoText').append(`<p><strong>Phone Number:</strong><br />Customer phone number, phone number will only be used in certain situations.</p>`)
        $('#infoText').append(`<p><strong>Email:</strong><br />Customer email address which will be point of contact.</p>`)
        break;
      case "CustomerAddress":
        $('#infoText').append(`<p><strong>Address Line 1:</strong><br />First line of company address.</p>`)
        $('#infoText').append(`<p><strong>Address Line 2:</strong><br />Second line of company address.</p>`)
        $('#infoText').append(`<p><strong>Town:</strong><br />Enter the company town.</p>`)
        $('#infoText').append(`<p><strong>Region:</strong><br />Enter the county of the company.`)
        $('#infoText').append(`<p><strong>Postcode:</strong><br />Must be a valid Postcode.`)
        $('#infoText').append(`<p><strong>Country:</strong><br />Select the country from the dropdown menu.`)
        break;
      case "Delivery":
        $('#infoText').append(`<p><strong>Despatch By:</strong><br />Select the date which the order should be despacthed by.</p>`)
        $('#infoText').append(`<p><strong>Service:</strong><br />Select which service the order should be sent via. Please choose the correct service to avoid any delays in shipping</p>`)
        $('#infoText').append(`<p><strong>Delivery Notee:</strong><br />Choose which delivery note should be attached to the order.</p>`)
        break;
      case "References":
        $('#infoText').append(`<p><strong>Reference: (optional)</strong><br />Optional reference which will be added to the shipping label.</p>`)
        $('#infoText').append(`<p><strong>Purchase Order: (optional)</strong><br />Optional purchase order number can be attached to an order.</p>`)
        break;
      case "Instructions":
        $('#infoText').append(`<p><strong>Packing Instructions: (optional)</strong><br />Optional information about how you would like the order to be packaged before shipping, for example...</p>`)
        break;
      case "Currency":
        $('#infoText').append(`<p><strong>Currency:</strong><br />Please choose what currency the order should be carried out with.</p>`)
        break;
      case "Items":
        $('#infoText').append(`<p><strong>Items:</strong><br />Add Products which are located in the table below the Items table. Once you have added products, enter the quantity which will be being sent as part of the GRN.</p>`)
        break;
      case "Products":
        $('#infoText').append(`<p><strong>Products:</strong><br />Choose products by clicking the Add button which are part of the Goods in Notification. Products which are selected will be transferred to items table.</p>`)
        break;

      default:
        break;
    }
    // Re-animate now the data opacity has been populated
    $('#infoText,#infoHeader').animate({ opacity: 1 }, 450, function () { });
  })
}

$('#service_formDiv').change(function (e) {
  option = $('#service').find('option[value="' + e.target.value + '"]')[0].innerHTML;
  console.log(option);
  if (option === 'ASDA CLICK AND COLLECT') {
    enterAddtionalDetails = `<div class="col-lg-12" id="asdaLabel"><label>Please add the Asda Store Number and Name for the ASDA Click and Collect Service.</label></div>`;
    storeCodeElement = `<div class="col-md-6" id="asdaStoreNumber_formDiv">                                                                                         
                                <div class="form-group">
                                    <div class="form-line">
                                        <input tabindex="15" placeholder=" " type="text" id="asdaStoreNumber" class="form-control" pattern=".{5}" title="Store number must be 5 characters" required>
                                        <label for="asdaStoreNumber">Asda Store Number</label>
                                    </div>
                                </div>
                            </div>`
    storeNameElement = `<div class="col-md-6" id="asdaStoreName_formDiv">                                                                                         
                                <div class="form-group">
                                    <div class="form-line">
                                        <input tabindex="15" placeholder=" " type="text" id="asdaStoreName" class="form-control" required>
                                        <label for="asdaStoreName">Asda Store Name</label>
                                    </div>
                                </div>
                            </div>`
    $('#service_formDiv').after(enterAddtionalDetails);
    $('#service_formDiv').after(storeCodeElement);
    $('#service_formDiv').after(storeNameElement);
  } else {
    $('#asdaLabel').remove();
    $('#asdaStoreNumber_formDiv').remove();
    $('#asdaStoreName_formDiv').remove();
  }
  if (option === 'CUSTOM SERVICE') {
    enterAddtionalDetails = `<div class="col-lg-12" id="customLabel"><label>Please ensure that your Custom Service has been set up correctly using the <a href="${baseUrl}customer_workspace/service_mapping_tool">Service Mapping Tool</a>.</label></div>`;
    customServiceElement = `<div class="col-md-12" id="customService_formDiv">                                                                                         
                                <div class="form-group">
                                    <div class="form-line">
                                        <input tabindex="15" placeholder=" " type="text" id="customService" class="form-control" required>
                                        <label for="customService">Custom Service Name</label>
                                    </div>
                                </div>
                            </div>`
    $('#service_formDiv').after(enterAddtionalDetails);
    $('#service_formDiv').after(customServiceElement);
  } else {
    $('#customService_formDiv').remove();
    $('#customLabel').remove();
  }
})