$('#packingListOrderNumber').submit(function (e) {
  e.preventDefault();
  orderNumber = $('#searchQuery').val()
  let companyCheck = orderNumber.substring(0, 3);  
  if (companyCheck !== companyCode) {
    return alertCustomError('Order number start with '+companyCode);
  }
  populatePackingList(orderNumber);
  alertCustomLoading();
})

const stagingURL = 'https://api-staging.3p-logistics.co.uk/';

let packing_box_data = null;
let boxListTableSettings = null;
let packingListTableSettings = null;

function populatePackingList(orderNumber) {
  url = `${stagingURL}welcome/db_packing_list_report?${apiKey}=5a539e58-28e2-40f5-94c0-06fe7c4f15eb&orderNumber=${orderNumber}`;
  if (boxListTableSettings !== null) {
    boxListTableSettings.destroy();
  }
  if (packingListTableSettings !== null) {
    packingListTableSettings.destroy();
  }
  $.ajax({
    type: 'GET',
    url: url,
    processData: false,
    success: function (response) {
      packing_box_data = response.data;    
      $('#packingListDiv').show();  
      if (Object.keys(packing_box_data).length > 0) {
        $('#shipmentID').html(`<strong>Shipment ID:</strong> ${packing_box_data.address[0].ShipmentId}`)
        $('#customerRef').html(`<strong>Customer Ref:</strong> ${packing_box_data.address[0].CustomerRef}`)
        $('#ASN').html(`<strong>ASN:</strong> ${packing_box_data.address[0].ASN}`)
        $('#addressText').html(`<strong>Customer Address:</strong><br /> 
                                ${packing_box_data.address[0].Name}<br />
                                ${packing_box_data.address[0].Line1}<br />
                                ${packing_box_data.address[0].Line2}<br />
                                ${packing_box_data.address[0].State}<br />
                                ${packing_box_data.address[0].PostCode}<br />
                                ${packing_box_data.address[0].Country}<br />`)
        boxListTableSettings = $('#boxInfoTable').DataTable({
          dom: 'Bfrtip',
          pageLength: 10,
          data: packing_box_data.boxInfo,
          responsive: true,
          columns: [
            { data: "BoxRef" },
            { data: "WEIGHT" },
            { data: "DIMS" },
            { data: "BoxQty" },
          ],
          buttons: [
          ],
          "initComplete": function (settings, json) {
            closeAllSwalWindows();
          }
        });      
        packingListTableSettings = $('#packingListTable').DataTable({
          dom: 'Bfrtip',
          pageLength: 10,
          data: packing_box_data.packingList,
          responsive: true,
          columns: [
            { data: "BoxRef" },
            { data: "Prod" },
            { data: "Description" },
            { data: "BoxQty" }
          ],
          buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
          ],
          "initComplete": function (settings, json) {
            closeAllSwalWindows();
          }
        })
      } else {
        alertCustomError('No Results Found.')
      }

    },
    error: function (x, h, r) {
      alertCustomError(x, h, r);
    }
  });
};

function create_packing_list_report() {
  orderREF = $('#orderDetails')[0]
  address = $('#addressDetails')[0]
  margins = {
    top: 5,
    left: 5,
  };

  var doc = new jsPDF();
  var col = ["BoxRef", "WEIGHT", "DIMS", "BoxQty"];
  var rows = [];
  var rows1 = [];

  /* The following array of object as response from the API req  */
  packing_box_data.boxInfo.forEach(element => {
    var temp = [element.BoxRef, element.WEIGHT, element.DIMS, element.BoxQty];
    rows.push(temp);

  });

  if (companyCode === 'WEL') {
    var col1 = ["BoxRef", 'SKU', "Description", "BoxQty"];
    packing_box_data.packingList.forEach(element => {
      var temp1 = [element.BoxRef, element.Prod, element.Description, element.BoxQty];
      rows1.push(temp1);
    });
  } else {
    var col1 = ["BoxRef", 'SKU', 'Batch ID', "Description", "BoxQty"];
    packing_box_data.packingList.forEach(element => {
      var temp1 = [element.BoxRef, element.Prod, element.BatchId, element.Description, element.BoxQty];
      rows1.push(temp1);
    });
  }

  var doc = new jsPDF();
  doc.setFontSize(12)

  doc.text('Shipment ID: ' + packing_box_data.address[0].ShipmentId, 15, 10)
  doc.text('Customer Ref: ' + packing_box_data.address[0].CustomerRef, 15, 15)
  doc.text('Customer ASN: ' + packing_box_data.address[0].ASN, 15, 20)

  doc.text('Customer Address', 195, 10, 'right')
  doc.text(packing_box_data.address[0].Name, 195, 15, 'right')
  doc.text(packing_box_data.address[0].Line1, 195, 20, 'right')
  doc.text(packing_box_data.address[0].Line2, 195, 25, 'right')
  doc.text(packing_box_data.address[0].State, 195, 30, 'right')
  doc.text(packing_box_data.address[0].PostCode, 195, 35, 'right')
  doc.text(packing_box_data.address[0].Country, 195, 40, 'right')

  doc.autoTable(col, rows, { startY: 45 });
  doc.autoTable(col1, rows1);
  doc.save(orderNumber + '_packing_list.pdf');
}