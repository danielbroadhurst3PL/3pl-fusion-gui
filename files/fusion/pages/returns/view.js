var apiTotalRequests = 1;

get_returns(true, true);

$('#returnsForm').submit(function (e) {
    e.preventDefault();
    var dataReturnsForm = $('#returnsForm' + ' :input');   
    var formattedData = api_format_form_data(dataReturnsForm)
    var dateShippedFrom = formattedData.dateShippedFrom;
    var dateShippedTo = formattedData.dateShippedTo;
    tableReturnsViewSettings.destroy();
    get_returns(true, true, dateShippedFrom, dateShippedTo);
    $('#returnsTitle').html(`Returns From ${dateShippedFrom} to ${dateShippedTo}`)
    apiTotalRequests += 1;
})