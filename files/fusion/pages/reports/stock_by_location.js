const url = `${urlApi}reports/stock_by_location?API-KEY=${key}`;
let apiTotalRequests = 0;

let tableSettings = null;
function populateTable(url) {
  if (tableSettings !== null) {
    tableSettings.destroy();
  }
  tableSettings = $('#stockByLocationTable').DataTable({
    dom: 'Bfrtip',
    pageLength: 20,
    ajax: url,
    "order": [[3, "desc"]],
    columns: [
      { data: "StockId" },
      { data: "BizId" },
      { data: "OwnerId" },
      {
        data: "SKUId", render: function (data) {
          return valueModalLink(true, modalProduct, data);
        }
      },
      { data: "State" },
      { data: "Site" },
      { data: "Facility" },
      { data: "Zone" },
      { data: "Bay" },
      { data: "Section" },
      { data: "Slot" },
      { data: "StU" },
      { data: "StT" },
      { data: "Qty" },
      { data: "UnitOfMeasure" },
      { data: "BoxQty" },
      { data: "Status" },
      { data: "DateProduction" },
      { data: "DateReceipt" },
      { data: "DateRelease" },
      { data: "DateExpiry" },
      { data: "Country" },
      { data: "BatchId" },
      { data: "AssignType" },
      { data: "AssignRef" },
      { data: "Consignment" },
      { data: "LoadId" },
      { data: "PairInd" },
      { data: "PairStU" },
      { data: "OriginStockId" },
      { data: "DateCreated" },
      { data: "DateHeld" },
      { data: "DateLastMoved" },
    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ]
  });
}

populateTable(url);