const url = `${urlApi}reports/stock_adjustment?API-KEY=${key}`;
let apiTotalRequests = 0;
let tableSettings = null;

function populateTable(url) {
    if (tableSettings !== null) {
        tableSettings.destroy();
    } 
  tableSettings = $('#stockAdjustmentTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 50,
      ajax: url,
      "order": [[ 0, "desc" ]],
      columns: [
          { data: "movementType" },
          { data: "skuId", render: function (data) {
            return valueModalLink(true, modalProduct, data);
          }},
          { data: "productDescription" },
          { data: "quantity" },
          { data: "oldStatus" },
          { data: "newStatus" },
          { data: "dateActioned" },
          { data: "moveLineId" }
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ]
  });
}

populateTable(url);


$('#stockAdjustmentForm').submit(function (e) {
    e.preventDefault();
    var dataCreateCreateForm = $('#stockAdjustmentForm' + ' :input');
    var apiFormatFormData = api_format_form_data(dataCreateCreateForm);
    if (apiFormatFormData['dateShippedFromTime']) {
        apiFormatFormData['timeFrom'] = `${apiFormatFormData['dateShippedFromTime'].split("-")[1].trim()}:00`;
        apiFormatFormData['timeTo'] = `${apiFormatFormData['dateShippedToTime'].split("-")[1].trim()}:59`;
        
        apiFormatFormData['dateFrom'] = api_date(apiFormatFormData['dateShippedFromTime'].split("-")[0].trim());
        apiFormatFormData['dateTo'] = api_date(apiFormatFormData['dateShippedToTime'].split("-")[0].trim());
        console.log();
        
    }    

    if (apiFormatFormData.dateFrom == null && apiFormatFormData.dateTo == null) {
        $("#divFilterShippedToValidateFuture").html('<span class="required-label">Please choose Date Start and End Times</span>') 
        return false;
    } 
        
    if (apiFormatFormData.dateFrom != null && apiFormatFormData.dateTo != null) {
        checkDateFrom = api_date(apiFormatFormData.dateFrom);
        checkDateTo = api_date(apiFormatFormData.dateTo);
        if (new Date(checkDateFrom) > new Date(checkDateTo)) {
            $("#divFilterShippedToValidateFuture").html('<span class="required-label">Start date cannot be in ahead of end date... Please reselect dates.</span>')
            return false;
        } else {
            $("#divFilterShippedToValidateFuture").html('')

        }
    }    
    queryUrl = `${url}&dateFrom=${apiFormatFormData.dateFrom}&dateTo=${apiFormatFormData.dateTo}&timeFrom=${apiFormatFormData.timeFrom}&timeTo=${apiFormatFormData.timeTo}`
    if (apiFormatFormData.sku) queryUrl += `&sku=${apiFormatFormData.sku}` 
    populateTable(queryUrl)
})