const url = `${urlApi}reports/serial_number_record?API-KEY=${key}`;
let apiTotalRequests = 0;

let tableSettings = null;
function populateTable(query) {
  $('#serialNumberRecord').show();
  $('#serialNumberRecordTitle').html(`Results for ${query}`)
  let searchURL = `${url}&query=${query}`
  if (tableSettings !== null) {
    tableSettings.destroy();
  }
  tableSettings = $('#serialNumberRecordTable').DataTable({
    dom: 'Bfrtip',
    pageLength: 10,
    ajax: searchURL,
    columns: [
      {
        data: "SKUId", render: function (data) {
          return valueModalLink(true, modalProduct, data);
        }
      },
      { data: "SerialNumber" },
      { data: "ShipmentId" },
      { data: "ShipmentStU" },

    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ]
  });
}

$('#serialNumberRecord').hide();

$('#serialNumberQuery').submit(function (e) {
  e.preventDefault()
  let searchQuery = $('#serialNumberSearch').val()  
  populateTable(searchQuery);
})

