
$(function() {

	const API_BULK_UPLOAD_TEMPLATE = urlApi + "/products/bulk_products_template?API-KEY=" + key; // GET
	const API_BULK_UPLOAD_ISSUES = urlApi + "/products/bulk_products_issues?API-KEY=" + key;
	const API_BULK_UPLOAD = urlApi + "/products/bulk_products_db";

	$(document).on('click', '#templateDownload', function() {
		window.location=API_BULK_UPLOAD_TEMPLATE
	});
	
	$(document).on('click', '#issueProductsDownload', function() {
		window.location=API_BULK_UPLOAD_ISSUES
	});
	
	$(document).on('click', '#bulkProductUpload', function() {
		var form = new FormData();
		if ($("#bulkFiles")[0].files.length === 0) {
			$("#fileValidate").html("Please attach your Bulk Upload File above before Uploading.")
			return false;
		}
		form.append("file", $("#bulkFiles")[0].files[0]);
		form.append("API-KEY", key);
		alertCustomCreating("Uploading bulk products. Please wait this may take a while");

		$.ajax({
    		type: 'POST',
    		url: API_BULK_UPLOAD,
    		data: form,
    		//dataType: "json",
    		cache: false,
    		contentType: false,
    		processData: false,
    		success: function(response) {
					console.log(response.errorMessage);
					successString = ""
					success = response.successMessage
					success.forEach(element => {
							successString = successString + element + "\n"
					});
					if (response.errors > 0) {
						let string = response.errorMessage.toLocaleString()
						alertCustomError(string);
					} else {
						alertCustomSuccess("POST", "Bulk Products File Uploaded Successfully\n" + "\n" + successString);
					}
    		},
    		error: function(x, h, r) {
				alertCustomError("Failed to upload bulk product file");
    		}
		});
	});
});