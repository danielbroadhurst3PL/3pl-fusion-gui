var apiTotalRequests = 4;

get_dgn_details(true);
get_dgn_types(true);
get_countries(true);
get_currency(true);


$("#" + formProductCreate).submit(function(e) {
	e.preventDefault();
	var dataProductCreateForm = $('#' + formProductCreate + ' :input');

	var apiFormatFormData = api_format_form_data(dataProductCreateForm);
	submit_products(apiFormatFormData);
});

function submit_products(apiData)
{
	alertCustomCreating("Creating Product " + apiData[sku]);
	var api3plProducts = urlApi + urlApiProducts;

	api(false, true, apiData[sku], api3plProducts, methodPost, apiData, function (createProduct){  
        $('#' + formProductCreate).trigger("reset");
	});
}

/**
 * Populates the sidebarinfomation area with info about the selected card.
 * Animates the switching of card info so sidebar can remain open.
 * @param {ID from the Form} formID 
 */
function formInformation(formID) {
	// Fade information out whilst we swtich information
	$('#infoText,#infoHeader').animate({opacity: 0},150, function () {
		// Re-write the Header and clear the text area
		$('#infoText').html("")
		$('#infoHeader').html("")
		infoTitle = formID.replace(/([A-Z])/g, ' $1').trim();
		$('#infoHeader').html(`${infoTitle}`)
		// Populate Data for the selected card
		switch (formID) {
			case "ProductInformation":
				$('#infoText').append(`<p><strong>Title:</strong><br />Title of the product.</p>`)
				$('#infoText').append(`<p><strong>SKU:</strong><br />The product code should be no longer than 45 characters and ideally should match the SKU code set up on your selling channels.</p>`)
				$('#infoText').append(`<p><strong>Description: (optional)</strong><br />This should represent markings that are already on the product, e.g. model or size. If the item is a set or a bundle, then the quantity in the set/bundle should be in the description. e.g. Energiser AA pack of 4.</p>`)
				$('#infoText').append(`<p><strong>Barcode:</strong><br />CODE128 or EAN formats are recognised by 3PL. Barcodes are used for scanning items during every step of the fulfilment process. This is the main contributor towards accuracy of locating items, for counting items and on despatch.</p>`)
				$('#infoText').append(`<p><strong>ASIN:</strong><br /> This is required if you would like to activate Seller Fulfilled Prime (SFP). 3PL has built an API with Amazon to facilitate SFP. You must apply for Seller Fulfilled Prime however 3PL meets all the requirements for achieving this on your behalf. SFP can take the cost out of sending consignment directly into Amazon whilst ensuring you still obtain the Buy Box.</p>`)
				$('#infoText').append(`<p><strong>Commodity Code:</strong><br />Are used by H.M. Revenue & Customs to classify goods to ensure the correct amounts of VAT and import duties are collected. Commodity codes are also required as part of new legislation related to the Fulfilment House Due Diligence Scheme (FHDDS). Commodity codes are freely available; a full list of commodity codes is available online.`)
				break;
			case "ProductDetails":
				$('#infoText').append(`<p><strong>DGN Classification:</strong><br />Is required on a transport document that gives details about the contents of a consignment to carriers, receiving authorities and forwarders. 3PL will store dangerous goods in limited quantities but not on a large scale. 3PL will use this information to ensure limited quantities like perfume or lithium batteries are shipped on the correct delivery service.</p><p>If the goods are shipped on the incorrect service for instance via air, generally the goods are destroyed, and the carrier will make contact to give the shipper a warning. It is the responsibility of the shipper/partner to ensure that the categorisation of dangerous goods is correct, as this can result in suspension of services or even litigation.</p>`)
				$('#infoText').append(`<p><strong>Country of Origin:</strong><br />Select the the Country of Origin for the product, this will be where the product is being manufactured.</p>`)
				$('#infoText').append(`<p><strong>Average Monthly Unit Sales:</strong><br />Estimate the average monthly unit sales of the product. This is used to help us store the product in the warehouse.</p>`)
				break;	
			case "WeightAndDimensions":
				$('#infoText').append(`<p><strong>Weights and Dimensions:</strong><br />Height, Width, Depth (in CMs) and weight (in kgs) –is required as this information is used for storing the item and during the packing process for choosing the most efficient delivery service.</p>`)
				break;	
			case "Currency":
				$('#infoText').append(`<p><strong>Currency:</strong><br />Select the standard currency which will be used for the product.</p>`)
				$('#infoText').append(`<p><strong>Value:</strong><br />Please enter the value of the product.</p>`)
				break;	
			case "Quantities":
				$('#infoText').append(`<p><strong>Inner & Master Cartons: (optional)</strong><br />If an item has got an inner box quantity or master carton quantity it must be populated in this field for the inner box / master carton pick tariff to work. If item has no inner box or master carton quantity or it is unknown, then zero should be put in these fields -a quantity of 1 is not permitted in these fields. If zero is put in these fields then the inner box /master carton pick tariff will not apply..</p>`)
				$('#infoText').append(`<p><strong>Pallet: (optional)</strong><br />If an item comes in a predefined pallet quantity then it should be populated in this field and the pallet pick tariff will apply. If the pallet quantity is set to zero, then the pallet pick tariff will not apply.</p>`)
				break;
			case "Extras":
				$('#infoText').append(`<p><strong>Expiry Date: (optional)</strong><br />Optional field for the Expiry Date of products which have an expiry.</p>`)
				$('#infoText').append(`<p><strong>Serial Number: (optional)</strong><br />If your require a serial number to be captured on despatch then this field should be set to yes. This is usually required on electronic devices that are related to health products.</p>`)
				$('#infoText').append(`<p><strong>Batch: (optional)</strong><br />If you require the batch to be captured on inbound then this field should be set to yes. This is usually for managing product revisions or modifications.</p>`)
					break;			
			default:
				break;
		}
		// Re-animate now the data opacity has been populated
		$('#infoText,#infoHeader').animate({opacity: 1},450, function () {});
	})
}
