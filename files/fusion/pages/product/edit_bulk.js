
$(function() {

	const API_BULK_UPLOAD_TEMPLATE = urlApi + "/products/bulk_products_template?API-KEY=" + key; // GET
	const API_BULK_UPLOAD = urlApi + "/products/edit_bulk_products_db";

	$(document).on('click', '#templateDownload', function() {
		window.location=API_BULK_UPLOAD_TEMPLATE
	});
	
	$(document).on('click', '#issueProductsDownload', function() {
		window.location=API_BULK_UPLOAD_ISSUES
	});
	
	$(document).on('click', '#editBulkProductUpload', function() {
		var form = new FormData();
		if ($("#editBulkFiles")[0].files.length === 0) {
			$("#fileValidate").html("Please attach your Edited Products File above before Uploading.")
			return false;
		}
		form.append("file", $("#editBulkFiles")[0].files[0]);
		form.append("API-KEY", key);
		alertCustomCreating("Editing products. Please wait this may take a while");

		$.ajax({
    		type: 'POST',
    		url: API_BULK_UPLOAD,
    		data: form,
    		//dataType: "json",
    		cache: false,
    		contentType: false,
    		processData: false,
    		success: function(response) {
				errorString = "";
				if (response.errorMessage.length > 0) {
					errorString = "\nErrors in File\n"
					errors = response.errorMessage
					errors.forEach(element => {
						errorString = errorString + element + "\n"
					});
				}
				successString = ""
                success = response.successMessage
                success.forEach(element => {
                    successString = successString + element + "\n"
                });
				alertCustomSuccess("POST", "Bulk Products File Uploaded Successfully\n" + errorString + "\n" + successString);
    		},
    		error: function(x, h, r) {
				alertCustomError("Failed to upload bulk product file");
    		}
		});
	});
});