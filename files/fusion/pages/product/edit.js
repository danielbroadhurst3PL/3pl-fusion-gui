var editPageSku = window.location.href;
editPageSku = editPageSku.split("/");
var apiTotalRequests = 5;

get_dgn_details(true);
get_dgn_types(true);
get_countries(true);
get_currency(true);
get_product(true, editPageSku[5], false, true, false);

function populate_edit_product_form(getProduct)
{
	console.log(getProduct);
	$( "#" + sku).val(getProduct.sku);
	document.getElementById(sku).disabled = true;
	$( "#" + title).val(getProduct.title);
	$( "#" + description).val(getProduct.description);
	$( "#" + barcode).val(getProduct.alternates ? getProduct.alternates.barcode.value : null);
	$( "#" + asin).val(getProduct.alternates ? getProduct.alternates.asin.value : null);
	$( "#" + dgnType).val(getProduct.dgn.type.id).change();
	$( "#" + dgnDetails).val(getProduct.dgn.details.id).change();
	$( "#" + countries).val(getProduct.country ? getProduct.country.id : 52).change();
	$( "#" + commodityCode).val(getProduct.commodityCode);
	$( "#" + height).val(getProduct.dimensions.height);
	$( "#" + width).val(getProduct.dimensions.width);
	$( "#" + depth).val(getProduct.dimensions.depth);
	$( "#" + weight).val(getProduct.weight.value);
	$( "#" + currency).val(getProduct.price? getProduct.price.currency.id : null).change();
	$( "#" + value).val(getProduct.price ? getProduct.price.value : 0);
	$( "#" + quantityInner).val(getProduct.quantity ? getProduct.quantity.inner : null);
	$( "#" + quantityMasterCarton).val(getProduct.quantity ? getProduct.quantity.masterCarton : null);
	$( "#" + quantityPallet).val(getProduct.quantity ? getProduct.quantity.pallet : null);
	$("#" + dateExpiry).prop("checked", (getProduct.date.expiry));
	$("#" + serial).prop("checked", (getProduct.capture.serial));
	$("#" + batch).prop("checked", (getProduct.capture.batch));
	$("#averageMonthlyUnitSales").val(getProduct.averageMonthlyUnitSales ? getProduct.averageMonthlyUnitSales : 0);
}

$("#" + formProductEdit).submit(function(e) {
	e.preventDefault();
	var dataProductEditForm = $('#' + formProductEdit + ' :input');

	var apiFormatFormData = api_format_form_data(dataProductEditForm);
	edit_product(apiFormatFormData);
});

function edit_product(apiData)
{
	alertCustomEditing("Editing Product " + apiData[sku]);
	var api3plProducts = urlApi + urlApiProducts;

	api(false, true, apiData[sku], api3plProducts, methodPut, apiData, function (editProduct){});
}
