/* var h = window.innerHeight;
$('body').css('max-height', h) */

$(function () {
    $("#formLogin").validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.input-group').append(error);
        },
        submitHandler: function (event) {
            var formId = $(event).attr("id");
            var formButtonId = formId.replace("form", "button");

            var inputUser = $("#user").val();
            var inputPassword = $("#password").val();
            var inputRememberMe = $("#rememberMe").val();

            login(formButtonId, inputUser, inputPassword, inputRememberMe);
        }
    });
});

function login(buttonId, user, password, rememberMe) {
    startButtonLoading(buttonId);

    $.ajax({
        url: baseUrl + urlSuffixLogin_Login,
        type: "POST",
        dataType: "json",
        data: {"user": user, "password": password, "rememberMe": rememberMe},
        success: function (data) {
            if (data["0"] === true) {
                window.location = (baseUrl + urlSuffixMain);
            }
            else {
                stopButtonLoading(buttonId);
                showNotificationError(data["1"]);
            }

        },
        error: function (request, status, error) {
            showNotificationError("Error #Login1 - Contact IT");
            stopButtonLoading(buttonId);
        }
    });
}

if (errorMessage) {
    alertCustomError(errorMessage);
}
