const noteUrl = `${urlApi}orders/add_note`;

$('#createOrderNote').submit(function (e) {
    e.preventDefault()
    var form = new FormData(); 
    if ($("#note_image")[0].files.length === 0) {
        $("#fileValidate").html("Please attach your Bulk Upload File above before Uploading.")
        return false;
    }    
    form.append("orderNumber", $('#orderNumber').val())
    form.append("note_body", $('#note_body').val())
    form.append("note_image", $("#note_image")[0].files[0]);
    form.append("API-KEY", key);
    alertCustomCreating("Adding note to Order Number");
    $.ajax({
        type: 'POST',
        url: noteUrl,
        data: form,
        //dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {					
            console.log(response);
            errorString = "";
            if (response.errors > 0) {
                errorString = "\nErrors in File\n"
                errors = response.errorMessage
                errors.forEach(element => {
                    errorString = errorString + element + "\n"
                });
                alertCustomError("\n" + errorString);
            } else {
                successString = ""
                success = response.successMessage
                success.forEach(element => {
                        successString = successString + element + "\n"
                });
                alertCustomSuccess("POST", "\n" + successString);
            }
        },
        error: function(x, h, r) {
            alertCustomError("Failed to upload note.");
        }
    });
})