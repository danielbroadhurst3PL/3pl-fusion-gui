let createNewsUrl = `${urlApi}news`;
let apiTotalRequests = 0;

tinymce.init({
  height: 300,
  selector: '#newsArticle',
  plugins: [
    'advlist autolink link image lists charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking',
    'table emoticons template paste help'
  ],
  toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons',
});

$('#formCreateNews').submit(function (e) {
  e.preventDefault();
  alertCustomLoading()
  let subject = $('#subject').val();
  let body = tinyMCE.get('newsArticle').getContent();
  let notify = $('#notify input:radio:checked').attr('id') ? $('#notify input:radio:checked').attr('id').split('_')[1] : null;
  let sendToContacts = $('#sendToContacts input:radio:checked').attr('id') ? $('#sendToContacts input:radio:checked').attr('id').split('_')[1] : null;
  let apiData = {
    'API-KEY': key,
    subject,
    body,
    notify,
    sendToContacts
  }
  api(false, true, "", createNewsUrl, methodPost, apiData, function (newsResponse) {
    apiTotalRequests += 1;
    displayNews(newsResponse.data);
  })
})

function displayNews(article) {
  closeAllSwalWindows()
  $('#newsBody').html('');
  $('#newsInfo').html('');
  $('#newsInfo').append(`<h2 class="modal-title">Subject: ${article[0].subject}</h2>`);
  $('#newsBody').append(`<div><p><strong>Notified: </strong><span id="newsNotify">${article[0].notify}</span></p></div>`);
  $('#newsBody').append(`<div><p><strong>Sent To: </strong><span id="newsSentTo">${article[0].send_to}</span></p></div>`);
  $('#newsBody').append(`<strong>Body:</strong> <br />${article[0].body.replace(/↵/g, '')}`);
  $('#newsModal').modal().show();
};