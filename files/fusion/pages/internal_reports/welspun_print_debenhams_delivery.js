$('#debenhamsDeliverNote').submit(function (e) {
    e.preventDefault();
    alertCustomLoading();
    var debenhamsDeliverNoteForm = $('#debenhamsDeliverNote' + ' :input');
    var debenhamsOrderNumber = debenhamsDeliverNoteForm[0].value;
    $.ajax({
        url: `${urlApi}orders/delivery_note_ftp?${apiKey}=${key}&orderNumber=${debenhamsOrderNumber}`,
        method: 'GET',
        success: function (data) {
            let response = data.data.docs[0];
            createPdf(response.data, debenhamsOrderNumber)
        },
        error: function (data) {

        }
    })
})

// The Base64 string of a simple PDF file
function createPdf(data, orderNumber) {
  var b64 = data;
  // Embed the PDF into the HTML page and show it to the user
  var obj = document.createElement('object');
  obj.style.width = '100%';
  obj.style.height = '842pt';
  obj.type = 'application/pdf';
  obj.data = "data:application/pdf;base64," + b64;
  $('.content').append(obj);
  // Insert a link that allows the user to download the PDF file
  var link = document.createElement('a');
  link.setAttribute('class', 'btn btn-success')
  link.innerHTML = 'Download PDF file';
  link.download = `${orderNumber}.pdf`;
  link.href = 'data:application/octet-stream;base64,' + b64;
  $('#debenhamsDeliverNote').append(link)
  closeAllSwalWindows();
}

