let apiTotalRequests = 0;

$('#pickpotReportWarehouseForm').submit(function (e) {
  alertCustomLoading();
  e.preventDefault();
  let warehouse = $('#warehouse').val();
  let pickpotUrl = `${urlApi}internal_reports/pickpot?${apiKey}=${key}&warehouse=${warehouse}`;
  populateTable(pickpotUrl);
})

let tableSettings = null;
function populateTable(url) {
  $('#pickpotReport').show()
  if (tableSettings !== null) {
    tableSettings.destroy();
  }
  tableSettings = $('#pickpotReportTable').DataTable({
    dom: 'Bfrtip',
    pageLength: 10,
    ajax: url,
    "order": [[3, "desc"]],
    columns: [
      { data: "ReleasedDate" },
      { data: "DateCreated" },
      { data: "OrderNo" },
      { data: "OrderStage" },
      { data: "OrderStatus" },
      { data: "RecipientName" },
      { data: "Client" },
      { data: "ShippingMethod" },
      { data: "OrderClass" },
      { data: "DelNote" },
      { data: "WFNotes" },
      { data: "Lines" },
      { data: "PCS" },
      { data: "CusRef" },
      { data: "CusRef2" },
    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ],
    "initComplete": function( settings, json ) {
      closeAllSwalWindows()
    }
  });
}

$('#pickpotReport').hide()
