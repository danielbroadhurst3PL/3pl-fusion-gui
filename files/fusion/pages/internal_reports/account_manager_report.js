apiTotalRequests = 0;
function get_account_manager_report(apiShowAlert, apiData) {
    tableAccountManagerSettings.clear()
    var api3plAccountManager = urlApi + 'welcome/account_manager_report?';

    if (!apiData) {
        api3plAccountManager += `${apiKey}=${key}`
    } else {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#accountManagerReport").offset().top - 100
        }, 2000);
    }

    $.each(apiData, function (key, val) {        
        if (val !== "") {
            if (key === 'dateShippedTo') {
                val = val;
            }

            if (key === 'dateShippedFrom') {
                val = val;
            }
            api3plAccountManager += key + '=' + val + '&';
        }
    });
    
    var apiData = "";
    
    api(false, apiShowAlert, "", api3plAccountManager, methodGet, apiData, function (getAccountManager) {
        
        accountManagerArray = getAccountManager.data;
        
        if (getAccountManager.results.total > 0) {
            $.each(getAccountManager.data, function (key, val) {
                if (val.DateClosed) {
                    dateClosedShortened = val.DateClosed.substring(0, val.DateClosed.length - 4);
                } else {
                    dateClosedShortened = "";
                }
                tableAccountManagerSettings.row.add([
                    val.ShipmentId,
                    val.Name,
                    val.PostCode,
                    val.DelCompany,
                    val.OrderClass,
                    val.CustomerRef,
                    val.Lines,
                    val.Pieces,
                    val.Boxes,
                    val.StageReadable,
                    val.ShippingMethod,
                    val.CarrierId,
                    val.CarrierTrackingNumber,
                    val["Special Instructions"],
                    dateCreated = val.DateCreated.substring(0, val.DateCreated.length - 4),
                    dateDueOut = val.DateDueOut.split(" ")[0],
                    dateClosedShortened
                ]);     
            })           
        } else {
            tableAccountManagerSettings.row.add([
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                ""
            ]);
        }
        tableAccountManagerSettings.draw()
        apiTotalRequests += 1;
    })
}

get_account_manager_report(true, )

$('#accountManagerForm').submit(function (e) {
    e.preventDefault();
    var dataCreateCreateForm = $('#accountManagerForm' + ' :input');

    var apiFormatFormData = api_format_form_data(dataCreateCreateForm);

    if (apiFormatFormData.dateShippedFrom != null && apiFormatFormData.dateShippedTo != null) {
        checkDateFrom = api_date(apiFormatFormData.dateShippedFrom);
        checkDateTo = api_date(apiFormatFormData.dateShippedTo);
        if (new Date(checkDateFrom) > new Date(checkDateTo)) {
            $("#divFilterShippedToValidateFuture").html('Date Shipped From cannot be in future... Please reselect dates.')
            return false;
        } else {
            $("#divFilterShippedToValidateFuture").html('')

        }
    }
    
    get_account_manager_report(true, apiFormatFormData)
})