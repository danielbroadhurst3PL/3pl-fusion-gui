const url = `${urlApi}warehouse_dashboard/consolidation_report?API-KEY=${key}`;
let consolidationTable = null;

function populateTable(url) {
    if (consolidationTable !== null) {
        consolidationTable.destroy();
    }
    consolidationTable = $('#consolidationReportTable').DataTable({
        dom: 'Bfrtip',
        pageLength: 50,
        ajax: url,
        "order": [[ 0, "desc" ]],
        columns: [
                { data: "NoOfLocations" },
                { data: "SKUId", render: function (data) {
                    return valueModalLink(true, modalProduct, data);
                }},
                { data: "Text" },
                { data: "TotalPieces" },
                { data: "Slot" },
                { data: "LocPieces" },
        ],
        buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
        ]
    });
}

populateTable(url);

const companyAPI = `${urlApi}companies?${apiKey}=${key}`;
let companiesArray = [];
$.ajax({
    url: companyAPI,
    success: function (response) {
        let companies = response.data;
        companies.forEach(company => {
            if (company.settings.active === true) {
                companiesArray.push(company)
            }
        });
        sortedArray = multiSort(companiesArray, {name: 'asc'});
        sortedArray.forEach(company => {            
            $('#companies3PL').append(`<option value="${company.name}" id="${company.id}">`)
        });
    }
})

$('#consolidationReportForm').submit(function (e) {
    e.preventDefault();
    var dataReturnsForm = $('#consolidationReportForm' + ' :input');   
    var formattedData = api_format_form_data(dataReturnsForm)
    let tempUrl = url;
    if (formattedData.companyList) {
        let companyCodeQuery = companiesArray.find(company => company.name === formattedData.companyList).code
        tempUrl += `&companyCode=${companyCodeQuery}`
    }
    populateTable(tempUrl);
    apiTotalRequests += 1;
})