let expectedReceivedTable = null;

$('#grnExpectedReceived').submit(function (e) {
    e.preventDefault();
    var grnExpectedReceivedForm = $('#grnExpectedReceived' + ' :input');
    var grnRef = grnExpectedReceivedForm[0].value;
    if (expectedReceivedTable !== null) {
        expectedReceivedTable.destroy();
    }
    if (grnRef != null) {
        let url = `https://api.3p-logistics.co.uk/welcome/expected_vs_received?${apiKey}=${key}&grnRef=${grnRef}`
        expectedReceivedTable = $('#grnExpectedReceivedTable').DataTable({
            dom: 'Bfrtip',
            pageLength: 50,
            ajax: url,
            columns: [
                {data: "ProdCode", render: function (data) {
                    return `<a href="JavaScript:void(0);" onclick="itemStuCheck('${grnRef}','${data}')">${data}</a>`
                }},
                {data: "Text"},
                {data: "Description"},
                {data: "EXPQTY"},
                {data: "RECIQTY"},
                {data: "VARIANCE", render: function (data) {
                    if (data != 0) {
                        return `${data}`;
                    }
                    return data;
                }}
            ],
            buttons: [
                'copy',
                'csv',
                'excelHtml5',
                'pdfHtml5',
                'print'
            ],
            "createdRow": function( row, data, dataIndex ) {                                
                if ( data["VARIANCE"] != 0 ) {
                    $(row).addClass( 'highlighted' );
                    let productCode = data["ProdCode"];
                    itemStuCheck(grnRef, productCode)
                }
              }
        })
    }
    $('#grnExpectedReceivedCard').show()
    $('#expectedReceivedTitle').html(grnRef)
})

let itemStuCheckTable = null;
function itemStuCheck(grnRef, productCode) {
    if (itemStuCheckTable !== null) {
        let addDataUrl = `${urlApi}welcome/item_stu_check?${apiKey}=${key}&grnRef=${grnRef}&productCode=${productCode}`
        $.ajax({
            url:addDataUrl,
            success: function (response) {
                let data = response.data;
                data.forEach(element => {
                    itemStuCheckTable.row.add({
                        "ConsignmentId": element.ConsignmentId,
                        "SKUId": element.SKUId,
                        "Description": element.Description,
                        "Text": element.Text,
                        "StU": element.StU,
                        "Qty": element.Qty,
                        "Status": element.Status,
                        "Stage": element.Stage
                    }).draw();
                });
            }
        })
        return;
    }
    let urlStu = `${urlApi}welcome/item_stu_check?${apiKey}=${key}&grnRef=${grnRef}&productCode=${productCode}`
    itemStuCheckTable = $('#grnItemStuCheckTable').DataTable({
        dom: 'Bfrtip',
        pageLength: 50,
        ajax: urlStu,
        columns: [
            {data: "ConsignmentId"},
            {data: "SKUId"},
            {data: "Description"},
            {data: "Text"},
            {data: "StU"},
            {data: "Qty"},
            {data: "Status"},
            {data: "Stage"}
        ],
        buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
        ]
    })
    $('#grnItemStuCheckCard').show()
    $('#itemStuCheckTitle').html(`${grnRef} - Variances`)
    $('#grnItemStuCheckTable_wrapper').find('.dt-buttons').append(`<button class="dt-button buttons-excel buttons-html5" tabindex="0" aria-controls="grnItemStuCheckTable" onclick="cleartable()" type="button"><span>Clear</span></button>`)
}

function cleartable() {
    itemStuCheckTable.clear().draw();
}