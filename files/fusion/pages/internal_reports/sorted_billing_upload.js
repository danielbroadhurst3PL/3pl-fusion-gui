const SORTED_BILLING_UPLOAD = `https://api.3p-logistics.co.uk/sorted_billing`

$('#sortedBillingUpload').submit(function (e) {
    e.preventDefault();
    var form = new FormData(); 
    if ($("#sortedFileUpload")[0].files.length === 0) {
        $("#fileValidate").html("Please choose your file above before Uploading.")
        return false;
    }
    form.append("file", $("#sortedFileUpload")[0].files[0]);
    form.append("API-KEY", '7467a781-abf0-4c01-8d7e-682dc1bb776e');

    $.ajax({
        type: 'POST',
        url: SORTED_BILLING_UPLOAD,
        data: form,
        //dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
            console.log(response);
            populateTable(SORTED_BILLING_UPLOAD)
        },
        error: function(x, h, r) {
            console.log(x,h,r)
        }
    });
    console.log(form);
})

$('#sortedBillingGetData').submit(function (e) {
    e.preventDefault();
    url = `${SORTED_BILLING_UPLOAD}?API-KEY=7467a781-abf0-4c01-8d7e-682dc1bb776e`;
    let formData = $('#sortedBillingGetData' + " :input")
    let formArray = api_format_form_data(formData)
    formArray['companyCode'] = companyCode;
    for (const key in formArray) {
        if (formArray.hasOwnProperty(key)) {
            const value = formArray[key];
            url += `&${key}=${value}`
        }
    }
    populateTable(url)
})
//$.fn.dataTable.ext.errMode = 'throw';
let sortedUploadTableSettings = null;
function populateTable(url) {
    if (sortedUploadTableSettings !== null) {
        sortedUploadTableSettings.destroy();
    }
    console.log(url);
    
    sortedUploadTableSettings = $('#sortedUploadTable').DataTable({
        dom: 'Bfrtip',
        pageLength: 10,
        ajax: {
            "url": url,
            "statusCode": {
                401: function (xhr, error, thrown) {
                    console.log(error)
                    return false
                }
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        },
        columns: [
            { data: "company_code" },
            { data: "electioReference" },
            { data: "orderReference" },
            { data: "state" },
            { data: "carrier_name" },
            { data: "service" },
            { data: "dateShipped" },
            { data: "totalCost", render: function (data) {
                if (data != null) {
                    return parseFloat(data).toFixed(2)
                } else {
                    return 0
                }
            } },
            { data: "numberOfPackages" },
            { data: "customerName" },
            { data: "addressLine1" },
            { data: "postcode" },
            { data: "country" },
            { data: "tracking" }
        ],
        buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
        ],
    });
}

const companyAPI = `https://api.3p-logistics.co.uk/companies?API-KEY=7467a781-abf0-4c01-8d7e-682dc1bb776e`;
let companiesArray = [];
$.ajax({
    url: companyAPI,
    success: function (response) {
        let companies = response.data;
        companies.forEach(company => {
            if (company.settings.active === true) {
                companiesArray.push(company)
            }
        });
        sortedArray = multiSort(companiesArray, {name: 'asc'});
        sortedArray.forEach(company => {            
            $('#companies3PL').append(`<option value="${company.name}" id="${company.id}">`)
        });
    }
})

/**
 * Sorts an array of objects by column/property.
 * @param {Array} array - The array of objects.
 * @param {object} sortObject - The object that contains the sort order keys with directions (asc/desc). e.g. { age: 'desc', name: 'asc' }
 * @returns {Array} The sorted array.
 */
function multiSort(array, sortObject = {}) {
    const sortKeys = Object.keys(sortObject);
    // Return array if no sort object is supplied.
    if (!sortKeys.length) {
        return array;
    }
    // Change the values of the sortObject keys to -1, 0, or 1.
    for (let key in sortObject) {
        sortObject[key] = sortObject[key] === 'desc' || sortObject[key] === -1 ? -1 : (sortObject[key] === 'skip' || sortObject[key] === 0 ? 0 : 1);
    }
    const keySort = (a, b, direction) => {
        direction = direction !== null ? direction : 1;
        if (a === b) { // If the values are the same, do not switch positions.
            return 0;
        }
        // If b > a, multiply by -1 to get the reverse direction.
        return a > b ? direction : -1 * direction;
    };
    return array.sort((a, b) => {
        let sorted = 0;
        let index = 0;
        // Loop until sorted (-1 or 1) or until the sort keys have been processed.
        while (sorted === 0 && index < sortKeys.length) {
            const key = sortKeys[index];
            if (key) {
                const direction = sortObject[key];

                sorted = keySort(a[key], b[key], direction);
                index++;
            }
        }
        return sorted;
    });
}

function api_format_form_data(dataFormPostPut) {    
    var objTempData = {};
    $.each(dataFormPostPut, function (dataKey, val) {
        if (val.localName == 'input') {
            if (val.id == 'id_service' && val.value != null) {
                objTempData['id'] = val.value
            }
            if (val.id == 'companyList') {
                result = companiesArray.find( ({name}) => name === val.value);
                objTempData['companyCode'] = result.code;
                return               
            }
            if (val.id == 'dateShippedFrom' || val.id == 'dateShippedTo') {                
                if (val.value != "" && val.value != null) {
                    console.log(val.value);
                    objTempData[val.id] = api_date(val.value);
                } else {
                    return;
                }
            }
            objTempData[val.id] = val.value;
        }
    });
    return objTempData;
}