let apiTotalRequests = 0;

const url = `${urlApi}reports/short_shipped_report?API-KEY=${key}`;
let shippedShortTable = null;

function populateTable(url) {
    if (shippedShortTable !== null) {
        shippedShortTable.destroy();
    } 
  shippedShortTable = $('#shippedShortTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      ajax: url,
      "order": [[ 0, "desc" ]],
      columns: [
            { data: "DateClosed" },
            {data: "ShipmentId", render: function (data) {
                if (data) {
                    return valueModalLink(true, modalOrder, data);
                }
                return 'N/A';
            }},
            {data: "SKUId", render: function (data) {
                return valueModalLink(true, modalProduct, data);
            }},
            { data: "Text" },
            { data: "QtyOrdered" },
            { data: "QtyShipped" },
            { data: "ShippedShort" },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ]
  });
}

populateTable(url);

$('#shippedShortForm').submit(function (e) {
    e.preventDefault()
    var dataForm = $('#shippedShortForm' + ' :input');
    var apiFormatFormData = api_format_form_data(dataForm);
    populateTable(`${url}&dateShippedFrom=${apiFormatFormData.dateShippedFrom}&dateShippedTo=${apiFormatFormData.dateShippedTo}`)
})