$('#cubiscanFinder').submit(function (e) {
    e.preventDefault();
    var cubiscanForm = $('#cubiscanFinder' + ' :input');
    var cubiscanSearchTerm = cubiscanForm[0].value;
    $.ajax({
        url: `${urlApi}cubiscan_serial?${apiKey}=${key}&SerialBarcode=${cubiscanSearchTerm}`,
        method: 'GET',
        success: function (data) {
            let response = data.data[0];
            $('input#SerialBarcode').val(`${response.SerialBarcode}`)
            $('input#SerialLength').val(`${response.SerialLength}`)
            $('input#SerialHeight').val(`${response.SerialHeight}`)
            $('input#SerialWidth').val(`${response.SerialWidth}`)
            $('input#SerialWeight').val(`${response.SerialWeight}`)
            $('#editCubiscanRecord').show();
            $('#deleteCubiscanRecord').show();
            $('#createCubiscanRecord').hide();
            $('#apiResponseBlock').show();
            $('#apiResponse').append(`<h2>${$('input#searchQuery').val()} loaded.</h2>`).show();
        },
        error: function (data) {
            $('#apiResponseBlock').show();
            $('#apiResponse').empty();
            $('#apiResponse').append(`<h2>${$('input#searchQuery').val()} not found.</h2>`).show();
        }
    })
})

$('#cubiscanFinderEdit').submit(function (e) {
    e.preventDefault();
})

function createRecord() {    
    var cubiscanForm = $('#cubiscanFinderEdit' + " :input");
    let formData = api_format_form_data(cubiscanForm);
    formData['API-KEY'] = key;
    $.ajax({
        url: `${urlApi}cubiscan_serial`,
        method: 'POST',
        data: formData,
        success: function (data) {
            if (data.success == 1) {
                $('input#searchQuery').val(`${formData['SerialBarcode']}`)
                $('form#cubiscanFinderEdit').trigger("reset");
                $('form#cubiscanFinder').trigger("submit");
                $('#apiResponse').empty();
                $('#apiResponse').append(`<h2>${formData['SerialBarcode']} created successfully.</h2>`).show();
            }
        }
    })
}

function updateRecord() {    
    var cubiscanForm = $('#cubiscanFinderEdit' + " :input");
    let formData = api_format_form_data(cubiscanForm);
    formData['API-KEY'] = key;
    $.ajax({
        url: `${urlApi}cubiscan_serial`,
        method: 'PUT',
        data: formData,
        success: function (data) {
            if (data.success == 1) {
                $('form#cubiscanFinderEdit').trigger("reset");
                $('form#cubiscanFinder').trigger("submit");
                $('#apiResponse').empty();
                $('#apiResponse').append(`<h2>${formData['SerialBarcode']} updated successfully.</h2>`).show();
            }
        }
    })
}

function deleteCubiscanRecord() {
    var cubiscanForm = $('#cubiscanFinderEdit' + " :input");
    let formData = api_format_form_data(cubiscanForm);
    formData['API-KEY'] = key;
    $.ajax({
        url: `${urlApi}cubiscan_serial`,
        method: 'DELETE',
        data: formData,
        success: function (data) {
            if (data.success == 1) {
                swal("Deleted!", "Cubiscan Record has been deleted.", "success");
                resetBothForms();
                $('#apiResponse').empty();
                $('#apiResponse').append(`<h2>${formData['SerialBarcode']} Deleted successfully.</h2>`).show();
            }
        }
    })
}

function deleteRecord() {
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this Cubiscan Serial!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            alertCustomLoading()
            deleteCubiscanRecord()
        } else {
            swal("Cancelled", "The Cubiscan Serial will not be deleted :)", "error");
        }
    });
}

function resetBothForms() {
    $('form#cubiscanFinder').trigger("reset");
    $('form#cubiscanFinderEdit').trigger("reset");
    $('#createCubiscanRecord').show();
    $('#editCubiscanRecord').hide();
    $('#deleteCubiscanRecord').hide();
    $('#apiResponse').empty();
}

function api_format_form_data(dataFormPostPut) {    
    var objTempData = {};
    $.each(dataFormPostPut, function (dataKey, val) {
        if (val.localName == 'input') {
            objTempData[val.id] = val.value;
        }
    });
    return objTempData;
}

// $('#createCubiscanRecord').hide();
$('#editCubiscanRecord').hide();
$('#deleteCubiscanRecord').hide();

$('#SerialBarcode').keyup( function (e) {
    $('#createCubiscanRecord').show();
})