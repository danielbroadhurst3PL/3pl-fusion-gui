const url = `https://api.3p-logistics.co.uk/warehouse_dashboard/required_replen_record?API-KEY=7467a781-abf0-4c01-8d7e-682dc1bb776e`;

let requiredReplenTable = null;
function populateTable(url) {
    requiredReplenTable = $('#requiredReplenTable').DataTable({
        dom: 'Bfrtip',
        pageLength: 10,
        ajax: url,
        columns: [
            { data: "SKUId" },
            { data: "BoxQty" },
            { data: "TITLE" },
            { data: "DESCRIPTION" },
            { data: "Area" },
            { data: "Status" },
            { data: "MinQty" },
            { data: "MaxQty" },
            { data: "ReplenAction" },
            { data: "Qty" },
            { data: "QtyDueIn" }
        ],
        buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
        ]
    });
}

populateTable(url);