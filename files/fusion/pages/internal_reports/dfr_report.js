const zendeskURL = "https://3plogistics.zendesk.com/api/v2/";
const zendeskSearch = "search.json?query=";
const zendeskAuth = "Basic cnlhbndhbGtlckAzcC1sb2dpc3RpY3MuY28udWsvdG9rZW46RGFtN1lVMlBDanFQOFF3NmZrVjZZVVNWSXpOUDRvQUw0ejB4eUNRSA==";
let weekStart = new Date();
weekStart.setDate((weekStart.getDate() - (weekStart.getDay() + 6) % 7) - 7);
let weekEnd = new Date(weekStart)
weekEnd.setDate(weekEnd.getDate() + 6);
let zendeskHeaders = {
  "method": "GET",
  "headers": {
    authorization: zendeskAuth,
    
  }
}

let ticketsOwnership3PL = [];
let ticketsOwnershipCourier = [];
let oldestTicket = [];
let zendeskTicketMetrics = [];
let openTickets = [];
let ticketCategories = {};
let colours = [];

function zendeskOwnderShip3PL(url) {
  zendeskHeaders.url = url;
  $.ajax(zendeskHeaders).done(function (response) {
    ticketsOwnership3PL = response;
    $('#ticketCount3PL').append(`<p>Tickets with 3PL Ownership: ${response.count}</p>`)
  });
}

function zendeskOwnderShipCourier(url) {
  zendeskHeaders.url = url;
  $.ajax(zendeskHeaders).done(function (response) {
    ticketsOwnershipCourier = response;
    $('#ticketCountCourier').append(`<p>Tickets with Courier Ownership: ${response.count}</p>`)
  });
}

function zendeskOldestOpenTicket(url) {
  zendeskHeaders.url = url;
  $.ajax(zendeskHeaders).done(function (response) {
    oldestTicket = response.results[0];
    $('#oldestTicket').html(`
    <p>Oldest Ticket: <a href="${oldestTicket.url}" _target="blank">${oldestTicket.id}</a></p>
    <p>Subject: ${oldestTicket.subject}</p>
    <p>Created: ${oldestTicket.created_at.split('T')[0]} ${oldestTicket.created_at.split('T')[1].substring(0, 8)}</p>
    <p>Last Updated: ${oldestTicket.updated_at.split('T')[0]} ${oldestTicket.updated_at.split('T')[1].substring(0, 8)}</p>`)
  });
}

function zendeskAverageResponseTime(url) {
  zendeskHeaders.url = url;
  $.ajax(zendeskHeaders).done(function (response) {
    zendeskTicketMetrics.push(...response.ticket_metrics);
    if (response.next_page && zendeskTicketMetrics.length < 500) {
      zendeskAverageResponseTime(response.next_page);
    }
    if (zendeskTicketMetrics.length === 500) {
      let responseTimes = [];
      let fullResolutionTimes = [];
      let shortestResponseTime = 0
      let longestResponseTime = 0;
      let averageResponseTime = 0;
      let shortestFullResolutuonTime = 0; 
      let longestFullResolutuonTime = 0; 
      let averageFullResolutuonTime = 0;
      zendeskTicketMetrics.forEach(metric => {
        if (metric.reply_time_in_minutes.business) {
          responseTimes.push(metric.reply_time_in_minutes.business);
        }
        if (metric.full_resolution_time_in_minutes.business) {
          fullResolutionTimes.push(metric.full_resolution_time_in_minutes.business);
        }
      });
      shortestResponseTime = Math.min(...responseTimes);
      longestResponseTime = Math.max(...responseTimes);
      averageResponseTime = responseTimes.reduce((a, b) => a + b) / responseTimes.length;
      shortestFullResolutuonTime = Math.min(...fullResolutionTimes);
      longestFullResolutuonTime = Math.max(...fullResolutionTimes);
      averageFullResolutuonTime = fullResolutionTimes.reduce((a, b) => a + b) / fullResolutionTimes.length;
      $('#averageResponseTime').html(`
      <p>Shortest Response time:<br />${convertTime(shortestResponseTime.toFixed(0))} minutes (Business)</p>
      <p>Longest Response time:<br />${convertTime(longestResponseTime.toFixed(0))} minutes (Business)</p>
      <p>Average Response time:<br />${convertTime(averageResponseTime.toFixed(0))} minutes (Business)</p>
      <small>These results are from the last 500 tickets</small>`);
      $('#averageFullResolutionTime').html(`
      <p>Shortest Full Resolution time:<br />${convertTime(shortestFullResolutuonTime.toFixed(0))} minutes (Business)</p>
      <p>Longest Full Resolution time:<br />${convertTime(longestFullResolutuonTime.toFixed(0))} minutes (Business)</p>
      <p>Average Full Resolution time:<br />${convertTime(averageFullResolutuonTime.toFixed(0))} minutes (Business)</p>
      <small>These results are from the last 500 tickets</small>`);
    }
  });
}

function zendeskOpenTickets(url) {
  zendeskHeaders.url = url;
  $.ajax(zendeskHeaders).done(function (response) {
    openTickets.push(...response.results);
    if (response.next_page) {
      zendeskOpenTickets(response.next_page)
    } else {
      openTickets.forEach(ticket => {
        let category = ticket.custom_fields.find(field => field.id == 22502176);
        if (category) {
          let val = category.value;
          if (ticketCategories.hasOwnProperty(val)) {
            ticketCategories[val] += 1;
          } else {
            ticketCategories[val] = 1;
            var generateColour = function() {
              var r = Math.floor(Math.random() * 255);
              var g = Math.floor(Math.random() * 255);
              var b = Math.floor(Math.random() * 255);
              return "rgb(" + r + "," + g + "," + b + ")";
           };
           colours.push(generateColour())
          }
        }
      });
      let labels = Object.keys(ticketCategories);
      let data = Object.values(ticketCategories);
      drawCategoryChart(labels, data, colours);
    }
  });
}


/**
 * Populates Stock Breakdown Datatable
 * 
 * @param {Boolean} apiShowAlert - Displays alert modal
 * @param {Boolean} valueModal - Adds a link to value to open Modal
 */
let tableShortShippedReport = null;
function drawCategoryChart(labels, data, colours) {
  var ctx = document.getElementById('myChart').getContext('2d');
  var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
      labels: labels,
      datasets: [{
        label: '# of Votes',
        data: data,
        backgroundColor: colours,
        borderWidth: 1
      }]
    },
    options: {
      legend: false
    }
  });
}

function get_short_shipped_report() {
  let totalOrders = 0;
  let reportsHeaders = {
    method: 'GET',
    url: `https://reports.3p-logistics.co.uk/api/v1/warehouse/reports/short-shipped-historic?dateFrom=${formatJSDate(weekStart)}&dateTo=${formatJSDate(weekEnd)}`,
    headers: {
      Authorization: "Basic d2FyZWhvdXNlQWRtaW46M1BMJldIRChBUEkp"
    },
    "timeout": 0
  }
  $.ajax(reportsHeaders).done(function (response) {
    let shortShippedData = response.data;
    if (tableShortShippedReport !== null) {
      tableShortShippedReport.destroy();
    }
    tableShortShippedReport = $('#shortShippedTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 10,
      data: shortShippedData,
      responsive: true,
      columns: [
        { data: "BizId" },
        { data: "Warehouse" },
        { data: "ORDERS" },
        { data: "LNS ORDERED" },
        { data: "PCS ORDERED" },
        { data: "PCS SHIPPED" },
        { data: "ORDERS SHIPPED SHORT" },
        { data: "PCS SHIPPED SHORT" },
        { data: "LNS SHIPPED SHORT" },
        { data: "ORD SUCCESS" },
        { data: "LNS SUCCESS" },
        { data: "PCS SUCCESS" },
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ],
    });
    shortShippedData.forEach(row => {
      totalOrders += row.ORDERS;
    });
    $('#ticketCount3PL').append(`<p>Number of Orders Shipped: ${totalOrders}</p>`)
    $('#ticketCountCourier').append(`<p>Number of Orders Shipped: ${totalOrders}</p>`)
    return
  })
}


zendeskOwnderShip3PL(`${zendeskURL}${zendeskSearch}tags:ownership_3pl*+created>${formatJSDate(weekStart)}+created<${formatJSDate(weekEnd)}`);
zendeskOwnderShipCourier(`${zendeskURL}${zendeskSearch}tags:ownership_courier*+created>${formatJSDate(weekStart)}+created<${formatJSDate(weekEnd)}`);
zendeskOldestOpenTicket(`${zendeskURL}${zendeskSearch}-tags:ownership_courier*+order_by:created+sort:asc+type:ticket+status:open`);
zendeskAverageResponseTime(`${zendeskURL}ticket_metrics.json`);
zendeskOpenTickets(`${zendeskURL}${zendeskSearch}order_by:created sort:asc type:ticket status:open`);
get_short_shipped_report()

// Helpers

function convertTime(number) {
  let minutes = number;
  let hours = minutes / 60;
  let roundedHours = Math.floor(hours);
  let tempMinutes = (hours - roundedHours) * 60;
  let roundedMinutes = Math.round(tempMinutes);
  return `${roundedHours} hour(s) and ${roundedMinutes} minute(s)`;
}