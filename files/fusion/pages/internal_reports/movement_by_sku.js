var apiTotalRequests = 1;

const url = `${urlApi}warehouse_dashboard/movement_by_sku?${apiKey}=${key}`

let initialCall = `${url}&companyCode=${companyCode}`

get_stock_by_movement(true, initialCall);

const companyAPI = `${urlApi}companies?${apiKey}=${key}`;
let companiesArray = [];
$.ajax({
    url: companyAPI,
    success: function (response) {
        let companies = response.data;
        companies.forEach(company => {
            if (company.settings.active === true) {
                companiesArray.push(company)
            }
        });
        sortedArray = multiSort(companiesArray, {name: 'asc'});
        sortedArray.forEach(company => {            
            $('#companies3PL').append(`<option value="${company.name}" id="${company.id}">`)
        });
    }
})

$('#movementBySkuForm').submit(function (e) {
    e.preventDefault();
    var dataReturnsForm = $('#movementBySkuForm' + ' :input');   
    var formattedData = api_format_form_data(dataReturnsForm)
    let tempUrl = url;
    if (formattedData.companyList) {
        let companyCodeQuery = companiesArray.find(company => company.name === formattedData.companyList).code
        tempUrl += `&companyCode=${companyCodeQuery}`
    }
    if (formattedData.dateShippedFrom) {
        tempUrl += `&dateShippedFrom=${formattedData.dateShippedFrom}`
    }
    if (formattedData.dateShippedTo) {
        tempUrl += `&dateShippedTo=${formattedData.dateShippedTo}`
    }
    get_stock_by_movement(true, tempUrl);
    apiTotalRequests += 1;
})