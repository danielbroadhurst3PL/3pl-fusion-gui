const url = `${urlApi}reports/recently_packed?API-KEY=${key}&warehouse=WAREHOUSE1`;
let recentlyPackedTableSettings = null;

function populateTable(url) {
  recentlyPackedTableSettings = $('#recentlyPackedTable').DataTable({
      dom: 'Bfrtip',
      pageLength: 50,
      ajax: url,
      "order": [[ 0, "desc" ]],
      columns: [
          { data: "DateClosed" },
          { data: "Operator" },
          { data: "FromSlot" },
          { data: "FromAssignRef" },
          { data: "LNS" },
          { data: "PCS" },
          { data: "ASN" },
          { data: "CustomerRef" },
          { data: "CarrierId" }
      ],
      buttons: [
        'copy',
        'csv',
        'excelHtml5',
        'pdfHtml5',
        'print'
      ]
  });
}

populateTable(url);