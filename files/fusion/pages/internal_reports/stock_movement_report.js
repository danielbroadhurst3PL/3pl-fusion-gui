apiTotalRequests = 0;

function get_stock_movement_report(apiShowAlert, apiData) {
    tableStockMovementReportSettings.clear()
    var api3plStockMovement = urlApi + 'reports/stock_movement?';

    if (!apiData) {
        api3plStockMovement += `${apiKey}=${key}`
    } else {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#stockMovementReport").offset().top - 100
        }, 2000);
    }
    $.each(apiData, function (key, val) {
        if (val !== "") {
            if (key === 'dateShippedTo') {
                val = val;
                api3plStockMovement += key + '=' + val + '&';
            }
            if (key === 'dateShippedFrom') {
                val = val;
                api3plStockMovement += key + '=' + val + '&';
            }
            if (key === 'timeShippedTo') {
                val = val;
                api3plStockMovement += key + '=' + val + '&';
            }
            if (key === 'timeShippedFrom') {
                val = val;
                api3plStockMovement += key + '=' + val + '&';
            }
            if (key === 'API-KEY') {
                val = val;
                api3plStockMovement += key + '=' + val + '&';
            }
            if (key === 'sku') {
                val = val;
                api3plStockMovement += key + '=' + val + '&';
            }
            if (key === 'MoveType') {
                val = val;
                api3plStockMovement += key + '=' + val + '&';
            }
        }
    });
    var apiData = "";
    api(false, apiShowAlert, "", api3plStockMovement, methodGet, apiData, function (getStockMovement) {
        accountManagerArray = getStockMovement.data;        
        if (getStockMovement.results.total > 0) {
            $.each(getStockMovement.data, function (key, val) {
                if (val.DateClosed) {
                    dateClosedShortened = val.DateClosed.substring(0, val.DateClosed.length - 4);
                } else {
                    dateClosedShortened = "";
                }
                tableStockMovementReportSettings.row.add([
                    val.MoveType,
                    val.MoveId,
                    val.MoveLineId,
                    val.Status,
                    val.Class,
                    val.SKUId,
                    val.QtyTasked,
                    val.QtyActioned,
                    val.UnitOfMeasure,
                    val.BoxQty,
                    val.FromFacility,
                    val.FromZone,
                    val.FromBay,
                    val.FromSection,
                    val.FromSlot,
                    val.FromStU,
                    val.FromStT,
                    val.FromOwnerId,
                    val.FromStatus,
                    val.FromAssignType,
                    val.FromAssignRef,
                    val.FromLoad,
                    val.ToFacility,
                    val.ToZone,
                    val.ToBay,
                    val.ToSection,
                    val.ToSlot,
                    val.ToStU,
                    val.ToStT,
                    val.ToOwnerId,
                    val.ToStatus,
                    val.ToAssignType,
                    val.ToAssignRef,
                    val.ToLoad,
                    val.Sequence,
                    val.JobId,
                    val.JobSequence,
                    val.Operator,
                    val.Supervisor,
                    val.ReasonId,
                    dateCreated = val.DateCreated.substring(0, val.DateCreated.length - 4),
                    val.DateSuspended,
                    dateCreated = val.DateCreated.substring(0, val.DateCreated.length - 4),
                    dateClosedShortened,
                    val.Stage
                ]);     
            })           
        } else {
            tableStockMovementReportSettings.row.add([
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
                "",
            ]);
        }
        tableStockMovementReportSettings.draw()
        apiTotalRequests += 1;
    })
}

get_stock_movement_report(true)

$('#stockMovementForm').submit(function (e) {
    e.preventDefault();
    var dataCreateCreateForm = $('#stockMovementForm' + ' :input');
    var apiFormatFormData = api_format_form_data(dataCreateCreateForm);
    if (apiFormatFormData['dateShippedFromTime']) {
        apiFormatFormData['timeShippedFrom'] = `${apiFormatFormData['dateShippedFromTime'].split("-")[1].trim()}:00`;
        apiFormatFormData['timeShippedTo'] = `${apiFormatFormData['dateShippedToTime'].split("-")[1].trim()}:59`;
    
        apiFormatFormData['dateShippedFrom'] = api_date(apiFormatFormData['dateShippedFromTime'].split("-")[0].trim());
        apiFormatFormData['dateShippedTo'] = api_date(apiFormatFormData['dateShippedToTime'].split("-")[0].trim());
    }    
        
    if (apiFormatFormData.dateShippedFrom != null && apiFormatFormData.dateShippedTo != null) {
        checkDateFrom = api_date(apiFormatFormData.dateShippedFrom);
        checkDateTo = api_date(apiFormatFormData.dateShippedTo);
        if (new Date(checkDateFrom) > new Date(checkDateTo)) {
            $("#divFilterShippedToValidateFuture").html('Date Shipped From cannot be in future... Please reselect dates.')
            return false;
        } else {
            $("#divFilterShippedToValidateFuture").html('')

        }
    }    
    get_stock_movement_report(true, apiFormatFormData)
})