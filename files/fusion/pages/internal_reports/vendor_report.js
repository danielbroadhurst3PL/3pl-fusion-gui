const url = `${urlApi}reports/vendor?API-KEY=${key}`;
let apiTotalRequests = 0;

let tableSettings = null;
function populateTable(url) {
  if (tableSettings !== null) {
    tableSettings.destroy();
  }
  tableSettings = $('#vendorReportTable').DataTable({
    dom: 'Bfrtip',
    pageLength: 10,
    ajax: url,
    "order": [[3, "desc"]],
    columns: [
      {
        data: "ShipmentId", render: function (data) {
          return `<a href="Javascript:void(0)" onClick="showPackingList('${data}')">${data}</a>`;
        }
      },
      { data: "Stage" },
      { data: "CustomerId" },
      { data: "DelNote" },
      { data: "CustomerRef" },
      { data: "ASN" },
      { data: "Service" },
      { data: "Carrier" },
      { data: "TrackingNo" },
      { data: "LNS" },
      { data: "PCS" },
      { data: "TransitItems" },
      { data: "DateCreated" },
      { data: "DueBy" },
      { data: "PriorityAllocation" },
      { data: "PriorityDespatch" },
    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ]
  });
}

$('#vendorPackingList').hide()
populateTable(url);

let packingListTableSettings = null;
function showPackingList(orderNumber) {
  $('#vendorPackingList').show()
  $([document.documentElement, document.body]).animate({
      scrollTop: $("#vendorPackingListTable").offset().top - 200
  }, 1500);
  let packingListUrl = `${urlApi}reports/vendor_packing_list?API-KEY=${key}&orderNumber=${orderNumber}`;
  if (packingListTableSettings !== null) {
    packingListTableSettings.destroy();
  }
  packingListTableSettings = $('#vendorPackingListTable').DataTable({
    dom: 'Bfrtip',
    pageLength: 10,
    ajax: packingListUrl,
    "order": [[1, "asc"]],
    columns: [
      { data: "AssignRef"},
      { data: "BoxNo" },
      { data: "SKUId" },
      { data: "StU" },
      { data: "PCS" },
    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ]
  });
}