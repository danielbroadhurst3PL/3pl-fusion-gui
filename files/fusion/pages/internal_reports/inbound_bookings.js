var apiTotalRequests = 1;

const url = `${urlApi}inbounds/internal?${apiKey}=${key}`

let initialCall = `${url}&companyCode=${companyCode}`

get_inbound_bookings_internal(true, initialCall);

/* const companyAPI = `${urlApi}companies?${apiKey}=${key}`;
let companiesArray = [];
$.ajax({
    url: companyAPI,
    success: function (response) {
        let companies = response.data;
        companies.forEach(company => {
            if (company.settings.active === true) {
                companiesArray.push(company)
            }
        });
        sortedArray = multiSort(companiesArray, {name: 'asc'});
        sortedArray.forEach(company => {            
            $('#companies3PL').append(`<option value="${company.name}" id="${company.id}">`)
        });
    }
}) */

$('#internalInboundForm').submit(function (e) {
    e.preventDefault();
    var dataReturnsForm = $('#internalInboundForm' + ' :input');   
    var formattedData = api_format_form_data(dataReturnsForm)
    let tempUrl = url;
    /* if (formattedData.companyList) {
        let companyCodeQuery = companiesArray.find(company => company.name === formattedData.companyList).code
        tempUrl += `&companyCode=${companyCodeQuery}`
    } */
    if (formattedData.dateShippedFrom) {
        tempUrl += `&dateSelectedFrom=${formattedData.dateShippedFrom}`
    }
    if (formattedData.dateShippedTo) {
        tempUrl += `&dateSelectedTo=${formattedData.dateShippedTo}`
    }
    get_inbound_bookings_internal(true, tempUrl);
    apiTotalRequests += 1;
})

function cancelInboundBooking(poNumber) {
    
}

/**
 * Displays alert modal to ask user to confirm they wish to delete the order.
 * 
 * @param {String} order - Order Number which will be deleted
 */
function cancelInboundBooking(poNumber) {
    swal({
        title: "Are you sure?",
        text: "This will cancel the Inbound!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Yes, cancel inbound!",
        cancelButtonText: "No, I made a mistake!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            alertCustomLoading()
            deleteInbound(poNumber)
        } else {
            swal("Cancelled", "The Inbound will continue being processed :)", "error");
        }
    });
}

/**
 * Calls the delete order endpoint with an order number and deletes from system.
 * 
 * @param {String} order - Order number which will be deleted
 */
function deleteInbound(poNumber) {
    apiOrdersDelete = `${urlApi}inbounds`;
    apiData = {'poNumber': poNumber, ["API-KEY"]:key}
    api(false, true, "", apiOrdersDelete, methodDelete, apiData, function (cancelInbound) {
        if (cancelInbound.status === 200) {
            swal("Deleted!", "Inbound has been cancelled.", "success");
        }
    })
}