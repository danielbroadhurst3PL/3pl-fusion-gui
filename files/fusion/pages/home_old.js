var apiTotalRequests = 7;

// Creates an array of Services which are offered.
var servicesPromise = new Promise(function (resolve, reject) {
    url = `${urlApi}services?API-KEY=${key}`;
    api(true, false, "", url, methodGet, "", function (getServices){
        serviceShippingMethods = getServices.data;
        resolve(serviceShippingMethods);        
    })
})

$(document).ready(() => {
    $('#selectpicker').on('change', (e) => {        
        if (e.target.id === 'ordersShipped') {
        shipLastWeek()
        } else if (e.target.id === 'currentOrders') {
        currentOrders()
        } else {
        currentStock()
        }
    });
});

getServices()
var services = [];
function getServices() {
    url =`${urlApi}services?API-KEY=${key}`;
    var apiData = "";
    api(true, false, "", url, methodGet, apiData, function (getServices) {
        services = getServices.data
    })
}

function currentOrders() {
    $("#shippingSection").animate({opacity:1},300)
    $("#shippingSection").show()
    $("#shippedLastWeek").animate({opacity:0},300)
    $("#shippedLastWeek").hide()
    $("#currentStockLevelsTable").animate({opacity:0},300)
    $("#currentStockLevelsTable").hide()
    $('#orderTableHome').hide()
    $('#shippedOrderHome').hide()
}

function shipLastWeek() {
    $("#shippingSection").animate({opacity:0},300)
    $("#shippingSection").hide()
    $("#currentStockLevelsTable").animate({opacity:0},300)
    $("#currentStockLevelsTable").hide()
    $("#shippedLastWeek").animate({opacity:1},300)
    $("#shippedLastWeek").show()
    $('#orderTableHome').hide()
    $('#shippedOrderHome').hide()
}

function currentStock() {
    $("#shippingSection").animate({opacity:0},300)
    $("#shippingSection").hide()
    $("#shippedLastWeek").animate({opacity:0},300)
    $("#shippedLastWeek").hide()
    $("#currentStockLevelsTable").animate({opacity:1},300)
    $("#currentStockLevelsTable").show()
    $('#orderTableHome').hide()
    $('#shippedOrderHome').hide()
}


function closeTable(tableId) {
    $( "#" + tableId ).animate({
        width: "100%",
        opacity: 0,
        left: "-1000px",
      }, 500 );
    $("#" + tableId).toggle()
}

startPage()
function startPage() {
    backOrder()
    shippedLastWeek()
}

/**
 * Today's Current Orders API Call and Chart.js functions.
 * API Call = https://${urlApi}orders/?status={STATUS}&API-KEY=
 */
backOrders = nextFlush = awaitingPick = awaitingPick = awaitingShip = fullData = []

function backOrder() {
    counter = 0
    dataArray = keys = values = [];
    array = ['BACK', 'NEXTFLUSH', 'AWAITINGPICK', 'AWAITINGPACK', 'AWAITINGSHIP', 'SHIPPED'];
    array.forEach(element => {
        var api3plOrdersBO = urlApi + 'orders/fusion_orders/' + '?status=' + element + '&' + apiKey + '=' + key;
        if (element == 'SHIPPED') {
            var fullDate = new Date()
            var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
            var currentDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + fullDate.getDate();
            api3plOrdersBO += `&dateShippedFrom=${currentDate}&dateShippedTo=${currentDate}`;
        }
        var apiData = "";
        api(false, false, "", api3plOrdersBO, methodGet, apiData, function (getOrders) {
            data = getOrders                        
            counter = counter + 1;
            switch (element) {
            case 'BACK':
                dataArray[0] = {'name':'Back Orders', 'count': data.results.total, 'selector': 'backOrders'};
                backOrders = data.data;
                fullData.push(backOrders)
                break;
            case 'NEXTFLUSH':
                dataArray[1] = {'name':'Next Flush', 'count': data.results.total, 'selector': 'nextFlush'};
                nextFlush = data.data;
                fullData.push(nextFlush)
                break;
            case 'AWAITINGPICK':
                dataArray[2] = {'name':'Awaiting Pick', 'count' : data.results.total, 'selector': 'awaitingPick'};
                awaitingPick = data.data;
                fullData.push(awaitingPick)
                break;
            case 'AWAITINGPACK':
                dataArray[3] = {'name':'Awaiting Pack', 'count' : data.results.total, 'selector': 'awaitingPack'};
                awaitingPack = data.data;
                fullData.push(awaitingPack)
                break;
            case 'AWAITINGSHIP':
                dataArray[4] = {'name':'Awaiting Shipping', 'count' : data.results.total, 'selector': 'awaitingShip'};
                awaitingShip = data.data;
                fullData.push(awaitingShip)
                break;
            case 'SHIPPED':
                dataArray[5] = {'name':'Shipped', 'count': data.results.total, 'selector': 'shipped'};
                shipped = data.data;
                fullData.push(shipped)
                break;
            }
            if (counter == 6) {
                graph(dataArray)
                cardDetails(dataArray)
                shippingData(fullData)
                if($('#shippingBreakdown').is(':hidden')) {
                    $("#shippingBreakdown").toggle();
                }
            }            
        })
    });
}

shipMethod = []
function shippingData(fullData) {
    fullData.forEach(fullDataElement => {
        fullDataElement.forEach(element => {
            count = 1
            tempShipMethod = element.ShippingMethod;            
            if (tempShipMethod == '685' || tempShipMethod == '684') {
                switch (tempShipMethod) {
                    case '685':
                        tempShipMethod = 'DROPSHIP';
                        break;
                    case '684':
                        tempShipMethod = 'EMBROID';
                        break;
                }
            }
            if (shipMethod.hasOwnProperty(tempShipMethod)) {
                count = shipMethod[tempShipMethod] + 1
                shipMethod[tempShipMethod] = count
            } else {
                shipMethod[tempShipMethod] = count
            }
        });
        
    });
    pieChartShipping(shipMethod)
}

function pieChartShipping(shippingDataChart) {
    keys = [];
    values = [];
    keys = Object.keys(shippingDataChart)
    values = Object.values(shippingDataChart)

    var config = {
        type: 'pie',
        data: {
            datasets: [{
                data: values,
                backgroundColor: ['rgb(213, 0, 87)','rgb(177, 177, 177)','rgb(0, 147, 201)','rgb(255, 198, 41)','rgb(84, 86, 90)','rgb(213, 0, 87)','rgb(0, 147, 201)','rgb(213,0,87)'],
                label: 'Shipping Methods for Current Orders'
            }],
            labels: keys
        },
        options: {
            responsive: true
        }
    };

    var ctx = document.getElementById('chart-area').getContext('2d');
    window.myPie = new Chart(ctx, config);
}

function cardDetails(dataArray) {    
    dataArray.forEach(element => {
        document.querySelector("#" + element.selector).innerHTML = element.count.toLocaleString();
    });
}

function ordersClicked(value) {    
    selectedTable = [];
    var tableSelector = 'viewOrder';
    var replacedStatus = '';
    switch (value) {
        case "backOrders":
            selectedTable = backOrders;
            value = 'BACK';
            replacedStatus = 'Back Orders';
            status = statusBack;
            break;
        case "nextFlush":
            selectedTable = nextFlush;
            value = 'NEXTFLUSH';
            replacedStatus = 'Next Flush';
            status = statusNextFlush;
            break;
        case "awaitingPick":
            selectedTable = awaitingPick;
            value = 'AWAITINGPICK';
            replacedStatus = 'Awaiting Pick';
            status = statusAwaitingPick;
            break;
        case "awaitingPack":
            selectedTable = awaitingPack;
            value = 'AWAITINGPACK';
            replacedStatus = 'Awaiting Pack';
            status = statusAwaitingPack;
            break;
        case "awaitingShip":
            tableSelector = 'shippedOrder';
            selectedTable = awaitingShip;
            value = 'AWAITINGSHIP';
            replacedStatus = 'Awaiting Ship';
            status = statusAwaitingShip;
            break;
        case "shipped":
            tableSelector = 'shippedOrder';
            selectedTable = shipped;
            value = 'SHIPPED';
            replacedStatus = 'Shipped';
            status = statusShipped;
            break;
    }

    if (tableSelector === 'viewOrder') {
        if (tableOrdersViewSettings !== null) {
            tableOrdersViewSettings.destroy();
        }        
        console.log(selectedTable);
        
        tableOrdersViewSettings = $('#' + tableOrdersView).DataTable({
            dom: 'Bfrtip',
            pageLength: 10,
            data: selectedTable,
            responsive: true,
            columns: [
                {data: "ConsignmentId", render: function (data) {
                    return valueModalLink(true, modalOrder, data);
                }},
                {data: "Name"},
                {data: "Line1"},
                {data: "CustomerRef"},
                {data: "ASN"},
                {data: "ShippingMethod", render: function (data) {
                    let shippingMethod = replace_shipping_method(data);
                    if (shippingMethod != undefined) {
                        return shippingMethod;
                    }
                    return data;
                }},
                {data: "DateShipment", render: function (data) {
                    return data.split(' ')[0];
                }},
                {data: "Stage", render: function (data) {
                    return replacedStatus;
                }},
                {data: "ConsignmentId", render: function (data) {                       
                    let orderButtons = check_order_edit(status, "button", data);   
                    return orderButtons;                         
                }}
            ],
            buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
            ],
        });
        if($('#shippedOrderHome').is(':visible')) {
            $("#shippedOrderHome").toggle();
        }
        title = document.querySelector("#orderTableHome > div > div > div.header > h2")
        title.innerHTML = replacedStatus
        if($('#orderTableHome').is(':hidden')) {
            $("#orderTableHome").toggle();
            $( "#orderTableHome" ).animate({
                width: "100%",
                opacity: 1,
                left: "0",
              }, 750 );
        }
    }
    if (tableSelector === 'shippedOrder') {        
        if (tableOrdersShippedViewSettings !== null) {
            tableOrdersShippedViewSettings.destroy();
        }
        tableOrdersShippedViewSettings = $('#' + tableOrdersShippedView).DataTable({
            dom: 'Bfrtip',
            pageLength: 10,
            data: selectedTable,
            responsive: true,
            columns: [
                {data: "ConsignmentId", render: function (data) {
                    return valueModalLink(true, modalOrder, data);
                }},
                {data: "Name"},
                {data: "Line1"},
                {data: "CustomerRef"},
                {data: "ASN"},
                {data: "CarrierId"},
                {data: "Stage", render: function (data) {
                    return replacedStatus;
                }},
                {data: "DateShipment", render: function (data) {
                    return data.split(' ')[0];
                }},
                {data: "DateClosed"},
                {data: "CarrierTrackingNumber"},
                
            ],
            buttons: [
            'copy',
            'csv',
            'excelHtml5',
            'pdfHtml5',
            'print'
            ],
            "createdRow": function( row, data, dataIndex ) {
                trackingLink = carrierId = trackingNumber = "";
                trackingNumber = data['CarrierTrackingNumber'];
                if (trackingNumber !== null) {   
                    trackingNumber = trackingNumber.trim();
                    if (trackingNumber !== "") {
                        carrierId = data['CarrierId'];
                        trackingLink = tackingNumberLink(trackingNumber, carrierId);
                        cell = $(row)[0].cells[7]
                        cell.innerHTML = trackingLink;                            
                    }                     
                }
            }
        });
        if($('#orderTableHome').is(':visible')) {
            $("#orderTableHome").toggle();
        }
        shippedTitle = document.querySelector("#shippedOrderHome > div > div > div.header > h2")
        shippedTitle.innerHTML =replacedStatus
        if($('#shippedOrderHome').is(':hidden')) {
            $("#shippedOrderHome").toggle();
            $( "#shippedOrderHome" ).animate({
                width: "100%",
                opacity: 1,
                left: "0",
            }, 500 );
        }
    }
    return
}

// Today's Current Order Graph
function graph(data) {
    document.querySelector("#shipping-data > div.demo-preloader").innerHTML = "";
    keys = [];
    values = [];
    data.forEach(element => {
        keys.push(element.name)
        values.push(element.count)
    });
    var ctx = document.getElementById('myChart').getContext('2d');
    window.myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: keys,
            datasets: [
                {
                    data: values,
                    backgroundColor: ['rgb(177, 177, 177)','rgb(213, 0, 87)','rgb(0, 147, 201)','rgb(255, 198, 41)','rgb(84, 86, 90)','rgb(0, 141, 206)'],
                }
            ]
          },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            tooltips:{
                callbacks:{
                    title: ()=>{}
                }
            },
            legend: {
                display: false
            },
        }
    });
}

/**
 * Orders Shipped in Past 7 Days Graph API Call and Chart.js Functions
 * API Call = https://${urlApi}orders/?type=status&status=BACK&API-KEY=
 */
let shippedDataArray = [];
dateShippedArray = testData = [];
success = '';
complete = 0;
function shippedLastWeek() {
    let url = `${urlApi}${urlApiOrders}fusion_shipped_orders_data?${apiKey}=${key}`
    apiData = "";
    api(true, false, "", url, methodGet, apiData, function (getShippedData) {        
        let shippedData = getShippedData.data;        
        let j = 0;
        let h = 0;
        for (let i = 0; i < shippedData.length; i++) {
            const element = shippedData[i];                        
            if (element.day === 'day' && element.day != null) {                
                let key = Object.keys(element)[0];
                let date = Object.keys(element)[1];
                dateShippedArray[j] = {'date': date, 'count': parseInt(element.Total), 'selector': key}
                j += 1;
            } else {
                let keySelector = Object.keys(element)[0];
                shippedDataArray[h] = {'name':keySelector, 'count': parseInt(element.Total), 'selector': keySelector};
                h += 1;
            }
        }
        graphPastWeek(dateShippedArray);  
        cardDetails(shippedDataArray)      
    })
    return
}

// Orders Shipped in Past 7 Days
function graphPastWeek(data) {    
    document.querySelector("#shipped-data > div.demo-preloader").innerHTML = "";
    keys = [];
    values = [];
    reversedArray = data.reverse()
    reversedArray.forEach(element => {
        keys.push(element.date)
        values.push(element.count)
    });
    keys.reverse()
    values.reverse();
    
    var ctx = document.getElementById('myChartShipped').getContext('2d');
    window.myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: keys,
            datasets: [
                {
                    label: 'Shipped Orders',
                    data: values,
                    backgroundColor: ['rgb(177, 177, 177)','rgb(0, 147, 201)','rgb(255, 198, 41)','rgb(84, 86, 90)','rgb(255, 198, 41)','rgb(213, 0, 87)','rgb(0, 147, 201)'],
                }
            ]
          },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            tooltips:{
                callbacks:{
                    title: ()=>{}
                }
            },
            legend: {
                display: false
            },
        }
    });
}

get_stock(false, true);

$('#darkMode').change(function() {
    if($(this).prop("checked")){
        $('body').addClass("darkMode");
      }else{
        $('body').removeClass("darkMode");
      }
})