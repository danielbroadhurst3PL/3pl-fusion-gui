let apiTotalRequests = 0;
let dateToday = new Date().toISOString().split('T')[0]
const url = `${urlApi}reports/historic_orders?API-KEY=${key}`;

let historicOrdersTable = null;
function populateTable(url) {
  alertCustomLoading();
  if (historicOrdersTable !== null) {
    historicOrdersTable.destroy();
  }
  historicOrdersTable = $('#historicOrdersTable').DataTable({
    dom: 'Bfrtip',
    pageLength: 10,
    ajax: url,
    "order": [[0, "desc"]],
    columns: [
      {
        data: "ShipmentId", render: function (data) {
          return valueModalLink(true, modalOrder, data);
        }
      },
      { data: "BizID" },
      { data: "CustomerName" },
      { data: "Warehouse" },
      { data: "CustomerRef" },
      { data: "ASN" },
      { data: "ShippingMethod" },
      { data: "CarrierTrackingNumber" },
      { data: "OrderLines" },
      { data: "OrderWeight" },
      { data: "OrderBoxes" },
      { data: "LineQty" },
      { data: "DateCreated" },
      { data: "DateClosed" },
      { data: "OrderStage" },
      { data: "SKUId" },
      { data: "StockStatus" },
      { data: "UnitOfMeasure" },
      { data: "QtyOrdered" },
      { data: "QtyRequired" },
      { data: "QtyShipped" },
      { data: "SOLineId" },
      { data: "Line" },
      { data: "LineStage" },
      { data: "Name" },
      { data: "Line1" },
      { data: "Line2" },
      { data: "Line3" },
      { data: "City" },
      { data: "State" },
      { data: "PostCode" },
      { data: "Country" }
    ],
    buttons: [
      'copy',
      'csv',
      'excelHtml5',
      'pdfHtml5',
      'print'
    ],
    "initComplete": function () {
      closeAllSwalWindows()
    },
  });
}

populateTable(`${url}&dateFrom=${dateToday}&dateTo=${dateToday}`);

$('#historicOrdersForm').submit(function (e) {
  e.preventDefault()
  var dataForm = $('#historicOrdersForm' + ' :input');
  var apiFormatFormData = api_format_form_data(dataForm);
  populateTable(`${url}&dateFrom=${apiFormatFormData.dateShippedFrom}&dateTo=${apiFormatFormData.dateShippedTo}`)
})