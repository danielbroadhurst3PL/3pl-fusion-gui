var apiTotalRequests = 0;

get_companies_form(false);

$("#" + formUserCreate).submit(function(e) {
    e.preventDefault();

	var dataCreateCreateForm = $('#' + formUserCreate + ' :input');

	var apiFormatFormData = api_format_form_data(dataCreateCreateForm);
	
	apiFormatFormData['active'] = 1;
    
	submit_company(apiFormatFormData);
});

function submit_company(apiData)
{
	alertCustomCreating(`Creating User ${apiData[nameFirst]} ${apiData[nameLast]}`);
	var api3plUser = urlApi + urlApiUsers;
	api(false, true, apiData, api3plUser, methodPost, apiData, function (createUser){  
    $('#' + formUserCreate).trigger("reset");
	});
}

function formInformation(formID) {
	$('#infoText').html("")
	infoTitle = formID.replace(/([A-Z])/g, ' $1').trim();
		
	$('#infoHeader').html(`${infoTitle}`)
	$('#infoText').append(`<p><strong>First Name:</strong><br />Clients First Name.</p>`)
	$('#infoText').append(`<p><strong>Last Name:</strong><br />Clients Last Name.</p>`)
	$('#infoText').append(`<p><strong>Email:</strong><br />Required, must be valid email address.</p>`)
	$('#infoText').append(`<p><strong>Password:</strong><br />Must be over 6 digits and contain a number.</p>`)
	$('#infoText').append(`<p><strong>Company:</strong><br />Select which company the client will be assigned to.</p>`)
	$('#infoText').append(`<p><strong>Account Type:</strong><br />Select the account type of the client.<br/><b><ul><li>Staff:</b> Staff Account</li><li><b>Customer:</b> Customer Account</li><li><b>Customer Admin:</b> Customer Admin Details</li><li><b>Staff Admin:</b> Staff Admin Account</li></ul>`)
}
