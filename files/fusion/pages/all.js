function triggerResize() {
    setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
    }, 600)
}
// Opens and Closes Left Side Menu Bar to reveal full screen fusion
$('#menu-close').click(function () {
    $('#leftsidebar').toggleClass('closed')
    $('#menu-close').toggleClass('closed')
    $('section.content').toggleClass('closed')
    triggerResize()
})

/**
 * Changes the information which is displayed in the sidebar.
 * formInformation() is required on individual pages.
 */
var sideInformationBar = false;
function toggleInfoBar() {
    switchCloseIcon()
    id = event.path[1].id;
    link = event.target.offsetParent.lastElementChild;
    if ($('body').hasClass("sidebarOpen")) {
        formInformation(id)
        iconChange(id, link, "closeInfoBar()", "close");
        sideInformationBar = false;
        return
    }
    $('body').toggleClass("sidebarOpen");
    $('.sideInformationBar').toggleClass("open");
    iconChange(id, link, "closeInfoBar()", "close");
    formInformation(id)
    sideInformationBar = false;
    triggerResize()
}

/**
 * Closes the right side information bar if the close icon is pressed.
 * switchCloseIcon changes any open information pages back to unopen state.
 */
function closeInfoBar() {
    //id = event.path[1].id;
    //link = event.target.offsetParent.firstElementChild;
    switchCloseIcon()
    $('body').toggleClass("sidebarOpen");
    $('.sideInformationBar').toggleClass("open");
    iconChange(id, link, "toggleInfoBar(" + id + ")", "info")
    sideInformationBar = false;
}

/**
 * Any icons which are open will be switched back to original state
 */
function switchCloseIcon() {
    multipleClose = $('.close')
    if (multipleClose.length >= 1) {
        for (let i = 0; i < multipleClose.length; i++) {
            const element = multipleClose[i];
            link = element.offsetParent.firstElementChild
            id = element.offsetParent.firstElementChild.children[0].id
            iconChange(id, link, "toggleInfoBar(" + id + ")", "info")
        }
    }
}

/**
 * Sets which icon will be displayed on change and sets function calls
 * @param {specifies the html element} id 
 * @param {sets the <li> item} link 
 * @param {sets the onclick function} onclickSet 
 * @param {sets material design icon} iconSelect 
 */
function iconChange(id, link, onclickSet, iconSelect) {
    link.innerHTML = ""
    ahref = document.createElement("a");
    ahref.href = "javascript:void(0);";
    ahref.setAttribute("onclick", onclickSet);
    ahref.setAttribute("id", id);
    icon = document.createElement("i");
    icon.setAttribute("class", "material-icons " + iconSelect);
    icon.innerHTML = iconSelect;
    ahref.appendChild(icon);
    link.appendChild(ahref);
}

// Slimscroll settings for right hand information bar
$('#infoTextDiv').slimScroll({
    height: '100vh',
    position: 'left',
    start: $('#infoTextDiv'),
    size: '4px',
});

// If Enter is pressed on the search bar then the search is carried out
$('.search-bar input[type="text"]').keypress(function (event) {
    var keycode = (event.keycode ? event.keycode : event.which);
    if (keycode == '13') {
        var value = $('.search-bar input[type="text"]').val()
        window.location.href = `${baseUrl}/orders/view?orderNumber=${value}`
    }
})

/**
 * On keyup, if query is above 5 then API endpoint is called
 * and autocomplete dropdown will be populated with order numbers
 */
orderNumbersArray = []; // Auto Complete Search Form.
$('#autocomplete').keyup(function (event) {
    let query = $('#autocomplete').val();
    query = query.toUpperCase();
    if (query.length > 5) {
        orderNumbersArray = [];
        urlCall = `${urlApi}orders/fusion_homepage_search/?API-KEY=${key}&searchQuery=${query}`;
        $.ajax({
            url: urlCall
        }).done(function (getOrderNumbers) {
            orderNumbersLike = getOrderNumbers.data;
            orderNumbersLike.forEach(element => {
                orderNumbersArray.push(element.ConsignmentId)
            });
            $("#autocomplete").autocomplete({
                source: orderNumbersArray
            });
        })
    }
})

let notificationExceptionInfoURL = `${urlApi}exceptions?${apiKey}=${key}`;
let promiseNotification = new Promise((resolve) => {
    $.ajax({
        url: notificationExceptionInfoURL,
        success: function (data) {
            resolve(data)
        }
    })
});

promiseNotification.then((data) => {
    let notifications = data.data;
    if (notifications.length > 0) {
        $('#notificationsLabel').html(`${notifications.length}`)
        notifications.forEach(exception => {
            $('#exceptionsList').append(`
            <li>
                <a href="${baseUrl}orders/edit/${exception.ShipmentId}">
                    <div class="icon-circle bg-red">
                        <i class="material-icons">error</i>
                    </div>
                    <div class="menu-info">
                        <h4>Error Type: ${exception.ErrorType}</h4>
                        <p>Reference: ${exception.ShipmentId}</p>
                    </div>
                </a>
            </li>`)
        });
    }
})


const reworkNotificationURL = `${urlApi}reworks/`;
let currentReworks = []
$.ajax({
    url: `${reworkNotificationURL}?${apiKey}=${key}`
}).done(function (response) {
    let openReworks = 0;
    currentReworks = response.data;
    currentReworks.forEach(rework => {
        if (rework.progress < 100) {
            openReworks++;
            $('#reworkMessagesList').append(`
            <li>
                <a href="javascript:void(0);" class=" waves-effect waves-block">
                <h4>
                ${rework.rework_id}
                <small>${rework.progress}%</small>
                </h4>
                <div class="progress">
                    <div class="progress-bar bg-black" role="progressbar" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100" style="width: ${rework.progress}%">
                    </div>
                </div>
                </a>
            </li>`)
        }
    })
    addToMessageCount(openReworks, 'reworkMessagesCount')
})
messageUrl = `${reworkNotificationURL}messages?${apiKey}=${key}&unread=true`;
$.ajax({
    url: messageUrl
}).done(function (response) {
    numberMessages = response.data;
    numberMessages.forEach(message => {
        $('#reworkMessagesList').append(`
      <li>
          <a href="${baseUrl}reworks/view/${message.rework_id}">
              <div class="icon-circle bg-red">
                  <i class="material-icons">mail</i>
              </div>
              <div class="menu-info">
                  <h4>${message.rework_id}</h4>
                  <h5>${message.subject}</h5>
              </div>
          </a>
      </li>`)
    });
    addToMessageCount(numberMessages.length, 'reworkMessagesCount')
})

let timeStudies = [];
timeStudyUrl = `${reworkNotificationURL}time_study?${apiKey}=${key}&unconfirmed=true`;
$.ajax({
    url: timeStudyUrl
}).done(function (response) {
    timeStudies = response.data;
    timeStudies.forEach(study => {
        $('#reworkMessagesList').append(`
      <li>
          <a href="${baseUrl}reworks/view/${study.rework_id}">
              <div class="icon-circle bg-red">
                  <i class="material-icons">alarm</i>
              </div>
              <div class="menu-info">
                  <h4>Time Study Ready - Please Confirm</h4>
                  <h5>${study.rework_id}</h5>
              </div>
          </a>
      </li>`)
    });
    addToMessageCount(timeStudies.length, 'reworkMessagesCount')
})


function addToMessageCount(amount, target) {
    console.log(amount, target);
    let value = 0;
    value = $('#' + target).html()
    if (value == null || value == "") {
        value = 0;
    } else {
        value = parseInt(value)
    }
    value += amount;
    if (value > 0) {
        $('#' + target).html(parseInt(value))
    }
}

$('#showApi').click(function () {
    $('#modalApiKey').modal().show()
    $('#usersApiKeyLive').html(liveKey)
    $('#usersApiKeyTest').html(testKey)
})

function displayNewsArticle(id) {
    let article = newsArticles.find(article => article.id === id);
    $('#modalNews').modal().show();
    $('#newsSubject').html(article.subject);
    $('#createdAt').html(article.created_at);
    $('#newsBody').html(article.body);
    markArticleRead(id);
}

function markArticleRead(id) {
    let readUrl = `${urlApi}news/read`;
    let apiData = {
        'API-KEY': key,
        'news_id': id
    }
    api(false, false, "", readUrl, methodPost, apiData, function (data) {
        let articles = data.data;
        populateNewsList(articles);
    })
}

let newsArticles = [];

function getLatestNews() {
    newsUrl = `${urlApi}news?${apiKey}=${key}&unread=true`;
    $.ajax({
        url: newsUrl
    }).done(function (response) {
        populateNewsList(response.data)
    });
}

function populateNewsList(articles) {
    $('#newsList').html("")
    $('#newsLabel').html("")
    newsArticles = articles;
    articles.forEach(article => {
        $('#newsList').append(`
            <li>
                <a href="Javascript:void(0)" onclick="displayNewsArticle('${article.id}')">
                    <div class="icon-circle bg-black">
                        <i class="material-icons">mail_outline</i>
                    </div>
                    <div class="menu-info">
                        <h4>${article.subject}</h4>
                        <p>${article.created_at}</p>
                    </div>
                </a>
            </li>`);
    });
    addToMessageCount(articles.length, 'newsLabel');
}
getLatestNews();
